
import { FormEnvoyer } from '../../../../SiteBundle/Resources/build/js/modules/form/envoyer'
import { getXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/getXHRJson'
import { postXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
import { remplitForm } from '../../../../SiteBundle/Resources/build/js/modules/remplitForm'
class Metariane {
    mkkmetarianeInitRelSaveOnClick() {
        this.Save()
        return false
    }
    async sendmkkmetarianeInitRelModifierOnClick(url) {
        let response = await getXHRJson(url)
        remplitForm($("#AddMetariane"), response)
        $('#AddMetariane').find('form').attr('action', url)
        $('#AddMetariane').modal('toggle')
    }
    mkkmetarianeInitRelModifierOnClick(e) {
        let url = $(e.currentTarget).data('show')
        this.sendmkkmetarianeInitRelModifierOnClick(url)
        e.preventDefault()
    }
    async sendEnregistrer() {
        let datapost = $('#AddMetariane').find('form').serializeForm()
        let response = await postXHRJson(
            $('#AddMetariane').find('form').attr('action'),
            datapost
        )
    }

    constructor() {
        $('[data-rel=\'save\']').on("click", this.mkkmetarianeInitRelSaveOnClick.bind(this))
        $(".ModifierMetariane").on('click', this.mkkmetarianeInitRelModifierOnClick.bind(this))
    }
    Enregistrer() {
        if (Scripts.Form.Verifier($('#AddMetariane').find('form'))) {
            this.sendEnregistrer()
        }
        return false
    }
    Save() {
        FormEnvoyer('#AddMetariane')
        $('#AddMetariane').find('form').attr('action', '#')
        $('#AddMetariane').modal('hide')
    }
}


export default () => {
    return new Metariane()
}
