
import { FormVerifier } from '../../../../SiteBundle/Resources/build/js/modules/form/verifier'
import { FormEnvoyer } from '../../../../SiteBundle/Resources/build/js/modules/form/envoyer'
import { postXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Templates {
  mkktemplateInitBoutonSaveOnClick(e) {
    FormEnvoyer('#FormTemplates')
    e.preventDefault()
  }

  async sendEnregistrer() {
    let datapost = $('#FormTemplates').serializeForm()
    let response = await postXHRJson(
      $('#FormTemplates').attr('action'),
      datapost
    )
  }
  constructor() {}
  Enregistrer() {
    if (FormVerifier($('#FormTemplates'))) {
      this.sendEnregistrer()
    }
    return false
  }
}


export default () => {
  return new Templates()
}
