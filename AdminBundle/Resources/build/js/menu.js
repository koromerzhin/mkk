
import { FormVerifier } from '../../../../SiteBundle/Resources/build/js/modules/form/verifier'
import { FormEnvoyer } from '../../../../SiteBundle/Resources/build/js/modules/form/envoyer'
import { getXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/getXHRJson'
import { postXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
import { LienActifDesactif } from '../../../../SiteBundle/Resources/build/js/modules/lienactifdesactif'
class Menu {
  mkkMenuInitBoutonSaveOnclick(e) {
    FormEnvoyer('#FormMenu')
    e.preventDefault()
  }
  mkkMenuInitPopupMenuMoveRelSaveOnClickOnSuccess(response) {
    document.location.reload()
  }
  async Save() {
    let datapost = {
      parent: $('#deplacermenu').val(),
      id: $('#PopupMenuMove').attr('data-id')
    }
    let response = await postXHRJson(
      $('#PopupMenuMove').attr('data-url'),
      datapost
    )
    document.location.reload()

  }
  mkkMenuInitPopupMenuMoveRelSaveOnClick(e) {
    this.Save()
    e.preventDefault()
  }
  mkkMenuInitPopupMenuMoveOnShow() {
    $('#deplacermenu').focus()
  }
  mkkMenuAjouterParentRelSaveOnClick(e) {
    this.AjouterParent($(e.currentTarget).attr('href'), $(e.currentTarget).data('id'))
    e.preventDefault()
  }
  async AjouterParent(href, id) {
    let response = await postXHRJson(
      href,
      {
        id: id
      }
    )
    document.location.reload()
  }
  AjouterParentOnClick(e) {
    let id = $(e.currentTarget).attr('data-id')
    let href = $(e.currentTarget).attr('href')
    $('#PopupParentMenu').find('a[data-rel=save]')
      .data('id', id).attr('href', href)
    $('#PopupParentMenu').modal('show')
    e.preventDefault()
  }
  DeplacerMenuOnClick(e) {
    let url = $(e.currentTarget).attr('href')
    let id = $(e.currentTarget).attr('data-id')
    $('#PopupMenuMove').attr('data-url', url)
    $('#PopupMenuMove').attr('data-id', id)
    this.Deplacer(url)
    e.preventDefault()
  }
  async Deplacer(url) {
    let response = await getXHRJson(url)
    $('#deplacermenu').html('')
    $(response).each(
      function() {
        let id = $(this).attr('id')
        let text = $(this).attr('text')
        let html = ''
        let option = document.createElement('option')
        $(option)
          .attr('value', id)
          .text(text)
        $('#deplacermenu').append(option)
      }
    )
    $('#PopupMenuMove').modal('show')
  }
  async AjouterParent(href, id) {
    let response = await postXHRJson(
      href,
      {
        id: id
      }
    )
    document.location.reload()
  }
  constructor() {
    $(".DeplacerMenu").on("click", this.DeplacerMenuOnClick.bind(this))
    $(".AjouterParent").on("click", this.AjouterParentOnClick.bind(this))
    $('#PopupMenuMove').on('shown.bs.modal', this.mkkMenuInitPopupMenuMoveOnShow)
    $('#PopupParentMenu').find('a[data-rel=save]').on("click", this.mkkMenuAjouterParentRelSaveOnClick.bind(this))
    $('#PopupMenuMove').find('a[data-rel=save]').on("click", this.mkkMenuInitPopupMenuMoveRelSaveOnClick.bind(this))
    $('.TableauMenu').each(
      function() {
        let classe = new LienActifDesactif('#' + $(this).attr('id'))
      }
    )
  }
  async sendEnregistrer() {
    let datapost = $('#FormMenu').serializeForm()
    let response = await postXHRJson(
      $('#FormMenu').attr('action'),
      datapost
    )

  }
  Enregistrer() {
    if (FormVerifier($('#FormMenu'))) {
      this.sendEnregistrer()
    }
    return false
  }
}


export default () => {
  return new Menu()
}
