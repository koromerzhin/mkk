import { FormEnvoyer } from '../../../../SiteBundle/Resources/build/js/modules/form/envoyer'
import { getXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/getXHRJson'
import { postXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
import { remplitForm } from '../../../../SiteBundle/Resources/build/js/modules/remplitForm'
import { FormVerifier } from '../../../../SiteBundle/Resources/build/js/modules/form/verifier'
class Categorie {
  mkkCategorieInitRelModifierOnClick(e) {
    let urlshow = $(e.currentTarget).data('show')
    let urlhref = $(e.currentTarget).attr('href')
    this.Modifier(urlshow, urlhref)
    e.preventDefault()
  }
  mkkCategorieInitRelSaveOnClick(e) {
    FormEnvoyer('#PopupFormulaire')
    e.preventDefault()
  }
  mkkCategorieInitRelSaveOnClickBoutonAddOnClick(e) {
    let url = $(this).attr('href')
    $('#categorie_nom').val('')
    $('#PopupFormulaireAjouter').show()
    $('#PopupFormulaire').find('form').attr('action', url)
    $('#PopupFormulaire').modal('toggle')
    e.preventDefault()
    return false
  }
  mkkCategorieInitPopupFormulaireOnHide() {
    $('#PopupFormulaireAjouter, #PopupFormulaireModifier').hide()
    $('#PopupFormulaire').find('form').attr('action', url)
    $('#PopupFormulaire').find('input,textarea').each(
      function() {
        $(this).val('')
      }
    )
    $('#PopupFormulaire').find('select').each(
      function() {
        $(this).select2('val', '')
      }
    )
  }
  async sendEnregistrer(categorie) {
    let datapost = $('#PopupFormulaire').find('form').serializeForm()
    let response = await postXHRJson($('#PopupFormulaire').find('form').attr('action'), datapost)
    $('#PopupFormulaire').find('form').attr('action', "#")
    $('#PopupFormulaire').modal('toggle')
  //document.location.reload()
  }
  constructor() {
    $('.ModifierCategorie').on("click", this.mkkCategorieInitRelModifierOnClick.bind(this))
    $('#PopupFormulaire').find('[data-rel=\'save\']').on("click", this.mkkCategorieInitRelSaveOnClick)
    $('#PopupFormulaire').on('hide', this.mkkCategorieInitPopupFormulaireOnHide)
  }
  async Modifier(urlshow, urlhref) {
    $('#PopupFormulaireModifier').show()
    let response = await getXHRJson(urlshow)
    $('#PopupFormulaire').find('input[type=radio]').each(
      function() {
        if ($(this).val() == response.type) {
          $(this).prop('checked', true)
        }
      }
    )
    remplitForm($("#PopupFormulaire"), response)
    $('#PopupFormulaire').find('form').attr('action', urlhref)
    $('#PopupFormulaire').modal('toggle')
  }
  Enregistrer() {
    if (FormVerifier($('#PopupFormulaire').find('form'))) {
      this.sendEnregistrer(this)
    }
    return false
  }
}

export default () => {
  return new Categorie()
}
