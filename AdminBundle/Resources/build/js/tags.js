

import { Popup } from './tags/popup'
import { FormVerifier } from '../../../../SiteBundle/Resources/build/js/modules/form/verifier'
import { FormEnvoyer } from '../../../../SiteBundle/Resources/build/js/modules/form/envoyer'
import { postXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Tags {
  mkkTagsInitRelModifierOnClick(e) {
    leturl = $(e).data('show')
    this.Modifier(url)
    e.preventDefault()
  }
  mkkTagsInitRelSaveOnClick(e) {
    this.Save()
    e.preventDefault()
  }
  mkkTagsInitBoutonAddOnClick(e) {
    let url = $(e.currentTarget).attr('href')
    this.Ajouter(url)
    e.preventDefault()
  }
  async sendEnregistrer() {
    let datapost = $('#PopupFormulaire').find('form').serializeForm()
    let response = await postXHRJson(
      $('#PopupFormulaire').find('form').attr('action'),
      datapost
    )
    this.Popup.Fermer()
    document.location.reload()
  }
  mkkTagsInitPopupFormulaireOnHide() {
    $('#PopupFormulaireAjouter, #PopupFormulaireModifier').hide()
    $('#PopupFormulaire').find('form').attr('action', "#")
    $('#PopupFormulaire').find('input,textarea').each(
      function() {
        $(this).val('')
      }
    )
    $('#PopupFormulaire').find('select').each(
      function() {
        $(this).select2('val', '')
      }
    )
  }
  constructor() {
    this.Popup = new Popup()
    $('[data-rel=\'modifier\']').on("click", this.mkkTagsInitRelModifierOnClick.bind(this))
    $('[data-rel=\'save\']').on("click", this.mkkTagsInitRelSaveOnClick.bind(this))
    $('#PopupFormulaire').on('hide', this.mkkTagsInitPopupFormulaireOnHide)
  }
  async Modifier(url) {
    $('#PopupFormulaireModifier').show()
    let response = await(url)
    $('#tag_nom').val($(response).attr('nom'))
    this.Popup.Ouvrir(url)
  }
  Ajouter(url) {
    $('#tag_nom').val('')
    $('#PopupFormulaireAjouter').show()
    this.Popup.Ouvrir(url)
  }
  Enregistrer() {
    if (FormVerifier($('#PopupFormulaire').find('form'))) {
      this.sendEnregistrer()
    }
    return false
  }
  Save() {
    FormEnvoyer('#PopupFormulaire')
    this.Popup.Fermer()
  }
}



export default () => {
  return new Tags()
}
