
import { Ajouter } from './bugs/ajouter'
import { Modifier } from './bugs/modifier'
import { postXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Bugs {
  BoutonModifierStatusOnClick(e) {
    this.Modifier.Popup()
    e.preventDefault()
  }
  PopupModifBugsOnClick(e) {
    this.Modifier.Save()
    e.preventDefault()
  }
  PopupAddBugsARelAddOnClick(e) {
    this.Ajouter.Save()
    e.preventDefault()
  }
  BoutonAddOnClick(e) {
    this.Ajouter.Popup()
    e.preventDefault()
  }
  constructor() {
    this.Ajouter = new Ajouter()
    this.Modifier = new Modifier()
    $('#BoutonModifierStatus').on('click', this.BoutonModifierStatusOnClick.bind(this))
    $('#PopupModifBugs').find('a[data-rel=\'add\']').on('click', this.PopupModifBugsOnClick.bind(this))
    $('#PopupAddBugs').find('a[data-rel=\'add\']').on('click', this.PopupAddBugsARelAddOnClick.bind(this))
  }
}



export default () => {
  return new Bugs()
}
