
import { Color } from './system/color'
import { Tags } from './system/tags'
import { FormVerifier } from '../../../../SiteBundle/Resources/build/js/modules/form/verifier'
import { FormEnvoyer } from '../../../../SiteBundle/Resources/build/js/modules/form/envoyer'
import { postXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class System {
  navstackedliOnClick() {
    $("html, body").animate(
      {
        scrollTop: 0
      },
      "slow"
    )
  }
  onclickBoutonSave(e) {
    FormEnvoyer('#FormSystem')
    e.preventDefault()
  }
  async sendEnregistrer() {
    let datapost = $('#FormSystem').serializeForm()
    let response = await postXHRJson(document.location, datapost)
  }
  constructor() {
    this.Color = Color,
    this.Tags = Tags,
    $(".nav-stacked").find("li").each(
      function() {
        $(this).on("click", this.navstackedliOnClick)
      }
    )
  }
  Enregistrer() {
    if (FormVerifier($('#FormSystem'))) {
      this.sendEnregistrer()
    }
    return false
  }
}



export default () => {
  return new System()
}
