

import { FormVerifier } from '../../../../SiteBundle/Resources/build/js/modules/form/verifier'
import { FormEnvoyer } from '../../../../SiteBundle/Resources/build/js/modules/form/envoyer'
import { getXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/getXHRJson'
import { postXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Bookmark {
  async sendEnregistrer() {
    let datapost = $('#FormBookmark').serializeForm()
    let response = await postXHRJson(
      $('#FormBookmark').attr('action'),
      datapost
    )
  }
  mkkBookmarkInitBoutonSaveOnClick(e) {
    FormEnvoyer('#FormBookmark')
    e.preventDefault()
  }
  constructor() {
    if ($('#FormBookmark').length) {
      this.Tags()
    }
  }
  async Tags() {
    let response = await getXHRJson($('#FormBookmark').attr('data-tag'))
    $('#bookmark_tag').select2(
      {
        tags: response.all,
        tokenSeparators: [','],
        allowClear: true
      }
    )
    $('#bookmark_tag').val(response.selected).trigger('change')
  }
  Enregistrer() {
    if (FormVerifier($('#FormBookmark'))) {
      this.sendEnregistrer()
    }
    return false
  }
}


export default () => {
  return new Bookmark()
}
