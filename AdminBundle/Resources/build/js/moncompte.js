import { FormVerifier } from '../../../../SiteBundle/Resources/build/js/modules/form/verifier'
import { FormEnvoyer } from '../../../../SiteBundle/Resources/build/js/modules/form/envoyer'
import { postXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Moncompte {
  mkkUserInitBoutonSaveOnClick(e) {
    FormEnvoyer("#FormulaireUser")
    e.preventDefault()
  }
  constructor() {
    //$("#FormulaireUser").on("submit", this.Enregistrer.bind(this))
  }
  async sendEnregistrer() {
    let datapost = $('#FormulaireUser').serializeForm()
    let response = await postXHRJson(
      $("#FormulaireUser").attr('action'),
      datapost
    )
  }
  Enregistrer() {
    if (FormVerifier($("#FormulaireUser"))) {
      this.sendEnregistrer()
    }
    return false
  }
}

export default () => {
  return new Moncompte()
}
