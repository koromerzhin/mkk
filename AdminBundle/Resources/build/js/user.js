
import { FormVerifier } from '../../../../SiteBundle/Resources/build/js/modules/form/verifier'
import { FormEnvoyer } from '../../../../SiteBundle/Resources/build/js/modules/form/envoyer'
import { postXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class User {
  mkkUserInitBoutonSaveOnClick(e) {
    FormEnvoyer("#FormulaireUser")
    e.preventDefault()
  }
  constructor() {}
  async sendEnregistrer(json) {
    let datapost = $('#FormulaireUser').serializeForm()
    let response = await postXHRJson(
      $("#FormulaireUser").attr('action'),
      datapost
    )
  }
  Enregistrer(json) {
    if (FormVerifier($("#FormulaireUser"))) {
      this.sendEnregistrer(json)
    }
    return false
  }
}



export default () => {
  return new User()
}
