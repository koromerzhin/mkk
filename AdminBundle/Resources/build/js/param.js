

import { Color } from './param/color'
import { Tags } from './param/tags'
import { FormVerifier } from '../../../../SiteBundle/Resources/build/js/modules/form/verifier'
import { FormEnvoyer } from '../../../../SiteBundle/Resources/build/js/modules/form/envoyer'
import { postXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Param {
  paramuploadchoixOnChange() {
    let val = $(this).select2("val")
    $(".tab-upload").each(
      function() {
        let id = $(this).attr('id')
        if (id == "tabs-upload" + val) {
          $(this).css("display", "block")
        } else {
          $(this).css("display", "none")
        }
      }
    )
  }
  onclickBoutonSave(e) {
    FormEnvoyer('#FormParam')
    e.preventDefault()
  }
  navstackedliOnClick() {
    $("html, body").animate(
      {
        scrollTop: 0
      },
      "slow"
    )
  }
  async sendEnregistrer() {
    let datapost = $('#FormParam').serializeForm()
    let response = await postXHRJson(
      $('#FormParam').attr('action'),
      datapost
    )
  }
  constructor() {
    this.Color = new Color()
    this.Tags = new Tags()
    $("#param_upload_choix").on("change", this.paramuploadchoixOnChange)
    let val = $("#param_upload_choix").find("option:first").val()
    $("#param_upload_choix").select2("val", val).trigger('change')
    $("#TabUpload").find(".tab-pane:first").show()
    $(".nav-stacked").find("li").each(
      function() {
        $(this).on("click", this.navstackedliOnClick)
      }
    )
    $(".colorpicker").each(
      function() {
        $(this).colorpicker()
      }
    )
  }
  Enregistrer() {
    if (FormVerifier($('#FormParam'))) {
      this.sendEnregistrer()
    }
    return false
  }
}



export default () => {
  return new Param()
}
