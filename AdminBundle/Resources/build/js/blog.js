

import { Accueil } from './blog/accueil'
import { Avant } from './blog/avant'
import { FormVerifier } from '../../../../SiteBundle/Resources/build/js/modules/form/verifier'
import { FormEnvoyer } from '../../../../SiteBundle/Resources/build/js/modules/form/envoyer'
import { getXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/getXHRJson'
import { postXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Blog {
  async sendEnregistrer() {
    let datapost = $('#FormBlog').serializeForm()
    let response = await postXHRJson($('#FormBlog').attr('action'), datapost)
  }
  BoutonOnSave(e) {
    FormEnvoyer("#FormBlog")
    e.preventDefault()
  }
  constructor() {
    this.Accueil = new Accueil()
    this.Avant = new Avant()
    if ($('#FormBlog').length) {
      this.Tags()
    }
  }
  Enregistrer() {
    if (FormVerifier($('#FormBlog'))) {
      this.sendEnregistrer()
    }
    return false
  }
  async Tags() {
    let response = await getXHRJson($('#FormBlog').attr('data-tag'))
    $('#blog_tag').select2(
      {
        tags: response.all,
        tokenSeparators: [','],
        allowClear: true
      }
    )
    $('#blog_tag').val(response.selected).trigger('change')
  }
}

export default () => {
  return new Blog()
}
