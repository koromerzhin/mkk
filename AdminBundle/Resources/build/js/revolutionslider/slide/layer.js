
import { getXHRJson } from '../../../../../../SiteBundle/Resources/build/js/modules/xhr/getXHRJson'
import { FormVerifier } from '../../../../../../SiteBundle/Resources/build/js/modules/form/verifier'
import { postXHRJson } from '../../../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Layer {
    async sendEnregistrer() {
        let datapost = $("#FormCaption").serializeForm()
        let response = postXHRJson(
            $("#FormCaption").attr("action"),
            datapost
        )
    document.location.reload()
    }
    Enregistrer() {
        if (FormVerifier($("#FormCaption"))) {
            this.sendEnregistrer()
        }
        return false
    }
    async sendPosition(datapost) {
        let response = await postXHRJson(
            $("#BoutonPosition").attr("href"),
            datapost
        )
    document.location.reload()

    }
    Position() {
        let tab         = {}
        tab['position'] = {}
        $('.table-sortable').find('tbody').find("tr").each(
            function (i) {
                tab['position'][i] = $(this).attr("data-order")
            }
        )
    this.sendPosition(tab)
    return false
    }
    async sendText(url) {
        let response = getXHRJson(url)
        $("#ModalLayer").find(".modal-body").html(response)
        $("#ModalLayer").modal('toggle')
    }
    Text(lien) {
        this.sendText($(lien).attr("href"))
        return false
    }
}

export { Layer }
