
import { FormEnvoyer } from '../../../../../SiteBundle/Resources/build/js/modules/form/envoyer'
import { FormVerifier } from '../../../../../SiteBundle/Resources/build/js/modules/form/verifier'
import { postXHRJson } from '../../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Slider {
  mkkSliderInitBoutonSaveOnClick(e) {
    FormEnvoyer("#FormSlider")
    e.preventDefault()
  }
  mkkSliderInitBoutonPositionOnClick() {
    this.SlidePosition()
    return false
  }
  mkkSliderInitNavTabsLiAOnClick() {
    if ($(this).attr("data-url") !== undefined) {
      localStorage.setItem('lastTab', $(this).attr('data-url'))
    }
  }
  constructor() {
    $("#BoutonPosition").on("click", this.mkkSliderInitBoutonPositionOnClick.bind(this))
    $(".nav-tabs").find("li").each(
      function() {
        $(this).find("a").on("click", this.mkkSliderInitNavTabsLiAOnClick)
      }
    )
    $('.table-sortable').find('tbody').sortable()
  }
  async sendSlidePosition(datapost) {
    let response = await postXHRJson(
      $('#BoutonPosition').attr("href"),
      datapost
    )
    document.location.reload()
  }
  SlidePosition() {
    let position = {}
    let texte = ""
    $('.table-sortable').find('tbody').find("tr").each(
      function(i) {
        id = $(this).attr('data-id')
        if (texte != "") {
          texte += ","
        }
        texte += id
      }
    )
    position["slides"] = texte
    this.sendSlidePosition(position)
  }
  async sendEnregistrer() {
    let datapost = $("#FormSlider").serializeForm()
    let response = await postXHRJson(
      $("#FormSlider").attr('action'),
      datapost
    )
  }
  Enregistrer() {
    if (FormVerifier($("#FormSlider"))) {
      this.sendEnregistrer()
    }
    return false
  }
}

export { Slider }
