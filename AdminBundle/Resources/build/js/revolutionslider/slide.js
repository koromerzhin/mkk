
import { Layer } from './slide/layer'
import { FormEnvoyer } from '../../../../../SiteBundle/Resources/build/js/modules/form/envoyer'
import { FormVerifier } from '../../../../../SiteBundle/Resources/build/js/modules/form/verifier'
import { postXHRJson } from '../../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Slide {
  mkkSlideInitModalLayerRelSaveOnClick() {
    FormEnvoyer("#FormCaption")
  }
  mkkSlideInitNavTabsLiAOnClick() {
    if ($(this).attr("data-url") !== undefined) {
      localStorage.setItem('lastTab', $(this).attr('data-url'))
    }
  }
  mkkSlideInitBoutonAddOnClick(e) {
    this.Text($(e))
    return false
  }
  mkkSlideInitBoutonSaveOnClick() {
    FormEnvoyer("#FormSlide")
    return false
  }
  mkkSlideInitBoutonPositionOnClick() {
    this.Position()
    return false
  }
  constructor() {
    this.Layer = new Layer()
    $("#ModalLayer").find("a[data-rel='save']").on("click", this.mkkSlideInitModalLayerRelSaveOnClick)
    $(".nav-tabs").find("li").each(
      function() {
        $(this).find("a").on("click", this.mkkSlideInitNavTabsLiAOnClick)
      }
    )
    $('.table-sortable').find('tbody').sortable()
    $("#BoutonPosition").on("click", this.mkkSlideInitBoutonPositionOnClick.bind(this))
  }
  async sendEnregistrer() {
    let datapost = $("#FormSlide").serializeForm()
    let response = await postXHRJson(
      $("#FormSlide").attr('action'),
      datapost
    )
  }
  Enregistrer() {
    if (FormVerifier($("#FormSlide"))) {
      this.sendEnregistrer()
    }
    return false
  }
}
export { Slide }
