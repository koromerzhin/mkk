

import { FormVerifier } from '../../../../SiteBundle/Resources/build/js/modules/form/verifier'
import { FormEnvoyer } from '../../../../SiteBundle/Resources/build/js/modules/form/envoyer'
import { getXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/getXHRJson'
import { postXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Noteinterne {
  async sendEnregistrer() {
    let datapost = $('#FormNoteinterne').serializeForm()
    let response = await postXHRJson(
      $('#FormNoteinterne').attr('action'),
      datapost
    )
  }
  mkkNoteinterneInittabdestinataireOnShow(e) {
    this.VoirPlus()
    $(window).on("scroll", this.mkkNoteinterneInittabdestinataireOnShowWindowOnScroll.bind(this))
  }
  mkkNoteinterneInittabdestinataireOnShowWindowOnScroll() {
    var mostOfTheWayDown = ($(document).height() - $(window).height()) * 1 / 3
    if ($(window).scrollTop() >= mostOfTheWayDown) {
      this.VoirPlus()
    }
  }
  mkkNoteinterneInitBoutonSaveOnClick(e) {
    this.Save()
    e.preventDefault()
  }
  mkkNoteinterneInitnoteinternetousOnClick() {
    if ($('#noteinterne_tous').is(':checked')) {
      $('#noteinterne_groupes, #noteinterne_entreprises, #noteinterne_users').select2('enable', false)
    } else {
      $('#noteinterne_groupes, #noteinterne_entreprises, #noteinterne_users').select2('enable', true)
    }
  }
  constructor() {
    if ($('#FormNoteinterne').length) {
      if ($('#noteinterne_groupes').length) {
        $('#noteinterne_groupes, #noteinterne_entreprises, #noteinterne_users').val('')
        $('#noteinterne_tous').on("click", this.mkkNoteinterneInitnoteinternetousOnClick)
      }
    }
    if ($('#tab-destinataire').hasClass('active')) {
      Noteinterne.VoirPlus()
    }
    if ($('#tab-destinataire').attr('data-url') != undefined) {
      $('a[href="#tab-destinataire"]').on('shown.bs.tab', this.mkkNoteinterneInittabdestinataireOnShow.bind(this))
    }
  }
  async VoirPlus() {
    if ($('#tab-destinataire').attr('data-url') != undefined) {
      let url = $('#tab-destinataire').attr('data-url')
      $('#tab-destinataire').removeAttr('data-url')
      let response = await getXHRJson(url)
      if (response.url != undefined) {
        $('#tab-destinataire').attr('data-url', response.url)
      }
      $('#tab-destinataire').find('tbody').append(response.html)
      let mostOfTheWayDown = ($(document).height() - $(window).height()) * 1 / 3
      if ($(window).scrollTop() >= mostOfTheWayDown) {
        Noteinterne.VoirPlus()
      }
    }
  }
  Save() {
    FormEnvoyer('#FormNoteinterne')
  }
  Enregistrer() {
    if (FormVerifier($('#FormNoteinterne'))) {
      this.sendEnregistrer()
    }
    return false
  }
}



export default () => {
  return new Noteinterne()
}
