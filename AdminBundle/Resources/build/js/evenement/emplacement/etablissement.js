import { postXHRJson } from '../../../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Etablissement {
    BoutonSaveClick(e) {
        e.preventDefault()
        let val = $('#LieuListing').select2('val')
        this.Add(val)
    }
    async Add(id) {
        let datapost = {
            'id': id
        }
        let response = await postXHRJson(
            $("#FormulaireEtablissementAdd").attr('action'),
            datapost,
            0
        )
    document.location.reload()
    }
    constructor() {
        $("#PopupAjoutEmplacementEtablissement").find("a[data-rel='save']").on("click", this.BoutonSaveClick.bind(this))
    }
    Choix() {
        $("#PopupAjoutEmplacementEtablissement").modal("toggle")
        console.log('choix')
    }
}

export { Etablissement }
