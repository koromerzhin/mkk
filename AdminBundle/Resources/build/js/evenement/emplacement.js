import { getXHRJson } from '../../../../../SiteBundle/Resources/build/js/modules/xhr/getXHRJson'
import { Etablissement } from './emplacement/etablissement'
class Emplacement {
    AjouterEmplacementClick(e) {
        $("#PopupAjoutEmplacementChoix").modal("toggle")
        e.preventDefault()
    }
    async addVille() {
        let response = await getXHRJson(
            $("#PopupAjoutEmplacementChoix").find("a[data-rel='ville']").attr('href')
        )
    document.location.reload()
    }
    BoutonVilleClick(e) {
        this.addVille()
        e.preventDefault()
    }
    BoutonEtablissementClick(e) {
        $("#PopupAjoutEmplacementChoix").modal("toggle")
        this.Etablissement.Choix()
        e.preventDefault()
    }
    constructor() {
        this.Etablissement = new Etablissement()
        $("#PopupAjoutEmplacementChoix").find("a[data-rel='ville']").on("click", this.BoutonVilleClick.bind(this))
        $("#PopupAjoutEmplacementChoix").find("a[data-rel='etablissement']").on("click", this.BoutonEtablissementClick.bind(this))

        $("#AjouterEmplacement").on("click", this.AjouterEmplacementClick)
    }
}

export { Emplacement }
