
class Popup {
    Ouvrir(url) {
        $('#PopupFormulaire').find('form').attr('action', url)
        $('#PopupFormulaire').modal('toggle')
    }
    Fermer() {
        $('#PopupFormulaire').find('form').attr('action', '#')
        $('#PopupFormulaire').modal('hide')
    }
}
export { Popup }
