
import { Popup } from './group/popup'
import { FormVerifier } from '../../../../SiteBundle/Resources/build/js/modules/form/verifier'
import { FormEnvoyer } from '../../../../SiteBundle/Resources/build/js/modules/form/envoyer'
import { getXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/getXHRJson'
import { postXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
import { remplitForm } from '../../../../SiteBundle/Resources/build/js/modules/remplitForm'
class Group {
  InitaRelModifierOnClick(e) {
    let url = $(e.currentTarget).data('show')
    this.Modifier(url)
    e.preventDefault()
  }
  onclickAddGroup(e) {
    this.Save()
    e.preventDefault()
  }
  onclickBoutonAdd(e) {
    let url = $(e.currentTarget).attr('href')
    this.Ajouter(url)
    e.preventDefault()
  }
  async sendEnregistrer() {
    let datapost = $('#FormGroup').serializeForm()
    let response = await postXHRJson(
      $('#FormGroup').attr('action'),
      datapost
    )
  }
  constructor() {
    this.Popup = new Popup()
    $(".ModifierGoup").on("click", this.InitaRelModifierOnClick.bind(this))
    $('#AddGroup').find('[data-rel=\'save\']').on("click", this.onclickAddGroup.bind(this))
  }
  Ajouter(url) {
    $('#group_code,#group_nom').val('')
    $('#AddGroup').show()
    $('#group_code').removeAttr('disabled')
    this.Popup.Ouvrir(url)
  }

  async Modifier(url) {
    $('#group_code').attr('disabled', 'disabled')
    let response = await getXHRJson(url)
    remplitForm($("#AddGroup"), response)
    this.Popup.Ouvrir(url)
    return false
  }
  Save() {
    FormEnvoyer('#FormGroup')
    this.Popup.Fermer()
  }
  Enregistrer() {
    if (FormVerifier($('#FormGroup'))) {
      this.sendEnregistrer()
    }
    return false
  }
}


export default () => {
  return new Group()
}
