import { FormVerifier } from '../../../../SiteBundle/Resources/build/js/modules/form/verifier'
import { FormEnvoyer } from '../../../../SiteBundle/Resources/build/js/modules/form/envoyer'
import { postXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
import { Emplacement } from './evenement/emplacement'
class Evenement {
  BoutonSaveOnClick() {
    FormEnvoyer("#FormEvenement")
    return false
  }
  ChangePlaceillimite() {
    if ($(this).val() == 1) {
      $("#evenement_totalnbplace").val(0).closest(".form-group").hide()
    } else {
      $("#evenement_totalnbplace").closest(".form-group").show()
    }
  }
  InitPlaceIllimite() {
    if ($("#evenement_placeillimite_0").length) {
      let val = $("input[name='evenement[type]']:checked").val()
      if (val == 0) {
        $("#evenement_totalnbplace").val(0).closest(".form-group").hide()
        $("input[name='evenement[placeillimite]']").each(
          function() {
            if ($(this).val() == 0) {
              $(this).prop("checked", true)
            }
          }
        )
        $("input[name='evenement[placeillimite]']").closest(".form-group").hide()
        $("#evenement_totalnbplace").closest(".form-group").hide()
      } else {
        $("input[name='evenement[placeillimite]']").closest(".form-group").show()
        val = $("input[name='evenement[placeillimite]']:checked").val()
        if (val == 0) {
          $("#evenement_totalnbplace").closest(".form-group").show()
        } else {
          $("#evenement_totalnbplace").closest(".form-group").hide()
        }
      }
    }
  }
  ChangeTypeEvenement() {
    let val = $(this).val()
    if (val == 0) {
      $("#evenement_totalnbplace").val(0).closest(".form-group").hide()
      $("input[name='evenement[placeillimite]']").each(
        function() {
          if ($(this).val() == 0) {
            $(this).prop("checked", true)
          }
        }
      )
      $("input[name='evenement[placeillimite]']").closest(".form-group").hide()
    } else {
      $("#evenement_totalnbplace").closest(".form-group").show()
      $("input[name='evenement[placeillimite]']").closest(".form-group").show()
    }
  }
  constructor() {
    this.Emplacement = new Emplacement()
    this.InitPlaceIllimite()
    $("input[name='evenement[type]']").on("change", this.ChangeTypeEvenement)
    $("input[name='evenement[placeillimite]']").on("change", this.ChangePlaceillimite)
  }
  async sendEnregistrer(datapost) {
    let response = await postXHRJson(
      $("#FormEvenement").attr('action'),
      datapost
    )
  }
  Enregistrer() {
    if (FormVerifier($("#FormEvenement"))) {
      let datapost = $("#FormEvenement").serializeForm()
      this.sendEnregistrer(datapost)
    }
    return false
  }
}
export default () => {
  return new Evenement()
}
