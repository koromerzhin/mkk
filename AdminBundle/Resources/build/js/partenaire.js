
import { FormVerifier } from '../../../../SiteBundle/Resources/build/js/modules/form/verifier'
import { FormEnvoyer } from '../../../../SiteBundle/Resources/build/js/modules/form/envoyer'
import { postXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Partenaire {
  mkkPartenaireInitBoutonSaveOnClick(e) {
    this.Save()
    e.preventDefault()
  }
  async sendEnregistrer() {
    let datapost = $('#FormPartenaire').serializeForm()
    let response = await postXHRJson(
      $('#FormPartenaire').attr('action'),
      datapost
    )
  }
  constructor() {
    this.traiter()
  }
  Save() {
    FormEnvoyer('#FormPartenaire')
  }
  Enregistrer() {
    if (FormVerifier($('#FormPartenaire'))) {
      this.sendEnregistrer()
    }
    return false
  }
}


export default () => {
  return new Partenaire()
}
