
import { FormVerifier } from '../../../../SiteBundle/Resources/build/js/modules/form/verifier'
import { FormEnvoyer } from '../../../../SiteBundle/Resources/build/js/modules/form/envoyer'
import { postXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Edito {
  mkkEditoInitBoutonSaveOnClick() {
    FormEnvoyer('#FormEdito')
    return false
  }
  async sendEnregistrer() {
    let datapost = $('#FormEdito').serializeForm()
    let response = await postXHRJson(
      $('#FormEdito').attr('action'),
      datapost
    )
  }
  constructor() {}
  Enregistrer() {
    if (FormVerifier($('#FormEdito'))) {
      this.sendEnregistrer()
    }

    return false
  }
}


export default () => {
  return new Edito()
}
