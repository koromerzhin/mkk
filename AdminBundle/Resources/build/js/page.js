
import { FormVerifier } from '../../../../SiteBundle/Resources/build/js/modules/form/verifier'
import { postXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Page {
  mkkPageInitBoutonSaveOnClick(e) {
    $("#FormPage").find("input[type=submit]").attr('data-actifbouton', 1).trigger("click")
    e.preventDefault()
  }
  async sendEnregistrer() {
    let datapost = $("#FormPage").serializeForm()
    let response = await postXHRJson(
      $("#FormPage").attr('action'),
      datapost
    )
  }
  constructor() {}
  Enregistrer() {
    if (FormVerifier($("#FormPage"))) {
      this.sendEnregistrer()
    }
    return false
  }
}


export default () => {
  return new Page()
}
