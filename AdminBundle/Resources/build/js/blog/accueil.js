
import { postXHRJson } from '../../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Accueil {
    IconAccueilOnclick(e) {
        let avant = $(e.currentTarget).attr('data-accueil')
        if (avant == 0) {
            let popup = 'PopupActiverAccueil'
            let etat  = 1
        } else {
            let popup = 'PopupDesactiverAccueil'
            let etat  = 0
        }
        let lien = $('#' + popup).find('a[data-rel=\'save\']')
        $(lien).attr(
            {
                href: $(this).attr('href'),
                'data-etat': etat
            }
        )
    $('#' + popup).modal('toggle')
    e.preventDefault()
    }
    async sendAccueil(url, datapost) {
        let response = await postXHRJson(url, datapost)
        document.location.reload()
    }
    PopupDesactiverAccueilDataRelSaveOnClick(e) {
        let datapost = {
            etat: $(e.currentTarget).attr('data-etat')
        }
        this.sendAccueil($(e.currentTarget).attr('href'), datapost)
        e.preventDefault()
    }
    constructor() {
        $('#PopupDesactiverAccueil,#PopupActiverAccueil').find('a[data-rel=\'save\']').on("click", this.PopupDesactiverAccueilDataRelSaveOnClick.bind(this))
        $('.IconAccueil').each(
            function () {
                let avant = $(this).attr('data-accueil')
                if (avant == 0) {
                    let title = $(this).attr('data-textactiver')
                } else {
                    let title = $(this).attr('data-textdesactiver')
                }
                $(this).attr('title', title)
                $(this).on("click", this.IconAccueilOnclick)
            }
        )
    }
}
export { Accueil }
