
import { postXHRJson } from '../../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Avant {
    IconAvantOnClick(e) {
        let avant = $(e.currentTarget).attr('data-avant')
        if (avant == 0) {
            let popup = 'PopupActiverAvant'
            let etat  = 1
        } else {
            let popup = 'PopupDesactiverAvant'
            let etat  = 0
        }
        let lien = $('#' + popup).find('a[data-rel=\'save\']')
        $(lien).attr(
            {
                href: $(this).attr('href'),
                'data-etat': etat
            }
        )
    $('#' + popup).modal('toggle')
    e.preventDefault()
    }
    async sendEnAvant(url, datapost) {
        let response = await postXHRJson(url, datapost)
        document.location.reload()
    }
    PopupDesactiverAvantblogavantinitOnClick(e) {
        let datapost = {
            etat: $(e.currentTarget).attr('data-etat')
        }
        this.sendEnAvant($(e.currentTarget).attr('href'), datapost)
        e.preventDefault()
    }
    constructor() {
        $('#PopupDesactiverAvant,#PopupActiverAvant').find('a[data-rel=\'save\']').on("click", this.PopupDesactiverAvantblogavantinitOnClick.bind(this))
        $('.IconAvant').each(
            function () {
                let avant = $(this).attr('data-avant')
                if (avant == 0) {
                    let title = $(this).attr('data-textactiver')
                } else {
                    let title = $(this).attr('data-textdesactiver')
                }
                $(this).attr('title', title)
                $(this).on("click", this.IconAvantOnClick)
            }
        )
    }
}

export { Avant }
