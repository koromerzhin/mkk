
class Tags {
    Ajouter(lien) {
        let table = $(lien).closest('fieldset').find('table')
        let html  = $(table).find('tfoot').html()
        $(table).find('tbody').append(html)
        return false
    }
    Supprimer(lien) {
        $(lien).closest('tr').remove()
        return false
    }
}
export { Tags }
