
import { FormEnvoyer } from '../../../../../SiteBundle/Resources/build/js/modules/form/envoyer'
import { FormVerifier } from '../../../../../SiteBundle/Resources/build/js/modules/form/verifier'
import { postXHRJson } from '../../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Modifier {
    async sendEnregistrer() {
        let datapost = $('#FormModifierBugs').serializeForm()
        let response = await postXHRJson(
            $('#FormModifierBugs').attr('action'),
            datapost
        )
    $('#PopupModifBugs').modal('toggle')
    }
    Popup() {
        $('#PopupModifBugs>form').reset()
        $('#PopupModifBugs').modal('toggle')
    }
    Save() {
        FormEnvoyer('#FormModifierBugs')
    }
    Enregistrer() {
        if (FormVerifier($('#FormModifierBugs'))) {
            this.sendEnregistrer()
        }
        return false
    }
}
export { Modifier }
