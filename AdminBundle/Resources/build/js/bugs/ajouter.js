
import { FormEnvoyer } from '../../../../../SiteBundle/Resources/build/js/modules/form/envoyer'
import { FormVerifier } from '../../../../../SiteBundle/Resources/build/js/modules/form/verifier'
import { postXHRJson } from '../../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Ajouter {
    async endEnregistrer() {
        let datapost = $('#FormAddBugs').serializeForm()
        let response = await postXHRJson(
            $('#FormAddBugs').attr('action'),
            datapost
        )
    $('#PopupAddBugs').modal('toggle')
    }
    Popup() {
        $('#PopupAddBugs>form').reset()
        $('#PopupAddBugs').modal('toggle')
    }
    Save() {
        FormEnvoyer('#FormAddBugs')
    }
    Enregistrer() {
        if (FormVerifier($('#FormAddBugs'))) {
            this.sendEnregistrer()
        }
        return false
    }
}

export { Ajouter }
