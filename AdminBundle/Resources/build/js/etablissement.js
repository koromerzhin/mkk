import { FormEnvoyer } from '../../../../SiteBundle/Resources/build/js/modules/form/envoyer'
import { FormVerifier } from '../../../../SiteBundle/Resources/build/js/modules/form/verifier'
import { getXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/getXHRJson'
import { postXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Etablissement {
  mkkEtablissementInitTabCabinetAOnClick(e) {
    $(e.currentTarget).tab('show')
  }
  mkkEtablissementInitetablissementtypeOnChange() {
    if ($('#etablissement_type').val() != 'plateforme') {
      $('#champnomsite').show()
      $('#champparent').show()
      $('#champnompf').hide()
    } else {
      $('#champnomsite').hide()
      $('#champparent').hide()
      $('#champnompf').show()
    }
  }
  mkkEtablissementInitListeAOnClick(e) {
    let position = markers[i].getPosition()
    map.setCenter(position)
    $(markers).each(
      function(j) {
        if (j != i) {
          infowindow[j].close()
        } else {
          infowindow[i].open(map, markers[i])
        }
      }
    )
    e.preventDefault()
  }
  async sendSave(url) {
    let response = getXHRJson(
      url
    )
  }
  mkkEtablissementInitBoutonCorrigerOnSave(e) {
    this.sendSave($(this).attr('href'))
    e.preventDefault()
  }
  mkkEtablissementInitBoutonSaveOnClick(e) {
    FormEnvoyer('#FormEtablissement')
    e.preventDefault()
  }
  mkkEtablissementInitSortableBoutonSaveOnClick() {
    this.Position($(this).attr('href'))
    return false
  }
  mkkEtablissementInitPopupNewEtabRelSaveOnClick() {
    let url = $('#BoutonAdd').attr('href')
    let etat = $('#newetabs_etat').select2('val')
    if (etat != '') {
      url += '?etat=' + etat
      document.location = url
    }
    return false
  }
  mkkEtablissementInitBoutonAddOnClick(e) {
    let type = $(e.currentTarget).attr('data-type')
    if (type >= 2) {
      $('#PopupNewEtab').modal('toggle')
      return false
    }
  }
  async sendEnregistrer() {
    let datapost = $('#FormEtablissement').serializeForm()
    let response = await postXHRJson(
      $('#FormEtablissement').attr('action'),
      datapost
    )
  }
  constructor() {
    $('#PopupNewEtab').find('a[data-rel=\'save\']').on('click', this.mkkEtablissementInitPopupNewEtabRelSaveOnClick)
    if ($('#mapglobe').length != 0) {
      var coordonnees = {
        latitude: 47.7,
        longitude: -3.4
      }
      var myLatlng = new google.maps.LatLng(coordonnees['latitude'], coordonnees['longitude'])
      var myOptions = {
        zoom: 11,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        panControl: false,
        zoomControl: true,
        mapTypeControl: false,
        scaleControl: false,
        streetViewControl: false
      }
      var geocoder = new google.maps.Geocoder()
      var bound = new google.maps.LatLngBounds()
      var markers = []
      var infowindow = []
      var map = new google.maps.Map($('#mapglobe')[0], myOptions)
      $('#ListingGps>li').each(
        function() {
          data = $(this).attr('data-gps').split(',')
          markers.push(
            new google.maps.Marker(
              {
                position: new google.maps.LatLng(data[0], data[1]),
                map: map,
                icon: 'http://maps.google.com/mapfiles/kml/pal4/icon24.png'
              }
            )
          )
          infowindow.push(
            new google.maps.InfoWindow(
              {
                content: $('#' + $(this).attr('data-html')).html()
              }
            )
          )
        }
      )
      position = ''
      nbr = 0
      $.each(
        markers,
        function(i) {
          if (i != undefined) {
            position = markers[i].getPosition()
            bound.extend(position)
            google.maps.event.addListener(
              markers[i],
              'click',
              function() {
                $('#ListingGps>li').each(
                  function(j) {
                    if (j == i) {
                      $('#infocabinet').html($(this).find('div[data-rel=\'infosupp\']').html()).show()
                    }
                  }
                )
                $(markers).each(
                  function(j) {
                    if (j != i) {
                      infowindow[j].close()
                    } else {
                      infowindow[i].open(map, markers[i])
                    }
                  }
                )
              }
            )
            nbr++
          }
        }
      )
      if (nbr >= 1) {
        if (nbr == 1) {
          map.setCenter(position)
        } else {
          map.fitBounds(bound)
        }
      }
    }
    $('#BoutonCorriger').on("click", this.mkkEtablissementInitBoutonCorrigerOnSave.bind(this))
    $('#Liste').find('a').each(
      function(i) {
        $(this).on("click", this.mkkEtablissementInitListeAOnClick)
      }
    )
    $('#champparent').hide()
    if ($('#etablissement_type').length) {
      $('#etablissement_type').on("change", this.mkkEtablissementInitetablissementtypeOnChange)
      if ($('#etablissement_type').val() != 'plateforme') {
        $('#champnomsite').show()
        $('#champparent').show()
        $('#champnompf').hide()
      } else {
        $('#champnomsite').hide()
        $('#champparent').hide()
        $('#champnompf').show()
      }
    }
    $('#TabCabinet a').on("click", this.mkkEtablissementInitTabCabinetAOnClick)
  }
  Enregistrer() {
    if (FormVerifier($('#FormEtablissement'))) {
      this.sendEnregistrer()
    }
    return false
  }
  async Position(url) {
    let position = {}
    $('.sortable').each(
      function() {
        let texte = ''
        $(this).find('li').each(
          function() {
            id = $(this).attr('data-rel')
            if (texte != '') {
              texte += ','
            }
            texte += id
          }
        )
        let rel = $(this).attr('data-rel')
        position[rel] = texte
      }
    )
    let response = postXHRJson(
      url,
      position
    )
    document.location.reload(true)
  }
}

export default () => {
  return new Etablissement()
}
