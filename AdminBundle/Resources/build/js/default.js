
import { getXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/getXHRJson'
import { postXHRJson } from '../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Backend {
  backendInitWindowOnResize() {
    this.TableFixed()
  }
  backendInitNavBarOnClick() {
    $('aside, header, main, footer').toggleClass(
      'active',
      function() {
        if ($('aside').position().left == 0) {
          $('aside').getNiceScroll().hide()
        } else {
          window.setTimeout(
            function() {
              $('aside').getNiceScroll().show().resize()
            },
            1000
          )
        }
      }
    )
    return false
  }
  backendInitCollapseOnShown() {
    this.TableFixed()
  }
  backendInitDataToggleTabOnShow() {
    this.TableFixed()
  }
  backendInitAsideOnClick(e) {
    aa = $(this).parent().html()
    bb = ''
    truc = $(this).parent()
    if ($('aside > ul > li.active').length) {
      bb = $('aside > ul > li.active').html()
      $('aside > ul > li.active').find('ul').slideToggle(700)
      $('aside > ul > li.active').toggleClass('active')
    }
    if (aa != bb) {
      truc.find('ul').slideToggle(700)
      truc.toggleClass('active')
    }
    e.stopPropagation()
    return false
  }
  backendInitpanelHideshowOnClick() {
    $(this).toggleClass('panel-hideshow-active')
    $(this).closest('h4').next().toggle(
      'blind',
      function() {
        $(this).toggleClass('panel-hideshow-content-active')
      }
    )
    $(this).closest('legend').next().toggle(
      'blind',
      function() {
        $(this).toggleClass('panel-hideshow-content-active')
      }
    )
    return false
  }
  backendInitModalOnShow(e) {
    this.TableFixed()
  }
  TestCheckbox() {
    $("input[type='checkbox']").each(
      function() {
        if ($(this).data('rel') != 'allselect' && $(this).attr('name') != 'selection[]') {
          $(this).bootstrapSwitch(
            {
              'size': 'small'
            }
          )
        }
      }
    )
  }
  NavTab() {
    $(".NavLang").on(
      'click',
      function(e) {
        $(this).closest('.nav-tabs').find('.NavSelect').html($(this).html())
      }
    )
  }
  constructor() {
    this.NavTab()
    this.TestCheckbox()
    $(".panel-noteinterne").find(".close").each(
      function() {
        $(this).on(
          "click",
          function() {
            $(this).closest(".panel").hide()
          }
        )
      }
    )
    this.MenuTop()
    this.NiceScroll()
    $('.navbar-asideopenclose').find('i').on("click", this.backendInitNavBarOnClick)
    $(window).on("resize", this.backendInitWindowOnResize.bind(this))
    this.TableFixed()
    $('.collapse').on('shown.bs.collapse', this.backendInitCollapseOnShown.bind(this))
    if ($('a[data-toggle="tab"]').length > 0) {
      $('a[data-toggle="tab"]').on('shown.bs.tab', this.backendInitDataToggleTabOnShow.bind(this))
    }
    $('.radio label').each(
      function() {
        $(this).removeClass('required')
      }
    )
    $('aside > ul > li').each(
      function() {
        if ($(this).find('ul').length) {
          $(this).find('> a').on("click", this.backendInitAsideOnClick)
        }
      }
    )
    $('.panel-hideshow').each(
      function() {
        $(this).on("click", this.backendInitpanelHideshowOnClick)
      }
    )
    $('.modal').on('shown.bs.modal', this.backendInitModalOnShow.bind(this))
  }
  TableFixed() {
    if ($('.table-fixed').length) {
      $('.table-fixed').each(
        function() {
          let fixedheader = $(this)
          fixedheader.find('tbody').height(fixedheader.attr('data-fixed-height'))
          fixedheader.find('tbody').find('tr:first-child').find('td').each(
            function(i, item) {
              fixedheader.find('thead').find('tr:first-child').find('th:nth-child(' + (i + 1) + ')').outerWidth($(item).outerWidth())
            }
          )
        }
      )
    }
  }
  async sendNoteinterne(url) {
    let response = await getXHRJson(url)
  }
  Noteinterne(lien) {
    this.sendNoteinterne($(lien).attr('href'))
    return false
  }
  NiceScroll() {
    $('aside').niceScroll(
      {
        styler: 'fb',
        cursorcolor: $('header > nav').css('background-color'),
        cursorwidth: '10',
        cursorborderradius: '0px',
        background: '#404040',
        spacebarenabled: false,
        cursorborder: ''
      }
    )
    $('html').niceScroll(
      {
        styler: 'fb',
        cursorcolor: $('header > nav').css('background-color'),
        cursorwidth: '12',
        cursorborderradius: '0px',
        background: '#404040',
        spacebarenabled: false,
        cursorborder: '',
        zindex: '1000'
      }
    )
  }
  MenuTopHover() {
    $('#MenuTop .dropdown').hover(
      function() {
        $('.dropdown-menu', this).stop(true, true).slideDown('fast')
        $(this).toggleClass('open')
      },
      function() {
        $('.dropdown-menu', this).stop(true, false).slideUp('fast')
        $(this).toggleClass('open')
      }
    )
  }
  backendMenuTopNavbarOnClick() {
    $('#MenuTop').find('ul.nav').slideToggle(
      'medium',
      function() {
        $(this).toggleClass('navbar-active')
        $(this).removeAttr('style')
      }
    )
  }
  WindowResize() {
    this.MenuTop()
  }

  MenuTop() {
    if (!$('#MenuTop').attr('data-size')) {
      let width = $('#MenuTop .nav').width() + 15
      if ($('aside.active').length == 1) {
        width += 200
      }
      if ($('.navbar-logo').length == 1) {
        width += $('.navbar-logo').width()
      }
      let taille = width
      $('#MenuTop').attr('data-size', taille)
      $('.navbar-navopenclose').on("click", this.backendMenuTopNavbarOnClick)
    // $('main').css('padding-top', $('header').height() + 'px')
    }
    // $('main').css('padding-top', $('header').height() + 'px')
    if (document.body.clientWidth < (parseInt($('#MenuTop').attr('data-size'))) || document.body.clientWidth < 1024) {
      $('aside').addClass('aside-mobile').removeClass('active')
      $('aside, header, main, footer').removeClass('active')
      $('header').addClass('header-mobile')
      $('#MenuTop .dropdown').each(
        function() {
          $(this).unbind('mouseenter mouseleave')
        }
      )
    } else {
      $('aside').removeClass('aside-mobile')
      $('header').removeClass('header-mobile')
      this.MenuTopHover()
    }
  }
}
export { Backend }
