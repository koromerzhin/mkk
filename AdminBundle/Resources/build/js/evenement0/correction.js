
import { getXHRJson } from '../../../../../SiteBundle/Resources/build/js/modules/xhr/getXHRJson'
import { postXHRJson } from '../../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Correction {
    mkkEvenementCorrectionInitBoutonCorrectionOnClick(e) {
        $("#PopupCorrection").modal("toggle")
        e.preventDefault()
    }
    mkkEvenementCorrectionInitPopupCorrectionOnClick(e) {
        this.Lancer()
        e.preventDefault()
    }
    constructor() {
        $("#BoutonCorrection").on("click", this.mkkEvenementCorrectionInitBoutonCorrectionOnClick)
        $("#PopupCorrection").find("a[data-rel='save']").on("click", this.mkkEvenementCorrectionInitPopupCorrectionOnClick.bind(this))
    }
    async Lancer() {
        let response = await getXHRJson($("#BoutonCorrection").attr('href'))
        document.location.reload()
    }
}

export { Correction }
