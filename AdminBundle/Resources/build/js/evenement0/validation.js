
import { getXHRJson } from '../../../../../SiteBundle/Resources/build/js/modules/xhr/getXHRJson'
import { postXHRJson } from '../../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Validation {
    constructor() {
        $("#BoutonValidation").on("click", this.mkkEvenementValidationInitBoutonValidationOnClick)
        $("#PopupValidation").find("a[data-rel='save']").on("click", this.mkkEvenementValidationInitPopupValidationRelSaveOnClick.bind(this))
    }
    mkkEvenementValidationInitBoutonValidationOnClick(e) {
        $("#PopupValidation").modal("toggle")
        e.preventDefault()
    }
    async sendSave() {
        let response = await getXHRJson($("#BoutonValidation").attr('href'))
        document.location.reload()
    }
    mkkEvenementValidationInitPopupValidationRelSaveOnClick(e) {
        this.sendSave()
        e.preventDefault()
    }
}

export { Validation }
