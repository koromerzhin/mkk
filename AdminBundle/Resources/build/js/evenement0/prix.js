
class Prix {
    constructor() {
        if ($("#TabPrix").length) {
            this.Principal()
        }
    }
    mkkEvenementPrixPrincipalInputFirstOnKeyup(event) {
        if (event.which == 13) {
            $(this).closest("tr").find("input:last").focus()
        }
    }

    mkkEvenementPrixPrincipalInputLastOnKeyup(event) {
        if (event.which == 13) {
            $(this).blur()
        }
    }
    Principal() {
        $("#TabPrix>tbody").find("tr").each(
            function () {
                $(this).find("input:first").each(
                    function () {
                        $(this).on('keyup', this.mkkEvenementPrixPrincipalInputFirstOnKeyup)
                    }
                )
                $(this).find("input:last").each(
                    function () {
                        $(this).on('keyup', this.mkkEvenementPrixPrincipalInputLastOnKeyup)
                    }
                )
            }
        )
    }
    Supprimer(prix) {
        $(prix).closest('tr').remove()
        this.Principal()
        return false
    }
    Ajouter() {
        tr        = $("#TabPrix>tbody").find("tr:last")
        td        = $(tr).find("td:first")
        continuer = 1
        $(td).find('input:last').each(
            function () {
                var valeur = $(this).val()
                if (!valeur) {
                    continuer = 0
                }
            }
        )
    if (continuer == 1) {
        html      = $("#TabPrix>tfoot").html()
        $("#TabPrix>tbody").append(html)
        selection = $("#TabPrix>tbody").find("span:last")
        this.Principal()
    }
    return false
    }
}

export { Prix }
