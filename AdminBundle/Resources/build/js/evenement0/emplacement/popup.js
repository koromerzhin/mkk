
class Popup {
    Ville() {
        $("#PopupAjoutEmplacementChoix").modal("toggle")
        $("#PopupAjoutEmplacementAdresse").show()
        $("#PopupAjoutEmplacementAdresse").modal("toggle")
        $("#PopupAjoutEmplacementAdresse").find("form").reset()
    }
    Etablissement() {
        $("#PopupAjoutEmplacementChoix").modal("toggle")
        $("#PopupAjoutEmplacementEtablissement").show()
        $("#PopupAjoutEmplacementEtablissement").modal("toggle")
    }
    Ouvrir() {
        $("#PopupAjoutEmplacementChoix").show()
        $("#PopupAjoutEmplacementChoix").modal("toggle")
    }
    Fermer() {
        $("#PopupAjoutEmplacementChoix").find("form").attr('dataid', 0)
        $("#PopupAjoutEmplacementChoix").modal("hide")
    }
}

export { Popup }
