

import { Popup } from './emplacement/popup'
import { FormEnvoyer } from '../../../../../SiteBundle/Resources/build/js/modules/form/envoyer'
import { FormVerifier } from '../../../../../SiteBundle/Resources/build/js/modules/form/verifier'
import { getXHRJson } from '../../../../../SiteBundle/Resources/build/js/modules/xhr/getXHRJson'
import { postXHRJson } from '../../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Emplacement {
    mkkEvenementEmplacementInitPopupModifDateRelSaveOnClick(e) {
        FormEnvoyer("#FormulaireModifierDate")
        e.preventDefault()
    }
    mkkEvenementEmplacementInitPopupModifLieuRelSaveOnClick(e) {
        FormEnvoyer("#formmodifLieu")
        e.preventDefault()
    }
    mkkEvenementEmplacementInitPopupModifAdresseRelSaveOnClick(e) {
        FormEnvoyer("#FormulaireAdresseModif")
        e.preventDefault()
    }
    mkkEvenementEmplacementInitPopupAjoutEmplacementEtablissementRelSaveOnClick(e) {
        FormEnvoyer("#FormulaireEtablissementAdd")
        e.preventDefault()
    }
    mkkEvenementEmplacementInitPopupAjoutEmplacementAdresseRelSaveOnClick(e) {
        FormEnvoyer("#FormulaireAdresseAdd")
        e.preventDefault()
    }
    mkkEvenementEmplacementInitPopupAjoutEmplacementChoixRelVilleOnClick(e) {
        Evenement.Emplacement.Popup.Ville()
        e.preventDefault()
    }
    mkkEvenementEmplacementInitPopupAjoutEmplacementChoixRelEtablissementOnClick(e) {
        Evenement.Emplacement.Popup.Etablissement()
        e.preventDefault()
    }
    mkkEvenementEmplacementInitPopupAjoutEmplacementAdresseOnHide() {
        $("#PopupAjoutEmplacementAdresse").find("input,textarea").each(
            function () {
                $(this).val("")
            }
        )
    }
    mkkEvenementEmplacementInitPopupAjoutEmplacementAdresseOnShow() {
        $("#PopupAjoutEmplacementAdresse").find("select[data-rel='InputPays']").each(
            function () {
                let val = $(this).select2("val")
                $(this).select2("destroy").select2(
                    {
                        formatNoMatches: function () {
                            return ""
                        },
                        placeholder: $(this).attr('placeholder'),
                        width: 'element',
                        allowClear: true,
                        formatSelection: FormatSelect2Pays,
                        formatResult: FormatSelect2Pays
                    }
                )
                if (val != "") {
                    $(this).select2("val", val)
                }
            }
        )
    }
    constructor() {
        this.Popup = new Popup()
        if ($("#tabs-lieu").length) {
            $("#PopupModifDate").find("a[data-rel='save']").on("click", this.mkkEvenementEmplacementInitPopupModifDateRelSaveOnClick)
            $("#PopupModifLieu").find("a[data-rel='save']").on("click", this.mkkEvenementEmplacementInitPopupModifLieuRelSaveOnClick)
            $("#PopupModifAdresse").find("a[data-rel='save']").on("click", this.mkkEvenementEmplacementInitPopupModifAdresseRelSaveOnClick)
            $("#PopupAjoutEmplacementEtablissement").find("a[data-rel='save']").on("click", this.mkkEvenementEmplacementInitPopupAjoutEmplacementEtablissementRelSaveOnClick)
            $("#PopupAjoutEmplacementAdresse").find("a[data-rel='save']").on("click", this.mkkEvenementEmplacementInitPopupAjoutEmplacementAdresseRelSaveOnClick)
            $("#PopupAjoutEmplacementChoix").find("a[data-rel='ville']").on("click", this.mkkEvenementEmplacementInitPopupAjoutEmplacementChoixRelVilleOnClick)
            $("#PopupAjoutEmplacementChoix").find("a[data-rel='etablissement']").on("click", this.mkkEvenementEmplacementInitPopupAjoutEmplacementChoixRelEtablissementOnClick)
            $("#PopupAjoutEmplacementAdresse").on("hide", this.mkkEvenementEmplacementInitPopupAjoutEmplacementAdresseOnHide)
            $("#PopupAjoutEmplacementAdresse").on("show.bs.modal", this.mkkEvenementEmplacementInitPopupAjoutEmplacementAdresseOnShow)
        }
    }
    Data() {
        document.location.reload()
    }
    async sendModifier(url) {
        let response = getXHRJson(url)
        if (response.url != undefined) {
            $("#FormulaireAdresseModif").attr('action', response.url)
        }
        for (let a in response.adresse) {
            if ($("#FormulaireAdresseModif").find("#adresse_" + a).length) {
                if (a != "pays") {
                    $("#FormulaireAdresseModif").find("#adresse_" + a).val(response.adresse[a])
                } else {
                    $("#FormulaireAdresseModif").find("#adresse_" + a).select2("val", response.adresse[a])
                }
            }
        }
        $("#PopupModifAdresse").find('form').attr('dataid', id)
        $("#PopupModifAdresse").modal("toggle")
    }
    Modifier(lien) {
        url = $(lien).attr('href')
        id  = $(lien).closest("tr").attr('data-rel')
        $("#PopupAjoutEmplacementChoix").find("form").attr('dataid', id)
        this.sendModifier()
        return false
    }
    Ajouter() {
        this.Popup.Ouvrir()
        return false
    }
    Save() {
        if ($("#LieuRadio2").is(":checked")) {
            FormEnvoyer("#choice2")
        } else if ($("#LieuRadio3").is(":checked")) {
            FormEnvoyer("#choice3")
        }
    }
    async sendAdresseEnregistrer() {
        let datapost = Scripts.Form.InitDataPost(
            $("#PopupAjoutEmplacementAdresse").find("form"),
            {
                'action': 'add',
                'type': 'adresse'
            }
        )
    let response     = await postXHRJson(
            $("#PopupAjoutEmplacementAdresse").find("form").attr('action'),
            datapost
        )
    Evenement.Emplacement.Popup.Fermer()
    document.location.reload()
    }
    AdresseEnregistrer() {
        if (FormVerifier($("#PopupAjoutEmplacementAdresse").find("form"))) {
            this.sendAdresseEnregistrer()
        }
        return false
    }
    async sendLieuEnregistrerAdd() {
        let datapost = $("#PopupAjoutEmplacementEtablissement").find("form").serializeForm()
        let response = await postXHRJson(
            $("#PopupAjoutEmplacementEtablissement").find("form").attr('action'),
            datapost
        )
    thisPopup.Fermer()
    document.location.reload()
    }
    LieuEnregistrerAdd() {
        if (FormVerifier($("#PopupAjoutEmplacementEtablissement").find("form"))) {
            if ($("#LieuListing").val() == "") {
                let url           = $("#FormulaireEtablissementAdd").attr('data-urletablissement')
                document.location = url
            } else {
                this.sendLieuEnregistrerAdd()
            }
        }
        return false
    }
    async sendAdresseModif() {
        let datapost = $("#FormulaireAdresseModif").serializeForm()
        let response = await postXHRJson(
            $("#FormulaireAdresseModif").attr('action'),
            datapost
        )
    $("#PopupModifAdresse").modal("toggle")
    this.Data()
    }
    AdresseModif() {
        if (FormVerifier($("#FormulaireAdresseModif"))) {
            this.sendAdresseModif()
        }
        return false
    }
}

export { Emplacement }
