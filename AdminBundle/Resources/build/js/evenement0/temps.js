

import { FormVerifier } from '../../../../../SiteBundle/Resources/build/js/modules/form/verifier'
import { postXHRJson } from '../../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Temps {
    mkkEvenementDateInitOnShow() {
        let emplacement = $("#lieu_idemplacement")
        let options     = $(emplacement).find("option")
        if ($(options).length == 1) {
            $(emplacement).closest(".input-group").hide()
        } else {
            $(emplacement).closest(".input-group").show()
        }
    }
    mkkEvenementDateInitPopupModifDateRelSuppOnClick(e) {
        this.Supprimer()
        e.preventDefault()
    }
    mkkEvenementDateInitFullCalendareventDrop(event) {
        this.eventDrop(event)
    }
    mkkEvenementDateInitFullCalendareventDropFullCalendareventResize(event) {
        this.eventResize(event)
    }
    mkkEvenementDateInitFullCalendareventClick(event) {
        this.Modif(event.dateid)
    }
    mkkEvenementDateInitFullCalendarselect(start, end) {
        this.select(start, end)
    }
    mkkEvenementDateInitFcButtonOnClick() {
        if (!$(this).hasClass("fc-agendaWeek-button") && !$(this).hasClass("fc-agendaDay-button") && !$(this).hasClass("fc-today-button")) {
            $('#calendar').fullCalendar('removeEvents')
        }
    }
    constructor() {
        $("#PopupModifDate").on("show.bs.modal", this.mkkEvenementDateInitOnShow)
        $("#PopupModifDate").find("a[data-rel='suppr']").on("click", this.mkkEvenementDateInitPopupModifDateRelSuppOnClick.bind(this))
        if ($("#calendar").length) {
            $('#calendar').fullCalendar(
                {
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'agendaWeek,agendaDay,month'
                    },
                    theme: true,
                    lang: 'fr',
                    allDaySlot: false,
                    defaultDate: moment(),
                    height: 500,
                    defaultView: 'agendaWeek',
                    selectable: true,
                    editable: true,
                    resizable: true,
                    selectHelper: true,
                    dragRevertDuration: 0,
                    events: $("#calendar").attr('data-url'),
                    eventDrop: this.mkkEvenementDateInitFullCalendareventDrop.bind(this),
                    eventResize: this.mkkEvenementDateInitFullCalendareventDropFullCalendareventResize.bind(this),
                    eventClick: this.mkkEvenementDateInitFullCalendareventClick.bind(this),
                    select: this.mkkEvenementDateInitFullCalendarselect.bind(this)
                }
            )
            $('.fc-button').on('click', this.mkkEvenementDateInitFcButtonOnClick)
        }
    }
    async select(start, end) {
        var eventData
        let datapost = {
            'eventid': $("#InputEmplacement").val(),
            'start': start.format('DD/MM/YYYY HH:mm'),
            'end': end.format('DD/MM/YYYY HH:mm')
        }
        let response = await postXHRJson(
            $("#calendar").attr('data-urladd'),
            datapost
        )
    $('#calendar').fullCalendar('unselect')
    if (response.id != undefined) {
        this.Modif(response.id)
    }
    $("#calendar").fullCalendar("refetchEvents")
    }
    async eventResize(event) {
        let datapost = {
            'id': event.dateid,
            'start': event.start.format('DD/MM/YYYY HH:mm'),
            'end': event.end.format('DD/MM/YYYY HH:mm')
        }
        let response = await postXHRJson(
            $("#calendar").attr('data-urlupdate'),
            datapost
        )
    if (response.id != undefined) {
        this.Modif(response.id)
    }
    $("#calendar").fullCalendar("refetchEvents")
    }
    async EventDrop(event) {
        let datapost = {
            'id': event.dateid,
            'start': event.start.format('DD/MM/YYYY HH:mm'),
            'end': event.end.format('DD/MM/YYYY HH:mm')
        }
        let response = await postXHRJson(
            $("#calendar").attr('data-urlupdate'),
            datapost
        )
    if (response.id != undefined) {
        this.Modif(response.id)
    }
    $("#calendar").fullCalendar("refetchEvents")
    }
    async Modif(id) {
        $("#PopupModifDate").attr('data-id', id)
        let datapost = {
            'id': id
        }
        let response = await postXHRJson(
            $("#calendar").attr('data-urlinfo'),
            datapost
        )
    placeillimite    = 0
    if (response.placeillimite != undefined) {
        if (response.placeillimite == true) {
            placeillimite = 1
        }
    }

    $("#lieu_placeillimite").select2("val", placeillimite)
    $("#lieu_idemplacement").select2("val", response.emplacement)
    $("#lieu_debut").val(response.debut).datetimepicker(
        {
            lang: 'fr',
            format: 'd/m/Y H:i'
          }
    )
    $("#lieu_fin").val(response.fin).datetimepicker(
        {
            lang: 'fr',
            format: 'd/m/Y H:i'
          }
    )
    $("#FormulaireModifierDate").attr('data-id', response.id)
    $("#lieu_place").val(0)
    if (response.place != undefined) {
        $("#lieu_place").val(response.place)
    }

    $("#PopupModifDate").modal("toggle")
    }
    async Supprimer() {
        let datapost = {
            'id': $("#PopupModifDate").attr('data-id')
        }
        let response = await postXHRJson(
            $("#calendar").attr('data-urldelete'),
            datapost
        )
    $("#PopupModifDate").modal("toggle")
    $("#calendar").fullCalendar("refetchEvents")
    }
    async sendSave() {
        let datapost = Scripts.Form.InitDataPost(
            $("#FormulaireModifierDate"),
            {
                'id': $("#FormulaireModifierDate").attr('data-id')
            }
        )
    let response     = await postXHRJson(
            $("#FormulaireModifierDate").attr('action'),
            datapost
        )
    $("#calendar").fullCalendar("refetchEvents")
    $("#PopupModifDate").modal("toggle")
    }
    Save() {
        if (FormVerifier($("#FormulaireModifierDate"))) {
            this.sendSave()
        }
        return false
    }
}
export { Temps }
