import { FormVerifier } from '../../../SiteBundle/Resources/build/js/modules/form/verifier'
import { FormEnvoyer } from '../../../SiteBundle/Resources/build/js/modules/form/envoyer'
import { MkkSite } from '../../../SiteBundle/Resources/build/site'
import { Backend } from './js/default'
export class MkkAdmin extends MkkSite {
    fix() {
      // Remove overflow from .wrapper if layout-boxed exists
      $(".layout-boxed > .wrapper").css('overflow', 'hidden')
      //Get window height and the wrapper height
      var footer_height = $('.main-footer').outerHeight() || 0
      var neg = $('.main-header').outerHeight() + footer_height
      var window_height = $(window).height()
      var sidebar_height = $(".sidebar").height() || 0
      //Set the min-height of the content and sidebar based on the
      //the height of the document.
      if ($("body").hasClass("fixed")) {
        $(".content-wrapper, .right-side").css('min-height', window_height - footer_height)
      } else {
        var postSetWidth
        if (window_height >= sidebar_height) {
          $(".content-wrapper, .right-side").css('min-height', window_height - neg)
          postSetWidth = window_height - neg
        } else {
          $(".content-wrapper, .right-side").css('min-height', sidebar_height)
          postSetWidth = sidebar_height
        }

        //Fix for the control sidebar height
        var controlSidebar = $(".control-sidebar")
        if (typeof controlSidebar !== "undefined") {
          if (controlSidebar.height() > postSetWidth)
            $(".content-wrapper, .right-side").css('min-height', controlSidebar.height())
        }
      }
    }
    tree(menu) {
    let _this = this
    let animationSpeed = 500
    $(document).off('click', menu + ' li a')
      .on('click', menu + ' li a', function(e) {
        //Get the clicked link and the next element
        let $this = $(this)
        let checkElement = $this.next()

        //Check if the next element is a menu and is visible
        if ((checkElement.is('.treeview-menu')) && (checkElement.is(':visible')) && (!$('body').hasClass('sidebar-collapse'))) {
          //Close the menu
          checkElement.slideUp(animationSpeed, function() {
            checkElement.removeClass('menu-open')
          //Fix the layout in case the sidebar stretches over the height of the window
          //_this.layout.fix();
          });
          checkElement.parent("li").removeClass("active")
        }
        //If the menu is not visible
        else if ((checkElement.is('.treeview-menu')) && (!checkElement.is(':visible'))) {
          //Get the parent menu
          let parent = $this.parents('ul').first()
          //Close all open menus within the parent
          let ul = parent.find('ul:visible').slideUp(animationSpeed)
          //Remove the menu-open class from the parent
          ul.removeClass('menu-open')
          //Get the parent li
          let parent_li = $this.parent("li")

          //Open the target menu and add the menu-open class
          checkElement.slideDown(animationSpeed, function() {
            //Add the class active to the parent li
            checkElement.addClass('menu-open')
            parent.find('li.active').removeClass('active')
            parent_li.addClass('active')
            //Fix the layout in case the sidebar stretches over the height of the window
            _this.fix()
          })
        }
        //if this isn't a link, prevent the page from being redirected
        if (checkElement.is('.treeview-menu')) {
          e.preventDefault()
        }
      })
  }
    activate(toggleBtn) {
      //Get the screen sizes
      let screenSizes = {
        xs: 480,
        sm: 768,
        md: 992,
        lg: 1200
      }

      //Enable sidebar toggle
      $(document).on('click', toggleBtn, function (e) {
        e.preventDefault()

        //Enable sidebar push menu
        if ($(window).width() > (screenSizes.sm - 1)) {
          if ($("body").hasClass('sidebar-collapse')) {
            $("body").removeClass('sidebar-collapse').trigger('expanded.pushMenu')
          } else {
            $("body").addClass('sidebar-collapse').trigger('collapsed.pushMenu')
          }
        }
        //Handle sidebar push menu for small screens
        else {
          if ($("body").hasClass('sidebar-open')) {
            $("body").removeClass('sidebar-open').removeClass('sidebar-collapse').trigger('collapsed.pushMenu')
          } else {
            $("body").addClass('sidebar-open').trigger('expanded.pushMenu')
          }
        }
      })

      $(".main-admin").click(function () {
        //Enable hide menu when clicking on the content-wrapper on small screens
        if ($(window).width() <= (screenSizes.sm - 1) && $("body").hasClass("sidebar-open")) {
          $("body").removeClass('sidebar-open')
        }
      })
    }
    BoutonSaveClick(e){
        e.preventDefault()
        let formID = $(e.currentTarget).attr('data-submit')
        FormEnvoyer('#'+formID)
    }
  constructor() {
    super()
    this.tree('.sidebar')
    this.activate("[data-toggle='offcanvas']")
    $("#BoutonSave").on("click", this.BoutonSaveClick.bind(this))
    console.log("mkk admin")
    let module = this.getModule()
    let route = this.getRoute()
    window.Backend = new Backend()
    if (route.indexOf("/categorie/") > -1) {
        import(/* webpackChunkName: "categorie-mkkadmin" */ './js/categorie').then(im => {
            window.Categorie = im.default()
        })
    } else {
      switch (module) {
        case "begroup":
            import(/* webpackChunkName: "group-mkkadmin" */ './js/group').then(im => {
                window.Group = im.default()
            })
          break
        case "bebookmark":
            import(/* webpackChunkName: "bookmark-mkkadmin" */ './js/bookmark').then(im => {
                window.Bookmark = im.default()
            })
          break
        case "beblog":
            import(/* webpackChunkName: "blog-mkkadmin" */ './js/blog').then(im => {
                window.Blog = im.default()
            })
          break
        case "bebug":
            import(/* webpackChunkName: "bugs-mkkadmin" */ './js/bugs').then(im => {
                window.Bugs = im.default()
            })
          break
        case "becontact":
            import(/* webpackChunkName: "contact-mkkadmin" */ './js/contact').then(im => {
                window.Contact = im.default()
            })
          break
        case "bediaporama":
            import(/* webpackChunkName: "diaporama-mkkadmin" */ './js/diaporama').then(im => {
                window.Diaporama = im.default()
            })
          break
        case "bedroit":
            import(/* webpackChunkName: "droit-mkkadmin" */ './js/droit').then(im => {
                window.Droit = im.default()
            })
          break
        case "beedito":
            import(/* webpackChunkName: "edito-mkkadmin" */ './js/edito').then(im => {
                window.Edito = im.default()
            })
          break
        case "beenseigne":
            import(/* webpackChunkName: "enseigne-mkkadmin" */ './js/etablissement').then(im => {
                window.Etablissement = im.default()
            })
          break
        case "beetablissement":
            import(/* webpackChunkName: "etablissement-mkkadmin" */ './js/etablissement').then(im => {
                window.Etablissement = im.default()
            })
          break
        case "beevenement":
            import(/* webpackChunkName: "evenement-mkkadmin" */ './js/evenement').then(im => {
                window.Evenement = im.default()
            })
          break
        case "belieu":
            import(/* webpackChunkName: "lieu-mkkadmin" */ './js/etablissement').then(im => {
                window.Lieu = im.default()
            })
          break
        case "bemenu":
            import(/* webpackChunkName: "etablissement-mkkadmin" */ './js/menu').then(im => {
                window.Menu = im.default()
            })
          break
        case "bemetariane":
            import(/* webpackChunkName: "metariane-mkkadmin" */ './js/metariane').then(im => {
                window.Metariane = im.default()
            })
          break
        case "bemoncompte":
            import(/* webpackChunkName: "moncompte-mkkadmin" */ './js/moncompte').then(im => {
                window.Moncompte = im.default()
            })
          break
        case "benoteinterne":
            import(/* webpackChunkName: "noteinterne-mkkadmin" */ './js/noteinterne').then(im => {
                window.Noteinterne = im.default()
            })
          break
        case "bepage":
            import(/* webpackChunkName: "page-mkkadmin" */ './js/page').then(im => {
                window.Page = im.default()
            })
          break
        case "beparam":
            import(/* webpackChunkName: "param-mkkadmin" */ './js/param').then(im => {
                window.Param = im.default()
            })
          break
        case "beslide":
          if (route.indexOf("/slide/") > -1) {
              import(/* webpackChunkName: "slide-mkkadmin" */ './js/revolutionslider/slide').then(im => {
                  window.Slide = im.default()
              })
          } else {
              import(/* webpackChunkName: "slider-mkkadmin" */ './js/revolutionslider/slider').then(im => {
                  window.Slider = im.default()
              })
          }
          break
        case "besystem":
            import(/* webpackChunkName: "system-mkkadmin" */ './js/system').then(im => {
                window.System = im.default()
            })
          break
        case "betag":
            import(/* webpackChunkName: "tags-mkkadmin" */ './js/tags').then(im => {
                window.Tags = im.default()
            })
          break
        case "betemplates":
            import(/* webpackChunkName: "templates-mkkadmin" */ './js/templates').then(im => {
                window.Templates = im.default()
            })
          break
        case "beuser":
            import(/* webpackChunkName: "user-mkkadmin" */ './js/user').then(im => {
                window.User = im.default()
            })
          break
      }
    }
  }
}
