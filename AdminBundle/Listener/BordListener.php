<?php

namespace Mkk\AdminBundle\Listener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class BordListener
{
    public function __construct(ContainerInterface $container, Session $session)
    {
        $this->session   = $session;
        $this->container = $container;
        $this->router    = $container->get('router');
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $route = $event->getRequest()->attributes->get('_route');
        if ($route == 'bord_index') {
            $groupManager    = $this->container->get('bdd.group_manager');
            $groupRepository = $groupManager->getRepository();
            $token           = $this->container->get('security.token_storage')->getToken();
            $visiteur        = $groupRepository->findoneby(['code' => 'visiteur']);
            if ($token == null) {
                $idgroup = $visiteur->getId();
            } else {
                $user = $token->getUser();
                if (is_string($user)) {
                    $idgroup = $visiteur->getId();
                } else {
                    $idgroup = $user->getRefGroup()->getId();
                }
            }

            $param = $this->container->get('mkk.param_service')->listing();
            if (isset($param['groupbord_' . $idgroup . '_accueil'])) {
                $newroute = $param['groupbord_' . $idgroup . '_accueil'];
                if ($newroute != $route) {
                    $event->setResponse(new RedirectResponse($this->router->generate($newroute)));
                }
            }
        }
    }
}
