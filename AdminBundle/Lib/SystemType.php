<?php

namespace Mkk\AdminBundle\Lib;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class SystemType extends AbstractTypeLib
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'dataroutes'      => [],
                'params'          => [],
                'csrf_protection' => false,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'system';
    }
}
