<?php

namespace Mkk\AdminBundle\Lib;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class ParamType extends AbstractTypeLib
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'      => null,
                'routes'          => [],
                'groups'          => [],
                'totallisting'    => [],
                'crontab'         => '',
                'robotstxt'       => '',
                'dataroutes'      => [],
                'params'          => [],
                'csrf_protection' => false,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'param';
    }
}
