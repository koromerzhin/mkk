<?php

namespace Mkk\AdminBundle\Lib;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class SearchContactType extends AbstractTypeLib
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'group_contacts'  => [],
                'csrf_protection' => false,
            ]
        );
    }
}
