<?php

namespace Mkk\AdminBundle\Controller;

use Mkk\AdminBundle\Controller\System\JsonTrait;
use Mkk\AdminBundle\Controller\System\SearchTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

trait SystemTrait
{
    use JsonTrait;
    use SearchTrait;

    /**
     * @Route("/system/", name="besystem_index")
     *
     * @param Request $request pile de requête
     *
     * @return html
     */
    public function systemIndexAction(Request $request)
    {
        $paramManager        = $this->get('bdd.param_manager');
        $metarianeManager    = $this->get('bdd.metariane_manager');
        $metarianeRepository = $metarianeManager->getRepository();
        $route               = $this->getRoutes();

        $data       = $this->get('mkk.param_service')->listing();
        $dataroutes = $metarianeRepository->getMetarianeRemplit();

        $actions = [
            ['type' => 'save', 'url' => 'besystem_index'],
        ];
        $this->get('mkk.action_service')->set($actions);
        $siteService = $this->get('mkk.site_service');
        $options     = [
            'params'     => $data,
            'dataroutes' => $dataroutes,
        ];

        $form = $siteService->getFormSystemParam('system', $data, $options);
        if ($siteService->isRedirect()) {
            $url      = $this->generateUrl('bord_index');
            $redirect = $this->redirect($url, 301);

            return $redirect;
        }

        $tmpl = $siteService->getHtmlForm('system', $form);

        $methods = get_class_methods($this);
        if (in_array('setSystem', $methods)) {
            $this->setSystem($request);
        }

        $twigHtml = $this->renderMkk(
            $request,
            'MkkAdminBundle:System:index.html.twig',
            [
                'routes' => $route,
                'form'   => $siteService->createView($form),
                'tmpl'   => $tmpl,
            ]
        );

        return $twigHtml;
    }
}
