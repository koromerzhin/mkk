<?php

namespace Mkk\AdminBundle\Controller\Etablissement;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

trait SearchTrait
{
    /**
     * @Route("/etablissement/search/group", name="beetablissement_searchgroup")
     */
    public function etablissementSearchGroupAction()
    {
        $response = $this->get('mkk.search_service')->getResponse('bdd.group_manager', 'searchGroupConnect');
        $json     = new JsonResponse($response);

        return $json;
    }

    /**
     * @Route("/etablissements/search/responsable", name="beetablissement_searchresponsable")
     *
     * Responsable
     *
     * @param Request $request Pile de requètes
     *
     * @return JsonResponse
     */
    public function etablissementSearchResponsableAction()
    {
        $response = $this->get('mkk.search_service')->getResponse('bdd.user_manager', 'searchUserConnect');
        $json     = new JsonResponse($response);

        return $json;
    }

    /**
     * @Route("/etablissement/search/nafsousclasse", name="beetablissement_searchnafsousclasse")
     */
    public function etablissementSearchNafSousClasseAction()
    {
        $response = $this->get('mkk.search_service')->getResponse('bdd.nafsousclasse_manager', 'searchNafSousClasse');
        $json     = new JsonResponse($response);

        return $json;
    }
}
