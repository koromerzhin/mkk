<?php

namespace Mkk\AdminBundle\Controller\Etablissement;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

trait JsonTrait
{
    /**
     * @Route("/etablissements/json/actif", name="beetablissement_json-actif")
     *
     * Rend actif un établissement
     *
     * @return JsonResponse
     */
    public function actifEtablissementAction(Request $request)
    {
        $retour = $this->actionActivate($request, 'Etablissement', 'id', 'setActif');

        return $retour;
    }

    /**
     * @Route("/etablissements/json/position", name="beetablissement_json-position")
     *
     * Position des etablissements suivant les types
     *
     * @param Request $request Pile de requêtes
     *
     * @return html
     */
    public function positionEtablissementAction(Request $request)
    {
        $response                = [];
        $etablissementManager    = $this->get('bdd.etablissement_manager');
        $etablissementRepository = $etablissementManager->getRepository();
        $params                  = $this->get('mkk.param_service')->listing();
        if (! $this->isPostAjax($request)) {
            $jsonResponse = new JsonResponse($response);

            return $jsonResponse;
        }

        if (isset($params['type_etablissement'])) {
            $types = $params['type_etablissement'];
            foreach ($types as $data) {
                $code = $data['code'];
                $post = explode(',', $request->request->get($code));
                foreach ($post as $position => $id) {
                    $etablissement = $etablissementRepository->find($id);
                    if ($etablissement) {
                        $etablissement->setPosition($position);
                        $etablissementManager->persist($etablissement);
                    }
                }
            }

            $etablissementManager->flush();
        }

        $jsonResponse = new JsonResponse($response);

        return $jsonResponse;
    }

    /**
     * @Route("/etablissements/json/user", name="beetablissement_json-searchuser")
     *
     * Description of what this does.
     *
     * @param Request $request Pile de requètes
     *
     * @return JsonResponse
     */
    public function userEtablissementAction(Request $request)
    {
        $userManager    = $this->get('bdd.user_manager');
        $userRepository = $userManager->getRepository();
        $response       = [];
        $container      = $this->get('service_container');
        $user           = $container->get('security.token_storage')->getToken()->getUser();
        $search         = $request->request->get('searchnewuser');
        $page           = $request->request->get('page');
        $url            = $request->request->get('url');
        $post           = $request->request->all();
        $tabgroups      = [];
        if (isset($param['group_utilisateurs'])) {
            $tabgroups = $param['group_utilisateurs'];
        }

        if (isset($post['id'])) {
            $user = $userRepository->find($post['id']);
            if ($user) {
                $response = [
                    'id'  => $user->getId(),
                    'nom' => $user->getAppel(),
                ];
            }
        } else {
            $groups                 = [];
            $data                   = $userRepository->searchUser($groups, $search, $page, 10);
            $response['pagination'] = $data['pagination'];
            $response['user']       = [];
            foreach ($data['user'] as $user) {
                $adresse = '';
                if (count($user[0]->getAdresses()) != 0) {
                    $adresses = $user[0]->getAdresses();
                    $adresse  = $adresses[0]->getInfo() . ' ' . $adresses[0]->getCp() . ' ' . $adresses[0]->getVille();
                }

                if ($url == '') {
                    $url = 'beuser_form';
                }

                $adresseurl         = $this->generateUrl($url, ['id' => $user[0]->getId()]);
                $response['user'][] = [
                    'id'      => $user[0]->getId(),
                    'appel'   => $user[0]->getAppel(),
                    'groupe'  => $user[0]->getRefGroup()->getNom(),
                    'societe' => $user[0]->getSociete(),
                    'url'     => $adresseurl,
                    'adresse' => $adresse,
                ];
            }
        }

        $jsonResponse = new JsonResponse($response);

        return $jsonResponse;
    }

    /**
     * @Route("/etablissements/json/search/{etablissement}", name="beetablissement_json-search")
     *
     * Description of what this does.
     *
     * @param Request       $request       Piles de requètes
     * @param Etablissement $etablissement Etablissement
     *
     * @return JsonResponse
     */
    public function searchEtablissementAction(Request $request, $etablissement)
    {
        $userManager    = $this->get('bdd.user_manager');
        $userRepository = $userManager->getRepository();
        $response       = [];
        $lettre         = $request->query->get('lettre');
        $page           = $request->query->get('page');
        $identity       = $request->request->get('id');
        if ($identity == '') {
            $response['users'] = [];
            $groups            = ['superadmin'];
            $data              = $userRepository->getsearchUser($etablissement, $groups, $lettre, $page, 10);
            foreach ($data['user'] as $key => $user) {
                $response['users'][$key] = ['id' => $user[0]->getId(), 'nom' => $user[0]->getAppel()];
            }

            $response['total'] = $data['pagination']['max'];
        } else {
            $user = $userRepository->find($identity);
            if ($user) {
                $response = [
                    'id'  => $user->getId(),
                    'nom' => $user->getAppel(),
                ];
            }
        }

        $jsonResponse = new JsonResponse($response);

        return $jsonResponse;
    }
}
