<?php

namespace Mkk\AdminBundle\Controller\Etablissement;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

trait UploadTrait
{
    /**
     * @Route("/etablissements/vignette/{md5}", name="beetablissement_uploadvignette")
     *
     * Description of what this does.
     *
     * @param Hash $md5 Hash de la vignette
     *
     * @return Response
     */
    public function vignetteEtablissementAction()
    {
        $options = [
            'max_number_of_files' => 1,
        ];

        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }

    /**
     * @Route("/etablissements/vuesexterne/{md5}", name="beetablissement_uploadvuesexterne")
     *
     * Description of what this does.
     *
     * @param Hash $md5 Hash de la vignette
     *
     * @return Response
     */
    public function vuesexterneEtablissementAction()
    {
        $options  = [];
        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }

    /**
     * @Route("/etablissements/vuesinterne/{md5}", name="beetablissement_uploadvuesinterne")
     *
     * Description of what this does.
     *
     * @param Hash $md5 Hash de la vignette
     *
     * @return Response
     */
    public function vuesinterneEtablissementAction()
    {
        $options  = [];
        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }

    /**
     * @Route("/etablissements/galerie/{md5}", name="beetablissement_uploadgalerie")
     *
     * Description of what this does.
     *
     * @param Hash $md5 Hash de la vignette
     *
     * @return Response
     */
    public function galerieEtablissementAction()
    {
        $options  = [];
        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }

    /**
     * @Route("/etablissements/pdf/{md5}", name="beetablissement_uploadpdf")
     *
     * Affiche le pdf des etablissements
     *
     * @param Hash $md5 Hash du PDF
     *
     * @return Response
     */
    public function pdfEtablissementAction()
    {
        $options = [
            'accept_file_types' => 'pdf',
        ];

        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }

    /**
     * @Route("/etablissements/vuesequipe/{md5}", name="beetablissement_uploadvuesequipe")
     *
     * Description of what this does.
     *
     * @param Hash $md5 Hash de la vignette
     *
     * @return Response
     */
    public function vuesequipeEtablissementAction()
    {
        $options  = [];
        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }
}
