<?php

namespace Mkk\AdminBundle\Controller\Moncompte;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

trait UploadTrait
{
    /**
     * @Route("/moncompte/upload/avatar/{md5}", name="bemoncompte_uploadavatar")
     * Gestion de l'avatar de l'utilisateur actuel
     *
     * @param Request $request Pile de requêtes
     * @param string  $md5     code unique pour l'upload
     *
     * @return Response
     */
    public function avatarAction()
    {
        $options = [
            'max_number_of_files' => 1,
        ];

        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }
}
