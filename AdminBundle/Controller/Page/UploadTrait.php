<?php

namespace Mkk\AdminBundle\Controller\Page;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

trait UploadTrait
{
    /**
     * @Route("/image/{md5}", name="bepage_uploadimage")
     * @Route("/fondimage/{md5}", name="bepage_uploadfondimage")
     * @Route("/filigramme/{md5}", name="bepage_uploadfiligramme")
     *
     * @param
     * @param mixed $md5
     */
    public function uploadimageAction()
    {
        $options = [
            'max_number_of_files' => 1,
        ];

        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }

    /**
     * @Route("/video/{md5}", name="bepage_uploadvideo")
     *
     * @param
     * @param mixed $md5
     */
    public function uploadvideoAction()
    {
        $options  = [
            'max_number_of_files' => 1,
            'upload_video'        => 1,
        ];
        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }

    /**
     * @Route("/galerie/{md5}", name="bepage_uploadgalerie")
     *
     * @param
     * @param mixed $md5
     */
    public function uploadgalerieAction()
    {
        $options  = [];
        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }
}
