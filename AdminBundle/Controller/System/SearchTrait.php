<?php

namespace Mkk\AdminBundle\Controller\System;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

trait SearchTrait
{
    /**
     * @Route("/system/search/group", name="besystem_searchgroup")
     */
    public function systemSearchGroupAction()
    {
        $response = $this->get('mkk.search_service')->getResponse('bdd.group_manager', 'searchGroupContact');
        $json     = new JsonResponse($response);

        return $json;
    }
}
