<?php

namespace Mkk\AdminBundle\Controller\Noteinterne;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

trait SearchTrait
{
    /**
     * @Route("/noteinterne/search/user", name="benoteinterne_searchuser")
     *
     * Liste des utilisateurs pour le moteur de recherche des notes internes
     *
     * @param Request $request Pile de requêtes
     *
     * @return JsonResponse
     */
    public function noteInterneSearchUserAction()
    {
        $response = $this->get('mkk.search_service')->getResponse('bdd.user_manager', 'searchUserConnect');
        $json     = new JsonResponse($response);

        return $json;
    }

    /**
     * @Route("/noteinterne/search/group", name="benoteinterne_searchgroup")
     *
     * Liste des groupes pour le moteur de recherche des notes internes
     *
     * @param Request $request Pile de requêtes
     *
     * @return JsonResponse
     */
    public function noteInterneSearchGroupAction()
    {
        $response = $this->get('mkk.search_service')->getResponse('bdd.group_manager', 'searchGroupConnect');
        $json     = new JsonResponse($response);

        return $json;
    }

    /**
     * @Route("/noteinterne/search/etablissement", name="benoteinterne_searchetablissement")
     *
     * Liste des établissements pour le moteur de recherche des notes internes
     *
     * @param Request $request Pile de requêtes
     *
     * @return JsonResponse
     */
    public function noteInterneSearchEtablissementAction()
    {
        $response = $this->get('mkk.search_service')->getResponse('bdd.etablissement_manager', 'searchEtablissement');
        $json     = new JsonResponse($response);

        return $json;
    }
}
