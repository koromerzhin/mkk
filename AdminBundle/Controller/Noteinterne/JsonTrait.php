<?php

namespace Mkk\AdminBundle\Controller\Noteinterne;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

trait JsonTrait
{
    /**
     * @Route("/noteinterne/json/lecture/{noteinterne}", name="benoteinterne_json-lectureindex", defaults={"page" = 0})
     * @Route("/noteinterne/json/lecture/{noteinterne}/{page}.html", name="benoteinterne_lecturepage")
     *
     * Page de des lectures des notes internes
     *
     * @param Request     $request     Pile de requêtes
     * @param Noteinterne $noteinterne Identifiant de la note interne
     * @param Page        $page        La page des lectures des notes internes
     *
     * @return JsonResponse
     */
    public function noteInterneLectureAction(Request $request, $noteinterne)
    {
        $listingService = $this->get('mkk.listing_service');
        $listingService->setRequest('bdd.noteinternelecture_manager', 'searchNoteInterneLecture');
        $json = [];
        if ($this->isPostAjax($request)) {
            $html = $this->get('templating')->render(
                'MkkAdminBundle:Noteinterne:lecture.html.twig'
            );
            $json = ['html' => $html];
            if ($this->get('paginator')->getPageCount() >= 2 && $this->get('paginator')->getPageCount() >= $this->get('paginator')->getPage()) {
                $next        = $this->get('paginator')->getPage() + 1;
                $route       = $this->get('paginator')->getRoute();
                $params      = $this->get('paginator')->getParams();
                $params      = array_merge($params, ['page' => $next]);
                $url         = $this->generateUrl($route, $params);
                $json['url'] = $url;
            }
        }

        $jsonResponse = new JsonResponse($json);

        return $jsonResponse;
    }

    /**
     * @Route("/noteinterne/json/confirm/{noteinternelecture}", name="benoteinterne_json-confirm", defaults={"id" = 0})
     *
     * Confirmation de lecture d'une note interne
     *
     * @param Noteinternelecture $noteinternelecture Identifiant de la lecture de la note interne
     *
     * @return JsonResponse
     */
    public function noteInterneConfirmAction($noteinternelecture)
    {
        $noteinternelectureManager    = $this->get('bdd.noteinternelecture_manager');
        $noteinternelectureRepository = $noteinternelectureManager->getRepository();
        $entity                       = $noteinternelectureRepository->find($noteinternelecture);
        $entity->setLecture(true);
        $entity->setDate(time());
        $noteinternelectureManager->persistAndFlush($entity);
        $responsejson['envoyer'] = 1;
        $jsonResponse            = new JsonResponse($responsejson);

        return $jsonResponse;
    }
}
