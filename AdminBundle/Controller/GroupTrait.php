<?php

namespace Mkk\AdminBundle\Controller;

use Mkk\AdminBundle\Controller\Group\ActionTrait as GroupActionTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

trait GroupTrait
{
    use GroupActionTrait;

    /**
     * @Route("/group/", name="begroup_index", defaults={"page": 0})
     * @Route("/group/{page}.html", name="begroup_page")
     */
    public function groupIndexAction(Request $request)
    {
        $listingService = $this->get('mkk.listing_service');
        $listingService->setRequest('bdd.group_manager', 'searchGroup');
        $groupManager = $this->get('bdd.group_manager');
        $groupTable   = $groupManager->getTable();
        $entity       = new $groupTable();
        $siteService  = $this->get('mkk.site_service');
        $form         = $siteService->getForm('mkk_admin.form.group', $groupManager, $entity, 'ajouter');
        if ($siteService->isRedirect()) {
            $url      = $this->generateUrl('begroup_index');
            $redirect = $this->redirect($url, 301);

            return $redirect;
        }

        $actions = [
            [
                'type' => 'add',
                'url'  => 'begroup_form',
            ],
            [
                'type'   => 'data',
                'entity' => 'group',
            ],
        ];

        $this->get('mkk.action_service')->set($actions);
        $twig = $this->renderMkk(
            $request,
            'MkkAdminBundle:Group:index.html.twig',
            [
                'form' => $siteService->createView($form),
            ]
        );

        return $twig;
    }

    /**
     * @Route("/group/form/{id}", name="begroup_form", defaults={"id": 0})
     *
     * @param mixed $id
     */
    public function groupFormAction(Request $request, $id)
    {
        $crudService = $this->get('mkk.crud_service');
        $crudService->setManager('bdd.group_manager');
        $crudService->setId($id);
        $crudService->setFormulaireID('FormGroup');
        $crudService->setUrl(
            [
                'listing' => 'begroup_index',
                'form'    => 'begroup_form',
            ]
        );
        $crudService->setController($this);
        $crudService->setFormService('mkk_admin.form.group');
        $crudService->setTwigHTML('MkkAdminBundle:Group:form.html.twig');
		$crudService->init();

        return $crudService->launch();
    }
}
