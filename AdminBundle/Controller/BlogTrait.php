<?php

namespace Mkk\AdminBundle\Controller;

use Mkk\AdminBundle\Controller\Blog\CategorieTrait;
use Mkk\AdminBundle\Controller\Blog\JsonTrait;
use Mkk\AdminBundle\Controller\Blog\SearchTrait;
use Mkk\AdminBundle\Controller\Blog\UploadTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Intl\Intl;

trait BlogTrait
{
    use CategorieTrait,
        JsonTrait,
        SearchTrait,
        UploadTrait;

    /**
     * @Route("/blog/", name="beblog_index", defaults={"page": 0})
     * @Route("/blog/{page}.html", name="beblog_page")
     *
     * Liste des articles du blog
     *
     * @param Request $request pile de requêtes
     * @param int     $page    numéro de la page
     *
     * @return Blog:index.html.twig
     */
    public function blogIndexAction(Request $request)
    {
        $listingService = $this->get('mkk.listing_service');
        $listingService->setRequest('bdd.blog_manager', 'searchBlog');
        $categorieManager    = $this->get('bdd.categorie_manager');
        $categorieRepository = $categorieManager->getRepository();
        $search              = ['type' => 'blog'];
        $categories          = $categorieRepository->findby($search);
        $categories          = count($categories);
        $languedispo         = [];
        $locale              = $request->getLocale();
        $language            = Intl::getLanguageBundle()->getLanguageNames($locale);

        $params = $this->get('mkk.param_service')->listing();
        if (! isset($params['languesite'])) {
            $flashbag = $request->getSession()->getFlashBag();
            $flashbag->add('warning', 'Langue du site non configuré');
            $url      = $this->generateUrl('bord_index');
            $redirect = $this->redirect($url, 301);

            return $redirect;
        }

        $bloglangueDispo = $params['languesite'];
        foreach ($bloglangueDispo as $code) {
            $nom               = $language[$code];
            $languedispo[$nom] = $code;
        }

        $options = [
            'languedispo' => $languedispo,
        ];
        $listingService->setSearch('admin.search.blog', $options);

        $actions             = [
            [
                'url'  => 'beblog_categorieindex',
                'text' => 'Catégories',
                'img'  => 'glyphicon-list',
            ],
        ];
        $categorieManager    = $this->get('bdd.categorie_manager');
        $categorieRepository = $categorieManager->getRepository();
        $categories          = $categorieRepository->findby(['type' => 'blog']);
        if (count($categories) != 0) {
            $actions[] = ['type' => 'add', 'url' => 'beblog_form'];
        }

        array_push(
            $actions,
            ['type' => 'actif', 'url' => 'beblog_json-actif'],
            ['type' => 'desactif', 'url' => 'beblog_json-actif']
        );
        $this->get('mkk.action_service')->set($actions);
        $param['categories'] = $categories;
        $twigindex           = 'MkkAdminBundle:Blog:index.html.twig';
        $twigHtml            = $this->renderMkk($request, $twigindex, $param);

        return $twigHtml;
    }

    /**
     * @Route("/blog/form/{id}", name="beblog_form", defaults={"id": 0})
     *
     * Formulaire d'un article du blog.
     *
     * @param Request $request pile de requêtes
     * @param int     $id      Identifiant de l'article du blog
     *
     * @return JsonResponse /  Blog:form.html.twig
     */
    public function blogFormAction(Request $request, $id)
    {
        $categorieManager    = $this->get('bdd.categorie_manager');
        $categorieRepository = $categorieManager->getRepository();
        $categories          = $categorieRepository->findby(['type' => 'blog']);
        if (count($categories) == 0) {
            $flashbag = $request->getSession()->getFlashBag();
            $flashbag->add('warning', 'Aucune catégorie est sauvegardé dans la base de données');
            $url      = $this->generateUrl('beblog_index');
            $redirect = $this->redirect($url, 301);

            return $redirect;
        }

        $crudService = $this->get('mkk.crud_service');
        $crudService->setManager('bdd.blog_manager');
        $crudService->setId($id);
        $crudService->setFormulaireID('FormBlog');
        $crudService->setUrl(
            [
                'listing' => 'beblog_index',
                'form'    => 'beblog_form',
            ]
        );
        $crudService->setController($this);
        $crudService->setFormService('mkk_admin.form.blog');
        $crudService->setTwigHTML('MkkAdminBundle:Blog:form.html.twig');
		$crudService->init();

        return $crudService->launch();
    }
}
