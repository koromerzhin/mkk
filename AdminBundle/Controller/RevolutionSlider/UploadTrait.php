<?php

namespace Mkk\AdminBundle\Controller\RevolutionSlider;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

trait UploadTrait
{
    /**
     * @Route("/revolution-slider/slide/image/{md5}", name="berevolutionslider_uploadslideimage")
     *
     * Upload image d'un slide
     *
     * @param Hash $md5 Hash de l'image
     *
     * @return Response
     */
    public function revolutionSliderSlideImageAction()
    {
        $options = [
             'max_number_of_files' => 1,
         ];

        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }
}
