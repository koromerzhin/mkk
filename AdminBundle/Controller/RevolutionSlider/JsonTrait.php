<?php

namespace Mkk\AdminBundle\Controller\RevolutionSlider;

use Mkk\AdminBundle\Form\RevolutionSlider\ParamType as SliderParamType;
use Mkk\AdminBundle\Form\RevolutionSlider\Slide\ImageType as SlideImageType;
use Mkk\AdminBundle\Form\RevolutionSlider\Slide\ParamType as SlideParamType;
use Mkk\AdminBundle\Form\RevolutionSlider\Slide\SlideType;
use Mkk\AdminBundle\Form\RevolutionSlider\SliderType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Intl\Intl;

trait JsonTrait
{
    /**
     * @Route("/revolution-slider/json/form/{id}", name="berevolutionslider_json-edit", defaults={"id": 0})
     *
     * Formulaire d'un slider.
     *
     * @param Request $request pile de requêtes
     * @param int     $id      Identifiant du slider
     *
     * @return JsonResponse / RevolutionSlider:form.html.twig
     */
    public function revolutionSliderEditAction(Request $request, $id)
    {
        $revolutionsliderManager    = $this->get('bdd.revolutionslider_manager');
        $revolutionsliderRepository = $revolutionsliderManager->getRepository();
        $revolutionsliderEntity     = $revolutionsliderManager->getTable();
        $etat                       = 'modifier';
        $slider                     = $revolutionsliderRepository->find($id);
        if (! $slider) {
            $slider = new $revolutionsliderEntity();
            $etat   = 'ajouter';
        }

        $param       = $this->get('mkk.param_service')->listing();
        $languedispo = [];
        $locale      = $request->getLocale();
        $language    = Intl::getLanguageBundle()->getLanguageNames($locale);
        if (! isset($param['languesite'])) {
            $flashbag = $request->getSession()->getFlashBag();
            $flashbag->add('warning', 'Langue du site non configuré');
            $url      = $this->generateUrl('bord_index');
            $redirect = $this->redirect($url, 301);

            return $redirect;
        }

        $bloglangueDispo = $param['languesite'];
        foreach ($bloglangueDispo as $code) {
            $nom               = $language[$code];
            $languedispo[$nom] = $code;
        }

        $options['languedispo'] = $languedispo;

        $formRevolutionSlider      = $this->createForm(SliderType::class, $slider, $options);
        $formRevolutionSliderParam = $this->createForm(SliderParamType::class, $slider->getParam());
        $slider->setParam($request->request->get('sliderparam'));

        $service = $this->get('mkk.post_service');
        $service->init($slider, $formRevolutionSlider, $etat);
        $response     = $service->getResponse();
        $jsonresponse = new JsonResponse($response);

        return $jsonresponse;
    }

    /**
     * @Route("/revolution-slider/json/{slider}/slide/edit/{id}", name="berevolutionslider_json-slideform", defaults={"id": 0})
     *
     * Formulaire d'un slide.
     *
     * @param Request $request pile de requêtes
     * @param int     $slider  Identifiant du slider
     * @param int     $id      Identifiant du slide
     *
     * @return JsonResponse / RevolutionSlide:form.html.twig
     */
    public function revolutionSliderSlideEditAction(Request $request, $slider, $id)
    {
        $revolutionsliderManager         = $this->get('bdd.revolutionslider_manager');
        $revolutionsliderRepository      = $revolutionsliderManager->getRepository();
        $revolutionsliderslideManager    = $this->get('bdd.revolutionsliderslide_manager');
        $revolutionsliderslideRepository = $revolutionsliderslideManager->getRepository();
        $revolutionsliderslideEntity     = $revolutionsliderslideManager->getTable();
        $etat                            = 'modifier';
        $slide                           = $revolutionsliderslideRepository->find($id);
        if (! $slide) {
            $slide = new $revolutionsliderslideEntity();
            $etat  = 'ajouter';
        }

        $formslide      = $this->createForm(SlideType::class, $slide);
        $formslideparam = $this->createForm(SlideParamType::class, $slide->getParam());
        $formslideimage = $this->createForm(SlideImageType::class, $slide->getParamimage());
        $slider         = $revolutionsliderRepository->find($slider);
        $slide->setRefRevolutionslider($slider);
        $slide->setParam($request->request->get('slideparam'));
        $slide->setParamimage($request->request->get('slideimage'));

        $service = $this->get('mkk.post_service');
        $service->init($slide, $formslide, $etat);
        $response     = $service->getResponse();
        $jsonresponse = new JsonResponse($response);

        return $jsonresponse;
    }
}
