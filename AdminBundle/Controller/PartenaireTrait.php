<?php

namespace Mkk\AdminBundle\Controller;

use Mkk\AdminBundle\Controller\Partenaire\CategorieTrait;
use Mkk\AdminBundle\Controller\Partenaire\JsonTrait;
use Mkk\AdminBundle\Controller\Partenaire\SearchTrait;
use Mkk\AdminBundle\Controller\Partenaire\UploadTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

trait PartenaireTrait
{
    use CategorieTrait;
    use JsonTrait;
    use SearchTrait;
    use UploadTrait;

    /**
     * @Route("/partenaire/", name="bepartenaire_index", defaults={"page": 0})
     * @Route("/partenaire/{page}.html", name="bepartenaire_page")
     *
     * Liste des partenaires
     *
     * @param Request $request pile de requêtes
     * @param int     $page    numéro de la page
     *
     * @return Partenaire:index.html.twig
     */
    public function partenaireIndexAction(Request $request)
    {
        $listingService = $this->get('mkk.listing_service');
        $listingService->setRequest('bdd.partenaire_manager', 'searchPartenaire');
        $listingService->setSearch('admin.search.partenaire');
        $actions = [
            [
                'url'  => 'bepartenaire_categorieindex',
                'text' => 'Catégories',
                'img'  => 'glyphicon-list',
            ],
        ];

        $categorieManager    = $this->get('bdd.categorie_manager');
        $categorieRepository = $categorieManager->getRepository();
        $categories          = $categorieRepository->findby(['type' => 'partenaire']);
        if (count($categories) != 0) {
            $actions[] = [
                'type' => 'add',
                'url'  => 'bepartenaire_form',
            ];
        }
        array_push(
            $actions,
            [
                'type' => 'actif',
                'url'  => 'bepartenaire_json-actif',
            ],
            [
                'type' => 'desactif',
                'url'  => 'bepartenaire_json-actif',
            ],
            [
                'type'    => 'data',
                'entity'  => 'partenaire',
            ]
        );

        $this->get('mkk.action_service')->set($actions);
        $twigHtml = $this->renderMkk(
            $request,
            'MkkAdminBundle:Partenaire:index.html.twig'
        );

        return $twigHtml;
    }

    /**
     * @Route("/partenaire/form/{id}", name="bepartenaire_form", defaults={"id": 0})
     *
     * Formulaire d'un partanaire.
     *
     * @param Request $request pile de requêtes
     * @param int     $id      Identifiant du partenaire
     *
     * @return JsonResponse /  Partenaire:form.html.twig
     */
    public function partenaireFormAction(Request $request, $id)
    {
        $categorieManager    = $this->get('bdd.categorie_manager');
        $categorieRepository = $categorieManager->getRepository();
        $categories          = $categorieRepository->findby(['type' => 'partenaire']);
        if (count($categories) == 0) {
            $flashbag = $request->getSession()->getFlashBag();
            $flashbag->add('warning', 'Aucune catégorie partenaire dans la base de données');
            $url      = $this->generateUrl('bepartenaire_index');
            $redirect = $this->redirect($url, 301);

            return $redirect;
        }

        $crudService = $this->get('mkk.crud_service');
        $crudService->setManager('bdd.partenaire_manager');
        $crudService->setId($id);
        $crudService->setFormulaireID('FormPartenaire');
        $crudService->setUrl(
            [
                'listing' => 'bepartenaire_index',
                'form'    => 'bepartenaire_form',
            ]
        );
        $crudService->setController($this);
        $crudService->setFormService('mkk_admin.form.partenaire');
        $crudService->setTwigHTML('MkkAdminBundle:Partenaire:form.html.twig');
        $crudService->init();

        return $crudService->launch();
    }
}
