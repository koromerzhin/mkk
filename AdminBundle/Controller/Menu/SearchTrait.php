<?php

namespace Mkk\AdminBundle\Controller\Menu;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

trait SearchTrait
{
    /**
     * @Route("/menu/search/url", name="bemenu_searchurl")
     */
    public function menuSearchUrlAction()
    {
        $json   = [
            'total'    => 0,
            'results'  => [],
        ];
        $id     = $request->request->get('id');
        $routes = $this->getRoutes();
        if ($id != '') {
            if ($id != '' && filter_var($id, FILTER_VALIDATE_URL)) {
                $json = [
                    'results' => [
                        [
                            'id'   => $id,
                            'nom'  => $id,
                        ],
                        [
                            'id'   => '',
                            'nom'  => 'CHAMPS VIDE',
                        ],
                    ],
                    'total' => 2,
                ];
            } elseif ($id != '') {
                $json = ['id' => $id, 'nom' => $id];
                if ($id == '') {
                    $json['nom'] = ' CHAMPS VIDE';
                }

                foreach ($routes as $key => $valeur) {
                    if ($key == $id) {
                        $json = [
                            'id'   => $key,
                            'nom'  => $valeur,
                        ];
                        break;
                    }
                }
            }
        } else {
            $lettre = trim($request->query->get('lettre'));
            $limit  = $request->query->get('limit');
            $page   = $request->query->get('page');
            if ($lettre != '' && filter_var($lettre, FILTER_VALIDATE_URL)) {
                $json = [
                'results' => [
                        [
                            'id'   => $lettre,
                            'nom'  => $lettre,
                        ],
                        [
                            'id'   => '',
                            'nom'  => 'CHAMPS VIDE',
                        ],
                    ],
                    'total' => 2,
                ];
            } elseif ($lettre != '') {
                $avant  = $routes;
                $routes = [];
                foreach ($avant as $key => $valeur) {
                    if (substr_count($key, $lettre) != 0 || substr_count($valeur, $lettre) != 0) {
                        $routes[$key] = $valeur;
                    }
                }

                if ($page <= 1) {
                    $debut = 0;
                } else {
                    $debut = $limit * ($page - 1);
                }

                $fin = $debut + $limit - 1;
                $i   = 0;
                foreach ($routes as $key => $valeur) {
                    if ($i >= $debut && $i <= $fin) {
                        $json['results'][] = ['id' => $key, 'nom' => $valeur];
                        if ($i == $fin) {
                            break;
                        }
                    }

                    ++$i;
                }

                $json['results'][] = ['id' => '', 'nom' => 'CHAMPS VIDE'];
                $json['total']     = count($json['results']);
            } else {
                if ($page <= 1) {
                    $debut = 0;
                } else {
                    $debut = $limit * ($page - 1);
                }

                $fin = $debut + $limit - 1;
                $i   = 0;
                foreach ($routes as $key => $valeur) {
                    if ($i >= $debut && $i <= $fin) {
                        $json['results'][] = ['id' => $key, 'nom' => $valeur];
                        if ($i == $fin) {
                            break;
                        }
                    }

                    ++$i;
                }

                $json['total'] = count($routes);
            }
        }

        $jsonresponse = new JsonResponse($json);

        return $jsonresponse;
    }
}
