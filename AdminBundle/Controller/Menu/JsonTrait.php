<?php

namespace Mkk\AdminBundle\Controller\Menu;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

trait JsonTrait
{
    /**
     * @Route("/menu/json/parent", name="bemenu_json-parent")
     */
    public function menuParentAction(Request $request)
    {
        $menuManager    = $this->get('bdd.menu_manager');
        $menuRepository = $menuManager->getRepository();
        $menuTable      = $menuManager->getTable();
        if ($this->isPostAjax($request)) {
            $responsejson = [];
            $entity       = new $menuTable();
            $post         = $request->request->all();
            if (isset($post['id'])) {
                $parent = $menuRepository->Find($post['id']);
                if (! $parent) {
                    throw $this->createNotFoundException('Unable to find Menu entity.');
                }

                $entity->setRefmenu($parent);
                $entity->setLibelle('nouveau lien');
                if (count($parent->getMenus()) == 0) {
                    $entity->setPosition(0);
                } else {
                    $entity->setPosition(count($parent->getMenus()) - 1);
                }

                $menuManager->persistAndFlush($entity);
            }

            $jsonresponse = new JsonResponse($responsejson);

            return $jsonresponse;
        }

        $url      = $this->generateUrl('bemenu_index');
        $redirect = $this->redirect($url, 301);

        return $redirect;
    }

    /**
     * @Route("/menu/json/move", name="bemenu_json-menu")
     */
    public function menuMoveAction(Request $request)
    {
        $menuManager    = $this->get('bdd.menu_manager');
        $menuRepository = $menuManager->getRepository();
        $responsejson   = [];
        if ($this->isPostAjax($request)) {
            $post = $request->request->all();
            if (isset($post['id']) && isset($post['parent'])) {
                $parent = $menuRepository->Find($post['parent']);
                $entity = $menuRepository->Find($post['id']);
                if ($parent && $entity) {
                    $entity->setRefMenu($parent);
                    $childs = $parent->getMenus();
                    if (count($childs) == 0) {
                        $position = 0;
                    } else {
                        $position = count($childs) - 1;
                    }

                    $entity->setPosition($position);
                    $menuManager->persistAndFlush($entity);
                }
            }
        } else {
            $menus = $menuRepository->findall();
            foreach ($menus as $key => $menu) {
                $responsejson[$key] = [
                    'id'   => $menu->getId(),
                    'text' => $menu->getId() . ' - ' . $menu->getLibelle() . ' (' . $menu->getClef() . ')',
                ];
            }
        }

        $jsonresponse = new JsonResponse($responsejson);

        return $jsonresponse;
    }

    /**
     * @Route("/menu/json/droit/{id}/{group}", name="bemenu_json-droit")
     *
     * @param mixed $id
     * @param mixed $group
     */
    public function menuDroitAction($id, $group)
    {
        $menuManager    = $this->get('bdd.menu_manager');
        $menuRepository = $menuManager->getRepository();
        $json           = [];
        $data           = $menuRepository->Find($id);
        if (! $data) {
            throw $this->createNotFoundException('Unable to find data entity.');
        }

        $refgroup = explode(',', $data->getRefGroup());
        if (in_array($group, $refgroup)) {
            unset($refgroup[array_search($group, $refgroup, true)]);
            $supprimer = 1;
        } else {
            $refgroup[] = $group;
            $supprimer  = 0;
        }

        $refgroup = implode(',', $refgroup);
        $data->setRefGroup($refgroup);
        $menuManager->persistAndFlush($data);
        $batchSize = 20;
        foreach ($data->getChild() as $i => $tab) {
            $refgroup = explode(',', $tab->getRefGroup());
            if ($supprimer == 1 && in_array($group, $refgroup)) {
                unset($refgroup[array_search($group, $refgroup, true)]);
            } elseif ($supprimer == 0 && ! in_array($group, $refgroup)) {
                $refgroup[] = $group;
            }

            $refgroup = implode(',', $refgroup);
            $tab->setRefGroup($refgroup);
            $menuManager->persist($tab);
            if (($i % $batchSize) == 0) {
                $menuManager->flush();
            }
        }

        $menuManager->flush();
        $data = $menuRepository->FindAll();
        foreach ($data as $tab) {
            $refgroup = explode(',', $tab->getRefGroup());
            foreach ($refgroup as $group) {
                $json[] = 'menu' . $tab->getId() . 'group' . $group;
            }
        }

        $jsonresponse = new JsonResponse($json);

        return $jsonresponse;
    }
}
