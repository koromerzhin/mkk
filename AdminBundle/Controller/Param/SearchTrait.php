<?php

namespace Mkk\AdminBundle\Controller\Param;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

trait SearchTrait
{
    /**
     * @Route("/param/search/group", name="beparam_searchgroup")
     */
    public function paramSearchGroupAction()
    {
        $response = $this->get('mkk.search_service')->getResponse('bdd.group_manager', 'searchGroupAll');
        $json     = new JsonResponse($response);

        return $json;
    }
}
