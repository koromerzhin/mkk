<?php

namespace Mkk\AdminBundle\Controller\Param;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

trait JsonTrait
{
    /**
     * @Route("/param/edit", name="beparam_json-edit")
     */
    public function paramJsonEditAction(Request $request)
    {
        $json         = [];
        $paramService = $this->get('mkk.param_service');
        $params       = $request->request->get('param');
        foreach ($params as $id => $val) {
            $paramService->save($id, $val);
        }

        $md5 = $request->request->get('md5');
        if (is_array($md5)) {
            $this->setFileUploadSystemParam($request, $md5);
        }

        $json['modifier'] = 1;
        $jsonresponse     = new JsonResponse($json);

        return $jsonresponse;
    }
}
