<?php

namespace Mkk\AdminBundle\Controller\Param;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

trait UploadTrait
{
    /**
     * @Route("/param/upload/delicious/{md5}", name="beparam_uploaddelicious")
     *
     * @param mixed $md5
     */
    public function paramDeliciousAction()
    {
        $options = [
            'max_number_of_files' => 1,
            'accept_file_types'   => 'html',
        ];

        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }

    /**
     * @Route("/param/upload/googlemaps/{md5}", name="beparam_uploadgooglemaps")
     *
     * @param mixed $md5
     */
    public function paramGoogleMapsAction()
    {
        $options = [
            'max_number_of_files' => 1,
            'image_versions'      => [],
        ];

        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }

    /**
     * @Route("/param/upload/login/{md5}", name="beparam_uploadlogin")
     *
     * @param mixed $md5
     */
    public function paramLoginAction()
    {
        $options = [
            'image_versions' => [],
        ];

        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }

    /**
     * @Route("/param/upload/avatar/{md5}", name="beparam_uploadavatar")
     *
     * @param mixed $md5
     */
    public function paramAvatarAction()
    {
        $options = [
            'max_number_of_files' => 1,
            'image_versions'      => [],
        ];

        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }

    /**
     * @Route("/param/upload/logo/{md5}", name="beparam_uploadlogo")
     *
     * @param mixed $md5
     */
    public function paramLogoAction()
    {
        $options = [
            'max_number_of_files' => 1,
        ];

        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }

    /**
     * @Route("/param/upload/logobackend/{md5}", name="beparam_uploadlogobackend")
     *
     * @param mixed $md5
     */
    public function paramLogobackendAction()
    {
        $options = [
            'max_number_of_files' => 1,
        ];

        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }

    /**
     * @Route("/param/upload/favicon/{md5}", name="beparam_uploadfavicon")
     *
     * @param mixed $md5
     */
    public function paramFaviconAction()
    {
        $options = [
            'max_number_of_files' => 1,
        ];

        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }
}
