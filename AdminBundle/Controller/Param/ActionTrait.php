<?php

namespace Mkk\AdminBundle\Controller\Param;

trait ActionTrait
{
    private function setParamSite($request)
    {
        $paramService = $this->get('mkk.param_service');
        $param        = $paramService->listing();

        if (! isset($param['longueurliste'])) {
            $param['longueurliste'] = 50;
        }

        if (! isset($param['desactivation_etat'])) {
            $param['desactivation_etat'] = 0;
        }

        if ($param['desactivation_etat'] == 'non') {
            $param['desactivation_etat'] = 0;
        } elseif ($param['desactivation_etat'] == 'oui') {
            $param['desactivation_etat'] = 1;
        }

        if (! isset($param['image_logo'])) {
            if (! is_file('logo.png')) {
                $param['image_logo'] = '';
            } else {
                $param['image_logo'] = 'logo.png';
            }
        }

        if (! isset($param['image_logobackend'])) {
            if (! is_file('logobackend.png')) {
                $param['image_logobackend'] = '';
            } else {
                $param['image_logobackend'] = 'logobackend.png';
            }
        }

        if (! is_dir('fichiers')) {
            mkdir('fichiers');
        }

        if (! is_dir('fichiers/login')) {
            mkdir('fichiers/login');
        }

        if (! isset($param['image_favicon'])) {
            if (! is_file('favicon.ico')) {
                $param['image_favicon'] = '';
            } else {
                $param['image_favicon'] = 'favicon.ico';
            }
        }

        if (! isset($param['image_login'])) {
            $param['image_login'] = glob('fichiers/login/*');
            if (! is_array($param['image_login'])) {
                $param['image_login'] = [];
            }
        } else {
            $param['image_login'] = $param['image_login'];
        }

        if (! isset($param['image_avatar'])) {
            $param['image_avatar'] = '';
        }

        if (! isset($param['image_googlemaps'])) {
            $param['image_googlemaps'] = '';
        }

        $tabmd5 = ['logo', 'logobackend', 'favicon', 'login', 'avatar', 'googlemaps', 'delicious'];
        if (isset($param['type_etablissement'])) {
            foreach ($param['type_etablissement'] as $tab) {
                $code     = $tab['code'];
                $tabmd5[] = 'etablissementpoint_' . $code;
                if (! isset($param['image_etablissementpoint_' . $code])) {
                    $param['image_etablissementpoint_' . $code] = '';
                }
            }
        }

        $uploadService = $this->get('mkk.upload_service');
        foreach ($tabmd5 as $code) {
            if (isset($param['image_' . $code])) {
                $md5 = md5(uniqid());
                $paramService->save('file_' . $code, $md5);
                $uploadService->init($md5, $param['image_' . $code]);
            }
        }

        return $param;
    }
}
