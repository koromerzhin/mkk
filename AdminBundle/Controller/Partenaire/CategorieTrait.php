<?php

namespace Mkk\AdminBundle\Controller\Partenaire;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

trait CategorieTrait
{
    /**
     * @Route("/partenaire/categorie/", name="bepartenaire_categorieindex", defaults={"page": 0})
     * @Route("/partenaire/categorie/{page}.html", name="bepartenaire_categoriepage")
     *
     * Liste des catégories des partenaires
     *
     * @param Request $request pile de requêtes
     * @param int     $page    numéro de la page
     *
     * @return html
     */
    public function partenairecategorieIndexAction(Request $request)
    {
        return $this->traitCategorieIndex($request, 'retour aux partenaires');
    }

    /**
     * @Route("/partenaire/categorie/json/vider", name="bepartenaire_categoriejson-vider")
     *
     * Vider la table
     *
     * @param Request $request pile de requêtes
     *
     * @return redirect
     */
    public function viderCategoriePartenaireAction(Request $request)
    {
        return $this->traitCategorieVider($request);
    }

    /**
     * @Route("/partenaire/categorie/form/{id}", name="bepartenaire_categorieform", defaults={"id": 0})
     *
     * Formulaire des categories de partenaire
     *
     * @param Request $request pile de requêtes
     * @param int     $id      Identifiant de la table catégorie
     *
     * @return json
     */
    public function partenairecategorieFormAction(Request $request, $id)
    {
        return $this->traitCategorieJson($request, $id);
    }

    /**
     * @Route("/partenaire/categorie/delete", name="bepartenaire_categoriejson-delete")
     *
     * Suppression d'une catégorie de partenaire
     *
     * @return json
     */
    public function partenairecategorieDeleteAction(Request $request)
    {
        return $this->traitCategorieDelete($request);
    }
}
