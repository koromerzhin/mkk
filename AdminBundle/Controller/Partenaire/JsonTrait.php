<?php

namespace Mkk\AdminBundle\Controller\Partenaire;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

trait JsonTrait
{
    /**
     * @Route("/partenaire/json/actif", name="bepartenaire_json-actif")
     *
     * Active/désactive un partenaire
     *
     * @return JsonResponse
     */
    public function partenaireActifAction(Request $request)
    {
        $json = $this->actionActivate($request, 'Partenaire', 'id', 'setActifPublic');

        return $json;
    }
}
