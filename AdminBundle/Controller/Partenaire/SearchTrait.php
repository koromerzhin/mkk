<?php

namespace Mkk\AdminBundle\Controller\Partenaire;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

trait SearchTrait
{
    /**
     * @Route("/partenaire/search/categorie", name="bepartenaire_searchcategorie")
     */
    public function partenaireSearchCategorieAction()
    {
        $response = $this->get('mkk.search_service')->getResponse('bdd.categorie_manager', 'searchCategoriePartenaire');
        $json     = new JsonResponse($response);

        return $json;
    }
}
