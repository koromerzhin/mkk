<?php

namespace Mkk\AdminBundle\Controller\Partenaire;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

trait UploadTrait
{
    /**
     * @Route("/partenaire/image/{md5}", name="bepartenaire_uploadimage")
     *
     * Upload d'une image d'un partenaire
     *
     * @param Hash $md5 Hash de l'image
     *
     * @return Response
     */
    public function partenaireImageAction()
    {
        $options = [
            'max_number_of_files' => 1,
        ];

        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }
}
