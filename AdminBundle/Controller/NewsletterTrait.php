<?php

namespace Mkk\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

trait NewsletterTrait
{
    /**
     * @Route("/newsletter", name="benewsletter_userindex", defaults={"page": 0})
     * @Route("/newsletter/{page}.html", name="benewsletter_userpage")
     *
     * Liste des utilisateurs inscrit à la newsletter
     *
     * @param Request $request pile de requêtes
     * @param int     $page    numéro de la page
     *
     * @return html
     */
    public function newsletterUserAction(Request $request)
    {
        $listingService = $this->get('mkk.listing_service');
        $listingService->setRequest('bdd.user_manager', 'searchUserNewsletter');
        $actions = [
            [
                'url'  => 'benewsletter_userexportcsv',
                'text' => 'Exporter en CSV',
                'img'  => 'glyphicon glyphicon-file',
            ],
        ];
        $this->get('mkk.action_service')->set($actions);
        $twigHtml = $this->renderMkk(
            $request,
            'MkkAdminBundle:Newsletter:index.html.twig'
        );

        return $twigHtml;
    }

    /**
     * @Route("/newsletter/exportcsv", name="benewsletter_userexportcsv")
     *
     * Export en CSV des utilisateurs inscrits à la newsletter
     *
     * @return Response
     */
    public function newsletterUserExportCsvAction()
    {
        $groupManager    = $this->get('bdd.group_manager');
        $groupRepository = $groupManager->getRepository();
        $group           = $groupRepository->findOneByCode('newsletter');
        if (! $group) {
            $group = new Group();
            $group->setCode('newsletter');
            $group->setNom('Newsletter');
            $em->persist($group);
            $em->flush();
        }

        $userManager    = $this->get('bdd.user_manager');
        $userRepository = $userManager->getRepository();
        $iterableResult = $userRepository->getListingNewsletterCsv();

        $handle = fopen('php://memory', 'r+');
        $tab    = [];
        fputcsv($handle, [utf8_decode('Nom'), utf8_decode('Prénom'), utf8_decode('E-mail')], ';');
        while (false != ($row = $iterableResult->next())) {
            if (isset($row[0]->getEmails()[0])) {
                $tab = [
                    'nom'    => utf8_decode($row[0]->getNom()),
                    'prenom' => utf8_decode($row[0]->getPrenom()),
                    'email'  => utf8_decode($row[0]->getEmails()[0]->getAdresse()),
                ];
            }
            fputcsv($handle, $tab, ';');
            $groupManager->detach($row[0]);
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        $response = new Response(
            $content,
            200,
            [
                'Content-Type'        => 'application/force-download;',
                'Content-Disposition' => 'attachment; filename="newsletter.csv"',
            ]
        );

        return $response;
    }
}
