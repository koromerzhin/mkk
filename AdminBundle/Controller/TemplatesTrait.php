<?php

namespace Mkk\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

trait TemplatesTrait
{
    /**
     * @Route("/templates/form/{id}", name="betemplates_form", defaults={"id": 0})
     *
     * Formulaire d'enregistrement de la templates
     *
     * @param Request $request Pile de requêtes
     * @param int     $id      Id de Templates
     *
     * @return json or html
     */
    public function templatesFormAction(Request $request, $id)
    {
        $crudService = $this->get('mkk.crud_service');
        $crudService->setManager('bdd.templates_manager');
        $crudService->setId($id);
        $crudService->setFormulaireID('FormTemplates');
        $crudService->setUrl(
            [
                'listing' => 'betemplates_index',
                'form'    => 'betemplates_form',
            ]
        );
        $crudService->setController($this);
        $crudService->setFormService('mkk_admin.form.templates');
        $crudService->setTwigHTML('MkkAdminBundle:Templates:form.html.twig');
		$crudService->init();

        return $crudService->launch();
    }

    /**
     * @Route("/templates/", name="betemplates_index", defaults={"page": 0})
     * @Route("/templates/{page}.html", name="betemplates_page")
     *
     * Liste des templates
     *
     * @param Request $request Pile de requêtes
     * @param int     $page    numero de la page actuel
     *
     * @return html
     */
    public function templatesIndexAction(Request $request)
    {
        $listingService = $this->get('mkk.listing_service');
        $listingService->setRequest('bdd.templates_manager', 'searchTemplates');
        $params = $this->get('mkk.param_service')->listing();
        if (! isset($params['languesite']) || ! isset($params['langueprincipal'])) {
            $flashbag = $request->getSession()->getFlashBag();
            $flashbag->add('warning', 'Aucune langue est configuré dans les paramètres');
            $url      = $this->generateUrl('bord_index');
            $redirect = $this->redirect($url, 301);

            return $redirect;
        }
        $actions = [
            [
                'type' => 'add',
                'url'  => 'betemplates_form',
            ],
            [
                'type'   => 'data',
                'entity' => 'templates',
            ],
        ];
        $this->get('mkk.action_service')->set($actions);
        $htmlTwig = $this->renderMkk(
            $request,
            'MkkAdminBundle:Templates:index.html.twig'
        );

        return $htmlTwig;
    }
}
