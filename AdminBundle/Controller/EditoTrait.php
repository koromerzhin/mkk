<?php

namespace Mkk\AdminBundle\Controller;

use Mkk\AdminBundle\Controller\Edito\JsonTrait;
use Mkk\AdminBundle\Controller\Edito\SearchTrait;
use Mkk\AdminBundle\Controller\Edito\UploadTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

trait EditoTrait
{
    use JsonTrait;
    use SearchTrait;
    use UploadTrait;

    /**
     * @Route("/edito/", name="beedito_index", defaults={"page": 0})
     * @Route("/edito/{page}.html", name="beedito_page")
     *
     * Liste des articles du blog
     *
     * @param Request $request pile de requêtes
     * @param int     $page    numéro de la page
     *
     * @return Blog:index.html.twig
     */
    public function editoIndexAction(Request $request)
    {
        $listingService = $this->get('mkk.listing_service');
        $listingService->setRequest('bdd.edito_manager', 'searchEdito');
        $categorieManager    = $this->get('bdd.categorie_manager');
        $categorieRepository = $categorieManager->getRepository();
        $editoManager        = $this->get('bdd.edito_manager');
        $editoRepository     = $editoManager->getRepository();
        $search              = ['type' => 'blog'];
        $categories          = $categorieRepository->findby($search);
        $categories          = count($categories);
        $listingService->setSearch('admin.search.edito');
        $actions = [
            ['type' => 'add', 'url' => 'beedito_form'],
            ['type' => 'actif', 'url' => 'beedito_json-actif'],
            ['type' => 'desactif', 'url' => 'beedito_json-actif'],
            ['type' => 'data', 'entity' => 'edito'],
        ];
        $this->get('mkk.action_service')->set($actions);
        $param['categories'] = $categories;
        $twigindex           = 'MkkAdminBundle:Edito:index.html.twig';
        $twigHtml            = $this->renderMkk($request, $twigindex, $param);

        return $twigHtml;
    }

    /**
     * @Route("/edito/form/{id}", name="beedito_form", defaults={"id": 0})
     *
     * Formulaire d'un article du blog.
     *
     * @param Request $request pile de requêtes
     * @param int     $id      Identifiant de l'article du blog
     *
     * @return JsonResponse /  Blog:form.html.twig
     */
    public function editoFormAction(Request $request, $id)
    {
        $crudService = $this->get('mkk.crud_service');
        $crudService->setManager('bdd.edito_manager');
        $crudService->setId($id);
        $crudService->setFormulaireID('FormEdito');
        $crudService->setUrl(
            [
                'listing' => 'beedito_index',
                'form'    => 'beedito_form',
            ]
        );
        $crudService->setController($this);
        $crudService->setFormService('mkk_admin.form.edito');
        $crudService->setTwigHTML('MkkAdminBundle:Edito:form.html.twig');
		$crudService->init();
		
        return $crudService->launch();
    }
}
