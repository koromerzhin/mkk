<?php

namespace Mkk\AdminBundle\Controller;

use Mkk\AdminBundle\Controller\Page\JsonTrait;
use Mkk\AdminBundle\Controller\Page\UploadTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

trait PageTrait
{
    use UploadTrait;

    /**
     * @Route("/page/", name="bepage_index", defaults={"page": 0})
     * @Route("/page/{page}.html", name="bepage_page")
     *
     * @param mixed $page
     */
    public function pageIndexAction(Request $request)
    {
        $listingService = $this->get('mkk.listing_service');
        $listingService->setRequest('bdd.page_manager', 'searchPage');
        $actions = [
            ['type' => 'add', 'url' => 'bepage_form'],
            ['type' => 'data', 'entity' => 'page'],
        ];
        $this->get('mkk.action_service')->set($actions);
        $routes   = $this->getRoutes();
        $twigHtml = $this->renderMkk(
            $request,
            'MkkAdminBundle:Page:index.html.twig',
            [
                'routes' => $routes,
            ]
        );

        return $twigHtml;
    }

    /**
     * @Route("/page/form/{id}", name="bepage_form", defaults={"id": 0})
     *
     * @param mixed $id
     */
    public function pageFormAction(Request $request, $id)
    {
        $crudService = $this->get('mkk.crud_service');
        $crudService->setManager('bdd.page_manager');
        $crudService->setId($id);
        $crudService->setFormulaireID('FormPage');
        $crudService->setUrl(
            [
                'listing' => 'bepage_index',
                'form'    => 'bepage_form',
            ]
        );
        $crudService->setController($this);
        $crudService->setFormService('mkk_admin.form.page');
        $crudService->setTwigHTML('MkkAdminBundle:Page:form.html.twig');
		$crudService->init();

        return $crudService->launch();
    }
}
