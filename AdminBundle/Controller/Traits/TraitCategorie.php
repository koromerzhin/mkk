<?php

namespace Mkk\AdminBundle\Controller\Traits;

use Symfony\Component\HttpFoundation\Request;

trait TraitCategorie
{
    /**
     * Liste des catégories du blog.
     *
     * @param Request $request pile de requêtes
     * @param int     $page    numéro de la page
     * @param mixed   $retour
     *
     * @return html
     */
    public function traitCategorieIndex(Request $request, $retour)
    {
        $listingService        = $this->get('mkk.listing_service');
        list($module, $partie) = explode('_', $request->get('_route'));
        unset($partie);
        $options = [
            'type' => str_replace('be', '', $module),
        ];
        $listingService->setRequest('bdd.categorie_manager', 'searchCategorie', $options);
        $categorieManager = $this->get('bdd.categorie_manager');
        $categorieTable   = $categorieManager->getTable();
        $entity           = new $categorieTable();
        $siteService      = $this->get('mkk.site_service');
        $form             = $siteService->getForm('mkk_admin.form.categorie', $categorieManager, $entity, 'ajouter');
        if ($siteService->isRedirect()) {
            $url      = $this->generateUrl($module . '_index');
            $redirect = $this->redirect($url, 301);

            return $redirect;
        }

        $actions = [
            [
                'url'  => $module . '_index',
                'text' => $retour,
                'img'  => 'glyphicon-list',
            ],
            ['type' => 'add', 'url' => $module . '_categorieform'],
            [
                'type'    => 'data',
                'entity'  => 'categorie',
            ],
        ];
        $this->get('mkk.action_service')->set($actions);
        $htmlTwig = $this->renderMkk(
            $request,
            'MkkAdminBundle:Categorie:index.html.twig',
            [
                'module' => $module,
                'form'   => $siteService->createView($form),
            ]
        );

        return $htmlTwig;
    }

    /**
     * Formulaire de catégorie de blog.
     *
     * @param Request $request pile de requêtes
     * @param int     $id      Identifiant de la catégorie
     *
     * @return json /  html
     */
    public function traitCategorieJson(Request $request, $id)
    {
        list($module, $partie) = explode('_', $request->get('_route'));
        unset($partie);
        $crudService = $this->get('mkk.crud_service');
        $crudService->setManager('bdd.categorie_manager');
        $crudService->setId($id);
        $crudService->setFormulaireID('FormCategorie');
        $crudService->setUrl(
            [
                'listing' => $module . '_categorieindex',
                'form'    => $module . '_categorieform',
            ]
        );
        $crudService->setController($this);
        $crudService->setFormService('mkk_admin.form.categorie');
        $crudService->setTwigHTML('MkkAdminBundle:Categorie:form.html.twig');
        $crudService->init();
        $entity = $crudService->getEntity();
        $entity->setType(str_replace('be', '', $module));

        return $crudService->launch();
    }
}
