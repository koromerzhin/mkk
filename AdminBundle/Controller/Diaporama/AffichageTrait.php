<?php

namespace Mkk\AdminBundle\Controller\Diaporama;

trait AffichageTrait
{
    public function affichageBlogDiaporamaIndex($request)
    {
        $actions = [
            ['type' => 'add', 'url' => 'bediaporama_form'],
        ];
        $this->get('mkk.action_service')->set($actions);
        $htmlTwig = $this->renderMkk(
            $request,
            'MkkAdminBundle:Diaporama:index.html.twig'
        );

        return $htmlTwig;
    }
}
