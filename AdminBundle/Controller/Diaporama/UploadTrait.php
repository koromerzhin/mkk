<?php

namespace Mkk\AdminBundle\Controller\Diaporama;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

trait UploadTrait
{
    /**
     * @Route("/diaporama/upload/images/{md5}", name="bediaporama_uploadimages")
     *
     * Upload images
     *
     * @param string $md5 code unique
     *
     * @return response
     */
    public function diaporamaImagesAction()
    {
        $options  = [];
        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }
}
