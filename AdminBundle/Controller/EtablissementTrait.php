<?php

namespace Mkk\AdminBundle\Controller;

use Mkk\AdminBundle\Controller\Etablissement\JsonTrait;
use Mkk\AdminBundle\Controller\Etablissement\SearchTrait;
use Mkk\AdminBundle\Controller\Etablissement\UploadTrait;
use Mkk\AdminBundle\Form\Etablissement\AjoutType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

trait EtablissementTrait
{
    use JsonTrait;
    use SearchTrait;
    use UploadTrait;

    /**
     * @Route("/etablissements/position", name="beetablissement_position")
     *
     * Position des etablissements suivant les types
     *
     * @param Request $request Pile de requêtes
     *
     * @return html
     */
    public function positionEtablissementAction(Request $request)
    {
        $etablissementManager    = $this->get('bdd.etablissement_manager');
        $etablissementRepository = $etablissementManager->getRepository();

        $params         = $this->get('mkk.param_service')->listing();
        $etablissements = [];

        if (! isset($params['etablissement_position']) || 1 != $params['etablissement_position']) {
            $flashbag = $request->getSession()->getFlashBag();
            $flashbag->add('warning', 'Gestion des positions désactivés');
            $url      = $this->generateUrl('beetablissement_index');
            $redirect = $this->redirect($url, 403);

            return $redirect;
        }

        if (isset($params['type_etablissement']) && 0 != count($params['type_etablissement'])) {
            $types = $params['type_etablissement'];
            foreach ($types as $data) {
                $code   = $data['code'];
                $search = [
                    'type' => $code,
                ];

                $order = [
                    'position' => 'ASC',
                ];

                $etablissements[$code] = $etablissementRepository->findby($search, $order);
            }
        }

        $actions = [
            [
                'type' => 'return',
                'url'  => 'beetablissement_index',
            ],
            [
                'type' => 'save',
                'url'  => 'beetablissement_json-position',
            ],
        ];
        $this->get('mkk.action_service')->set($actions);
        $htmlTwig = $this->renderMkk(
            $request,
            'MkkAdminBundle:Etablissement:position.html.twig',
            [
                'etablissements' => $etablissements,
            ]
        );

        return $htmlTwig;
    }

    /**
     * @Route("/etablissements/", name="beetablissement_index", defaults={"page":0})
     * @Route("/etablissements/{page}.html", name="beetablissement_page")
     *
     * Page des établissements
     *
     * @param Request $request Pile de requètes
     * @param Page    $page    Page actuelle des établissements
     *
     * @return JsonResponse / Etablissement:index.html.twig
     */
    public function indexEtablissementAction(Request $request)
    {
        $listingService    = $this->get('mkk.listing_service');
        $route             = $request->attributes->get('_route');
        $params            = $this->get('mkk.param_service')->listing();
        $typeEtablissement = [];
        if (isset($params['type_etablissement']) && 0 != count($params['type_etablissement'])) {
            $data = $params['type_etablissement'];
            foreach ($data as $val) {
                $key                     = $val['code'];
                $typeEtablissement[$key] = $val['nom'];
            }
        }

        $param   = [];
        $newtype = $this->createForm(AjoutType::class, [], ['type' => $typeEtablissement]);
        $listingService->setRequest('bdd.etablissement_manager', 'searchEtablissementSaufEnseigne');
        $actions = [];
        if (0 != count($typeEtablissement)) {
            $actions[] = [
                'type' => 'add',
                'url'  => 'beetablissement_form',
                'attr' => ['data-type' => count($typeEtablissement)],
            ];
        }

        if (isset($params['etablissement_position']) && 1 == $params['etablissement_position']) {
            $actions[] = [
                'id'   => 'BoutonPosition',
                'text' => 'Position',
                'url'  => 'beetablissement_position',
            ];
        }

        $actions[] = ['type' => 'actif', 'url' => 'beetablissement_json-actif'];
        $actions[] = ['type' => 'desactif', 'url' => 'beetablissement_json-actif'];
        $actions[] = ['type' => 'data', 'entity' => 'etablissement'];

        $params  = $this->get('mkk.param_service')->listing();
        $options = [
            'secteur'  => (isset($params['etablissement_secteur'])),
            'activite' => (isset($params['etablissement_secteur'])),
        ];
        $listingService->setSearch('admin.search.etablissement', $options);
        $this->get('mkk.action_service')->set($actions);
        $param['formnewetab'] = $newtype->createView();
        $route                = $request->attributes->get('_route');

        $htmlTwig = $this->renderMkk($request, 'MkkAdminBundle:Etablissement:index.html.twig', $param);

        return $htmlTwig;
    }

    /**
     * @Route("/etablissements/form/{id}", name="beetablissement_form", defaults={"id":0})
     *
     * Description of what this does.
     *
     * @param Request       $request Pile de requètes
     * @param Etablissement $id      Identifiant de l'établissement
     *
     * @return JsonResponse
     */
    public function formEtablissementAction(Request $request, $id)
    {
        $etablissementManager    = $this->get('bdd.etablissement_manager');
        $etablissementRepository = $etablissementManager->getRepository();
        $etablissementEntity     = $etablissementManager->getTable();
        $userManager             = $this->get('bdd.user_manager');
        $horaireManager          = $this->get('bdd.horaire_manager');
        $horaireEntity           = $horaireManager->getTable();
        $params                  = $this->get('mkk.param_service')->listing();
        $bundleform              = $this->getBundle("\AdminBundle");
        $etat                    = 'modifier';
        $entity                  = $etablissementRepository->find($id);
        $route                   = $request->attributes->get('_route');
        if (! $entity) {
            if (! isset($params['type_etablissement']) || 0 == count($params['type_etablissement'])) {
                $flashbag = $request->getSession()->getFlashBag();
                $flashbag->add('warning', 'Formulaire introuvable');
                $url      = $this->generateUrl('beetablissement_index');
                $redirect = $this->redirect($url, 301);

                return $redirect;
            }

            $etat   = 'ajouter';
            $entity = new $etablissementEntity();

            $etatcode = $request->query->get('etat');
            if ('' == $etatcode) {
                if (isset($params['type_etablissement']) && 0 != count($params['type_etablissement'])) {
                    $data     = $params['type_etablissement'];
                    $etatcode = $data[0]['code'];
                }
            }

            $entity->setType($etatcode);
        }

        $horaires = $entity->getHoraires();
        if (7 != count($horaires)) {
            for ($i=1; $i <= 7; ++$i) {
                $horaire = new $horaireEntity();
                $horaire->setRefEtablissement($entity);
                $horaire->setJour($i);
                $entity->addHoraire($horaire);
            }
        }

        $formtype = [];
        if (in_array('formtypeEtablissement', get_class_methods($this))) {
            $formtype = $this->formtypeEtablissement();
        }

        if ('enseigne' == $entity->getType()) {
            $formetablissement = 'mkk_admin.formetablissement.enseigne';
        } else {
            $formetablissement = 'hdm_admin.formetablissement.etablisement';
        }

        $crudService = $this->get('mkk.crud_service');
        $crudService->setManager('bdd.etablissement_manager');
        $crudService->setEntity($entity);
        $crudService->setFormulaireID('FormEtablissement');
        $crudService->setUrl(
            [
                'listing' => 'beetablissement_index',
                'form'    => 'beetablissement_form',
            ]
        );
        $crudService->setController($this);
        $crudService->setFormService($formetablissement);
        $crudService->setTwigHTML('MkkAdminBundle:Etablissement:form.html.twig');
        $crudService->init();

        return $crudService->launch();
    }

    /**
     * @Route("/enseigne", name="beenseigne_index")
     *
     * Description of what this does.
     *
     * @param Request $request Piles de requètes
     *
     * @return formAction($request, $id)
     */
    public function enseigneEtablissementAction(Request $request)
    {
        $etablissementManager    = $this->get('bdd.etablissement_manager');
        $etablissementRepository = $etablissementManager->getRepository();
        $etablissementEntity     = $etablissementManager->getTable();
        $entity                  = $etablissementRepository->findoneBy(['type' => 'enseigne']);
        if (! $entity) {
            $entity = new $etablissementEntity();
            $entity->setType('enseigne');
            $etablissementManager->persistAndFlush($entity);
        }

        $crudService = $this->get('mkk.crud_service');
        $crudService->setManager('bdd.etablissement_manager');
        $crudService->setId($entity->getId());
        $crudService->setFormulaireID('FormEnseigne');
        $crudService->setUrl(
            [
                'listing'    => 'beenseigne_index',
                'form'       => 'beenseigne_index',
            ]
        );
        $crudService->setController($this);
        $crudService->setFormService('mkk_admin.formetablissement.enseigne');
        $crudService->setTwigHTML('MkkAdminBundle:Etablissement:form.html.twig');
        $crudService->init();

        return $crudService->launch();
    }
}
