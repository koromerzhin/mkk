<?php

namespace Mkk\AdminBundle\Controller;

use Mkk\AdminBundle\Controller\Evenement\CategorieTrait;
use Mkk\AdminBundle\Controller\Evenement\JsonTrait;
use Mkk\AdminBundle\Controller\Evenement\SearchTrait;
use Mkk\AdminBundle\Controller\Evenement\UploadTrait;
use Symfony\Component\HttpFoundation\Request;

trait EvenementTrait
{
    use CategorieTrait;
    use JsonTrait;
    use SearchTrait;
    use UploadTrait;

    /**
     * @Route("/evenement/", name="beevenement_index", defaults={"page": 0})
     * @Route("/evenement/{page}.html", name="beevenement_page")
     *
     * @param
     * @param mixed $page
     */
    public function evenementindexAction(Request $request)
    {
        $listingService = $this->get('mkk.listing_service');
        $listingService->setRequest('bdd.evenement_manager', 'searchEvenement');
        $prixManager    = $this->get('bdd.prix_manager');
        $prixRepository = $prixManager->getRepository();
        $tarifs         = count($prixRepository->findall());
        $listingService->setSearch('admin.search.evenement');
        $actions             = [
            [
                'url'  => 'beevenement_categorieindex',
                'text' => 'Catégories',
                'img'  => 'glyphicon-list',
            ],
            ['type' => 'actif', 'url' => 'beevenement_json-actif'],
            ['type' => 'desactif', 'url' => 'beevenement_json-actif'],
            ['type' => 'data', 'entity' => 'evenement'],
        ];
        $categorieManager    = $this->get('bdd.categorie_manager');
        $categorieRepository = $categorieManager->getRepository();
        $categories          = $categorieRepository->findby(['type' => 'evenement']);
        if (count($categories) != 0) {
            $actions = array_merge(
                $actions,
                [
                    [
                        'type' => 'add',
                        'url'  => 'beevenement_form',
                    ],
                ]
            );
        }
        $this->get('mkk.action_service')->set($actions);
        $twigHtml = $this->renderMkk(
            $request,
            'MkkAdminBundle:Evenement:index.html.twig',
            [
                'tarifs' => $tarifs,
            ]
        );

        return $twigHtml;
    }

    /**
     * @Route("/evenement/form/{id}", name="beevenement_form", defaults={"id": 0})
     *
     * @param
     * @param mixed $id
     */
    public function evenementformAction(Request $request, $id)
    {
        $categorieManager    = $this->get('bdd.categorie_manager');
        $categorieRepository = $categorieManager->getRepository();
        $categories          = $categorieRepository->findby(['type' => 'evenement']);
        if (count($categories) == 0) {
            $flashbag = $request->getSession()->getFlashBag();
            $flashbag->add('warning', 'Aucune catégorie évènement est présent dans la base de données');
            $url      = $this->generateUrl('beevenement_index');
            $redirect = $this->redirect($url, 301);

            return $redirect;
        }

        $crudService = $this->get('mkk.crud_service');
        $crudService->setManager('bdd.evenement_manager');
        $crudService->setId($id);
        $crudService->setFormulaireID('FormEvenement');
        $crudService->setUrl(
            [
                'listing' => 'beevenement_index',
                'form'    => 'beevenement_form',
            ]
        );
        $crudService->setController($this);
        $crudService->setFormService('mkk_admin.form.evenement');
        $crudService->setTwigHTML('MkkAdminBundle:Evenement:form.html.twig');
		$crudService->init();

        return $crudService->launch();
    }
}
