<?php

namespace Mkk\AdminBundle\Controller;

use Mkk\AdminBundle\Controller\Menu\JsonTrait;
use Mkk\AdminBundle\Controller\Menu\SearchTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

trait MenuTrait
{
    use JsonTrait;
    use SearchTrait;

    /**
     * @Route("/menu/form/{id}", name="bemenu_form", defaults={"id": 0})
     *
     * @param mixed $id
     */
    public function menuFormAction(Request $request, $id)
    {
        $crudService = $this->get('mkk.crud_service');
        $crudService->setManager('bdd.menu_manager');
        $crudService->setId($id);
        $crudService->setFormulaireID('FormMenu');
        $crudService->setUrl(
            [
                'listing' => 'bemenu_index',
                'form'    => 'bemenu_form',
            ]
        );
        $crudService->setController($this);
        $crudService->setFormService('mkk_admin.form.menu');
        $crudService->setTwigHTML('MkkAdminBundle:Menu:form.html.twig');
		$crudService->init();

        return $crudService->launch();
    }

    /**
     * @Route("/menu/", name="bemenu_index", defaults={"id": 0})
     */
    public function menuIndexAction(Request $request)
    {
        $menuManager     = $this->get('bdd.menu_manager');
        $menuRepository  = $menuManager->getRepository();
        $groupManager    = $this->get('bdd.group_manager');
        $groupRepository = $groupManager->getRepository();
        $data            = $menuRepository->getListing();
        $menuchoix       = [];

        $move = $this->createFormBuilder();
        $move->add('exemple', 'choice', ['mapped' => false, 'label' => 'Nom', 'choices' => $menuchoix]);
        $groups  = $groupRepository->findAll();
        $actions = [
            ['type' => 'add', 'url' => 'bemenu_form'],
            ['type' => 'data', 'entity' => 'menu'],
        ];
        $this->get('mkk.action_service')->set($actions);
        $twigHtml = $this->renderMkk(
            $request,
            'MkkAdminBundle:Menu:index.html.twig',
            [
                'groups' => $groups,
                'data'   => $data,
            ]
        );

        return $twigHtml;
    }

    /**
     * @Route("/menu/position/{id}/{etat}", name="bemenu_position")
     *
     * @param mixed $id
     * @param mixed $etat
     */
    public function menuPositionAction($id, $etat)
    {
        $menuManager    = $this->get('bdd.menu_manager');
        $menuRepository = $menuManager->getRepository();
        $hash           = '#menu' . $id;
        $data           = $menuRepository->Find($id);
        if (! $data) {
            throw $this->createNotFoundException('Unable to find Menu entity.');
        }

        $position = $data->getPosition();
        if ($etat == 'down') {
            $position = $position + 1;
        } else {
            $position = $position - 1;
        }

        $child = $data->getRefMenu()->getMenus();
        $menu  = [];
        foreach ($child as $id => $row) {
            if ($etat == 'up' && $position == $id) {
                $menu[] = $data;
            }

            if ($row->getId() != $data->getId()) {
                $menu[] = $row;
            }

            if ($etat == 'down' && $position == $id) {
                $menu[] = $data;
            }
        }

        $batchSize = 20;
        $i         = 0;
        foreach ($menu as $id => $row) {
            $row->setPosition($id);
            $menuManager->persist($row);
            ++$i;
            if (($i % $batchSize) == 0) {
                $menuManager->flush();
            }
        }

        $menuManager->flush();

        $url      = $this->generateUrl('bemenu_index');
        $redirect = $this->redirect($url . $hash, 301);

        return $redirect;
    }
}
