<?php

namespace Mkk\AdminBundle\Controller\Droit;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

trait JsonTrait
{
    /**
     * @Route("/droit/set/{module}/{action}/{group}", name="bedroit_json-set", defaults={"group": ""})
     *
     * @param mixed $module
     * @param mixed $action
     * @param mixed $group
     */
    public function droitSetAction($module, $action, $group)
    {
        $actionManager    = $this->get('bdd.action_manager');
        $actionRepository = $actionManager->getRepository();
        $json             = [];
        if ($group != '') {
            $this->setActionGroup($module, $action, $group);
        } else {
            $this->setActionModule($module, $action);
        }

        $actions = $actionRepository->findby(['module' => $module]);
        foreach ($actions as $tab) {
            if ($tab->getCodes() != '') {
                $codes = explode(',', $tab->getCodes());
                foreach ($codes as $action) {
                    $code   = 'Module' . $tab->getModule() . 'Action';
                    $code   = $code . $action . 'Group' . $tab->getRefGroup()->getId();
                    $json[] = $code;
                }
            }
        }

        $actionManager->flush();
        $json = new JsonResponse($json);

        return $json;
    }
}
