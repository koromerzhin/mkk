<?php

namespace Mkk\AdminBundle\Controller\Droit;

trait ActionTrait
{
    public function setActionModule($module, $action)
    {
        $actionManager    = $this->get('bdd.action_manager');
        $actionRepository = $actionManager->getRepository();
        $actionEntity     = $actionManager->getTable();
        $groupManager     = $this->get('bdd.group_manager');
        $groupRepository  = $groupManager->getRepository();
        $groups           = $groupRepository->findall();
        foreach ($groups as $group) {
            $idgroup = $group->getId();
            $search  = [
                'module'   => $module,
                'refgroup' => $idgroup,
            ];
            $actions = $actionRepository->findoneBy($search);
            if (! $actions) {
                $routes = $this->generateRoute();
                foreach (array_keys($routes) as $code) {
                    if ($code == $module) {
                        $actions = new $actionEntity();
                        $actions->setModule($module);
                        $actions->setRefGroup($group);
                        break;
                    }
                }
            }

            if ($actions) {
                $codes   = $actions->getCodes();
                $newcode = [];
                if ($codes != '') {
                    $newcode = explode(',', $codes);
                }

                $codes   = $newcode;
                $codes   = $this->setRoute($action, $module, $codes);
                $newcode = '';
                if (count($codes) != 0) {
                    $newcode = implode(',', $codes);
                }

                $actions->setCodes($newcode);
                $actionManager->persistAndFlush($actions);
            }
        }
    }

    public function setActionGroup($module, $action, $group)
    {
        $groupManager     = $this->get('bdd.group_manager');
        $groupRepository  = $groupManager->getRepository();
        $actionManager    = $this->get('bdd.action_manager');
        $actionRepository = $actionManager->getRepository();
        $actionEntity     = $actionManager->getTable();

        $search  = [
            'module'   => $module,
            'refgroup' => $group,
        ];
        $actions = $actionRepository->findoneBy($search);
        $group   = $groupRepository->find($group);
        if (! $group) {
            return false;
        }

        if (! $actions) {
            $routes = $this->generateRoute();
            foreach (array_keys($routes) as $code) {
                if ($code == $module) {
                    $actions = new $actionEntity();
                    $actions->setModule($module);
                    $actions->setRefGroup($group);
                    break;
                }
            }
        }

        if ($actions) {
            $codes   = $actions->getCodes();
            $newcode = [];
            if ($codes != '') {
                $newcode = explode(',', $codes);
            }

            $codes   = $newcode;
            $codes   = $this->setRoute($action, $module, $codes);
            $newcode = '';
            if (count($codes) != 0) {
                $newcode = implode(',', $codes);
            }

            $actions->setCodes($newcode);
            $actionManager->persistAndFlush($actions);
        }
    }

    /**
     * Sauvegarde les droits.
     *
     * @param string $action si c'est pour toute une colonne ou une case spécifique
     * @param string $module code du module
     * @param array  $codes  tableau des droits
     *
     * @return array
     */
    public function setRoute($action, $module, $codes)
    {
        if ($action == 'all') {
            $json = $this->setActionAll($module, $codes);

            return $json;
        }

        $json = $this->setActionOther($codes, $action);

        return $json;
    }

    /**
     * Génère la liste des routes.
     *
     * @return array
     */
    public function generateRoute()
    {
        $routes = [];
        foreach (array_keys($this->getRoutes()) as $tab) {
            list($module, $action) = explode('_', $tab);
            if ($module != 'besearch'
                && substr_count($action, 'search') == 0
                && substr_count($action, 'upload') == 0
                && substr_count($action, 'vider') == 0
                && substr_count($action, 'page') == 0
                && substr_count($action, 'json') == 0
            ) {
                if (! isset($routes[$module])) {
                    $routes[$module] = '';
                } else {
                    $routes[$module] = $routes[$module] . ',';
                }

                $routes[$module] = $routes[$module] . $action;
            }
        }

        $list   = $routes;
        $routes = [];
        foreach ($list as $module => $action) {
            $routes[] = [
                'module' => $module,
                'action' => explode(',', $action),
            ];
        }

        ksort($routes);

        return $routes;
    }

    /**
     * Enregistre les routes (cas une case).
     *
     * @param array  $codes  tableau des routes
     * @param string $action fin de la route
     *
     * @return array
     */
    private function setActionOther($codes, $action)
    {
        $trouver = 0;
        foreach ($codes as $code) {
            if ($action == $code) {
                $trouver = 1;
                break;
            }
        }

        if ($trouver == 0) {
            $codes[] = $action;

            return $codes;
        }

        $list  = $codes;
        $codes = [];
        foreach ($list as $key => $tab) {
            if ($tab != $action) {
                $codes[$key] = $tab;
            }
        }

        return $codes;
    }

    /**
     * Enregistre les routes (cas toute la colonne).
     *
     * @param string $module module qui prendra en compte ses modifs de droits
     * @param array  $codes  liste des droits
     *
     * @return array
     */
    private function setActionAll($module, $codes)
    {
        $routes = '';
        foreach (array_keys($this->getRoutes()) as $tab) {
            list($choix, $action) = explode('_', $tab);
            if ($module == $choix) {
                if ($routes != '') {
                    $routes = $routes . ',';
                }

                $routes = $routes . $action;
            }
        }

        $routes   = explode(',', $routes);
        $newcodes = [];
        if (count($routes) != count($codes)) {
            $newcodes = $routes;
        }

        return $newcodes;
    }
}
