<?php

namespace Mkk\AdminBundle\Controller\Backend;

trait DebugTrait
{
    public function getPhpmd()
    {
        $folders = glob('../src/*');
        $phpmd   = [];
        foreach ($folders as $folder) {
            $file = $folder . '/phpmd.xml';
            if (is_file($file)) {
                $tab = $this->traiterXmlPhpMd($file);
                if ($tab['violator'] != 0) {
                    $phpmd[$folder] = $tab;
                }
            }
        }

        return $phpmd;
    }

    public function getPhpcpd()
    {
        $folders = glob('../src/*');
        $phpmd   = [];
        foreach ($folders as $folder) {
            $file = $folder . '/phpcpd.log';
            if (is_file($file)) {
                $tab = file_get_contents($file);
                if (substr_count($tab, '0.00% duplicated') == 0) {
                    $phpmd[$folder] = $tab;
                }
            }
        }

        return $phpmd;
    }

    public function getPhpcs()
    {
        $folders = glob('../src/*');
        $phpmd   = [];
        foreach ($folders as $folder) {
            $file = $folder . '/phpcs.json';
            if (is_file($file)) {
                $json   = file_get_contents($file);
                $tab    = json_decode($json, true);
                $oldtab = $tab;
                foreach (array_keys($oldtab['files']) as $file) {
                    $dossier                = str_replace('..', '', $folder);
                    $newfile                = substr($file, strpos($file, $dossier) + strlen($dossier) + 1);
                    $tab['files'][$newfile] = $oldtab['files'][$file];
                    unset($tab['files'][$file]);
                    if ($tab['files'][$newfile]['errors'] == 0 && $tab['files'][$newfile]['warnings'] == 0) {
                        unset($tab['files'][$newfile]);
                    }
                }
                $phpmd[$folder] = $tab;
            }
        }

        return $phpmd;
    }

    private function traiterXmlPhpMd($file)
    {
        $tab      = [
            'files'    => [],
            'violator' => 0,
        ];
        $contents = file_get_contents($file);
        if (strlen($contents) != 0) {
            $objet = simplexml_load_file($file);
            foreach ($objet->file as $file) {
                $attributes = $file->attributes();
                $fileName   = $attributes->name;
                $fileName   = substr($fileName, strpos($fileName, 'src/') + strlen('src/'));
                $fileName   = substr($fileName, strpos($fileName, '/') + 1);

                $tab['files'][$fileName] = [];
                foreach ($file->violation as $violation) {
                    $attributes                = $violation->attributes();
                    $data                      = [
                        'text'     => $violation->__toString(),
                        'priority' => $attributes->priority->__toString(),
                        'ruleset'  => $attributes->ruleset->__toString(),
                        'rule'     => $attributes->rule->__toString(),
                        'ligne'    => $attributes->beginline->__toString(),
                        'url'      => $attributes->externalInfoUrl->__toString(),
                    ];
                    $tab['files'][$fileName][] = $data;
                }
            }
            $total = 0;
            foreach ($tab['files'] as $data) {
                $total = $total + count($data);
            }
            $tab['violator'] = $total;
        }

        return $tab;
    }
}
