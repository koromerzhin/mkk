<?php

namespace Mkk\AdminBundle\Controller\Backend;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

trait ActionTrait
{
    /**
     * Activé ou désactivé une ligne.
     *
     * @param string $table       Entité
     * @param string $search      champs de recherche
     * @param string $fonction    Fonction qui correction a l'action
     * @param array  $possibilite Différentes possibilités
     *
     * @return JsonResponse ou redirect
     */
    private function actionActivate(Request $request, $table, $search, $fonction, $possibilite = [])
    {
        $table             = strtolower($table);
        $code              = 'bdd.' . $table . '_manager';
        $tableManager      = $this->get($code);
        $tableRepository   = $tableManager->getRepository();
        $post['etat']      = $request->request->get('etat');
        $post['selection'] = $request->request->get('selection');
        if (! $this->isPostAjax($request) || $post['selection'] == '') {
            $url      = $this->generateUrl('beuser_index');
            $redirect = $this->redirect($url, 301);

            return $redirect;
        }

        $responsejson = ['supprimer' => 0];
        $selection    = explode(',', $post['selection']);
        foreach ($selection as $id) {
            $data = $tableRepository->findby([$search => $id]);
            if (isset($possibilite['get'])) {
                $this->actionSelectionPossibilite($data, $possibilite, $fonction, $post);
            } else {
                $this->actionSelection($data, $fonction, $post);
            }
        }

        $this->getDoctrine()->getManager()->flush();
        $jsonresponse = new JsonResponse($responsejson);

        return $jsonresponse;
    }

    /**
     * Duplique une ligne ou plusieurs.
     *
     * @param string $table  table qui subit la suppression
     * @param string $retour route en cas d'erreur
     *
     * @return JsonResponse|Redirect
     */
    private function actionDuplicate(Request $request, $table, $retour)
    {
        set_time_limit(0);
        $post['selection'] = $request->request->get('selection');
        $responsejson      = ['duplicate' => 0];
        if ($this->isPostAjax($request)) {
            try {
                $responsejson = $this->tryDuplicate($table, $post, $responsejson);
            } catch (Exception $e) {
                if ($this->debugAdmin()) {
                    $responsejson['message'] = $e->getMessage();
                }

                $responsejson['duplicate'] = 2;
            }
        } else {
            $url      = $this->generateUrl($retour);
            $redirect = $this->redirect($url, 301);

            return $redirect;
        }

        $jsonresponse = new JsonResponse($responsejson);

        return $jsonresponse;
    }

    /**
     * Try delete.
     *
     * @param string $table        table qui subit la suppression
     * @param string $search       champs de recherche
     * @param string $post         post
     * @param string $responsejson array de retour
     *
     * @return array
     */
    private function tryDelete($table, $search, $post, $responsejson)
    {
        $table           = strtolower($table);
        $code            = 'bdd.' . $table . '_manager';
        $tableManager    = $this->get($code);
        $tableRepository = $tableManager->getRepository();
        set_time_limit(0);
        $batchSize = 5;
        if ($post['id'] != '') {
            $selection = [$post['id']];
        } elseif ($post['selection'] != '') {
            $selection = explode(',', $post['selection']);
        }

        $i = 0;
        foreach ($selection as $id) {
            $data = $tableRepository->findby([$search => $id]);
            foreach ($data as $entity) {
                ++$i;
                $responsejson['supprimer'] = 1;
                $tableManager->remove($entity);
                if (($i % $batchSize) == 0) {
                    $tableManager->flush();
                }
            }
        }

        $tableManager->flush();

        return $responsejson;
    }

    /**
     * Try duplicate.
     *
     * @param string $table        table qui subit la suppression
     * @param string $post         post
     * @param string $responsejson array de retour
     *
     * @return array
     */
    private function tryDuplicate($table, $post, $responsejson)
    {
        $table           = strtolower($table);
        $code            = 'bdd.' . $table . '_manager';
        $tableManager    = $this->get($code);
        $tableRepository = $tableManager->getRepository();
        set_time_limit(0);
        $em = $this->getDoctrine()->getManager();

        $param     = $this->get('mkk.param_service')->listing();
        $batchSize = 5;
        if ($post['selection'] != '') {
            $selection = explode(',', $post['selection']);
        }

        $i = 0;
        foreach ($selection as $id) {
            $data = $tableRepository->findby(['id' => $id]);
            foreach ($data as $entity) {
                ++$i;
                $responsejson['duplicate'] = 1;
                $copy                      = clone $entity;
                $methods                   = get_class_methods($copy);
                if (in_array('getAlias', $methods)) {
                    $copy->setAlias('');
                }

                if (in_array('getActif', $methods)) {
                    $copy->setActif(0);
                }

                if (in_array('getActifpublic', $methods)) {
                    $copy->setActifpublic(0);
                }

                if (in_array('getActifPublic', $methods)) {
                    $copy->setActifPublic(0);
                }

                if (in_array('getLangue', $methods) && isset($param[strtolower($table) . 'langue_dispo'])) {
                    $languedispo = $param[strtolower($table) . 'langue_dispo'];
                    $langue      = $copy->getLangue();
                    if (count($languedispo) == 2) {
                        foreach ($languedispo as $newlangue) {
                            if ($newlangue != $langue) {
                                $copy->setLangue($newlangue);
                            }
                        }
                    } else {
                        $copy->setLangue('');
                    }
                }

                $em->persist($copy);
                if (($i % $batchSize) == 0) {
                    $em->flush();
                }
            }
        }

        $this->getDoctrine()->getManager()->flush();

        return $responsejson;
    }

    /**
     * Activation / desactivation pour une selection quand possibilité est remplit.
     *
     * @param array  $data        liste de la selection
     * @param array  $possibilite Différentes possibilités
     * @param string $fonction    Fonction qui correction a l'action
     * @param array  $post        Pile de requête
     *
     * @return nothing
     */
    private function actionSelectionPossibilite($data, $possibilite, $fonction, $post)
    {
        $batchSize = 5;
        foreach ($data as $i => $entity) {
            $avant = $possibilite['get'];
            $etat  = $entity->$avant();
            if (in_array($post['etat'], array_flip($possibilite['data'][$etat]))) {
                $entity->$fonction($post['etat']);
                $this->getDoctrine()->getManager()->persist($entity);
                if (($i % $batchSize) == 0) {
                    $this->getDoctrine()->getManager()->flush();
                }

                if (isset($possibilite['after'])) {
                    $after = $possibilite['after'];
                    $possibilite['controller']->$after($entity);
                }
            }

            $this->getDoctrine()->getManager()->persist($entity);
            if (($i % $batchSize) == 0) {
                $this->getDoctrine()->getManager()->flush();
            }
        }
    }

    /**
     * Activation / desactivation pour une selection.
     *
     * @param array  $data     liste de la selection
     * @param string $fonction Fonction qui correction a l'action
     * @param array  $post     Pile de requête
     *
     * @return nothing
     */
    private function actionSelection($data, $fonction, $post)
    {
        $batchSize = 5;
        foreach ($data as $i => $entity) {
            $entity->$fonction($post['etat']);
            $this->getDoctrine()->getManager()->persist($entity);
            if (($i % $batchSize) == 0) {
                $this->getDoctrine()->getManager()->flush();
            }
        }
    }
}
