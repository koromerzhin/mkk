<?php

namespace Mkk\AdminBundle\Controller\Bookmark;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

trait UploadTrait
{
    /**
     * @Route("/bookmark/image/{md5}", name="bebookmark_uploadimage")
     *
     * Upload d'une image d'un article du blog
     *
     * @param Request $request pile de requetes
     * @param Hash    $md5     Hash de l'image'
     *
     * @return Response
     */
    public function bookmarkImageAction()
    {
        $options = [
            'max_number_of_files' => 1,
        ];

        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }
}
