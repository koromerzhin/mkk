<?php

namespace Mkk\AdminBundle\Controller\Bookmark;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

trait JsonTrait
{
    /**
     * @Route("/bookmark/json/tagselection/{id}", name="bebookmark_json-tagselection")
     *
     * Selection des tags
     *
     * @param int $id Identifiant de la table blog
     *
     * @return json
     */
    public function bookmarkTagSelectionAction($id)
    {
        $bookmarkManager    = $this->get('bdd.bookmark_manager');
        $bookmarkRepository = $bookmarkManager->getRepository();
        $tagManager         = $this->get('bdd.tag_manager');
        $tagRepository      = $tagManager->getRepository();
        $tab['all']         = [];
        $tab['selected']    = [];
        $tags               = $tagRepository->findAll();
        foreach ($tags as $key => $tag) {
            $tab['all'][$key] = $tag->getNom();
        }

        if ($id != 0) {
            $bookmark = $bookmarkRepository->find($id);
            foreach ($bookmark->getTags() as $key => $tag) {
                $tab['selected'][$key] = $tag->getNom();
            }
        }

        $json = new JsonResponse($tab);

        return $json;
    }
}
