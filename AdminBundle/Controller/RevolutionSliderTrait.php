<?php

namespace Mkk\AdminBundle\Controller;

use Mkk\AdminBundle\Controller\RevolutionSlider\JsonTrait;
use Mkk\AdminBundle\Controller\RevolutionSlider\UploadTrait;
use Mkk\AdminBundle\Form\RevolutionSlider\ParamType as SliderParamType;
use Mkk\AdminBundle\Form\RevolutionSlider\Slide\ImageType as SlideImageType;
use Mkk\AdminBundle\Form\RevolutionSlider\Slide\LayerType as SlideLayerType;
use Mkk\AdminBundle\Form\RevolutionSlider\Slide\ParamType as SlideParamType;
use Mkk\AdminBundle\Form\RevolutionSlider\Slide\SlideType;
use Mkk\AdminBundle\Form\RevolutionSlider\SliderType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Intl\Intl;

trait RevolutionSliderTrait
{
    use JsonTrait;
    use UploadTrait;

    /**
     * @Route("/revolution-slider/", name="berevolutionslider_index", defaults={"page": 0})
     * @Route("/revolution-slider/{page}.html", name="berevolutionslider_page")
     *
     * Liste des sliders
     *
     * @param Request $request pile de requêtes
     * @param int     $page    numéro de la page
     *
     * @return RevolutionSlider:index.html.twig
     */
    public function revolutionSliderIndexAction(Request $request)
    {
        $listingService = $this->get('mkk.listing_service');
        $listingService->setRequest('bdd.revolutionslider_manager', 'searchRevolutionSlider');
        $actions = [
            [
                'url'  => 'berevolutionslider_form',
                'text' => 'Ajouter',
                'img'  => 'glyphicon glyphicon-plus',
            ],
        ];
        $this->get('mkk.action_service')->set($actions);
        $twigIndex = 'MkkAdminBundle:RevolutionSlider:Slider/index.html.twig';
        $twigHtml  = $this->renderMkk($request, $twigIndex);

        return $twigHtml;
    }

    /**
     * @Route("/revolution-slider/form/{id}", name="berevolutionslider_form", defaults={"id": 0})
     *
     * Formulaire d'un slider.
     *
     * @param Request $request pile de requêtes
     * @param int     $id      Identifiant du slider
     *
     * @return JsonResponse / RevolutionSlider:form.html.twig
     */
    public function revolutionSliderFormAction(Request $request, $id)
    {
        $revolutionsliderManager    = $this->get('bdd.revolutionslider_manager');
        $revolutionsliderRepository = $revolutionsliderManager->getRepository();
        $revolutionsliderEntity     = $revolutionsliderManager->getTable();
        $etat                       = 'modifier';
        $slider                     = $revolutionsliderRepository->find($id);
        if (! $slider) {
            $slider = new $revolutionsliderEntity();
            $etat   = 'ajouter';
        }

        $param       = $this->get('mkk.param_service')->listing();
        $languedispo = [];
        $locale      = $request->getLocale();
        $language    = Intl::getLanguageBundle()->getLanguageNames($locale);
        if (! isset($param['languesite'])) {
            $flashbag = $request->getSession()->getFlashBag();
            $flashbag->add('warning', 'Langue du site non configuré');
            $url      = $this->generateUrl('bord_index');
            $redirect = $this->redirect($url, 301);

            return $redirect;
        }

        $bloglangueDispo = $param['languesite'];
        foreach ($bloglangueDispo as $code) {
            $nom               = $language[$code];
            $languedispo[$nom] = $code;
        }

        $options['languedispo'] = $languedispo;

        $formRevolutionSlider      = $this->createForm(SliderType::class, $slider, $options);
        $formRevolutionSliderParam = $this->createForm(SliderParamType::class, $slider->getParam());
        $actions                   = [
            [
                'type' => 'return',
                'url'  => 'berevolutionslider_index',
            ],
            [
                'type' => 'save',
                'url'  => 'berevolutionslider_form',
                'data' => ['id' => $id],
            ],
        ];

        $this->get('mkk.action_service')->set($actions);
        $param = [
            'slider'          => $slider,
            'formslider'      => $formRevolutionSlider->createView(),
            'formsliderparam' => $formRevolutionSliderParam->createView(),
        ];

        $twigIndex = 'MkkAdminBundle:RevolutionSlider:Slider/form.html.twig';
        $twigHtml  = $this->renderMkk($request, $twigIndex, $param);

        return $twigHtml;
    }

    /**
     * @Route("/revolution-slider/{id}/slides", name="berevolutionslider_slides")
     *
     * Slides d'un slider.
     *
     * @param Request $request pile de requêtes
     * @param int     $id      Identifiant du slider
     *
     * @return JsonResponse / RevolutionSlider:form.html.twig
     */
    public function revolutionSliderSlidesAction(Request $request, $id)
    {
        $revolutionsliderManager    = $this->get('bdd.revolutionslider_manager');
        $revolutionsliderRepository = $revolutionsliderManager->getRepository();
        $slider                     = $revolutionsliderRepository->find($id);
        $slides                     = $slider->getRevolutionSliderSlides();

        $actions = [
            [
                'type' => 'return',
                'url'  => 'berevolutionslider_index',
            ],
            [
                'type' => 'add',
                'url'  => 'berevolutionslider_slideform',
                'data' => ['slider' => $id],
            ],
            [
                'url'  => 'berevolutionslider_slidesposition',
                'text' => 'Enregistrer les positions',
                'data' => ['slider' => $id],
                'img'  => 'glyphicon-floppy-saved',
                'id'   => 'BoutonPosition',
            ],
            [
                'id'   => 'BoutonDupliquer',
                'url'  => 'berevolutionslider_slidesduplicate',
                'data' => ['slider' => $id],
                'text' => 'Dupliquer',
                'img'  => 'glyphicon-retweet',
            ],
            [
                'type' => 'actif',
                'url'  => 'berevolutionslider_slidesactif',
                'data' => ['slider' => $id],
            ],
            [
                'type' => 'desactif',
                'url'  => 'berevolutionslider_slidesactif',
                'data' => ['slider' => $id],
            ],
            [
                'type' => 'delete',
                'url'  => 'berevolutionslider_slidesdelete',
                'data' => ['slider' => $id],
            ],
        ];

        $this->get('mkk.action_service')->set($actions);
        $param = [
            'slider' => $slider,
            'slides' => $slides,
        ];

        $twigIndex = 'MkkAdminBundle:RevolutionSlider:Slider/slides.html.twig';
        $twigHtml  = $this->renderMkk($request, $twigIndex, $param);

        return $twigHtml;
    }

    /**
     * @Route("/revolution-slider/{slider}/slides/position", name="berevolutionslider_slidesposition")
     *
     * Positions des slides
     *
     * @param Request $request pile de requêtes
     *
     * @return JsonResponse
     */
    public function revolutionSliderSlidesPositionAction(Request $request)
    {
        $revolutionsliderslideManager    = $this->get('bdd.revolutionsliderslide_manager');
        $revolutionsliderslideRepository = $revolutionsliderslideManager->getRepository();
        $responsejson                    = [
            'enregistrer' => true,
        ];

        $post      = $request->request->all();
        $batchSize = 5;
        $i         = 0;
        $ids       = explode(',', $post['slides']);
        foreach ($ids as $key => $id) {
            $slide = $revolutionsliderslideRepository->find($id);
            if ($slide) {
                $slide->setPosition($key + 1);
                $revolutionsliderslideManager->persist($slide);
                ++$i;
                if (($i % $batchSize) == 0) {
                    $revolutionsliderslideManager->flush();
                }
            }
        }

        $revolutionsliderslideManager->flush();
        $jsonResponse = new JsonResponse($responsejson);

        return $jsonResponse;
    }

    /**
     * @Route("/revolution-slider/{slider}/slides/actif", name="berevolutionslider_slidesactif")
     *
     * Active/désactive un partenaire
     *
     * @return JsonResponse
     */
    public function revolutionSliderSlidesActifAction(Request $request)
    {
        $json = $this->actionActivate($request, 'RevolutionSliderSlide', 'id', 'setActifPublic');

        return $json;
    }

    /**
     * @Route("/revolution-slider/{slider}/slides/duplicate", name="berevolutionslider_slidesduplicate")
     *
     * Duplication un slider
     *
     * @param int $slider Identifiant du slider
     *
     * @return JsonResponse
     */
    public function revolutionSliderSlidesDuplicateAction(Request $request)
    {
        $json = $this->actionDuplicate($request, 'RevolutionSliderSlide', 'berevolutionslider_slides');

        return $json;
    }

    /**
     * @Route("/revolution-slider/{slider}/slide/form/{id}", name="berevolutionslider_slideform", defaults={"id": 0})
     *
     * Formulaire d'un slide.
     *
     * @param Request $request pile de requêtes
     * @param int     $slider  Identifiant du slider
     * @param int     $id      Identifiant du slide
     *
     * @return JsonResponse / RevolutionSlide:form.html.twig
     */
    public function revolutionSliderSlideFormAction(Request $request, $slider, $id)
    {
        $revolutionsliderManager         = $this->get('bdd.revolutionslider_manager');
        $revolutionsliderRepository      = $revolutionsliderManager->getRepository();
        $revolutionsliderslideManager    = $this->get('bdd.revolutionsliderslide_manager');
        $revolutionsliderslideRepository = $revolutionsliderslideManager->getRepository();
        $revolutionsliderslideEntity     = $revolutionsliderslideManager->getTable();
        $etat                            = 'modifier';
        $slide                           = $revolutionsliderslideRepository->find($id);
        if (! $slide) {
            $slide = new $revolutionsliderslideEntity();
            $etat  = 'ajouter';
        }

        $formslide      = $this->createForm(SlideType::class, $slide);
        $formslideparam = $this->createForm(SlideParamType::class, $slide->getParam());
        $formslideimage = $this->createForm(SlideImageType::class, $slide->getParamimage());
        $actions        = [
                [
                    'type' => 'return',
                    'url'  => 'berevolutionslider_slides',
                    'data' => ['id' => $slider],
                ],
                [
                    'type' => 'save',
                    'url'  => 'berevolutionslider_slideform',
                    'data' => [
                        'slider' => $slider,
                        'id'     => $id,
                    ],
                ],
            ];
        $this->get('mkk.action_service')->set($actions);

        $param = [
                'id'             => $id,
                'slider'         => $slider,
                'entity'         => $slide,
                'formslide'      => $formslide->createView(),
                'formslideparam' => $formslideparam->createView(),
                'formslideimage' => $formslideimage->createView(),
            ];

        $twigIndex = 'MkkAdminBundle:RevolutionSlider:Slide/form.html.twig';
        $twigHtml  = $this->renderMkk($request, $twigIndex, $param);

        return $twigHtml;
    }

    /**
     * @Route("/revolution-slider/slide/{id}/layers", name="berevolutionslider_slidelayers")
     *
     * Liste des layers d'un slide.
     *
     * @param Request $request pile de requêtes
     * @param int     $id      Identifiant du slide
     *
     * @return JsonResponse / RevolutionSlide:form.html.twig
     */
    public function revolutionSliderSlideLayersAction(Request $request, $id)
    {
        $revolutionsliderslideManager    = $this->get('bdd.revolutionsliderslide_manager');
        $revolutionsliderslideRepository = $revolutionsliderslideManager->getRepository();
        $slide                           = $revolutionsliderslideRepository->find($id);
        $slider                          = $slide->getRefRevolutionslider();

        $param = [
            'entity' => $slide,
            'slider' => $slider->getId(),
        ];

        $actions = [
            [
                'type' => 'return',
                'url'  => 'berevolutionslider_slides',
                'data' => ['id' => $slider->getId()],
            ],
            [
                'type' => 'add',
                'url'  => 'berevolutionslider_slidelayertext',
                'data' => [
                    'id' => $id,
                ],
            ],
            [
                'id'   => 'BoutonPosition',
                'url'  => 'berevolutionslider_slidelayerposition',
                'text' => 'Enregistrer les positions',
                'img'  => 'glyphicon glyphicon-floppy-save',
                'data' => ['id' => $slide->getId()],
            ],
            [
                'type' => 'delete',
                'url'  => 'berevolutionslider_slidelayerdelete',
                'data' => ['id' => $id],
            ],
        ];
        $this->get('mkk.action_service')->set($actions);
        $twigIndex = 'MkkAdminBundle:RevolutionSlider:Slide/layers.html.twig';
        $twigHtml  = $this->renderMkk($request, $twigIndex, $param);

        return $twigHtml;
    }

    /**
     * @Route("/revolution-slider/slide/{id}/layer/text/{order}", name="berevolutionslider_slidelayertext", defaults={"order": ""})
     *
     * Ajout / modification d'un layer d'un slide
     *
     * @param Request $request pile de requêtes
     * @param int     $id      Identifiant du slide
     * @param int     $order   Classement du layer
     *
     * @return JsonResponse / RevolutionSlide:form.html.twig
     */
    public function revolutionSliderSlideLayerTextAction(Request $request, $id, $order)
    {
        $revolutionsliderslideManager    = $this->get('bdd.revolutionsliderslide_manager');
        $revolutionsliderslideRepository = $revolutionsliderslideManager->getRepository();
        $slide                           = $revolutionsliderslideRepository->find($id);
        $form                            = $this->createForm(SlideLayerType::class);
        $layers                          = $slide->getLayers();
        if ($order != '') {
            $form = $this->createForm(SlideLayerType::class, $layers[$order]);
        }

        $tablayer = $request->request->get('slidelayer');
        if ($this->isPostAjax($request) && $tablayer) {
            if ($order != '') {
                $layers[$order] = $tablayer;
            } else {
                $layers[] = $tablayer;
            }

            $slide->setLayers(json_encode($layers));
            $revolutionsliderslideManager->persistAndFlush($slide);
            $html = '';
        } else {
            $html = $this->get('templating')->render(
                'MkkAdminBundle:RevolutionSlider:Slide/tmpl/json/form-layer-texte.html.twig',
                [
                    'form'  => $form->createView(),
                    'slide' => $slide,
                ]
            );
        }

        $jsonResponse = new JsonResponse($html);

        return $jsonResponse;
    }

    /**
     * @Route("/revolution-slider/slide/{id}/layer/delete", name="berevolutionslider_slidelayerdelete")
     *
     * Supression d'un layer d'un slide.
     *
     * @param Request $request pile de requêtes
     * @param int     $id      Identifiant du slide
     *
     * @return JsonResponse / RevolutionSlide:form.html.twig
     */
    public function revolutionSliderSlideLayerDeleteAction(Request $request, $id)
    {
        $revolutionsliderslideManager    = $this->get('bdd.revolutionsliderslide_manager');
        $revolutionsliderslideRepository = $revolutionsliderslideManager->getRepository();
        $selection                       = explode(',', $request->request->get('selection'));
        $slide                           = $revolutionsliderslideRepository->find($id);
        $layers                          = $slide->getLayers();
        foreach ($selection as $value) {
            unset($layers[$value]);
        }

        $layers = array_values($layers);
        $slide->setLayers(json_encode($layers));
        $revolutionsliderslideManager->persistAndFlush($slide);
        $html         = '';
        $jsonResponse = new JsonResponse($html);

        return $jsonResponse;
    }

    /**
     * @Route("/revolution-slider/slide/{id}/layer/position", name="berevolutionslider_slidelayerposition")
     *
     * Sauvegarde des positions des layers d'un slide.
     *
     * @param Request $request pile de requêtes
     * @param int     $id      Identifiant du slide
     *
     * @return JsonResponse / RevolutionSlide:form.html.twig
     */
    public function revolutionSliderSlideLayerPositionAction(Request $request, $id)
    {
        $revolutionsliderslideManager    = $this->get('bdd.revolutionsliderslide_manager');
        $revolutionsliderslideRepository = $revolutionsliderslideManager->getRepository();
        $slide                           = $revolutionsliderslideRepository->find($id);
        $positions                       = $request->request->get('position');
        $layers                          = $slide->getLayers();
        foreach ($positions as $value) {
            $l[] = $layers[$value];
        }

        $slide->setLayers(json_encode($l));
        $revolutionsliderslideManager->persistAndFlush($slide);
        $html         = '';
        $jsonResponse = new JsonResponse($html);

        return $jsonResponse;
    }
}
