<?php

namespace Mkk\AdminBundle\Controller;

use Mkk\AdminBundle\Controller\Bugs\ApiTrait;
use Mkk\AdminBundle\Controller\Bugs\SearchTrait;
use Mkk\AdminBundle\Form\Bugs\AjouterType;
use Mkk\AdminBundle\Form\Bugs\ModifierType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

trait BugsTrait
{
    use SearchTrait;
    use ApiTrait;

    /**
     * @Route("/bugs/modif/{id}", name="bebugs_modif")
     *
     * @param mixed $id
     */
    public function bugsModifAction(Request $request, $id)
    {
        $this->bugsInit();
        $json         = [];
        $bugspost     = $request->request->get('bugs');
        $trackers     = $this->status();
        $listTrackers = [];
        foreach ($trackers as $tracker) {
            $codeId                = $tracker['id'];
            $listTrackers[$codeId] = $tracker['name'];
        }

        if (isset($bugspost['status_id'])) {
            $codeId = $bugspost['status_id'];
            $this->update(
                $id,
                [
                    'status_id' => $codeId,
                ]
            );
        }

        if (isset($bugspost['notes']) && $bugspost['notes'] != '') {
            $this->update(
                $id,
                [
                    'notes' => $bugspost['notes'],
                ]
            );
        }

        $json['modifier'] = 1;
        $json['url']      = $this->generateUrl('bebugs_issue', ['id' => $id]);

        return new JsonResponse($json);
    }

    /**
     * @Route("/bugs/create", name="bebugs_create")
     */
    public function bugsCreateAction(Request $request)
    {
        $userManager    = $this->get('bdd.user_manager');
        $userRepository = $userManager->getRepository();
        $this->bugsInit();
        $json         = [];
        $bugspost     = $request->request->get('bugs');
        $user         = $this->get('security.token_storage')->getToken()->getUser();
        $pour         = $userRepository->find($bugspost['pour']);
        $customFields = [
            [
                'id'    => 2,
                'name'  => 'url',
                'value' => $bugspost['url'],
            ],
            [
                'id'    => 3,
                'name'  => 'userid',
                'value' => $pour->getId(),
            ],
            [
                'id'    => 6,
                'name'  => 'usernom',
                'value' => $pour->getAppel(),
            ],
            [
                'id'    => 4,
                'name'  => 'groupeid',
                'value' => $pour->getRefGroup()->getId(),
            ],
            [
                'id'    => 7,
                'name'  => 'groupenom',
                'value' => $pour->getRefGroup()->getId(),
            ],
            [
                'id'    => 5,
                'name'  => 'creerid',
                'value' => $user->getId(),
            ],
            [
                'id'    => 9,
                'name'  => 'creernom',
                'value' => $user->getAppel(),
            ],
        ];

        $envoyer = [
            'tracker_id'    => $bugspost['tracker'],
            'priority_id'   => $bugspost['priorite'],
            'subject'       => $bugspost['sujet'],
            'description'   => $bugspost['description'],
            'custom_fields' => $customFields,
        ];
        $this->create($envoyer);
        $json['ajouter'] = 1;
        $json['url']     = $this->generateUrl('bebugs_index');
        $jsonresponse    = new JsonResponse($json);

        return $jsonresponse;
    }

    /**
     * @Route("/bugs/", name="bebugs_index", defaults={"page": 0})
     * @Route("/bugs/{page}.html", name="bebugs_page")
     *
     * @param mixed $page
     */
    public function bugsIndexAction(Request $request)
    {
        $this->bugsInit();
        $param   = [];
        $limit   = 25;
        $page    = $request->query->get('page');
        $combien = $request->query->get('combien');
        if ($combien != '' && $combien >= 5 && $combien <= 50) {
            $limit = $combien;
        }

        $param['limit']  = $limit;
        $param['offset'] = ($page - 1) * $limit;
        if ($page <= 1) {
            $param['offset'] = 0;
        }

        $searchparams = $request->query->all();
        $options      = [];
        $trackers     = $this->trackers();
        foreach ($trackers as $tracker) {
            $id                       = $tracker['id'];
            $options['trackers'][$id] = $tracker['name'];
        }

        $listing = $this->status();
        foreach ($listing as $status) {
            $id                     = $status['id'];
            $options['status'][$id] = $status['name'];
        }

        $listing = $this->priorities();
        foreach ($listing as $priority) {
            $id                       = $priority['id'];
            $options['priority'][$id] = $priority['name'];
        }
        $param              = array_merge($request->query->all(), $param);
        $liste              = $this->issues($param);
        $data               = [];
        $data['pagination'] = $this->paginationTraitement($liste['total_count'], $page, $liste['limit']);

        $actions = [
            ['type' => 'add', 'url' => 'bebugs_create'],
        ];
        $this->get('mkk.action_service')->set($actions);
        $user    = $this->get('security.token_storage')->getToken()->getUser();
        $options = [
            'trackers'   => [],
            'priorities' => [],
            'pour'       => $user->getId(),
        ];

        $trackers = $this->trackers();
        foreach ($trackers as $tracker) {
            $id = $tracker['id'];
            if (in_array($tracker['id'], [1, 2, 7, 6])) {
                $options['trackers'][$id] = $tracker['name'];
            }
        }

        $priorities = $this->priorities();
        foreach ($priorities as $prioritie) {
            $id                         = $prioritie['id'];
            $options['priorities'][$id] = $prioritie['name'];
        }

        $form = $this->createForm(AjouterType::class, [], $options);
        foreach ($liste['issues'] as $idssue => $issue) {
            foreach (['created', 'updated', 'closed'] as $code) {
                $code = $code . '_on';
                if (isset($issue[$code])) {
                    $temps = '';
                    if (substr_count($issue[$code], 'Z') != 0) {
                        list($jour, $heure)             = explode('T', $issue[$code]);
                        list($annee, $mois, $jour)      = explode('-', $jour);
                        list($heure, $minute, $seconde) = explode(':', str_replace('Z', '', $heure));
                        $temps                          = mktime($heure, $minute, $seconde, $mois, $jour, $annee);
                    }

                    $liste['issues'][$idssue][$code] = $temps;
                }
            }
        }

        $listingService->setSearch('admin.search.bugs', $options);
        $twig = 'MkkAdminBundle:Bugs:index.html.twig';
        $html = $this->renderMkk(
            $request,
            $twig,
            [
                'issues'   => $liste['issues'],
                'formbugs' => $form->createView(),
            ]
        );

        return $html;
    }

    /**
     * @Route("/bugs/issue/{id}", name="bebugs_issue")
     *
     * @param mixed $id
     */
    public function bugsIssueAction(Request $request, $id)
    {
        $this->bugsInit();
        $info  = $this->issueshow($id);
        $issue = $info['issue'];
        foreach (['created', 'updated', 'closed'] as $code) {
            $code = $code . '_on';
            if (isset($issue[$code])) {
                $temps = '';
                if (substr_count($issue[$code], 'Z') != 0) {
                    list($jour, $heure)             = explode('T', $issue[$code]);
                    list($annee, $mois, $jour)      = explode('-', $jour);
                    list($heure, $minute, $seconde) = explode(':', str_replace('Z', '', $heure));
                    $temps                          = mktime($heure, $minute, $seconde, $mois, $jour, $annee);
                }

                $issue[$code] = $temps;
            }
        }

        $parsedown            = new \Parsedown();
        $issue['description'] = $parsedown->text(str_replace("\n", '<br />', $issue['description']));
        if (isset($issue['journals'])) {
            foreach ($issue['journals'] as $idjournal => $journal) {
                foreach (['created', 'updated', 'closed'] as $code) {
                    $code = $code . '_on';
                    if (isset($journal[$code])) {
                        $temps = '';
                        if (substr_count($journal[$code], 'Z') != 0) {
                            list($jour, $heure)             = explode('T', $journal[$code]);
                            list($annee, $mois, $jour)      = explode('-', $jour);
                            list($heure, $minute, $seconde) = explode(':', str_replace('Z', '', $heure));
                            $temps                          = mktime($heure, $minute, $seconde, $mois, $jour, $annee);
                        }

                        $issue['journals'][$idjournal][$code] = $temps;
                    }

                    if (isset($issue['journals'][$idjournal]['notes'])) {
                        $issue['journals'][$idjournal]['notes'] = $parsedown->text($issue['journals'][$idjournal]['notes']);
                    }
                }
            }
        }

        $listing  = $this->trackers();
        $trackers = [];
        foreach ($listing as $tracker) {
            $id            = $tracker['id'];
            $trackers[$id] = $tracker['name'];
        }

        $listing = $this->status();
        $status  = [];
        foreach ($listing as $tracker) {
            $id          = $tracker['id'];
            $status[$id] = $tracker['name'];
        }

        $data = [
            1 => [],
            2 => [],
        ];
        if (isset($issue['tracker'])) {
            $data[1][] = [
                'nom'   => 'Type de demande',
                'value' => $issue['tracker']['name'],
            ];
        }

        if (isset($issue['status'])) {
            $data[1][] = [
                'nom'   => 'Statut',
                'value' => $issue['status']['name'],
            ];
        }

        if (isset($issue['priority'])) {
            $data[1][] = [
                'nom'   => 'Priorité',
                'value' => $issue['priority']['name'],
            ];
        }

        if (isset($issue['custom_fields'])) {
            foreach ($issue['custom_fields'] as $custom) {
                if (isset($custom['value'])) {
                    $nom   = '';
                    $value = $custom['value'];
                    if ($custom['name'] == 'url') {
                        $nom = 'Url';
                        if (! filter_var($custom['value'], FILTER_VALIDATE_URL)) {
                            $nom = '';
                        }

                        $value = "<a href='" . $value . "' target='_new'>" . $value . '</a>';
                    } elseif ($custom['name'] == 'groupenom') {
                        $nom = 'Groupe utilisateur';
                    } elseif ($custom['name'] == 'usernom') {
                        $nom = 'Utilisateur';
                    }

                    if ($nom != '') {
                        $data[1][] = [
                            'nom'   => $nom,
                            'value' => $value,
                        ];
                    }
                }
            }
        }

        if (isset($issue['created_on'])) {
            $data[2][] = [
                'nom'   => 'Début',
                'value' => date('d/m/Y H:i:s', $issue['created_on']),
            ];
        }

        if (isset($issue['done_ratio'])) {
            $data[2][] = [
                'nom'   => '% réalisé',
                'value' => $issue['done_ratio'],
            ];
        }

        if (isset($issue['estimated_hours'])) {
            $value     = number_format($issue['estimated_hours'], 2, '.', ',') . ' heure(s)';
            $data[2][] = [
                'nom'   => 'Temps estimé',
                'value' => $value,
            ];
        }

        if (isset($issue['spent_hours'])) {
            $value     = number_format($issue['spent_hours'], 2, '.', ',') . ' heure(s)';
            $data[2][] = [
                'nom'   => 'Temps passé',
                'value' => $value,
            ];
        }

        $actions = [
            ['type' => 'return', 'url' => 'bebugs_index'],
            [
                'id'   => 'BoutonModifierStatus',
                'url'  => 'bebugs_modif',
                'img'  => 'glyphicon-floppy-saved',
                'text' => 'Modifier',
                'data' => ['id' => $issue['id']],
            ],
        ];
        $this->get('mkk.action_service')->set($actions);
        $options = [];
        if ($issue['status']['id'] == '7') {
            $options['idstatus'] = $issue['status']['id'];
            $options['status']   = [];
            $listing             = $this->status();
            foreach ($listing as $tracker) {
                $id = $tracker['id'];
                if ($id == 8 || $id == 5 || $id == 7) {
                    $options['status'][$id] = $tracker['name'];
                }
            }
        }

        $form = $this->createForm(ModifierType::class, [], $options);
        $twig = 'MkkAdminBundle:Bugs:issue.html.twig';
        $html = $this->renderMkk(
            $request,
            $twig,
            [
                'issue'     => $issue,
                'trackers'  => $trackers,
                'status'    => $status,
                'dataligne' => $data,
                'formbugs'  => $form->createView(),
            ]
        );

        return $html;
    }

    private function bugsInit()
    {
        $this->getParam();
        $this->initApi();
    }

    private function paginationTraitement($total, $page, $limit, $data = [])
    {
        $tab = [
            'max'       => $total,
            'data'      => $data,
            'limit'     => $limit,
            'offset'    => 0,
            'courant'   => $page,
            'precedent' => [],
            'suivant'   => [],
            'total'     => ceil($total / $limit),
        ];
        if ($page <= 1) {
            $tab['courant'] = 1;
            $tab['offset']  = 0;
        } elseif ($page >= ceil($total / $limit)) {
            $tab['offset']  = (ceil($total / $limit) - 1) * $limit;
            $tab['courant'] = ceil($total / $limit);
        } else {
            $tab['offset'] = ($page - 1) * $limit;
        }

        $avant = $tab['courant'] - 5;
        for ($i = $avant; $i < $tab['courant']; ++$i) {
            if ($i >= 1) {
                $tab['precedent'][$i] = $i;
            }
        }

        for ($i = $tab['courant'] + 1; $i <= $tab['total']; ++$i) {
            if ($i - $tab['courant'] <= 5) {
                $tab['suivant'][$i] = $i;
            } else {
                break;
            }
        }

        if ($tab['max'] == 0) {
            $tab['offset'] = 0;
        }

        return $tab;
    }
}
