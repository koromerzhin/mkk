<?php

namespace Mkk\AdminBundle\Controller\Bugs;

use Redmine\Client as RedmineClient;

trait ApiTrait
{
    public function getParam()
    {
        $param       = $this->get('mkk.param_service')->listing();
        $this->param = $param;
    }

    public function initApi()
    {
        $apikey     = $this->param['bug_code'];
        $identifier = $this->param['bug_project'];
        $url        = $this->param['bug_url'];
        if ($url != '' && $apikey != '' && $identifier != '') {
            $this->client = new RedmineClient($url, $apikey);
            $this->client->setCheckSslCertificate(true);
            $this->client->setPort(80);
            $this->trackers     = $this->client->api('tracker')->all();
            $this->issue_status = $this->client->api('issue_status')->all();
            $projets            = $this->client->api('project')->all();
            $retour             = $this->client->api('project')->all(['limit' => $projets['total_count']]);
            $this->projects     = $retour['projects'];
            foreach ($this->projects as $project) {
                if ($project['identifier'] == $identifier) {
                    $this->project = $project;
                }
            }
        }
    }

    public function issueshow($id)
    {
        $show = $this->client->api('issue')->show($id, ['include' => 'journals']);

        return $show;
    }

    public function trackers()
    {
        $tab = $this->client->api('tracker')->all();

        return $tab['trackers'];
    }

    public function priorities()
    {
        $tab = $this->client->api('issue_priority')->all();

        return $tab['issue_priorities'];
    }

    public function status()
    {
        $tab = $this->client->api('issue_status')->all();

        return $tab['issue_statuses'];
    }

    public function create($post)
    {
        $post['project_id'] = $this->project['id'];
        $this->client->api('issue')->create($post);
    }

    public function issues($param = [])
    {
        $param['project_id'] = $this->project['id'];
        if (! isset($param['limit'])) {
            $param['limit'] = '25';
        }

        if (! isset($param['status_id'])) {
            $param['status_id'] = '*';
        }

        $retour = $this->client->api('issue')->all($param);

        return $retour;
    }

    public function update($id, $post)
    {
        $this->client->api('issue')->update($id, $post);
    }
}
