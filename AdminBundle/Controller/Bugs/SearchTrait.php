<?php

namespace Mkk\AdminBundle\Controller\Bugs;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

trait SearchTrait
{
    /**
     * @Route("/bugs/search/user", name="bebugs_searchuser")
     */
    public function bugSearchUserAction()
    {
        $response = $this->get('mkk.search_service')->getResponse('bdd.user_manager', 'searchUserConnect');
        $json     = new JsonResponse($response);

        return $json;
    }
}
