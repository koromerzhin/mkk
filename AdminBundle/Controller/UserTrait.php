<?php

namespace Mkk\AdminBundle\Controller;

use Mkk\AdminBundle\Controller\User\JsonTrait;
use Mkk\AdminBundle\Controller\User\SearchTrait;
use Mkk\AdminBundle\Controller\User\UploadTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

trait UserTrait
{
    use JsonTrait;
    use SearchTrait;
    use UploadTrait;

    /**
     * @Route("/user/connect/{id}", name="beuser_connect")
     *
     * @param mixed $id
     */
    public function userConnectAction(Request $request, $id)
    {
        $userManager    = $this->get('bdd.user_manager');
        $userRepository = $userManager->getRepository();
        if (is_numeric($id)) {
            $search = ['id' => $id];
        } else {
            $search = ['salt' => $id];
        }

        $user        = $userRepository->findoneby($search);
        $token       = $this->get('security.token_storage')->getToken();
        $testnumeric = ($id != $token->getUser()->getId() && is_numeric($id));
        $testsalt    = ($id != $token->getUser()->getSalt() && ! is_numeric($id));
        if ($user && ($testnumeric || $testsalt)) {
            $token = new UsernamePasswordToken($user, null, 'main', ['ROLE_ADMIN']);
            $this->get('security.token_storage')->setToken($token);
        }

        $url      = $this->generateUrl('bord_index');
        $redirect = $this->redirect($url, 301);

        return $redirect;
    }

    /**
     * @Route("/user/form/{id}", name="beuser_form", defaults={"id": 0})
     *
     * @param mixed $id
     */
    public function userFormAction(Request $request, $id)
    {
        $param        = $this->get('mkk.param_service')->listing();
        $groupconnect = [];
        if (! isset($param['group_connect']) || count($param['group_connect']) == 0) {
            $flashbag = $request->getSession()->getFlashBag();
            $flashbag->add('warning', 'Group connectés non paramétrés');
            $url      = $this->generateUrl('bord_index');
            $redirect = $this->redirect($url, 301);

            return $redirect;
        }

        $crudService = $this->get('mkk.crud_service');
        $crudService->setManager('bdd.user_manager');
        $crudService->setId($id);
        $crudService->setFormulaireID('FormulaireUser');
        $crudService->setUrl(
            [
                'listing' => 'beuser_index',
                'form'    => 'beuser_form',
            ]
        );
        $crudService->setController($this);
        $crudService->setFormService('mkk_admin.form.user');
        $crudService->setTwigHTML('MkkAdminBundle:User:form.html.twig');
		$crudService->init();

        return $crudService->launch();
    }

    /**
     * @Route("/user/", name="beuser_index", defaults={"page": 0})
     * @Route("/user/{page}.html", name="beuser_page")
     *
     * @param mixed $page
     */
    public function userIndexAction(Request $request)
    {
        $listingService = $this->get('mkk.listing_service');
        $listingService->setRequest('bdd.user_manager', 'searchUserAdmin');
        $param     = $this->get('mkk.param_service')->listing();
        $tabgroups = [];
        if (isset($param['group_connect'])) {
            $tabgroups = $param['group_connect'];
        }
        if (count($tabgroups) == 0) {
            $flashbag = $request->getSession()->getFlashBag();
            $flashbag->add('warning', 'Group connectés non paramétrés');
            $url      = $this->generateUrl('bord_index');
            $redirect = $this->redirect($url, 301);

            return $redirect;
        }
        $actions = [
            ['type' => 'add', 'url' => 'beuser_form'],
            ['type' => 'actif', 'url' => 'beuser_json-actif'],
            ['type' => 'desactif', 'url' => 'beuser_json-actif'],
        ];

        $param        = $this->get('mkk.param_service')->listing();
        $groupconnect = [];
        if (isset($param['group_connect'])) {
            $groupconnect = $param['group_connect'];
        }

        $user = $this->get('security.token_storage')->getToken()->getUser();
        $listingService->setSearch('admin.search.user');
        $this->get('mkk.action_service')->set($actions);

        $totalnb    = [];
        $totalsomme = [];
        $user       = $this->get('security.token_storage')->getToken()->getUser();
        $methods    = get_class_methods($user);
        foreach ($methods as $method) {
            $method = strtolower($method);
            if (substr_count($method, 'gettotalnb') != 0) {
                $method           = substr($method, 3);
                $totalnb[$method] = $method;
            }
        }

        $methods = get_class_methods($user);
        foreach ($methods as $method) {
            $method = strtolower($method);
            if (substr_count($method, 'gettotalsomme') != 0) {
                $method              = substr($method, 3);
                $totalsomme[$method] = $method;
            }
        }

        $twiglist = 'MkkAdminBundle:User:index.html.twig';
        $htmlTwig = $this->renderMkk(
            $request,
            $twiglist,
            [
                'totalnb'           => $totalnb,
                'totalsomme'        => $totalsomme,
            ]
        );

        return $htmlTwig;
    }
}
