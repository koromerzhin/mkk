<?php

namespace Mkk\AdminBundle\Controller;

use Mkk\AdminBundle\Controller\Moncompte\JsonTrait;
use Mkk\AdminBundle\Controller\Moncompte\UploadTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

trait MoncompteTrait
{
    use UploadTrait;

    /**
     * @Route("/moncompte", name="bemoncompte_profil")
     * Gestion du profil
     *
     * @param Request $request Pile de requêtes
     *
     * @return html or json
     */
    public function moncompteProfilAction(Request $request)
    {
        $crudService = $this->get('mkk.crud_service');
        $crudService->setManager('bdd.user_manager');
        $crudService->setId($this->get('security.token_storage')->getToken()->getUser()->getId());
        $crudService->setFormulaireID('FormMoncompte');
        $crudService->setUrl(
            [
                'listing' => 'bemoncompte_profil',
                'form'    => 'bemoncompte_profil',
            ]
        );
        $crudService->setController($this);
        $crudService->setFormService('mkk_admin.form.moncompte');
        $crudService->setTwigHTML('MkkAdminBundle:Moncompte:index.html.twig');
		$crudService->init();

        return $crudService->launch();
    }
}
