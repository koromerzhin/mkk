<?php

namespace Mkk\AdminBundle\Controller;

use Mkk\AdminBundle\Controller\Metariane\JsonTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

trait MetarianeTrait
{
    use JsonTrait;

    /**
     * @Route("/metatagsariane/form/{id}", name="bemetariane_form", defaults={"id": 0})
     *
     * @param mixed $id
     */
    public function metarianeJsonEditAction(Request $request, $id)
    {
        $crudService = $this->get('mkk.crud_service');
        $crudService->setManager('bdd.metariane_manager');
        $crudService->setId($id);
        $crudService->setFormulaireID('FormMetariane');
        $crudService->setUrl(
            [
                'listing' => 'bemetariane_index',
                'form'    => 'bemetariane_form',
            ]
        );
        $crudService->setController($this);
        $crudService->setFormService('mkk_admin.form.metariane');
        $crudService->setTwigHTML('MkkAdminBundle:Metariane:form.html.twig');
		$crudService->init();

        return $crudService->launch();
    }

    /**
     * @Route("/metatagsariane/", name="bemetariane_index")
     *
     * @param Request $request pile de requêtes
     */
    public function metarianeIndexAction(Request $request)
    {
        $metarianeManager    = $this->get('bdd.metariane_manager');
        $metarianeRepository = $metarianeManager->getRepository();
        $selection           = $request->query->get('selection');

        $metarianeManager = $this->get('bdd.metariane_manager');
        $metarianeTable   = $metarianeManager->getTable();
        $entity           = new $metarianeTable();
        $siteService      = $this->get('mkk.site_service');
        $form             = $siteService->getForm('mkk_admin.form.metariane', $metarianeManager, $entity, 'ajouter');
        if ($siteService->isRedirect()) {
            $url      = $this->generateUrl('bemetariane_index');
            $redirect = $this->redirect($url, 301);

            return $redirect;
        }

        $routes = [];
        foreach (array_keys($this->getRoutes()) as $tab) {
            list($module, $action) = explode('_', $tab);
            unset($action);
            if (! isset($routes[$module])) {
                $routes[$module] = $module;
            }
        }

        ksort($routes);
        if ($selection != '') {
            $metariane = $metarianeRepository->listing($selection);
        } else {
            $metariane = [];
        }

        $metarianeData      = $this->get('mkk.metariane_data');
        $tabnotroute        = $metarianeData->getTab();
        $metarianes         = $metarianeRepository->getMetarianeRemplit();
        $searchparams       = $request->query->all();
        $options['choices'] = [];
        foreach ($metarianes as $key => $value) {
            $options['choices'][$key] = $value;
        }

        $listingService = $this->get('mkk.listing_service');
        $listingService->setSearch('admin.search.metariane', $options);
        $htmlTwig = $this->renderMkk(
            $request,
            'MkkAdminBundle:Metariane:index.html.twig',
            [
                'tabnotroute' => $tabnotroute,
                'selection'   => $selection,
                'entities'    => $metariane,
                'metarianes'  => $metarianes,
                'form'        => $siteService->createView($form),
            ]
        );

        return $htmlTwig;
    }
}
