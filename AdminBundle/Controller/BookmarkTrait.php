<?php

namespace Mkk\AdminBundle\Controller;

use Mkk\AdminBundle\Controller\Bookmark\JsonTrait;
use Mkk\AdminBundle\Controller\Bookmark\UploadTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

trait BookmarkTrait
{
    use JsonTrait;
    use UploadTrait;

    /**
     * @Route("/bookmark/", name="bebookmark_index", defaults={"page": 0})
     * @Route("/bookmark/{page}.html", name="bebookmark_page")
     *
     * Liste des articles du blog
     *
     * @param Request $request pile de requêtes
     *
     * @return Bookmark:index.html.twig
     */
    public function bookmarkIndexAction(Request $request)
    {
        $listingService = $this->get('mkk.listing_service');
        $listingService->setRequest('bdd.bookmark_manager', 'searchBookmark');
        $actions = [
            ['type' => 'add', 'url' => 'bebookmark_form'],
            ['type' => 'data', 'entity' => 'bookmark'],
        ];
        $this->get('mkk.action_service')->set($actions);
        $twigindex = 'MkkAdminBundle:Bookmark:index.html.twig';
        $twigHtml  = $this->renderMkk(
            $request,
            $twigindex
        );

        return $twigHtml;
    }

    /**
     * @Route("/bookmark/form/{id}", name="bebookmark_form", defaults={"id": 0})
     *
     * Formulaire d'un article du blog.
     *
     * @param Request $request pile de requêtes
     * @param int     $id      Identifiant de l'article du blog
     *
     * @return JsonResponse /  Bookmark:form.html.twig
     */
    public function bookmarkFormAction(Request $request, $id)
    {
        $crudService = $this->get('mkk.crud_service');
        $crudService->setManager('bdd.bookmark_manager');
        $crudService->setId($id);
        $crudService->setFormulaireID('FormBookmark');
        $crudService->setUrl(
            [
                'listing' => 'bebookmark_index',
                'form'    => 'bebookmark_form',
            ]
        );
        $crudService->setController($this);
        $crudService->setFormService('mkk_admin.form.bookmark');
        $crudService->setTwigHTML('MkkAdminBundle:Bookmark:form.html.twig');
		$crudService->init();

        return $crudService->launch();
    }
}
