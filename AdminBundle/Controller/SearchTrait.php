<?php

namespace Mkk\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

trait SearchTrait
{
    public function searchResponsable($request)
    {
        $groupManager    = $this->get('bdd.group_manager');
        $groupRepository = $groupManager->getRepository();
        $userManager     = $this->get('bdd.user_manager');
        $userRepository  = $userManager->getRepository();
        $response        = [];
        $container       = $this->get('service_container');
        $user            = $container->get('security.token_storage')->getToken()->getUser();
        $request         = $this->get('request');
        $search          = $request->request->get('searchnewuser');
        $page            = $request->request->get('page');
        $lettre          = $request->query->get('lettre');
        $get             = $request->query->all();
        $post            = $request->request->all();

        $param     = $this->get('mkk.param_service')->listing();
        $tabgroups = [];
        if (isset($param['group_responsable'])) {
            foreach ($param['group_responsable'] as $key => $idgroup) {
                $group           = $groupRepository->find($idgroup);
                $code            = $group->getCode();
                $tabgroups[$key] = $code;
            }
        }

        if (isset($get['lettre'])) {
            $search            = ['lettre' => $lettre];
            $tabgroups         = [];
            $response['users'] = [];
            $data              = $userRepository->searchUser($tabgroups, $search, $page, 10);
            foreach ($data['user'] as $key => $user) {
                $response['users'][$key] = ['id' => $user[0]->getId(), 'nom' => $user[0]->getAppel()];
            }

            $response['total'] = $data['pagination']['max'];
        } elseif (isset($post['id'])) {
            $user = $userRepository->find($post['id']);
            if ($user) {
                $response = [
                    'id'  => $user->getId(),
                    'nom' => $user->getAppel(),
                ];
            }
        }

        $jsonResponse = new JsonResponse($response);

        return $jsonResponse;
    }

    public function searchUser($request)
    {
        $groupManager    = $this->get('bdd.group_manager');
        $groupRepository = $groupManager->getRepository();
        $userManager     = $this->get('bdd.user_manager');
        $userRepository  = $userManager->getRepository();
        $response        = [];
        $container       = $this->get('service_container');
        $user            = $container->get('security.token_storage')->getToken()->getUser();
        $request         = $this->get('request');
        $search          = $request->request->get('searchnewuser');
        $page            = $request->request->get('page');
        $lettre          = $request->query->get('lettre');
        $url             = $request->request->get('url');
        $get             = $request->query->all();
        $post            = $request->request->all();
        if (isset($get['lettre'])) {
            $groups    = $groupRepository->findby([]);
            $tabgroups = [];
            foreach ($groups as $key => $group) {
                $code = $group->getCode();
                if ($code == 'superadmin') {
                    if ($user->getRefGroup()->getCode() == 'superadmin') {
                        $tabgroups[$key] = $code;
                    }
                } else {
                    $tabgroups[$key] = $code;
                }
            }

            $search            = ['lettre' => $lettre];
            $tabgroups         = [];
            $response['users'] = [];
            $data              = $userRepository->searchUser($tabgroups, $search, $page, 10);
            foreach ($data['user'] as $key => $user) {
                $response['users'][$key] = ['id' => $user[0]->getId(), 'nom' => $user[0]->getAppel()];
            }

            $response['total'] = $data['pagination']['max'];
        } elseif (isset($post['id'])) {
            $user = $userRepository->find($post['id']);
            if ($user) {
                $response = [
                    'id'  => $user->getId(),
                    'nom' => $user->getAppel(),
                ];
            }
        } else {
            $groups                 = [];
            $data                   = $userRepository->searchUser($groups, $search, $page, 10);
            $response['pagination'] = $data['pagination'];
            $response['user']       = [];
            foreach ($data['user'] as $user) {
                $adresse = '';
                if (count($user[0]->getAdresses()) != 0) {
                    $adresses = $user[0]->getAdresses();
                    $adresse  = $adresses[0]->getInfo() . ' ' . $adresses[0]->getCp() . ' ' . $adresses[0]->getVille();
                }

                if ($url == '') {
                    $url = 'beuser_form';
                }

                $adresseurl         = $this->generateUrl($url, ['id' => $user[0]->getId()]);
                $response['user'][] = [
                    'id'      => $user[0]->getId(),
                    'appel'   => $user[0]->getAppel(),
                    'groupe'  => $user[0]->getRefGroup()->getNom(),
                    'societe' => $user[0]->getSociete(),
                    'url'     => $adresseurl,
                    'adresse' => $adresse,
                ];
            }
        }

        $jsonResponse = new JsonResponse($response);

        return $jsonResponse;
    }

    public function searchGroupContact($request)
    {
        $groupManager    = $this->get('bdd.group_manager');
        $groupRepository = $groupManager->getRepository();
        $response        = [];
        $request         = $this->get('request');
        $lettre          = $request->query->get('lettre');
        $page            = $request->query->get('page');
        $id              = $request->request->get('id');
        if ($id != '') {
            $group = $groupRepository->find($id);
            if ($group) {
                $responsejson = [
                    'id'  => $group->getId(),
                    'nom' => $group->getNom(),
                ];
            }
        } else {
            $groups    = $groupRepository->findby([]);
            $tabgroups = [];
            foreach ($groups as $key => $group) {
                $tabgroups[$key] = $group->getId();
            }

            $data                   = $groupRepository->search($tabgroups, $lettre, $page, 10);
            $responsejson['groups'] = [];
            foreach ($data['group'] as $key => $group) {
                $responsejson['groups'][$key] = ['id' => $group->getId(), 'nom' => $group->getNom()];
            }

            $responsejson['total'] = $data['pagination']['max'];
        }

        $jsonResponse = new JsonResponse($response);

        return $jsonResponse;
    }
}
