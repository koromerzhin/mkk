<?php

namespace Mkk\AdminBundle\Controller;

use Mkk\AdminBundle\Controller\Contact\ActionTrait;
use Mkk\AdminBundle\Controller\Contact\ExportTrait;
use Mkk\AdminBundle\Controller\Contact\SearchTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Page controller.
 */
trait ContactTrait
{
    use SearchTrait;
    use ActionTrait;
    use ExportTrait;

    /**
     * @Route("/contact/", name="becontact_index", defaults={"page": 0})
     * @Route("/contact/{page}.html", name="becontact_page")
     *
     * Liste des contacts
     *
     * @param Request $request pile de requêtes
     * @param int     $page    numéro de la page
     *
     * @return html
     */
    public function contactindexAction(Request $request)
    {
        $listingService = $this->get('mkk.listing_service');
        $listingService->setRequest('bdd.user_manager', 'searchContact');
        $params = $request->query->all();
        $user   = $this->get('security.token_storage')->getToken()->getUser();

        $param          = $this->get('mkk.param_service')->listing();
        $groupscontacts = [];
        if (isset($param['group_contacts'])) {
            $groupscontacts = $param['group_contacts'];
        }

        if (count($groupscontacts) == 0) {
            $flashbag = $request->getSession()->getFlashBag();
            $flashbag->add('warning', 'Aucun groupe de contact est paramétrés dans la base de données');
            $url      = $this->generateUrl('bord_index');
            $redirect = $this->redirect($url, 301);

            return $redirect;
        }

        $options['group_contacts'] = $groupscontacts;
        $listingService->setSearch('admin.search.contact', $options);

        $actions = [
            ['type' => 'add', 'url' => 'becontact_form', 'class' => 'BoutonAddContact'],
            [
                'id'   => 'Boutonvcard',
                'url'  => $this->generateUrl('becontact_exportvcard', $params),
                'img'  => 'glyphicon-share',
                'text' => 'Vcard',
            ],
            [
                'id'   => 'Boutoncsv',
                'url'  => $this->generateUrl('becontact_exportcsv', $params),
                'img'  => 'glyphicon-share',
                'text' => 'CSV',
            ],
        ];

        $totalnb    = [];
        $totalsomme = [];
        $user       = $this->get('security.token_storage')->getToken()->getUser();
        $methods    = get_class_methods($user);
        foreach ($methods as $method) {
            $method = strtolower($method);
            if (substr_count($method, 'gettotalnb') != 0) {
                $method           = substr($method, 3);
                $totalnb[$method] = $method;
            }
        }

        $methods = get_class_methods($user);
        foreach ($methods as $method) {
            $method = strtolower($method);
            if (substr_count($method, 'gettotalsomme') != 0) {
                $method              = substr($method, 3);
                $totalsomme[$method] = $method;
            }
        }

        $this->get('mkk.action_service')->set($actions);
        $twigHtml = $this->renderMkk(
            $request,
            'MkkAdminBundle:Contact:index.html.twig',
            [
                'totalnb'        => $totalnb,
                'totalsomme'     => $totalsomme,
                'templateSearch' => '',
            ]
        );

        return $twigHtml;
    }

    /**
     * @Route("/contact/form/{id}", name="becontact_form", defaults={"id": 0})
     *
     * Formulaire de modification d'un contact
     *
     * @param Request $request Pile de requêtes
     * @param int     $id      Entity User
     *
     * @return html / json
     */
    public function contactformAction(Request $request, $id)
    {
        $crudService = $this->get('mkk.crud_service');
        $crudService->setManager('bdd.user_manager');
        $crudService->setId($id);
        $crudService->setFormulaireID('FormContact');
        $crudService->setUrl(
            [
                'listing' => 'becontact_index',
                'form'    => 'becontact_form',
            ]
        );
        $crudService->setController($this);
        $crudService->setFormService('mkk_admin.form.contact');
        $crudService->setTwigHTML('MkkAdminBundle:Contact:form.html.twig');
		$crudService->init();

        return $crudService->launch();
    }
}
