<?php

namespace Mkk\AdminBundle\Controller;

use Mkk\AdminBundle\Controller\Param\ActionTrait;
use Mkk\AdminBundle\Controller\Param\JsonTrait;
use Mkk\AdminBundle\Controller\Param\SearchTrait;
use Mkk\AdminBundle\Controller\Param\UploadTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

trait ParamTrait
{
    use UploadTrait;
    use JsonTrait;
    use SearchTrait;
    use ActionTrait;

    /**
     * @Route("/param/", name="beparam_index")
     */
    public function paramIndexAction(Request $request)
    {
        $paramManager    = $this->get('bdd.param_manager');
        $paramRepository = $paramManager->getRepository();
        $paramEntity     = $paramManager->getTable();
        $param           = $this->setParamSite($request);
        $actions         = [
            ['type' => 'save', 'url' => 'beparam_index'],
        ];
        $this->get('mkk.action_service')->set($actions);
        $siteService         = $this->get('mkk.site_service');
        $data                = $this->get('mkk.param_service')->listing();
        $metarianeManager    = $this->get('bdd.metariane_manager');
        $metarianeRepository = $metarianeManager->getRepository();
        $dataroutes          = $metarianeRepository->getMetarianeRemplit();
        $data                = $this->get('mkk.param_service')->listing();
        $form                = $siteService->getFormSystemParam('param', $data);
        if ($siteService->isRedirect()) {
            $url      = $this->generateUrl('bord_index');
            $redirect = $this->redirect($url, 301);

            return $redirect;
        }

        $tmpl     = $siteService->getHtmlForm('param', $form);
        $twigHtml = $this->renderMkk(
            $request,
            'MkkAdminBundle:Param:index.html.twig',
            [
                'form'   => $siteService->createView($form),
                'tmpl'   => $tmpl,
            ]
        );

        return $twigHtml;
    }
}
