<?php

namespace Mkk\AdminBundle\Controller;

use Mkk\AdminBundle\Controller\Droit\ActionTrait as DroitActionTrait;
use Mkk\AdminBundle\Controller\Droit\JsonTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

trait DroitTrait
{
    use DroitActionTrait;
    use JsonTrait;

    /**
     * @Route("/droit/", name="bedroit_index")
     */
    public function droitIndexAction(Request $request)
    {
        $metarianeManager    = $this->get('bdd.metariane_manager');
        $metarianeRepository = $metarianeManager->getRepository();
        $actionManager       = $this->get('bdd.action_manager');
        $actionRepository    = $actionManager->getRepository();
        $groupManager        = $this->get('bdd.group_manager');
        $groupRepository     = $groupManager->getRepository();
        $selection           = $request->query->get('selection');
        $emplacement         = $request->query->get('emplacement');
        $data                = $actionRepository->findAll();
        $groups              = $groupRepository->findall();
        $routes              = $this->generateRoute();
        $metarianes          = $metarianeRepository->getMetarianeRemplit();
        foreach ($routes as $route) {
            $module = $route['module'];
            if (! isset($metarianes[$module])) {
                $metarianes[$module] = $module;
            }
        }

        foreach (array_keys($metarianes) as $module) {
            $trouver = 0;
            foreach ($routes as $route) {
                if ($route['module'] == $module) {
                    $trouver = 1;
                    break;
                }
            }

            if ($trouver == 0) {
                unset($metarianes[$module]);
            }
        }

        $searchparams       = $request->query->all();
        $options['choices'] = $metarianes;
        $listingService     = $this->get('mkk.listing_service');
        $listingService->setSearch('admin.search.droit', $options);
        $backend = 0;
        if (substr($selection, 0, 2) == 'be') {
            $backend = 1;
        }

        $twigHtml = $this->renderMkk(
            $request,
            'MkkAdminBundle:Droit:index.html.twig',
            [
                'backend'     => $backend,
                'emplacement' => $emplacement,
                'selection'   => $selection,
                'groups'      => $groups,
                'routesdroit' => $routes,
                'metarianes'  => $metarianes,
                'data'        => $data,
            ]
        );

        return $twigHtml;
    }
}
