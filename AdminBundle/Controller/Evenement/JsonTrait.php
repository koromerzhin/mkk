<?php

namespace Mkk\AdminBundle\Controller\Evenement;

use Mkk\AdminBundle\Form\AdresseType;
use Mkk\AdminBundle\Form\Evenement\DateType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

trait JsonTrait
{
    /**
     * @Route("/evenement/json/actif", name="beevenement_json-actif")
     *
     * Active/désactive un article du blog
     *
     * @return JsonResponse
     */
    public function evenementActifAction(Request $request)
    {
        $jsonResponse = $this->actionActivate($request, 'Evenement', 'id', 'setPublier');

        return $jsonResponse;
    }

    /**
     * @Route("/evenement/json/add/adresse/{id}", name="beevenement_jsonaddadresse", defaults={"id": 0})
     *
     * Active/désactive un article du blog
     *
     * @param mixed $id
     *
     * @return JsonResponse
     */
    public function evenementAddAdresseAction(Request $request, $id)
    {
        $evenementManager    = $this->get('bdd.evenement_manager');
        $evenementRepository = $evenementManager->getRepository();
        $evenement           = $evenementRepository->find($id);
        $adresseManager      = $this->get('bdd.adresse_manager');
        $adresseEntity       = $adresseManager->getTable();
        $emplacementManager  = $this->get('bdd.emplacement_manager');
        $emplacementEntity   = $emplacementManager->getTable();
        $tab                 = [];
        if ($evenement) {
            $emplacement = new $emplacementEntity();
            $emplacement->setRefEvenement($evenement);
            $evenementManager->persistAndFlush($emplacement);
            $adresse = new $adresseEntity();
            $adresse->setRefEmplacement($emplacement);
            $adresseManager->persistAndFlush($adresse);
        }

        $response = new JsonResponse($tab);

        return $response;
    }

    /**
     * @Route("/evenement/json/date/save", name="beevenement_json-datesave")
     */
    public function evenementDateSaveAction(Request $request)
    {
        $responsejson          = [];
        $dateManager           = $this->get('bdd.date_manager');
        $dateRepository        = $dateManager->getRepository();
        $emplacementManager    = $this->get('bdd.emplacement_manager');
        $emplacementRepository = $emplacementManager->getRepository();
        $dateid                = $request->get('id');
        $date                  = $dateRepository->find($dateid);
        if (! $date) {
            $json = new JsonResponse($responsejson);

            return $json;
        }
        $options = [
            'place' => 0,
        ];
        $params  = $this->get('mkk.param_service')->listing();
        if (isset($params['evenement_place'])) {
            $options['place'] = $params['evenement_place'];
        }

        $evenement         = $date->getRefEmplacement()->getRefEvenement();
        $choiceEmplacement = [];
        foreach ($evenement->getEmplacements() as $emplacement) {
            $idemplacement = $emplacement->getId();
            $adresses      = $emplacement->getAdresses();
            if (count($adresses) != 0) {
                $adresse = $adresses[0];
                $info    = $adresse->getInfo();
                $cp      = $adresse->getCp();
                $ville   = $adresse->getVille();
                $text    = $info . ' - ' . $cp . ' ' . $ville;

                $choiceEmplacement[$idemplacement] = $text;
            } else {
                $etablissement = $emplacement->getRefEtablissement();
                $text          = $etablissement->getNom();

                $choiceEmplacement[$idemplacement] = $text;
            }
        }

        $options['choice_emplacement'] = $choiceEmplacement;
        $options['type']               = $evenement->getType();
        $formdate                      = $this->createForm(DateType::class, $date, $options);
        if (! $this->isPostAjax($request)) {
            $retour = new JsonResponse($responsejson);

            return $retour;
        }

        $request = $this->getRequest();
        $service = $this->get('mkk.post_service');
        $service->init($date, $formdate, 'modifier');
        $response     = $service->getResponse();
        $jsonresponse = new JsonResponse($response);

        return $jsonresponse;
    }

    /**
     * @Route("/evenement/json/date/info", name="beevenement_json-dateinfo")
     */
    public function evenementDateInfoAction(Request $request)
    {
        $responsejson   = [];
        $dateManager    = $this->get('bdd.date_manager');
        $dateRepository = $dateManager->getRepository();
        $dateid         = $request->get('id');
        $date           = $dateRepository->find($dateid);
        if (! $date) {
            $json = new JsonResponse($responsejson);

            return $json;
        }
        $serializer                  = $this->container->get('serializer');
        $dataSerializer              = $serializer->serialize($date, 'json');
        $responsejson                = json_decode($dataSerializer, true);
        $responsejson['emplacement'] = $date->getRefEmplacement()->getId();
        $responsejson['debut']       = date('d/m/Y H:i', $responsejson['debut']);
        $responsejson['fin']         = date('d/m/Y H:i', $responsejson['fin']);
        $json                        = new JsonResponse($responsejson);

        return $json;
    }

    /**
     * @Route("/evenement/json/date/update", name="beevenement_json-dateupdate")
     *
     * @param mixed $id
     */
    public function evenementDateUpdateAction(Request $request)
    {
        $responsejson   = [];
        $datestart      = $request->get('start');
        $dateend        = $request->get('end');
        $dateid         = $request->get('id');
        $dateManager    = $this->get('bdd.date_manager');
        $dateRepository = $dateManager->getRepository();
        $date           = $dateRepository->find($dateid);
        if (! $date) {
            $json = new JsonResponse($responsejson);

            return $json;
        }
        $date->setDebut($datestart);
        $date->setFin($dateend);
        $dateManager->persistAndFlush($date);
        $responsejson['id'] = $date->getId();
        $json               = new JsonResponse($responsejson);

        return $json;
    }

    /**
     * @Route("/evenement/json/date/add", name="beevenement_json-dateadd")
     *
     * @param mixed $id
     */
    public function evenementDateAddAction(Request $request)
    {
        $responsejson          = [];
        $dateManager           = $this->get('bdd.date_manager');
        $dateEntity            = $dateManager->getTable();
        $emplacementManager    = $this->get('bdd.emplacement_manager');
        $emplacementRepository = $emplacementManager->getRepository();
        $enventid              = $request->get('eventid');
        $datestart             = $request->get('start');
        $dateend               = $request->get('end');
        $date                  = new $dateEntity();
        $emplacement           = $emplacementRepository->find($enventid);
        if (! $emplacement) {
            $json = new JsonResponse($responsejson);

            return $json;
        }

        $date->setRefEmplacement($emplacement);
        $date->setDebut($datestart);
        $date->setFin($dateend);
        $dateManager->persistAndFlush($date);
        $responsejson['id'] = $date->getId();
        $json               = new JsonResponse($responsejson);

        return $json;
    }

    /**
     * @Route("/evenement/json/date/delete", name="beevenement_json-datedelete")
     *
     * @param mixed $id
     */
    public function evenementDateDeleteAction(Request $request)
    {
        $responsejson   = [];
        $dateManager    = $this->get('bdd.date_manager');
        $dateRepository = $dateManager->getRepository();
        $dateid         = $request->get('id');
        $date           = $dateRepository->find($dateid);
        if (! $date) {
            $json = new JsonResponse($responsejson);

            return $json;
        }

        $dateManager->removeAndFlush($date);
        $json = new JsonResponse($responsejson);

        return $json;
    }

    /**
     * @Route("/evenement/json/date/{id}", name="beevenement_json-date", defaults={"id": 0})
     *
     * @param mixed $id
     */
    public function evenementdateAction($id)
    {
        $responsejson        = [];
        $evenementManager    = $this->get('bdd.evenement_manager');
        $evenementRepository = $evenementManager->getRepository();
        $responsejson        = [];
        $evenement           = $evenementRepository->find($id);
        $params              = $this->get('mkk.param_service')->listing();
        $tabcolor            = [];
        if (isset($params['evenement_couleur'])) {
            $couleur  = $params['evenement_couleur'];
            $tabcolor = explode(',', $couleur);
        }
        if (! $evenement) {
            $json = new JsonResponse($responsejson);

            return $json;
        }

        $place  = 0;
        $params = $this->get('mkk.param_service')->listing();
        if (isset($params['evenement_place'])) {
            $place = $params['evenement_place'];
        }

        $totalNbEmplacement = $evenement->getTotalnbemplacement();
        foreach ($evenement->getEmplacements() as $i => $emplacement) {
            $eventid = $emplacement->getId();
            $dates   = $emplacement->getDates();
            $lieu    = $emplacement->getRefEtablissement();
            if ($lieu) {
                $titre = $lieu->getNom();
            } else {
                foreach ($emplacement->getAdresses() as $adresse) {
                    if ($adresse->getInfo() != null) {
                        $titre = $adresse->getInfo() . " \n " . $adresse->getCp() . ' - ' . $adresse->getVille();
                    } else {
                        $titre = $adresse->getCp() . ' - ' . $adresse->getVille();
                    }
                    break;
                }
            }
            if (count($dates) != 0) {
                foreach ($dates as $date) {
                    if ($totalNbEmplacement == 1) {
                        $titre = '';
                    }

                    $newtitre = $titre;

                    if ($place == 1 && $evenement->getType() == 0) {
                        if ($date->getPlaceillimite() == 0) {
                            $newtitre = $newtitre . "\n place disponible: " . $date->getPlace();
                        } else {
                            $newtitre = $newtitre . "\n places illimités";
                        }
                    } else {
                        $newtitre = $newtitre;
                    }

                    array_push(
                        $responsejson,
                        [
                            'title'   => $newtitre,
                            'start'   => date('c', $date->getDebut()),
                            'end'     => date('c', $date->getFin()),
                            'eventid' => $eventid,
                            'color'   => $tabcolor[$i],
                            'dateid'  => $date->getId(),
                        ]
                    );
                }
            }
        }

        $json = new JsonResponse($responsejson);

        return $json;
    }

    /**
     * @Route("/evenement/json/emplacement/lieu/{id}", name="beevenement_jsonaddlieu", defaults={"id": 0})
     *
     * @param mixed $id
     */
    public function evenementEmplacementLieuAction(Request $request, $id)
    {
        $responsejson            = [];
        $emplacementManager      = $this->get('bdd.emplacement_manager');
        $emplacementEntity       = $emplacementManager->getTable();
        $etablissementManager    = $this->get('bdd.etablissement_manager');
        $etablissementRepository = $etablissementManager->getRepository();
        $evenementManager        = $this->get('bdd.evenement_manager');
        $evenementRepository     = $evenementManager->getRepository();
        $evenement               = $evenementRepository->find($id);
        if (! $evenement) {
            $json =  new JsonResponse($responsejson);

            return $json;
        }

        $idlieu = $request->request->get('id');
        if (! isset($idlieu)) {
            $json =  new JsonResponse($responsejson);

            return $json;
        }

        $etablissement = $etablissementRepository->find($idlieu);
        if (! $etablissement) {
            $json =  new JsonResponse($responsejson);

            return $json;
        }

        $emplacement = new $emplacementEntity();
        $emplacement->setRefEtablissement($etablissement);
        $emplacement->setRefEvenement($evenement);
        $emplacementManager->persistAndFlush($emplacement);
        $json =  new JsonResponse($responsejson);

        return $json;
    }

    /**
     * @Route("/evenement/json/emplacement/save/{id}", name="beevenement_json-emplacementsave", defaults={"id": 0})
     *
     * @param mixed $id
     */
    public function evenementEmplacementSaveAction(Request $request, $id)
    {
        $emplacementManager    = $this->get('bdd.emplacement_manager');
        $emplacementRepository = $emplacementManager->getRepository();
        $responsejson          = [];
        $em                    = $this->getDoctrine()->getManager();
        $emplacement           = $emplacementRepository->find($id);
        if (! $emplacement) {
            $json = new JsonResponse($responsejson);

            return $json;
        }

        $adresses = $emplacement->getAdresses();
        foreach ($adresses as $adresse) {
            break;
        }

        $em->persist($emplacement);
        $adresse->setRefEmplacement($emplacement);
        $formadresse = $this->createForm(AdresseType::class, $adresse);
        $service     = $this->get('mkk.post_service');
        $service->init($adresse, $formadresse, 'ajouter');
        $response     = $service->getResponse();
        $jsonresponse = new JsonResponse($response);

        return $jsonresponse;
    }

    /**
     * @Route("/evenement/json/emplacement/update/{id}", name="beevenement_json-emplacementupdate", defaults={"id": 0})
     *
     * @param mixed $id
     */
    public function evenementEmplacementUpdateAction(Request $request, $id)
    {
        $emplacementManager    = $this->get('bdd.emplacement_manager');
        $emplacementRepository = $emplacementManager->getRepository();
        $responsejson          = [];
        $emplacement           = $emplacementRepository->find($id);
        if (! $emplacement) {
            $json =  new JsonResponse($responsejson);

            return $json;
        }

        $responsejson['url'] = $this->generateUrl('beevenement_json-emplacementsave', ['id' => $id]);
        $id                  = $request->request->get('id');
        $adresses            = $emplacement->getAdresses();
        if (count($adresses) != 0) {
            foreach ($adresses as $adresse) {
                $responsejson['adresse'] = [
                    'info'  => $adresse->getInfo(),
                    'pays'  => strtoupper($adresse->getPays()),
                    'ville' => $adresse->getVille(),
                    'cp'    => $adresse->getCp(),
                    'gps'   => $adresse->getGps(),
                    'lat'   => $adresse->getGpslat(),
                    'lon'   => $adresse->getGpslon(),
                ];
            }
        }

        $json =  new JsonResponse($responsejson);

        return $json;
    }

    /**
     * @Route("/evenement/json/emplacement/adresse/{id}", name="beevenement_json-emplacementadresse", defaults={"id": 0})
     *
     * @param mixed $id
     */
    public function evenementEmplacementAdresseAction(Request $request, $id)
    {
        $responsejson        = [];
        $emplacementManager  = $this->get('bdd.emplacement_manager');
        $emplacementEntity   = $emplacementManager->getTable();
        $evenementManager    = $this->get('bdd.evenement_manager');
        $evenementRepository = $evenementManager->getRepository();
        $em                  = $this->getDoctrine()->getManager();
        $evenement           = $evenementRepository->find($id);
        if (! $evenement) {
            $json =  new JsonResponse($responsejson);

            return $json;
        }

        $emplacement = new $emplacementEntity();
        $emplacement->setRefEvenement($evenement);
        $emplacementManager->persistAndFlush($emplacement);
        $adresseManager = $this->get('bdd.adresse_manager');
        $adresseEntity  = $adresseManager->getTable();
        $adresse        = new $adresseEntity();
        $adresse->setRefEmplacement($emplacement);
        $adresse->setRefEmplacement($emplacement);
        $formadresse = $this->createForm(AdresseType::class, $adresse);

        $service = $this->get('mkk.post_service');
        $service->init($adresse, $formadresse, 'ajouter');
        $response     = $service->getResponse();
        $jsonresponse = new JsonResponse($response);

        return $jsonresponse;
    }

    /**
     * @Route("/evenement/json/edit/{id}", name="beevenement_json-edit", defaults={"id": 0})
     *
     * @param
     * @param mixed $id
     */
    public function evenementeditAction(Request $request, $id)
    {
        $evenementManager    = $this->get('bdd.evenement_manager');
        $evenementRepository = $evenementManager->getRepository();
        $evenementEntity     = $evenementManager->getTable();
        $etat                = 'modifier';
        $evenement           = $evenementRepository->find($id);
        if (! $evenement) {
            $etat      = 'ajouter';
            $evenement = new $evenementEntity();
            $evenement->setExterne(0);
        }

        $options = [];
        if ($etat != 'ajouter') {
            $options['adresses'] = 1;
        }

        $params = $this->get('mkk.param_service')->listing();
        if (isset($params['evenement_place'])) {
            $options['place'] = $params['evenement_place'];
        }

        $siteService = $this->get('mkk.site_service');
        $form        = $siteService->getForm('mkk_admin.form.evenement', $evenementManager, $evenement, $etat, $options);
        if ($siteService->isRedirect()) {
            $url      = $this->generateUrl('beevenement_index');
            $redirect = $this->redirect($url, 301);

            return $redirect;
        }

        $service = $this->get('mkk.post_service');
        $service->init($evenement, $form, $etat);
        $response     = $service->getResponse();
        $jsonresponse = new JsonResponse($response);

        return $jsonresponse;
    }

    /**
     * @Route("/evenement/json/validationcorrection/{evenement}", name="beevenement_json-validationcorrection", defaults={"evenement": 0})
     *
     * @param mixed $evenement
     */
    public function evenementValidationCorrectionAction($evenement)
    {
        $validation          = 0;
        $evenementManager    = $this->get('bdd.evenement_manager');
        $evenementRepository = $evenementManager->getRepository();
        $row                 = $evenementRepository->find($evenement);
        if ($row) {
            $validation = 1;
            $row->setValidation(1);
            $row->setCorrection(1);
            $row->setPublier(1);
            $evenementManager->persistAndFlush($row);
        }
        $responsejson = [
            'validation' => $validation,
        ];

        $json = new JsonResponse($responsejson);

        return $json;
    }

    /**
     * @Route("/evenement/json/validation/{evenement}", name="beevenement_json-validation", defaults={"evenement": 0})
     *
     * @param mixed $evenement
     */
    public function evenementvalidationAction($evenement)
    {
        $validation          = 0;
        $evenementManager    = $this->get('bdd.evenement_manager');
        $evenementRepository = $evenementManager->getRepository();
        $row                 = $evenementRepository->find($evenement);
        if ($row) {
            $validation = 1;
            $row->setValidation(1);
            $evenementManager->persistAndFlush($row);
        }
        $responsejson = [
            'validation' => $validation,
        ];

        $json = new JsonResponse($responsejson);

        return $json;
    }

    /**
     * @Route("/evenement/json/correction/{evenement}", name="beevenement_json-correction", defaults={"evenement": 0})
     *
     * @param mixed $evenement
     */
    public function evenementcorrectionAction($evenement)
    {
        $correction          = 0;
        $evenementManager    = $this->get('bdd.evenement_manager');
        $evenementRepository = $evenementManager->getRepository();
        $row                 = $evenementRepository->find($evenement);
        if ($row) {
            $row->setCorrection(1);
            $row->setPublier(1);
            $evenementManager->persistAndFlush($row);
            $correction = 1;
        }

        $responsejson = [
            'correction' => $correction,
        ];

        $json = new JsonResponse($responsejson);

        return $json;
    }
}
