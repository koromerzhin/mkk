<?php

namespace Mkk\AdminBundle\Controller\Evenement;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

trait CategorieTrait
{
    /**
     * @Route("/evenement/categorie/", name="beevenement_categorieindex", defaults={"page": 0})
     * @Route("/evenement/categorie/{page}.html", name="beevenement_categoriepage")
     *
     * Liste des catégories du blog
     *
     * @param Request $request pile de requêtes
     * @param int     $page    numéro de la page
     *
     * @return html
     */
    public function categorieEvenementIndexAction(Request $request)
    {
        return $this->traitCategorieIndex($request, 'retour au evenement');
    }

    /**
     * @Route("/evenement/categorie/form/{id}", name="beevenement_categorieform", defaults={"id": 0})
     *
     * Formulaire de catégorie de blog.
     *
     * @param Request $request pile de requêtes
     * @param int     $id      Identifiant de la catégorie
     *
     * @return json /  html
     */
    public function categorieEvenementJsonAction(Request $request, $id)
    {
        return $this->traitCategorieJson($request, $id);
    }

    /**
     * @Route("/evenement/categorie/json/delete", name="beevenement_categoriejson-delete")
     *
     * Supprimer une catégorie
     *
     * @return json
     */
    public function categorievenementDeleteAction(Request $request)
    {
        return $this->traitCategorieDelete($request);
    }

    /**
     * @Route("/evenement/categorie/json/vider", name="beevenement_categoriejson-vider")
     *
     * Supprimer une catégorie
     *
     * @return json
     */
    public function categorieevenementViderAction(Request $request)
    {
        return $this->traitCategorieVider($request);
    }
}
