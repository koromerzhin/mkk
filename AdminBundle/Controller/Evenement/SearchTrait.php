<?php

namespace Mkk\AdminBundle\Controller\Evenement;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

trait SearchTrait
{
    /**
     * @Route("/evenement/search/categorie", name="beevenement_searchcategorie")
     */
    public function evenementSearchCategorieAction()
    {
        $response = $this->get('mkk.search_service')->getResponse('bdd.categorie_manager', 'searchCategorieEvenement');
        $json     = new JsonResponse($response);

        return $json;
    }

    /**
     * @Route("/evenement/search/lieu", name="beevenement_searchlieu")
     */
    public function evenementSearchLieuAction()
    {
        $response = $this->get('mkk.search_service')->getResponse('bdd.etablissement_manager', 'searchEtablissementSaufEnseigne');
        $json     = new JsonResponse($response);

        return $json;
    }
}
