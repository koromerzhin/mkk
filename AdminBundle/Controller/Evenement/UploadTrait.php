<?php

namespace Mkk\AdminBundle\Controller\Evenement;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

trait UploadTrait
{
    /**
     * @Route("/evenement/galerie/{md5}", name="beevenement_uploadgalerie")
     *
     * @param mixed $md5
     */
    public function evenementgalerieAction()
    {
        $options  = [];
        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }

    /**
     * @Route("/evenement/vignette/{md5}", name="beevenement_uploadvignette")
     *
     * @param mixed $md5
     */
    public function evenementvignetteAction()
    {
        $options = [
            'max_number_of_files' => 1,
        ];

        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }
}
