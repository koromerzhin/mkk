<?php

namespace Mkk\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

trait DataTrait
{
    /**
     * @Route("/data/", name="bedata_index")
     *
     * @param Request $request pile de requêtes
     *
     * @return JsonResponse
     */
    public function dataIndexAction()
    {
        $dataService  = $this->get('mkk.data_service');
        $response     = $dataService->all();
        $jsonresponse = new JsonResponse($response);

        return $jsonresponse;
    }

    /**
     * @Route("/data/list/{entity}", name="bedata_list")
     *
     * @param Request $request pile de requêtes
     * @param mixed   $entity
     * @param mixed   $id
     *
     * @return JsonResponse
     */
    public function dataListAction($entity)
    {
        $dataService  = $this->get('mkk.data_service');
        $response     = $dataService->list($entity);
        $jsonresponse = new JsonResponse($response);

        return $jsonresponse;
    }

    /**
     * @Route("/data/show/{entity}/{id}", name="bedata_show")
     *
     * @param Request $request pile de requêtes
     * @param mixed   $entity
     * @param mixed   $id
     *
     * @return JsonResponse
     */
    public function dataShowAction($entity, $id)
    {
        $dataService  = $this->get('mkk.data_service');
        $response     = $dataService->show($entity, $id);
        $jsonresponse = new JsonResponse($response);

        return $jsonresponse;
    }

    /**
     * @Route("/data/delete/{entity}", name="bedata_delete")
     *
     * @param Request $request pile de requêtes
     * @param mixed   $entity
     *
     * @return JsonResponse
     */
    public function dataDeleteAction($entity)
    {
        $dataService  = $this->get('mkk.data_service');
        $response     = $dataService->delete($entity);
        $jsonresponse = new JsonResponse($response);

        return $jsonresponse;
    }

    /**
     * @Route("/data/empty/{entity}", name="bedata_empty")
     *
     * @param Request $request pile de requêtes
     * @param mixed   $entity
     *
     * @return JsonResponse
     */
    public function dataEmptyAction($entity)
    {
        $dataService  = $this->get('mkk.data_service');
        $response     = $dataService->empty($entity);
        $jsonresponse = new JsonResponse($response);

        return $jsonresponse;
    }

    /**
     * @Route("/data/duplicate/{entity}", name="bedata_duplicate")
     *
     * @param Request $request pile de requêtes
     * @param mixed   $entity
     *
     * @return JsonResponse
     */
    public function dataDuplicateAction($entity)
    {
        $dataService  = $this->get('mkk.data_service');
        $response     = $dataService->duplicate($entity);
        $jsonresponse = new JsonResponse($response);

        return $jsonresponse;
    }
}
