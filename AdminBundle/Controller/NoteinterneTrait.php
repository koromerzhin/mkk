<?php

namespace Mkk\AdminBundle\Controller;

use Mkk\AdminBundle\Controller\Noteinterne\JsonTrait;
use Mkk\AdminBundle\Controller\Noteinterne\SearchTrait;
use Mkk\SiteBundle\Entity\Noteinterne;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

trait NoteinterneTrait
{
    use JsonTrait;
    use SearchTrait;

    /**
     * @Route("/noteinterne/", name="benoteinterne_index", defaults={"page" = 0})
     * @Route("/noteinterne/{page}.html", name="benoteinterne_page")
     *
     * Page des notes internes
     *
     * @param Request $request Pile de requêtes
     *
     * @return Noteinterne:index.html.twig
     */
    public function noteInterneIndexAction(Request $request)
    {
        $listingService = $this->get('mkk.listing_service');
        $listingService->setRequest('bdd.noteinterne_manager', 'searchNoteInterne');
        $listingService->setSearch('admin.search.noteinterne');
        $actions = [
            [
                'type' => 'add',
                'url'  => 'benoteinterne_form',
            ],
            [
                'type'    => 'data',
                'entity'  => 'noteinterne',
            ],
        ];
        $this->get('mkk.action_service')->set($actions);
        $htmlTwig = $this->renderMkk(
            $request,
            'MkkAdminBundle:Noteinterne:index.html.twig'
        );

        return $htmlTwig;
    }

    /**
     * @Route("/noteinterne/form/{id}", name="benoteinterne_form", defaults={"id" = 0})
     *
     * Formulaire d'une note interne
     *
     * @param Request     $request Pile de requête
     * @param Noteinterne $id      Identifiant de la note interne
     *
     * @return JsonResponse / Noteinterne:form.html.twig
     */
    public function noteInterneFormAction(Request $request, $id)
    {
        $crudService = $this->get('mkk.crud_service');
        $crudService->setManager('bdd.noteinterne_manager');
        $crudService->setId($id);
        $crudService->setFormulaireID('FormNoteInterne');
        $crudService->setUrl(
            [
                'listing' => 'benoteinterne_index',
                'form'    => 'benoteinterne_form',
            ]
        );
        $crudService->setController($this);
        $crudService->setFormService('mkk_admin.form.noteinterne');
        $crudService->setTwigHTML('MkkAdminBundle:Noteinterne:form.html.twig');
		$crudService->init();

        return $crudService->launch();
    }
}
