<?php

namespace Mkk\AdminBundle\Controller\Blog;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

trait UploadTrait
{
    /**
     * @Route("/blog/vignette/{md5}", name="beblog_uploadvignette")
     *
     * Upload d'une vignette d'un article du blog
     *
     * @param Hash $md5 Hash de la vignette
     *
     * @return Response
     */
    public function blogVignetteAction()
    {
        $options = [
            'max_number_of_files' => 1,
        ];

        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }

    /**
     * @Route("/blog/image/{md5}", name="beblog_uploadimage")
     *
     * Upload d'une image d'un article du blog
     *
     * @param Hash $md5 Hash de l'image'
     *
     * @return Response
     */
    public function blogImageAction()
    {
        $options = [
            'max_number_of_files' => 1,
        ];

        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }

    /**
     * @Route("/blog/galerie/{md5}", name="beblog_uploadgalerie")
     *
     * Upload d'une ou plusieurs images d'un article du blog
     *
     * @param Hash $md5 Hash de la galerie
     *
     * @return Response
     */
    public function blogGalerieAction()
    {
        $options  = [];
        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }
}
