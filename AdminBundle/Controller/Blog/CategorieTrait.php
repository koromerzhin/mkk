<?php

namespace Mkk\AdminBundle\Controller\Blog;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

trait CategorieTrait
{
    /**
     * @Route("/blog/categorie/", name="beblog_categorieindex", defaults={"page": 0})
     * @Route("/blog/categorie/{page}.html", name="beblog_categoriepage")
     *
     * Liste des catégories du blog
     *
     * @param Request $request pile de requêtes
     * @param int     $page    numéro de la page
     *
     * @return html
     */
    public function categorieIndexAction(Request $request)
    {
        return $this->traitCategorieIndex($request, 'retour au blog');
    }

    /**
     * @Route("/blog/categorie/form/{id}", name="beblog_categorieform", defaults={"id": 0})
     *
     * Formulaire de catégorie de blog.
     *
     * @param Request $request pile de requêtes
     * @param int     $id      Identifiant de la catégorie
     *
     * @return json /  html
     */
    public function categorieJsonAction(Request $request, $id)
    {
        return $this->traitCategorieJson($request, $id);
    }

    /**
     * @Route("/blog/categorie/json/delete", name="beblog_categoriejson-delete")
     *
     * Supprimer une catégorie
     *
     * @return json
     */
    public function categorieDeleteAction(Request $request)
    {
        return $this->traitCategorieDelete($request);
    }

    /**
     * @Route("/blog/categorie/json/vider", name="beblog_categoriejson-vider")
     *
     * Supprimer une catégorie
     *
     * @return json
     */
    public function categorieViderAction(Request $request)
    {
        return $this->traitCategorieVider($request);
    }
}
