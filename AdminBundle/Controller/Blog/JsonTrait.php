<?php

namespace Mkk\AdminBundle\Controller\Blog;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Intl\Intl;

trait JsonTrait
{
    /**
     * @Route("/blog/json/dupliquer", name="beblog_json-dupliquer")
     *
     * Dupliquer les articles
     *
     * @return JsonResponse
     */
    public function blogDuplicaterAction(Request $request)
    {
        $json = $this->actionDuplicate($request, 'Blog', 'beblog_index');

        return $json;
    }

    /**
     * @Route("/blog/json/actif", name="beblog_json-actif")
     *
     * Active/désactive un article du blog
     *
     * @return JsonResponse
     */
    public function blogActifAction(Request $request)
    {
        $jsonResponse = $this->actionActivate($request, 'Blog', 'id', 'setActifPublic');

        return $jsonResponse;
    }

    /**
     * @Route("/blog/json/avant/{id}", name="beblog_json-avant")
     *
     * Met en avant un article du blog
     *
     * @param Request $request pile de requêtes
     * @param int     $id      Identifiant de l'article du blog
     *
     * @return JsonResponse
     */
    public function blogAvantAction(Request $request, $id)
    {
        $request->request->set('selection', $id);

        $retour = $this->actionActivate($request, 'Blog', 'id', 'setAvant');

        return $retour;
    }

    /**
     * @Route("/blog/json/accueil/{id}", name="beblog_accueil")
     *
     * Mettre à l'accueil un article du blog
     *
     * @param Request $request pile de requêtes
     * @param int     $id      Identifiant de l'article du blog
     *
     * @return JsonResponse
     */
    public function blogAccueilAction(Request $request, $id)
    {
        $blogManager    = $this->get('bdd.blog_manager');
        $blogRepository = $blogManager->getRepository();
        $request->request->set('selection', $id);
        $blogs     = $blogRepository->findAll();
        $batchSize = 5;
        if (count($blogs) != 0) {
            foreach ($blogs as $i => $blog) {
                $blog->setAccueil(0);
                $blogManager->persist($blog);
                if (($i % $batchSize) == 0) {
                    $blogManager->flush();
                }
            }

            $blogManager->flush();
        }

        $jsonResponse = $this->actionActivate($request, 'Blog', 'id', 'setAccueil');

        return $jsonResponse;
    }

    /**
     * @Route("/blog/json/tagselection/{id}", name="beblog_tagselection")
     *
     * Selection des tags
     *
     * @param int $id Identifiant de la table blog
     *
     * @return json
     */
    public function blogTagSelectionAction($id)
    {
        $blogManager     = $this->get('bdd.blog_manager');
        $blogRepository  = $blogManager->getRepository();
        $tagManager      = $this->get('bdd.tag_manager');
        $tagRepository   = $tagManager->getRepository();
        $tab['all']      = [];
        $tab['selected'] = [];
        $tags            = $tagRepository->findAll();
        foreach ($tags as $key => $tag) {
            $tab['all'][$key] = $tag->getNom();
        }

        if ($id != 0) {
            $blog = $blogRepository->find($id);
            foreach ($blog->getTags() as $key => $tag) {
                $tab['selected'][$key] = $tag->getNom();
            }
        }

        $json = new JsonResponse($tab);

        return $json;
    }
}
