<?php

namespace Mkk\AdminBundle\Controller\Blog;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

trait SearchTrait
{
    /**
     * @Route("/blog/search/categorie", name="beblog_searchcategorie")
     */
    public function blogSearchCategorieAction()
    {
        $response = $this->get('mkk.search_service')->getResponse('bdd.categorie_manager', 'searchCategorieBlog');
        $json     = new JsonResponse($response);

        return $json;
    }

    /**
     * @Route("/blog/search/redacteur", name="beblog_searchredacteur")
     */
    public function blogSearchRedacteurAction()
    {
        $response = $this->get('mkk.search_service')->getResponse('bdd.user_manager', 'searchUserConnect');
        $json     = new JsonResponse($response);

        return $json;
    }
}
