<?php

namespace Mkk\AdminBundle\Controller;

use Mkk\AdminBundle\Controller\Backend\ActionTrait;
use Mkk\AdminBundle\Controller\Backend\DebugTrait;
use Mkk\AdminBundle\Controller\Traits\TraitCategorie;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

trait BackendTrait
{
    use TraitCategorie;
    use ActionTrait;
    use DebugTrait;
    use BlogTrait;
    use BookmarkTrait;
    use BugsTrait;
    use ContactTrait;
    use DiaporamaTrait;
    use DataTrait;
    use DroitTrait;
    use EditoTrait;
    use EtablissementTrait;
    use EvenementTrait;
    use GroupTrait;
    use MenuTrait;
    use MailerTrait;
    use MetarianeTrait;
    use MoncompteTrait;
    use NewsletterTrait;
    use NoteinterneTrait;
    use PageTrait;
    use ParamTrait;
    use PartenaireTrait;
    use RevolutionSliderTrait;
    use SystemTrait;
    use TagTrait;
    use TemplatesTrait;
    use UserTrait;

    /**
     * @Route("/debug", name="bedebug_index")
     * Affichage des différents librairies du site
     *
     * @param Request $request Pile de requêtes
     *
     * @return html
     */
    public function debugAction(Request $request)
    {
        $phpmd  = $this->getPhpmd();
        $phpcpd = $this->getPhpcpd();
        $phpcs  = $this->getPhpcs();
        $param  = [
            'phpmd'   => $phpmd,
            'phpcpd'  => $phpcpd,
            'phpcs'   => $phpcs,
        ];

        $htmlTwig = $this->renderMkk(
            $request,
            'MkkAdminBundle:Debug:index.html.twig',
            $param
        );

        return $htmlTwig;
    }

    /**
     * @Route("/fichiers", name="befichier_index")
     * Gestion des fichiers
     *
     * @param Request $request Pile de requêtes
     *
     * @return html
     */
    public function fichierIndexAction(Request $request)
    {
        $actions = [
            ['type' => 'return', 'url' => 'bord_index', 'text' => 'Retour accueil'],
        ];
        if (! is_dir('filemanager')) {
            mkdir('filemanager');
            $this->cpy('../public/filemanager', 'filemanager');
        }

        $this->get('mkk.action_service')->set($actions);
        $filename = $request->server->get('SCRIPT_FILENAME');
        $url      = str_replace(['app.php', 'app_dev.php', '/web/'], '', $filename);
        $url      = substr($url, strrpos($url, '/') + 1);
        $urlsite  = $this->getUrlSite($request);
        $md5      = md5('48tp6QNp' . $url . date('m/Y'));
        $code     = $urlsite;
        $code     = $code . '/filemanager/dialog.php?type=0&lang=fr_FR&popup=0&crossdomain=0&akey=' . $md5;
        $json     = ['urlfilemanager' => $code];
        $htmlTwig = $this->renderMkk($request, 'MkkAdminBundle:Default:fichiers.html.twig', $json);

        return $htmlTwig;
    }

    /**
     * @Route("/document", name="bedocument_index")
     * Gestion des documents
     *
     * @param Request $request Pile de requêtes
     *
     * @return html
     */
    public function documentIndexAction(Request $request)
    {
        $actions = [
            ['type' => 'return', 'url' => 'bord_index', 'text' => 'Retour accueil'],
        ];
        $this->get('mkk.action_service')->set($actions);
        $filename = $request->server->get('SCRIPT_FILENAME');
        $url      = str_replace(['app.php', 'app_dev.php', '/web/'], '', $filename);
        $url      = substr($url, strrpos($url, '/') + 1);
        $urlsite  = $this->getUrlSite($request);
        if (! is_dir('fichiers/documents')) {
            mkdir('fichiers/documents');
        }

        $md5      = md5('48tp6QNp' . $url . date('m/Y') . 'dir=documents');
        $url      = $urlsite;
        $url      = $url . '/filemanager/dialog.php?type=0&lang=fr_FR&popup=0&crossdomain=0&akey=' . $md5;
        $json     = ['urlfilemanager' => $url];
        $htmlTwig = $this->renderMkk($request, 'MkkAdminBundle:Default:fichiers.html.twig', $json);

        return $htmlTwig;
    }

    private function setFileUploadSystemParam($request, $md5)
    {
        $paramService = $this->get('mkk.param_service');
        $data         = [];
        foreach ($md5 as $code => $id) {
            $fichiers = glob('tmp/' . $id . '/*.*');
            if (count($fichiers) == 1) {
                foreach ($fichiers as $fichier) {
                    if (is_file($fichier)) {
                        $extension = substr($fichier, strrpos($fichier, '.'));
                        $newfile   = $code . $extension;
                        if ($code == 'avatar') {
                            $newfile = 'fichiers/' . $newfile;
                        } elseif ($code == 'login') {
                            $newfile = 'fichiers/login/' . $newfile;
                        }

                        if ($code != 'login') {
                            $data[$code] = $newfile;
                        } else {
                            $data[$code] = [$newfile];
                        }

                        @copy($fichier, $newfile);
                    }
                }
            } elseif (count($fichiers) > 1) {
                $data[$code] = [];
                foreach ($fichiers as $key => $fichier) {
                    if (is_file($fichier)) {
                        $newfile = substr($fichier, strrpos($fichier, '/') + 1);
                        if ($code == 'login') {
                            $newfile = 'fichiers/login/' . $newfile;
                        }

                        @copy($fichier, $newfile);
                        $data[$code][$key] = $newfile;
                    }
                }
            } else {
                if ($code == 'login') {
                    $data['login'] = [];
                } else {
                    $data[$code] = '';
                }
            }
        }

        foreach ($data as $code => $val) {
            $paramService->save($code, $val);
        }

        $this->traitementUploadDelicious($request);
    }

    private function traitementUploadDelicious(Request $request)
    {
        $md5 = $request->request->get('md5');
        if (! isset($md5['delicious'])) {
            return false;
        }
        $files = $this->getUploadFiles($request);
        if (! isset($files['delicious'][0])) {
            return false;
        }

        $file     = $files['delicious'][0];
        $contents = file_get_contents($file);
        $this->traiterContentsDelicious($contents);
    }

    private function getUploadFiles(Request $request)
    {
        $data        = [];
        $filename    = $request->server->get('SCRIPT_FILENAME');
        $emplacement = dirname($filename);
        $md5         = $request->request->get('md5');
        foreach ($md5 as $fichier => $code) {
            $data[$fichier] = [];
            if (is_dir($emplacement . '/tmp/' . $code)) {
                $fichiers = glob($emplacement . '/tmp/' . $code . '/*');
                if (count($fichiers) != 0) {
                    foreach ($fichiers as $file) {
                        if (is_file($file)) {
                            $data[$fichier][] = $file;
                        }
                    }
                }
            }
        }

        return $data;
    }

    private function traiterContentsDelicious($html)
    {
        $tab = explode("\n", $html);
        foreach ($tab as $i => $row) {
            if (substr_count($row, '<DD>') == 1) {
                $tab[$i] = $tab[$i] . '</DD>';
            }
        }

        $html = implode($tab);
        $doc  = new \DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTML($html);
        $as     = $doc->getElementsByTagName('a');
        $tabUrl = [];
        foreach ($as as $row) {
            $href  = $row->getAttribute('href');
            $date  = $row->getAttribute('add_date');
            $tags  = $row->getAttribute('tags');
            $title = $row->textContent;
            $md5   = md5($href);

            $tabUrl[$md5] = [
                    'url'         => $href,
                    'date'        => $date,
                    'tags'        => explode(',', $tags),
                    'title'       => $title,
                    'description' => '',
                ];
        }
        $tabUrl = $this->getComments($tabUrl, $html);
        $this->enregistrerDelicious($tabUrl);
    }

    private function enregistrerDelicious($tabUrl)
    {
        $paramManager    = $this->get('bdd.param_manager');
        $paramRepository = $paramManager->getRepository();
        $bookmarkManager = $this->get('bdd.bookmark_manager');
        $batchSize       = 5;
        $i               = 0;
        foreach ($tabUrl as $row) {
            $this->enregistrerBookmarkDelicious($batchSize, $i, $row);
            ++$i;
        }

        $bookmarkManager->flush();
        $params = $this->get('mkk.param_service')->listing();
        if (isset($params['image_delicious'])) {
            if (is_file($params['image_delicious'])) {
                unlink($params['image_delicious']);
            }
        }

        $search = [
                'code' => 'image_delicious',
            ];

        $param = $paramRepository->findoneby($search);
        if ($param) {
            $paramManager->removeAndFlush($param);
        }
    }

    private function enregistrerBookmarkDelicious($batchSize, $i, $row)
    {
        $bookmarkManager    = $this->get('bdd.bookmark_manager');
        $bookmarkRepository = $bookmarkManager->getRepository();
        $bookmarkEntity     = $bookmarkManager->getTable();
        $tagManager         = $this->get('bdd.tag_manager');
        $tagRepository      = $tagManager->getRepository();
        $tagEntity          = $tagManager->getTable();
        $search             = [
                'url' => $row['url'],
            ];

        $bookmark = $bookmarkRepository->findoneby($search);
        if (! $bookmark) {
            $bookmark = new $bookmarkEntity();
            $bookmark->setDate($row['date']);
            $bookmark->setUrl($row['url']);
        }

        $bookmark->setTitre($row['title']);
        $bookmark->setDescription($row['description']);
        $bookmark->setMetaTitre($row['title']);
        $bookmark->setMetaDescription($row['description']);
        $bookmark->setMetaKeywords(trim(implode(' ', $row['tags'])));
        $tags = $bookmark->getTags();
        if (count($tags) != 0) {
            foreach ($tags as $tag) {
                $bookmark->removeTag($tag);
                $tag->removeBookmark($bookmark);
                $tagManager->persist($tag);
            }
            $tagManager->flush();
        }

        $presentTags = [];
        foreach ($row['tags'] as $val) {
            $val = strtolower(trim($val));
            if ($val != '' && ! isset($presentTags[$val])) {
                $presentTags[$val] = 1;
                $search            = [
                        'nom' => $val,
                    ];

                $nbtag = $tagRepository->findOneBy($search);
                if (! $nbtag) {
                    $tag = new $tagEntity();
                    $tag->setNom($val);
                    $tag->addBookmark($bookmark);
                    $bookmark->addTag($tag);
                    $tagManager->persist($tag);
                } else {
                    $nbtag->addBookmark($bookmark);
                    $bookmark->addTag($nbtag);
                    $tagManager->persist($nbtag);
                }
            }
        }
        $bookmarkManager->persist($bookmark);
        if (($i % $batchSize) == 0) {
            $bookmarkManager->flush();
        }
    }

    private function getComments($tabUrl, $html)
    {
        $doc = new \DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTML($html);
        $dd = $doc->getElementsByTagName('dd');
        foreach ($dd as $row) {
            $text                        = $row->textContent;
            $traitement                  = substr($html, 0, strpos($html, $text));
            $href                        = $this->getUrlDelicious($traitement);
            $md5                         = md5($href);
            $tabUrl[$md5]['description'] = $text;
        }

        return $tabUrl;
    }

    private function getUrlDelicious($traitement)
    {
        $doc = new \DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTML($traitement);
        $as     = $doc->getElementsByTagName('a');
        $length = $as->length;
        $url    = $as->item($length - 1)->getAttribute('href');

        return $url;
    }
}
