<?php

namespace Mkk\AdminBundle\Controller\User;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

trait UploadTrait
{
    /**
     * @Route("/user/upload/avatar/{md5}", name="beuser_uploadavatar")
     *
     * @param mixed $md5
     */
    public function userUploadAvatarAction()
    {
        $options = [
            'max_number_of_files' => 1,
        ];

        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }
}
