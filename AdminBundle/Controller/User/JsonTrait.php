<?php

namespace Mkk\AdminBundle\Controller\User;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

trait JsonTrait
{
    /**
     * @Route("/user/json/actif", name="beuser_json-actif")
     */
    public function userActifAction(Request $request)
    {
        $json = $this->actionActivate($request, 'User', 'id', 'setEnabled');

        return $json;
    }
}
