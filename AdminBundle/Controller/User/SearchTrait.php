<?php

namespace Mkk\AdminBundle\Controller\User;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

trait SearchTrait
{
    /**
     * @Route("/user/search/etablissement", name="beuser_searchetablissement")
     */
    public function userSearchEtablissementAction()
    {
        $response = $this->get('mkk.search_service')->getResponse('bdd.etablissement_manager', 'searchEtablissement');
        $json     = new JsonResponse($response);

        return $json;
    }

    /**
     * @Route("/user/search/group", name="beuser_searchgroup")
     */
    public function userSearchGroupAction()
    {
        $response = $this->get('mkk.search_service')->getResponse('bdd.group_manager', 'searchGroupConnect');
        $json     = new JsonResponse($response);

        return $json;
    }

    /**
     * @Route("/user/search/user", name="beuser_searchuser")
     *
     * Recherche d'un utilisateur
     *
     * @param Request $request pile de requêtes
     *
     * @return json
     */
    public function userSearchUserAction()
    {
        $groupManager    = $this->get('bdd.group_manager');
        $groupRepository = $groupManager->getRepository();
        $userManager     = $this->get('bdd.user_manager');
        $userRepository  = $userManager->getRepository();
        $responsejson    = [];
        $user            = $this->get('security.token_storage')->getToken()->getUser();
        $request         = $this->get('request');
        $search          = $request->request->get('searchnewuser');
        $page            = $request->request->get('page');
        $lettre          = $request->query->get('lettre');
        $url             = $request->request->get('url');
        $get             = $request->query->all();
        $post            = $request->request->all();
        if (isset($get['lettre'])) {
            $groups    = $groupRepository->findby([]);
            $tabgroups = [];
            foreach ($groups as $key => $group) {
                $code = $group->getCode();
                if ($code == 'superadmin') {
                    if ($user->getRefGroup()->getCode() == 'superadmin') {
                        $tabgroups[$key] = $code;
                    }
                } else {
                    $tabgroups[$key] = $code;
                }
            }

            $search                = [
                'lettre' => $lettre,
            ];
            $tabgroups             = [];
            $responsejson['users'] = [];
            $data                  = $userRepository->searchUser($tabgroups, $search, $page, 10);
            foreach ($data['user'] as $key => $user) {
                $responsejson['users'][$key] = ['id' => $user[0]->getId(), 'nom' => $user[0]->getAppel()];
            }

            $responsejson['total'] = $data['pagination']['max'];
        } elseif (isset($post['id'])) {
            $user = $userRepository->find($post['id']);
            if ($user) {
                $responsejson = [
                    'id'  => $user->getId(),
                    'nom' => $user->getAppel(),
                ];
            }
        } else {
            $groups                     = [];
            $data                       = $userRepository->searchUser($groups, $search, $page, 10);
            $responsejson['pagination'] = $data['pagination'];
            $responsejson['user']       = [];
            foreach ($data['user'] as $user) {
                $adresse = '';
                if (count($user[0]->getAdresses()) != 0) {
                    $adresses = $user[0]->getAdresses();
                    $adresse  = $adresses[0]->getInfo() . ' ' . $adresses[0]->getCp() . ' ' . $adresses[0]->getVille();
                }

                if ($url == '') {
                    $url = 'beuser_form';
                }

                $adresseurl             = $this->generateUrl($url, ['id' => $user[0]->getId()]);
                $responsejson['user'][] = [
                    'id'      => $user[0]->getId(),
                    'appel'   => $user[0]->getAppel(),
                    'groupe'  => $user[0]->getRefGroup()->getNom(),
                    'societe' => $user[0]->getSociete(),
                    'url'     => $adresseurl,
                    'adresse' => $adresse,
                ];
            }
        }

        $json = new JsonResponse($responsejson);

        return $json;
    }
}
