<?php

namespace Mkk\AdminBundle\Controller\Contact;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;

trait ExportTrait
{
    /**
     * @Route("/contact/export/vcard", name="becontact_exportvcard")
     *
     * Liste des contacts en VCARD
     *
     * @param Request $request pile de requêtes
     *
     * @return file.vcard
     */
    public function vcardAction(Request $request)
    {
        $user     = $this->get('security.token_storage')->getToken()->getUser();
        $response = new StreamedResponse(
            function () use ($request, $user) {
                $this->streamVcard($request, $user);
            }
        );
        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-Disposition', 'attachment; filename="export' . date('d-m-Y_his') . '.vcf"');

        return $response;
    }

    /**
     * @Route("/contact/export/csv", name="becontact_exportcsv")
     *
     * Liste des contacts en CSV
     *
     * @param Request $request pile de requêtes
     *
     * @return file.csv
     */
    public function csvAction(Request $request)
    {
        $userManager     = $this->get('bdd.user_manager');
        $userRepository  = $userManager->getRepository();
        $groupManager    = $this->get('bdd.group_manager');
        $groupRepository = $groupManager->getRepository();
        $em              = $this->getDoctrine()->getManager();
        $user            = $this->get('security.token_storage')->getToken()->getUser();
        $response        = new StreamedResponse(
            function () use ($em, $request, $user, $initsearch, $searchparams) {
                ini_set('memory_limit', '-1');

                $serviceTel           = $this->get('mkk.telephone_service');
                $client               = $groupRepository->findoneby(['code' => 'particulier']);
                $group                = ['particulier'];
                $order                = [];
                $selection            = $request->query->get('selection');
                $sens                 = $request->query->get('order');
                $get['recherche']     = $request->query->get('recherche_contact');
                $get['typecontact']   = $request->query->get('typecontact');
                $get['ca_debut']      = $request->query->get('contact_ca_debut');
                $get['ca_fin']        = $request->query->get('contact_ca_fin');
                $get['plateforme']    = $request->query->get('plateforme');
                $get['etablissement'] = $request->query->get('etablissement');
                $get['commercial']    = $request->query->get('commercial');
                $get['chantier']      = $request->query->get('chantier');
                $get['modele']        = $request->query->get('modele_hidden');
                if ($get['recherche'] != '') {
                    $initsearch                  = 1;
                    $search['recherche_contact'] = $get['recherche'];
                }

                if ($get['typecontact'] != '') {
                    $initsearch            = 1;
                    $search['typecontact'] = $get['typecontact'];
                }

                if ($get['plateforme'] != '') {
                    $initsearch           = 1;
                    $search['plateforme'] = $get['plateforme'];
                }

                if ($get['etablissement'] != '') {
                    $initsearch              = 1;
                    $search['etablissement'] = $get['etablissement'];
                }

                if ($get['commercial'] != '') {
                    $initsearch           = 1;
                    $search['commercial'] = $get['commercial'];
                }

                if ($get['ca_fin'] != '' && $get['ca_debut'] != '') {
                    $initsearch         = 1;
                    $search['ca_fin']   = $get['ca_fin'];
                    $search['ca_debut'] = $get['ca_debut'];
                }

                $users = [];
                if ($selection != '') {
                    $selection = explode(',', $selection);
                    foreach ($selection as $id) {
                        $client = $userRepository->find($id);
                        array_push($users, $client);
                    }
                } else {
                    $users = $userRepository->getListingGroupCsvVcard($group, $request->query->all(), null, null, '', $user);
                }

                $tabMaxAdresseTel = $userRepository->getMaxAdressesTelCsv(
                    $group,
                    $request->query->all(),
                    null,
                    null,
                    '',
                    $user
                );
                $handle           = fopen('php://output', 'r+');
                $data             = [
                    'id'          => 'id',
                    'inscription' => 'inscription',
                    'civilite'    => 'civilite',
                    'nom'         => 'nom',
                    'prenom'      => 'prenom',
                    'societe'     => 'societe',
                    'prospect'    => 'Client/prospect',
                    'groupe'      => 'groupe',
                    'revendeur'   => 'revendeur',
                    'observation' => 'observation',
                    'totalca'     => 'Total CA',
                    'totaldevis'  => 'Total Devis',
                ];
                for ($i = 1; $i <= $tabMaxAdresseTel['maxEmail']; ++$i) {
                    $data['email' . $i] = 'email' . $i;
                }

                for ($i = 1; $i <= $tabMaxAdresseTel['maxAdresse']; ++$i) {
                    $data['adresse_' . $i]      = 'adresse ' . $i;
                    $data['adresse_cp' . $i]    = 'cp ' . $i;
                    $data['adresse_ville' . $i] = 'ville ' . $i;
                }

                for ($j = 1; $j <= $tabMaxAdresseTel['maxTelephone']; ++$j) {
                    $data['telephone' . $j] = 'telephone' . $j;
                }

                fputcsv($handle, $data);
                foreach ($users as $user) {
                    $observations = $user->getObservations();
                    $observations = str_replace(',', '', $observations);
                    $observations = preg_replace('/\s+/', ' ', $observations);
                    $observations = trim($observations);
                    $type         = $user->getType();
                    if ($type == '') {
                        $type = 'prospect';
                    }

                    $configuser = $user->getConfigTab();
                    $totalca    = (isset($configuser['totalca'])) ? 0 : $configuser['totalca'];
                    $totaldevis = (isset($configuser['totaldevis'])) ? 0 : $configuser['totaldevis'];
                    $data       = [
                        'id'          => $user->getId(),
                        'inscription' => $user->getDateinscription(),
                        'civilite'    => $user->getCivilite(),
                        'nom'         => $user->getNom(),
                        'prenom'      => $user->getPrenom(),
                        'societe'     => $user->getSociete(),
                        'prospect'    => $type,
                        'groupe'      => $user->getRefGroup(),
                        'revendeur'   => $user->getRefVendeur(),
                        'observation' => $observations,
                        'totalca'     => $totalca,
                        'totaldevis'  => $totaldevis,
                    ];
                    for ($i = 1; $i <= $tabMaxAdresseTel['maxEmail']; ++$i) {
                        $data['email' . $i] = '';
                    }

                    for ($i = 1; $i <= $tabMaxAdresseTel['maxAdresse']; ++$i) {
                        $data['adresse_' . $i]      = '';
                        $data['adresse_cp' . $i]    = '';
                        $data['adresse_ville' . $i] = '';
                    }

                    for ($j = 1; $j <= $tabMaxAdresseTel['maxTelephone']; ++$j) {
                        $data['telephone' . $j] = '';
                    }

                    $iemail = 1;
                    foreach ($user->getEmails() as $mail) {
                        $data['email' . $iemail] = $mail->getAdresse();
                        ++$iemail;
                        $em->detach($mail);
                    }

                    $iadresse = 1;
                    foreach ($user->getAdresses() as $adresse) {
                        $info                              = $adresse->getInfo();
                        $info                              = str_replace(',', '', $info);
                        $info                              = preg_replace('/\s+/', ' ', $info);
                        $info                              = trim($info);
                        $data['adresse_' . $iadresse]      = $info;
                        $data['adresse_cp' . $iadresse]    = trim($adresse->getCp());
                        $data['adresse_ville' . $iadresse] = trim($adresse->getVille());
                        ++$iadresse;
                        $em->detach($adresse);
                    }

                    $itel = 1;
                    foreach ($user->getTelephones() as $tel) {
                        $telephone = '';
                        if ($tel->getChiffre() != '') {
                            $telephone = $serviceTel->parse($tel->getChiffre());
                        }

                        if ($telephone) {
                            $data['telephone' . $itel] = $telephone;
                            ++$itel;
                            $em->detach($tel);
                        }
                    }

                    fputcsv($handle, $data);
                    $em->detach($user);
                }

                fclose($handle);
            }
        );

        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-Disposition', 'attachment; filename="export' . date('d-m-Y_his') . '.csv"');

        return $response;
    }
}
