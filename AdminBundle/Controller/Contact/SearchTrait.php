<?php

namespace Mkk\AdminBundle\Controller\Contact;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

trait SearchTrait
{
    /**
     * @Route("/contact/search", name="becontact_search")
     *
     * Recherche d'un contact
     *
     * @param Request $request pile de requêtes
     *
     * @return json
     */
    public function searchAction()
    {
        $userManager              = $this->get('bdd.user_manager');
        $userRepository           = $userManager->getRepository();
        $responsejson['contacts'] = [];
        $search                   = $request->request->get('searchcontactaffaire');
        $contacts                 = $userRepository->searchContac($search);
        foreach ($contacts as $contact) {
            $adresstext = '';
            foreach ($contact->getAdresses() as $adresse) {
                if ($adresstext != '') {
                    $adresstext = $adresstext . '<br />';
                }

                $adresstext = $adresstext . $adresse->getInfo();
                $adresstext = $adresstext . '<br />' . $adresse->getCp() . ' ' . $adresse->getVille();
                $adresstext = $adresstext . ' (' . $adresse->getPays() . ')';
            }

            array_push(
                $responsejson['contacts'],
                [
                    'id'      => $contact->getId(),
                    'appel'   => $contact->getAppel(),
                    'type'    => $contact->getType(),
                    'groupe'  => $contact->getRefGroup()->getNom(),
                    'adresse' => $adresstext,
                ]
            );
        }

        $json = new JsonResponse($responsejson);

        return $json;
    }

    /**
     * @Route("/contact/search/client", name="becontact_searchclient")
     *
     * Recherche d'un client
     *
     * @param Request $request pile de requêtes
     *
     * @return json
     */
    public function clientAction()
    {
        $response = $this->get('mkk.search_service')->getResponse('bdd.user_manager', 'searchUserClient');
        $json     = new JsonResponse($response);

        return $json;
    }

    /**
     * @Route("/contact/search/commercial", name="becontact_searchcommercial")
     *
     * Recherche du commercial
     *
     * @param Request $request pile de requêtes
     *
     * @return json
     */
    public function commercialAction()
    {
        $response = $this->get('mkk.search_service')->getResponse('bdd.user_manager', 'searchUserConnect');
        $json     = new JsonResponse($response);

        return $json;
    }

    /**
     * @Route("/contact/search/commerciaux", name="becontact_searchcommerciaux")
     *
     * C'est quoi cette fonction ?
     * Recherche des commerciaux
     *
     * @return json
     */
    public function commerciauxAction()
    {
        $etablissementManager    = $this->get('bdd.etablissement_manager');
        $etablissementRepository = $etablissementManager->getRepository();
        $userManager             = $this->get('bdd.user_manager');
        $userRepository          = $userManager->getRepository();
        $token                   = $this->get('security.token_storage')->getToken();
        $utilisateur             = $token->getUser();
        $listing                 = $this->arrayTabEtablissement($utilisateur);
        if (count($listing) != 0) {
            $listing = implode(',', array_flip($listing));
        } else {
            $listing = '';
        }

        $tab            = [
            'plateforme'   => [],
            'revendeur'    => [],
            'pointconseil' => [],
            'user'         => [],
        ];
        $etablissements = $etablissementRepository->findSelect($listing);
        foreach ($etablissements as $etablissement) {
            $type            = $etablissement->getType();
            $id              = $etablissement->getId();
            $tab[$type][$id] = $etablissement->getNom();
        }

        $users = $userRepository->findSelect($listing);
        foreach ($users as $user) {
            $id               = $user->getId();
            $tab['user'][$id] = $user->getAppel();
        }

        $responsejson = [
            'plateforme'   => [],
            'revendeur'    => [],
            'pointconseil' => [],
            'user'         => [],
        ];
        foreach (array_keys($responsejson) as $code) {
            $responsejson[$code] = array_flip($tab[$code]);
            ksort($responsejson[$code]);
        }

        $json = new JsonResponse($responsejson);

        return $json;
    }

    /**
     * @Route("/contact/search/group", name="becontact_searchgroup")
     */
    public function contactSearchGroupAction()
    {
        $response = $this->get('mkk.search_service')->getResponse('bdd.group_manager', 'searchGroupContact');
        $json     = new JsonResponse($response);

        return $json;
    }
}
