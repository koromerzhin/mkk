<?php

namespace Mkk\AdminBundle\Controller\Contact;

use Sabre\VObject;
use Symfony\Component\Intl\Intl;

/**
 * Page controller.
 */
trait ActionTrait
{
    /**
     * get the vcard of the contact.
     *
     * @param mixed $user
     */
    public function getVcard($user)
    {
        $nom      = trim($user->getNom());
        $civilite = trim($user->getCivilite());
        $prenom   = trim($user->getPrenom());
        $vcard    = new VObject\Component\VCard(
            [
                'FN' => "$prenom $nom",
                'N'  => [$nom, $prenom, '', '', $civilite],
            ]
        );
        if (count($user->getEmails()) != 0) {
            foreach ($user->getEmails() as $email) {
                $vcard->add(
                    'EMAIL',
                    $email->getAdresse(),
                    [
                        'type' => $email->getType(),
                    ]
                );
            }
        }

        if (count($user->getTelephones()) != 0) {
            foreach ($user->getTelephones() as $telephone) {
                if ($telephone->getUtilisation() == 'glyphicon-phone-alt') {
                    $code = 'HOME';
                } elseif ($telephone->getUtilisation() == 'glyphicon-phone') {
                    $code = 'CELL';
                } else {
                    $code = 'UNKNOWN';
                }

                $vcard->add(
                    'TEL',
                    $telephone->getChiffre(),
                    [
                        'type' => $code,
                    ]
                );
            }
        }

        if (count($user->getAdresses()) != 0) {
            foreach ($user->getAdresses() as $adresse) {
                $c     = Intl::getRegionBundle()->getCountryNames('fr');
                $pays  = $adresse->getPays();
                $info  = $adresse->getInfo();
                $cp    = $adresse->getCp();
                $ville = $adresse->getVille();
                $pays  = array_key_exists(strtoupper($pays), $c) ? $c[strtoupper($pays)] : $pays;
                $vcard->add(
                    'ADR',
                    ['', '', $info, $ville, '', $cp, $pays]
                );
            }
        }

        $text = $vcard->serialize();

        return $text;
    }

    public function streamVcard($request, $user)
    {
        set_time_limit(0);
        $users  = $this->rechercheContact($request, $user);
        $handle = fopen('php://output', 'r+');
        foreach ($users as $utilisateur) {
            $text = $this->getVcard($utilisateur);
            fwrite($handle, $text);
        }

        fclose($handle);
    }

    private function rechercheContact($request, $user)
    {
        $userManager     = $this->get('bdd.user_manager');
        $userRepository  = $userManager->getRepository();
        $groupManager    = $this->get('bdd.group_manager');
        $groupRepository = $groupManager->getRepository();
        $client          = $groupRepository->findoneby(['code' => 'particulier']);

        $search = [
            'group' => [
                $client->getId(),
            ],
        ];

        $group                = ['particulier'];
        $order                = [];
        $selection            = $request->query->get('selection');
        $get['recherche']     = $request->query->get('recherche_contact');
        $get['typecontact']   = $request->query->get('typecontact');
        $get['ca_debut']      = $request->query->get('contact_ca_debut');
        $get['ca_fin']        = $request->query->get('contact_ca_fin');
        $get['plateforme']    = $request->query->get('plateforme');
        $get['etablissement'] = $request->query->get('etablissement');
        $get['commercial']    = $request->query->get('commercial');
        $get['chantier']      = $request->query->get('chantier');
        $get['modele']        = $request->query->get('modele_hidden');
        if ($get['recherche'] != '') {
            $search['recherche_contact'] = $get['recherche'];
        }

        if ($get['typecontact'] != '') {
            $search['typecontact'] = $get['typecontact'];
        }

        if ($get['plateforme'] != '') {
            $search['plateforme'] = $get['plateforme'];
        }

        if ($get['etablissement'] != '') {
            $search['etablissement'] = $get['etablissement'];
        }

        if ($get['commercial'] != '') {
            $search['commercial'] = $get['commercial'];
        }

        if ($get['ca_fin'] != '' && $get['ca_debut'] != '') {
            $search['ca_fin']   = $get['ca_fin'];
            $search['ca_debut'] = $get['ca_debut'];
        }

        $users = [];
        if ($selection != '') {
            $selection = explode(',', $selection);
            foreach ($selection as $id) {
                $client = $userRepository->find($id);
                array_push($users, $client);
            }
        } else {
            $users = $userRepository->getListingGroupCsvVcard(
                $group,
                $request->query->all(),
                null,
                null,
                '',
                $user
            );
        }

        return $users;
    }
}
