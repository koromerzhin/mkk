<?php

namespace Mkk\AdminBundle\Controller;

use Mkk\AdminBundle\Controller\Mailer\JsonTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

trait MailerTrait
{
    use JsonTrait;

    /**
     * @Route("/mailer/", name="bemailer_index", defaults={"page": 0})
     * @Route("/mailer/{page}.html", name="bemailer_page")
     *
     * Liste des articles du blog
     *
     * @param Request $request pile de requêtes
     *
     * @return Mailer:index.html.twig
     */
    public function mailerIndexAction(Request $request)
    {
        $listingService = $this->get('mkk.listing_service');
        $listingService->setRequest('bdd.mailer_manager', 'searchMailer');
        $actions = [
            ['type' => 'data', 'entity' => 'mailer'],
        ];
        $this->get('mkk.action_service')->set($actions);
        $twigindex = 'MkkAdminBundle:Mailer:index.html.twig';
        $twigHtml  = $this->renderMkk(
            $request,
            $twigindex
        );

        return $twigHtml;
    }

    /**
     * @Route("/mailer/show/{id}", name="bemailer_show", defaults={"id": 0})
     *
     * Formulaire d'un article du blog.
     *
     * @param Request $request pile de requêtes
     * @param int     $id      Identifiant de l'article du blog
     *
     * @return JsonResponse /  Mailer:form.html.twig
     */
    public function mailerFormAction(Request $request, $id)
    {
        $mailerManager    = $this->get('bdd.mailer_manager');
        $mailerRepository = $mailerManager->getRepository();
        $entity           = $mailerRepository->find($id);
        if (! $entity) {
            $flashbag = $request->getSession()->getFlashBag();
            $flashbag->add('warning', 'Données introuvable');
            $url      = $this->generateUrl('bemailer_index');
            $redirect = $this->redirect($url, 301);

            return $redirect;
        }

        $actions = [
            ['type' => 'return', 'url' => 'bemailer_index'],
        ];
        $this->get('mkk.action_service')->set($actions);
        $htmlTwig = $this->renderMkk(
            $request,
            'MkkAdminBundle:Mailer:show.html.twig',
            [
                'entity' => $entity,
            ]
        );

        return $htmlTwig;
    }
}
