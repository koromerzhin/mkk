<?php

namespace Mkk\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

trait TagTrait
{
    /**
     * @Route("/tag/", name="betag_index", defaults={"page": 0})
     * @Route("/tag/{page}.html", name="betag_page")
     *
     * Liste des tags
     *
     * @param Request $request pile de requêtes
     * @param int     $page    numéro de la page
     *
     * @return html
     */
    public function tagIndexAction(Request $request)
    {
        $listingService = $this->get('mkk.listing_service');
        $listingService->setRequest('bdd.tag_manager', 'searchTag');
        $tagManager  = $this->get('bdd.categorie_manager');
        $tageTable   = $tagManager->getTable();
        $entity      = new $tageTable();
        $siteService = $this->get('mkk.site_service');
        $form        = $siteService->getForm('mkk_admin.form.tag', $tagManager, $entity, 'ajouter');
        if ($siteService->isRedirect()) {
            $url      = $this->generateUrl('betag_index');
            $redirect = $this->redirect($url, 301);

            return $redirect;
        }

        $actions = [
            ['type' => 'add', 'url' => 'betag_form'],
            ['type' => 'data', 'entity' => 'tag'],
        ];

        $this->get('mkk.action_service')->set($actions);
        $htmlTwig = $this->renderMkk(
            $request,
            'MkkAdminBundle:Tag:index.html.twig',
            [
                'form' => $siteService->createView($form),
            ]
        );

        return $htmlTwig;
    }

    /**
     * @Route("/tag/form/{id}", name="betag_form", defaults={"id": 0})
     *
     * Formulaire des tags
     *
     * @param Request $request pile de requêtes
     * @param int     $id      identifiant de la table tag
     *
     * @return html / json
     */
    public function tagFormAction(Request $request, $id)
    {
        $crudService = $this->get('mkk.crud_service');
        $crudService->setManager('bdd.tag_manager');
        $crudService->setId($id);
        $crudService->setFormulaireID('FormTag');
        $crudService->setUrl(
            [
                'listing' => 'betag_index',
                'form'    => 'betag_form',
            ]
        );
        $crudService->setController($this);
        $crudService->setFormService('mkk_admin.form.tag');
        $crudService->setTwigHTML('MkkAdminBundle:Tag:form.html.twig');
		$crudService->init();

        return $crudService->launch();
    }
}
