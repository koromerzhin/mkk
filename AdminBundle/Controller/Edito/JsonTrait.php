<?php

namespace Mkk\AdminBundle\Controller\Edito;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

trait JsonTrait
{
    /**
     * @Route("/edito/json/actif", name="beedito_json-actif")
     *
     * Active/désactive un article du blog
     *
     * @return JsonResponse
     */
    public function editoActifAction(Request $request)
    {
        $jsonResponse = $this->actionActivate($request, 'Edito', 'id', 'setPublier');

        return $jsonResponse;
    }

    /**
     * @Route("/edito/json/avant/{id}", name="beedito_json-avant")
     *
     * Met en avant un article du blog
     *
     * @param Request $request pile de requêtes
     * @param int     $id      Identifiant de l'article du blog
     *
     * @return JsonResponse
     */
    public function editoAvantAction(Request $request, $id)
    {
        $request->request->set('selection', $id);

        $retour = $this->actionActivate($request, 'Edito', 'id', 'setAvant');

        return $retour;
    }

    /**
     * @Route("/edito/json-accueil/{id}", name="beedito_json-accueil")
     *
     * Mettre à l'accueil un article du blog
     *
     * @param Request $request pile de requêtes
     * @param int     $id      Identifiant de l'article du blog
     *
     * @return JsonResponse
     */
    public function editoAccueilAction(Request $request, $id)
    {
        $editoManager    = $this->get('bdd.edito_manager');
        $editoRepository = $editoManager->getRepository();
        $request->request->set('selection', $id);
        $blogs     = $editoRepository->findAll();
        $batchSize = 5;
        if (count($blogs) != 0) {
            foreach ($blogs as $i => $blog) {
                $blog->setAccueil(0);
                $editoManager->persist($blog);
                if (($i % $batchSize) == 0) {
                    $editoManager->flush();
                }
            }

            $editoManager->flush();
        }

        $jsonResponse = $this->actionActivate($request, 'Edito', 'id', 'setAccueil');

        return $jsonResponse;
    }
}
