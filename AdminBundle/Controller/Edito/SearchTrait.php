<?php

namespace Mkk\AdminBundle\Controller\Edito;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

trait SearchTrait
{
    /**
     * @Route("/edito/search/redacteur", name="beedito_searchredacteur")
     */
    public function editoSearchRedacteurAction()
    {
        $response = $this->get('mkk.search_service')->getResponse('bdd.user_manager', 'searchUserConnect');
        $json     = new JsonResponse($response);

        return $json;
    }
}
