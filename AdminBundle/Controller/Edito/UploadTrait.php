<?php

namespace Mkk\AdminBundle\Controller\Edito;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

trait UploadTrait
{
    /**
     * @Route("/edito/vignette/{md5}", name="beedito_uploadvignette")
     *
     * Upload d'une vignette d'un article du blog
     *
     * @param Hash $md5 Hash de la vignette
     *
     * @return Response
     */
    public function editoVignetteAction()
    {
        $options = [
            'max_number_of_files' => 1,
        ];

        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }

    /**
     * @Route("/edito/image/{md5}", name="beedito_uploadimage")
     *
     * Upload d'une image d'un article du blog
     *
     * @param Hash $md5 Hash de l'image'
     *
     * @return Response
     */
    public function editoImageAction()
    {
        $options = [
            'max_number_of_files' => 1,
        ];

        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }

    /**
     * @Route("/edito/galerie/{md5}", name="beedito_uploadgalerie")
     *
     * Upload d'une ou plusieurs images d'un article du blog
     *
     * @param Hash $md5 Hash de la galerie
     *
     * @return Response
     */
    public function editoGalerieAction()
    {
        $options  = [];
        $retour   = $this->get('mkk.upload_service')->ajax($options);
        $response = new Response($retour);

        return $response;
    }
}
