<?php

namespace Mkk\AdminBundle\Controller;

use Mkk\AdminBundle\Controller\Diaporama\AffichageTrait;
use Mkk\AdminBundle\Controller\Diaporama\JsonTrait;
use Mkk\AdminBundle\Controller\Diaporama\UploadTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

trait DiaporamaTrait
{
    use AffichageTrait;
    use UploadTrait;

    /**
     * @Route("/diaporama/", name="bediaporama_index", defaults={"page": 0})
     * @Route("/diaporama/{page}.html", name="bediaporama_page")
     *
     * Liste des diaporama.
     *
     * @param Request $request pile de requêtes
     * @param int     $page    identifiant de la page
     *
     * @return html
     */
    public function diaporamaIndexAction(Request $request)
    {
        $listingService = $this->get('mkk.listing_service');
        $listingService->setRequest('bdd.diaporama_manager', 'searchDiaporama');
        $htmlTwig = $this->affichageBlogDiaporamaIndex($request);

        return $htmlTwig;
    }

    /**
     * @Route("/diaporama/form/{id}", name="bediaporama_form", defaults={"id": 0})
     *
     * Formulaire du diaporama
     *
     * @param Request $request pile de requêtes
     * @param int     $id      Identifiant du diaporama
     *
     * @return json /  html
     */
    public function diaporamaFormAction(Request $request, $id)
    {
        $crudService = $this->get('mkk.crud_service');
        $crudService->setManager('bdd.diaporama_manager');
        $crudService->setId($id);
        $crudService->setFormulaireID('FormDiaporama');
        $crudService->setUrl(
            [
                'listing' => 'bediaporama_index',
                'form'    => 'bediaporama_form',
            ]
        );
        $crudService->setController($this);
        $crudService->setFormService('mkk_admin.form.diaporama');
        $crudService->setTwigHTML('MkkAdminBundle:Diaporama:form.html.twig');
		$crudService->init();

        return $crudService->launch();
    }
}
