<?php

namespace Mkk\AdminBundle\Controller\Metariane;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

trait JsonTrait
{
    /**
     * @Route("/metatagsariane/json/module", name="bemetariane_json-module")
     */
    public function metarianeJsonModuleAction(Request $request)
    {
        $metarianeManager    = $this->get('bdd.metariane_manager');
        $metarianeRepository = $metarianeManager->getRepository();
        $metarianeEntity     = $metarianeManager->getTable();
        $jsonresponse        = [
            'new' => 0,
        ];

        $selection = trim($request->query->get('selection'));
        $routes    = $this->getRoutes();
        $recherche = [];
        if ($selection != '') {
            foreach (array_keys($routes) as $route) {
                if (substr_count($route, $selection) != 0) {
                    $recherche[] = $route;
                }
            }
        }

        $batchSize = 20;
        $i         = 0;
        foreach ($recherche as $route) {
            $entity = $metarianeRepository->findoneby(['route' => $route]);
            if (! $entity) {
                $entity = new $metarianeEntity();
                $entity->setRoute($route);
                ++$i;
                $metarianeManager->persist($entity);
                $jsonresponse['new'] = 1;
            }

            if (($i % $batchSize) == 0) {
                $metarianeManager->flush();
            }
        }

        $metarianeManager->flush();

        $routes = [];
        foreach (array_keys($this->getRoutes()) as $tab) {
            list($module, $action) = explode('_', $tab);
            if (! isset($routes[$module])) {
                $routes[$module] = '';
            } else {
                $routes[$module] = $routes[$module] . ',';
            }

            $routes[$module] = $routes[$module] . $action;
        }

        foreach ($routes as $module => $actions) {
            $routes[$module] = explode(',', $actions);
        }

        ksort($routes);

        $metarianes = $metarianeRepository->findall();
        $i          = 0;
        foreach ($metarianes as $metariane) {
            $route = $metariane->getRoute();
            if (substr_count($route, '_') != 0) {
                list($module, $action) = explode('_', $route);
                if (! isset($routes[$module])) {
                    ++$i;
                    $responsejson['new'] = 1;
                    $metarianeManager->remove($metariane);
                }
            } else {
                if (! isset($routes[$route])) {
                    ++$i;
                    $responsejson['new'] = 1;
                    $metarianeManager->remove($metariane);
                }
            }

            if (($i % $batchSize) == 0) {
                $metarianeManager->flush();
            }
        }

        $metarianeManager->flush();

        $jsonresponse = new JsonResponse($jsonresponse);

        return $jsonresponse;
    }
}
