<?php

namespace Mkk\AdminBundle\Controller\Group;

trait ActionTrait
{
    /**
     * Initialise les routes.
     */
    public function initRoute()
    {
        $routes          = $this->generateDroitRoute();
        $groupManager    = $this->get('bdd.group_manager');
        $groupRepository = $groupManager->getRepository();
        $groups          = $groupRepository->findall();
        $this->saveNewRoute($routes, $groups);
    }

    /**
     * Génère la liste des routes.
     *
     * @return array
     */
    public function generateDroitRoute()
    {
        $routes = [];
        foreach (array_keys($this->getRoutes()) as $tab) {
            list($module, $action) = explode('_', $tab);
            if ($module != 'besearch'
                && substr_count($action, 'search') == 0
                && substr_count($action, 'upload') == 0
                && substr_count($action, 'vider') == 0
                && substr_count($action, 'page') == 0
            ) {
                if (! isset($routes[$module])) {
                    $routes[$module] = '';
                } else {
                    $routes[$module] = $routes[$module] . ',';
                }

                $routes[$module] = $routes[$module] . $action;
            }
        }

        $list   = $routes;
        $routes = [];
        foreach ($list as $module => $action) {
            $routes[] = [
                'module' => $module,
                'action' => explode(',', $action),
            ];
        }

        ksort($routes);

        return $routes;
    }

    /**
     * Sauvegarde les nouvelles routes.
     *
     * @param array $routes liste des routes
     * @param array $groups liste des groups
     *
     * @return bool
     */
    private function saveNewRoute($routes, $groups)
    {
        $actionManager    = $this->get('bdd.action_manager');
        $actionRepository = $actionManager->getRepository();
        $actionEntity     = $actionManager->getTable();
        $traitement       = 0;
        $batchSize        = 20;
        $countI           = 0;
        foreach ($routes as $tab) {
            foreach ($groups as $group) {
                $action = $actionRepository->findBy(
                    [
                        'module'   => $tab['module'],
                        'refgroup' => $group->getId(),
                    ]
                );
                if (count($action) == 0) {
                    $action = new $actionEntity();
                    $action->setModule($tab['module']);
                    $action->setCodes(implode(',', $tab['action']));
                    $action->setRefGroup($group);
                    $actionManager->persist($action);
                    ++$countI;
                    if (($countI % $batchSize) == 0) {
                        $actionManager->flush();
                    }

                    $traitement = 1;
                }
            }
        }

        $actionManager->flush();

        return $traitement;
    }
}
