<?php

namespace Mkk\AdminBundle\Form\Collection;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdressesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'type',
            Type\TextType::class,
            [
                'label'    => 'Type',
                'required' => false,
                'attr'     => [
                    'class'       => 'InputAdresseUtilisation',
                    'placeholder' => 'Type',
                ],
            ]
        );
        $builder->add(
            'info',
            Type\TextareaType::class,
            [
                'label'    => 'form.label.adresse',
                'required' => false,
                'attr'     => [
                    'class'       => 'mceNoEditor',
                    'placeholder' => 'form.label.adresse',
                ],
            ]
        );
        $builder->add(
            'cp',
            Type\TextType::class,
            [
                'label'    => 'form.label.codepostal',
                'required' => false,
                'attr'     => [
                    'data-url'    => 'script_cpville',
                    'class'       => 'InputAdresseCp',
                    'placeholder' => 'form.label.codepostal',
                ],
            ]
        );
        $builder->add(
            'ville',
            Type\TextType::class,
            [
                'label'    => 'form.label.ville',
                'required' => false,
                'attr'     => [
                    'class'       => 'InputAdresseVille',
                    'placeholder' => 'form.label.ville',
                ],
            ]
        );
        $builder->add(
            'pays',
            Type\CountryType::class,
            [
                'label'       => 'form.label.pays',
                'required'    => false,
                'data'        => 'FR',
                'placeholder' => 'Choisir le pays',
                'attr'        => [
                    'class'       => 'InputAdressePays',
                    'placeholder' => 'form.label.pays',
                ],
            ]
        );
        $builder->add(
            'gps',
            Type\TextType::class,
            [
                'mapped'   => false,
                'label'    => 'GPS',
                'required' => false,
                'attr'     => [
                    'class'    => 'InputAdresseGps',
                ],
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'      => BUNDLEU . '\SiteBundle\Entity\Adresse',
                'csrf_protection' => false,
            ]
        );
    }
}
