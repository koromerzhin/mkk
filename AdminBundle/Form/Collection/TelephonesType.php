<?php

namespace Mkk\AdminBundle\Form\Collection;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TelephonesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'type',
            Type\HiddenType::class,
            [
                'required' => false,
                'attr'     => [
                    'class' => 'InputTelType',
                ],
            ]
        );
        $builder->add(
            'pays',
            Type\HiddenType::class,
            [
                'required' => false,
                'attr'     => [
                    'class' => 'InputTelPays',
                ],
            ]
        );
        $builder->add(
            'utilisation',
            Type\TextType::class,
            [
                'label'    => 'Utilisation',
                'required' => false,
                'attr'     => [
                    'class'       => 'InputTelUtilisation',
                    'placeholder' => 'Utilisation',
                ],
            ]
        );
        $builder->add(
            'chiffre',
            Type\TextType::class,
            [
                'label'    => 'Chiffre',
                'required' => false,
                'attr'     => [
                    'class'        => 'InputTelChiffre',
                    'autocomplete' => 'off',
                ],
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'      => BUNDLEU . '\SiteBundle\Entity\Telephone',
                'csrf_protection' => false,
            ]
        );
    }
}
