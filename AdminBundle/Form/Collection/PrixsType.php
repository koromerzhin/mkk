<?php

namespace Mkk\AdminBundle\Form\Collection;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PrixsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'type',
            Type\TextType::class,
            [
                'label'    => 'Type',
                'required' => false,
                'attr'     => [
                    'placeholder' => 'Type',
                ],
            ]
        );
        $builder->add(
            'chiffre',
            Type\NumberType::class,
            [
                'label'    => 'Montant',
                'required' => false,
                'attr'     => [
                    'placeholder' => 'Montant',
                ],
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'      => BUNDLEU . '\SiteBundle\Entity\Prix',
                'csrf_protection' => false,
            ]
        );
    }
}
