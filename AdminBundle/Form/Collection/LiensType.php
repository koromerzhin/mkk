<?php

namespace Mkk\AdminBundle\Form\Collection;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LiensType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'nom',
            Type\TextType::class,
            [
                'label'    => 'Nom',
                'required' => false,
                'attr'     => [
                    'placeholder' => 'Nom',
                ],
            ]
        );
        $builder->add(
            'adresse',
            Type\UrlType::class,
            [
                'label'    => 'Url',
                'required' => false,
                'attr'     => [
                    'placeholder' => 'http://',
                ],
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'      => BUNDLEU . '\SiteBundle\Entity\Lien',
                'csrf_protection' => false,
            ]
        );
    }
}
