<?php

namespace Mkk\AdminBundle\Form\Collection;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ListingsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'module',
            Type\TextType::class,
            [
                'label'    => 'Module',
                'required' => false,
                'attr'     => [
                    'readonly'    => 'readonly',
                    'placeholder' => 'Module',
                ],
            ]
        );

        $choices = [
            0   => 'Valeur par défaut',
            5   => 5,
            10  => 10,
            15  => 15,
            20  => 20,
            25  => 25,
            50  => 50,
            100 => 100,
            150 => 150,
            200 => 200,
        ];
        $builder->add(
            'val',
            Type\ChoiceType::class,
            [
                'label'    => 'Valeur',
                'required' => false,
                'choices'  => $choices,
                'attr'     => [
                    'placeholder' => 'Valeur',
                ],
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'csrf_protection' => false,
            ]
        );
    }
}
