<?php

namespace Mkk\AdminBundle\Form\Collection;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LieuxType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'nom',
            Type\TextType::class,
            ['label' => 'Enseigne']
        );
        $builder->add(
            'adresses',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.adresse'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label'        => 'Adresses',
            ]
        );
        $builder->add(
            'liens',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.lien'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label'        => 'Liens',
            ]
        );
        $builder->add(
            'emails',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.email'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label'        => 'Emails',
            ]
        );
        $builder->add(
            'telephones',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.telephone'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label'        => 'Téléphones',
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'      => BUNDLEU . '\SiteBundle\Entity\Etablissement',
                'csrf_protection' => false,
            ]
        );
    }
}
