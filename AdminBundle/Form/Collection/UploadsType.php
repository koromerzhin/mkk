<?php

namespace Mkk\AdminBundle\Form\Collection;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UploadsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'url',
            Type\HiddenType::class,
            [
                'label'    => 'Module',
                'required' => false,
                'attr'     => [
                    'readonly'    => 'readonly',
                    'placeholder' => 'Module',
                ],
            ]
        );
        $builder->add(
            'min_height',
            Type\IntegerType::class,
            [
                'required' => false,
                'label'    => 'min_height',
                'attr'     => ['min' => 0],
            ]
        );
        $builder->add(
            'max_height',
            Type\IntegerType::class,
            [
                'required' => false,
                'label'    => 'max_height',
                'attr'     => ['min' => 0],
            ]
        );
        $builder->add(
            'min_width',
            Type\IntegerType::class,
            [
                'required' => false,
                'label'    => 'min_width',
                'attr'     => ['min' => 0],
            ]
        );
        $builder->add(
            'max_width',
            Type\IntegerType::class,
            [
                'required' => false,
                'label'    => 'max_width',
                'attr'     => ['min' => 0],
            ]
        );
        $builder->add(
            'image-max_width',
            Type\IntegerType::class,
            [
                'required' => false,
                'label'    => 'max_width',
                'attr'     => ['min' => 0],
            ]
        );
        $builder->add(
            'image-max_height',
            Type\IntegerType::class,
            [
                'required' => false,
                'label'    => 'max_height',
                'attr'     => ['min' => 0],
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'csrf_protection' => false,
            ]
        );
    }
}
