<?php

namespace Mkk\AdminBundle\Form\Collection;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmailsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'type',
            Type\TextType::class,
            [
                'label'    => 'Type',
                'required' => false,
                'attr'     => [
                    'class'       => 'InputEmailUtilisation',
                    'placeholder' => 'Type',
                ],
            ]
        );
        $builder->add(
            'adresse',
            Type\EmailType::class,
            [
                'label'    => 'adresse',
                'required' => false,
                'attr'     => [
                    'placeholder'  => 'adresse@test.fr',
                    'autocomplete' => 'off',
                ],
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'      => BUNDLEU . '\SiteBundle\Entity\Email',
                'csrf_protection' => false,
            ]
        );
    }
}
