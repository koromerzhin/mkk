<?php

namespace Mkk\AdminBundle\Form\Collection;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsersType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'nom',
            Type\TextType::class,
            [
                'label'    => 'Nom',
                'attr'     => [
                    'placeholder' => 'Nom',
                ],
            ]
        );
        $civilite = [];
        foreach ($this->params['civilite'] as $data) {
            $code            = $data['code'];
            $nom             = $data['nom'];
            $civilite[$code] = $nom;
        }

        $builder->add(
            'civilite',
            Type\ChoiceType::class,
            [
                'expanded' => true,
                'label'    => 'Civilité',
                'choices'  => $civilite,
            ]
        );
        $builder->add(
            'prenom',
            Type\TextType::class,
            [
                'label'    => 'Prénom',
                'attr'     => [
                    'placeholder' => 'Prénom',
                ],
            ]
        );
        $builder->add(
            'username',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Identifiant',
            ]
        );
        $builder->add(
            'plainPassword',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Mot de passe',
            ]
        );

        $builder->add(
            'adresses',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => AdressesType::class,
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label'        => 'Adresses',
            ]
        );
        $builder->add(
            'emails',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => EmailsType::class,
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label'        => 'Emails',
            ]
        );
        $builder->add(
            'telephones',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => TelephonesType::class,
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label'        => 'Téléphones',
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'      => BUNDLEU . '\SiteBundle\Entity\User',
                'csrf_protection' => false,
            ]
        );
    }
}
