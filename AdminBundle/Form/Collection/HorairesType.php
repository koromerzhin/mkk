<?php

namespace Mkk\AdminBundle\Form\Collection;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HorairesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'jour',
            Type\HiddenType::class,
            [
                'required' => true,
            ]
        );
        $builder->add(
            'dm',
            Type\TextType::class,
            [
                'required' => false,
                'attr'     => [
                    'class'       => 'HoraireAM',
                    'placeholder' => '00:00',
                ],
            ]
        );
        $builder->add(
            'fm',
            Type\TextType::class,
            [
                'required' => false,
                'attr'     => [
                    'class'       => 'HoraireAM',
                    'placeholder' => '00:00',
                ],
            ]
        );
        $builder->add(
            'da',
            Type\TextType::class,
            [
                'required' => false,
                'attr'     => [
                    'class'       => 'HorairePM',
                    'placeholder' => '00:00',
                ],
            ]
        );
        $builder->add(
            'fa',
            Type\TextType::class,
            [
                'required' => false,
                'attr'     => [
                    'class'       => 'HorairePM',
                    'placeholder' => '00:00',
                ],
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'      => BUNDLEU . '\SiteBundle\Entity\Horaire',
                'csrf_protection' => false,
            ]
        );
    }
}
