<?php

namespace Mkk\AdminBundle\Form\Etablissement;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AjoutType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'etat',
            Type\ChoiceType::class,
            [
                'label'   => "Sélectionnez le type d'établissement",
                'choices' => $options['type'],
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'csrf_protection' => false,
                'type'            => [],
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'newetabs';
    }
}
