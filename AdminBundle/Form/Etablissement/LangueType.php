<?php

namespace Mkk\AdminBundle\Form\Etablissement;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Mkk\SiteBundle\Type\MceEditorType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LangueType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'descriptionactivite',
            MceEditorType::class,
            [
                'label'    => 'Description',
                'required' => false,
            ]
        );
        $builder->add(
            'meta_titre',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Titre',
            ]
        );
        $builder->add(
            'meta_description',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Description',
            ]
        );
        $builder->add(
            'meta_keywords',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Mots clefs',
            ]
        );
        unset($options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'      => BUNDLEU . '\SiteBundle\Entity\Etablissement',
                'csrf_protection' => false,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'config';
    }
}
