<?php

namespace Mkk\AdminBundle\Form\Etablissement;

use Mkk\AdminBundle\Form\Traits\EtablissementTrait;
use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EnseigneType extends AbstractTypeLib
{
    use EtablissementTrait;

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'      => BUNDLEU . '\SiteBundle\Entity\Etablissement',
                'responsable'     => '',
                'params'          => []
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'etablissement';
    }
}
