<?php

namespace Mkk\AdminBundle\Form\Param;

use Mkk\AdminBundle\Lib\ParamType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;

class LoginType extends ParamType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'filelogin',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Image background',
                'attr'     => [
                    'data-upload' => 'beparam_uploadlogin',
                ],
            ]
        );
        $builder->add(
            'login_titre',
            Type\TextType::class,
            [
                'label'    => 'Titre',
                'required' => false,
            ]
        );
        $builder->add(
            'login_soustitre',
            Type\TextType::class,
            [
                'label'    => 'Sous-titre',
                'required' => false,
            ]
        );
        $builder->add(
            'login_videobackground',
            Type\TextType::class,
            [
                'label'    => 'Video background',
                'required' => false,
            ]
        );
        $builder->add(
            'login_videomute',
            Type\ChoiceType::class,
            [
                'required'    => false,
                'label'       => 'video mute',
                'placeholder' => 'video mute',
                'choices'     => [
                    'true'  => 'true',
                    'false' => 'false',
                ],
            ]
        );
        unset($options);
    }
}
