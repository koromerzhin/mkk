<?php

namespace Mkk\AdminBundle\Form\Param;

use Mkk\AdminBundle\Lib\ParamType;
use Mkk\SiteBundle\Type\OuiNonType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;

class SiteType extends ParamType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'meta_theme_color',
            Type\TextType::class,
            [
                'label'    => 'Couleur barre navigateur',
                'required' => false,
                'attr'     => [
                    'class' => 'colorpicker',
                ],
            ]
        );
        $builder->add(
            'seo_titre',
            OuiNonType::class,
            [
                'label'    => 'Nom du site dans les titres',
            ]
        );
        $builder->add(
            'langueprincipal',
            Type\LanguageType::class,
            [
                'label'    => 'Langue principal',
                'required' => true,
            ]
        );
        $builder->add(
            'languesite',
            Type\LanguageType::class,
            [
                'label'    => 'Langue(s) disponible',
                'required' => true,
                'multiple' => true,
            ]
        );
        $builder->add(
            'meta_titre',
            Type\TextType::class,
            [
                'label' => 'Titre',
            ]
        );
        $builder->add(
            'signature',
            Type\TextareaType::class,
            [
                'label'    => 'Signature',
                'required' => false,
            ]
        );
        $builder->add(
            'crontab',
            Type\TextareaType::class,
            [
                'label'    => ' ',
                'required' => false,
                'attr'     => [
                    'rows' => 10,
                ],
            ]
        );
        $builder->add(
            'droitlegaux',
            Type\TextareaType::class,
            [
                'label'    => 'Droits légaux',
                'required' => false,
            ]
        );
        $builder->add(
            'desactivation_etat',
            OuiNonType::class,
            [
                'label'    => 'Etat',
            ]
        );
        $builder->add(
            'desactivation_raison',
            Type\TextareaType::class,
            [
                'label'    => 'Raison',
                'required' => false,
            ]
        );
        $builder->add(
            'emailsite',
            Type\EmailType::class,
            [
                'label' => 'Email principal',
            ]
        );
        $builder->add(
            'payssite',
            Type\CountryType::class,
            [
                'label' => 'Pays',
            ]
        );
        $builder->add(
            'emailreply',
            Type\EmailType::class,
            [
                'required' => false,
                'label'    => 'Email de réponse',
            ]
        );
    }
}
