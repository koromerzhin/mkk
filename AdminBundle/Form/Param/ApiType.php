<?php

namespace Mkk\AdminBundle\Form\Param;

use Mkk\AdminBundle\Lib\ParamType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;

class ApiType extends ParamType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'filedelicious',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Export files delicious',
                'attr'     => [
                    'data-upload' => 'beparam_uploaddelicious',
                ],
            ]
        );
        $builder->add(
            'disqus_url',
            Type\TextType::class,
            [
                'label'    => 'url',
                'required' => false,
                'attr'     => [
                    'placeholder' => '//machin.disqus.com/',
                ],
            ]
        );
        $builder->add(
            'google_id',
            Type\TextType::class,
            [
                'label'    => 'App ID',
                'required' => false,
            ]
        );
        $builder->add(
            'google_secret',
            Type\TextType::class,
            [
                'label'    => 'App Secret',
                'required' => false,
            ]
        );
        $builder->add(
            'facebook_key',
            Type\TextType::class,
            [
                'label'    => 'App ID',
                'required' => false,
            ]
        );
        $builder->add(
            'facebook_secret',
            Type\TextType::class,
            [
                'label'    => 'App Secret',
                'required' => false,
            ]
        );
        $builder->add(
            'twitter_key',
            Type\TextType::class,
            [
                'label'    => 'API Key',
                'required' => false,
            ]
        );
        $builder->add(
            'twitter_secret',
            Type\TextType::class,
            [
                'label'    => 'API Secret',
                'required' => false,
            ]
        );
        $builder->add(
            'bug_url',
            Type\UrlType::class,
            [
                'label'    => 'url',
                'required' => false,
            ]
        );
        $builder->add(
            'bug_project',
            Type\TextType::class,
            [
                'label' => 'Identifiant du projet', 'required' => false,
            ]
        );
        $builder->add(
            'bug_code',
            Type\TextType::class,
            [
                'label' => "Clé d'accès API", 'required' => false,
            ]
        );
        $builder->add(
            'recaptcha_clef',
            Type\TextType::class,
            [
                'label' => 'Clé du site', 'required' => false,
            ]
        );
        $builder->add(
            'recaptcha_secret',
            Type\TextType::class,
            [
                'label' => 'Clé secrète', 'required' => false,
            ]
        );
        $builder->add(
            'piwik_url',
            Type\TextType::class,
            [
                'label' => 'URL (sans http://)', 'required' => false,
            ]
        );
        $builder->add(
            'piwik_id',
            Type\TextType::class,
            [
                'label' => 'ID', 'required' => false,
            ]
        );
        $builder->add(
            'google_analytic',
            Type\TextType::class,
            [
                'label' => 'Analytics', 'required' => false,
            ]
        );
        $builder->add(
            'google_keyapi',
            Type\TextType::class,
            [
                'label' => 'Key API', 'required' => false,
            ]
        );
        $builder->add(
            'google_webmastertool',
            Type\TextType::class,
            [
                'label' => 'Webmaster Tool', 'required' => false,
            ]
        );
        $builder->add(
            'geonames',
            Type\TextType::class,
            [
                'label'    => 'Code geonames',
                'required' => false,
            ]
        );
        $builder->add(
            'delicious_pseudo',
            Type\TextType::class,
            [
                'label'    => 'pseudo',
                'required' => false,
            ]
        );
        unset($options);
    }
}
