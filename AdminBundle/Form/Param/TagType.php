<?php

namespace Mkk\AdminBundle\Form\Param;

use Mkk\AdminBundle\Lib\ParamType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;

class TagType extends ParamType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'tags_telephone',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.tag'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'label'        => 'Utilisateur',
            ]
        );
        $builder->add(
            'tags_telephone_etablissement',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.tag'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'label'        => 'Établissement',
            ]
        );
        $builder->add(
            'tags_telephone_evenement',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.tag'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'label'        => 'Évenement',
            ]
        );
        $builder->add(
            'tags_email',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.tag'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'label'        => 'Utilisateur',
            ]
        );
        $builder->add(
            'tags_email_etablissement',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.tag'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'label'        => 'Établissement',
            ]
        );
        $builder->add(
            'tags_email_evenement',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.tag'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'label'        => 'Évenement',
            ]
        );
        $builder->add(
            'tags_adresse',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.tag'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'label'        => 'Utilisateur',
            ]
        );
        $builder->add(
            'tags_adresse_etablissement',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.tag'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'label'        => 'Établissement',
            ]
        );
        $builder->add(
            'tags_adresse_evenement',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.tag'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'label'        => 'Évenement',
            ]
        );
        $builder->add(
            'tags_client',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.tag'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'label'        => 'Tags client',
            ]
        );
        $builder->add(
            'civilite',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.civilite'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'label'        => 'Civilité',
            ]
        );
    }
}
