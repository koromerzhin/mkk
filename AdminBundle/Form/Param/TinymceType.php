<?php

namespace Mkk\AdminBundle\Form\Param;

use Mkk\AdminBundle\Lib\ParamType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;

class TinymceType extends ParamType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $this->container;
        if (isset($this->params['group_contacts'])) {
            $groupscontact = $this->params['group_contacts'];
        }
        $groups          = [];
        $groupManager    = $container->get('bdd.group_manager');
        $groupRepository = $groupManager->getRepository();
        $data            = $groupRepository->findall();
        foreach ($data as $group) {
            $id       = $group->getId();
            $groups[] = [
                $id => $group->getNom(),
            ];
        }

        $builder->add(
            'tinymce_filemanageracces',
            Type\ChoiceType::class,
            [
                'required' => false,
                'expanded' => true,
                'label'    => '  ',
                'multiple' => true,
                'choices'  => $groups,
            ]
        );
    }
}
