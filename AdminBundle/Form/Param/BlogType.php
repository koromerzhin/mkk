<?php

namespace Mkk\AdminBundle\Form\Param;

use Mkk\AdminBundle\Lib\ParamType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;

class BlogType extends ParamType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'blogmettre_avant',
            Type\ChoiceType::class,
            [
                'label'   => 'Mettre en avant',
                'choices' => ['1' => 'Activé', '0' => 'Désactivé'],
            ]
        );
        $builder->add(
            'blogcacherpardefautredacteur',
            Type\ChoiceType::class,
            [
                'label'   => 'Cacher par défaut le rédacteur',
                'choices' => ['0' => 'Désactivé', '1' => 'Activé'],
            ]
        );
        $builder->add(
            'blogmettre_accueil',
            Type\ChoiceType::class,
            [
                'label'   => "Mettre à l'accueil",
                'choices' => ['1' => 'Activé', '0' => 'Désactivé'],
            ]
        );
        unset($options);
    }
}
