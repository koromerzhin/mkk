<?php

namespace Mkk\AdminBundle\Form\Param;

use Mkk\AdminBundle\Lib\ParamType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;

class BddType extends ParamType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $groupscontact = [];
        $container     = $this->container;
        if (isset($this->params['group_contacts'])) {
            $groupscontact = $this->params['group_contacts'];
        }
        $groups          = [];
        $groupManager    = $container->get('bdd.group_manager');
        $groupRepository = $groupManager->getRepository();
        $data            = $groupRepository->findall();
        foreach ($data as $group) {
            $id       = $group->getId();
            $groups[] = [
                $id => $group->getNom(),
            ];
        }

        if (count($groupscontact) != 0) {
            $groupscontact = implode(',', $groupscontact);

            $builder->add(
                'groupcontactdefault',
                Type\TextType::class,
                [
                    'label'    => 'Groupe de contact par défaut',
                    'attr'     => [
                        'placeholder' => 'Choisir le groupe contact par défaut',
                        'data-url'    => 'beparam_searchgroup',
                        'data-uri'    => 1,
                    ],
                ]
            );
        }

        $builder->add(
            'group_utilisateurs',
            Type\ChoiceType::class,
            [
                'required' => false,
                'expanded' => true,
                'label'    => 'Utilisateurs',
                'multiple' => true,
                'choices'  => $groups,
            ]
        );
        $builder->add(
            'group_connect',
            Type\ChoiceType::class,
            [
                'required' => false,
                'expanded' => true,
                'label'    => 'Utilisateurs',
                'multiple' => true,
                'choices'  => $groups,
            ]
        );
        $builder->add(
            'group_contacts',
            Type\ChoiceType::class,
            [
                'required' => false,
                'expanded' => true,
                'label'    => 'Contacts',
                'multiple' => true,
                'choices'  => $groups,
            ]
        );
        $builder->add(
            'group_responsables',
            Type\ChoiceType::class,
            [
                'required' => false,
                'expanded' => true,
                'label'    => 'Responsables',
                'multiple' => true,
                'choices'  => $groups,
            ]
        );
    }
}
