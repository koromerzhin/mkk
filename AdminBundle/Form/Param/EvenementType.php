<?php

namespace Mkk\AdminBundle\Form\Param;

use Mkk\AdminBundle\Lib\ParamType;
use Mkk\SiteBundle\Type\OuiNonType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;

class EvenementType extends ParamType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'evenement_place',
            OuiNonType::class,
            [
                'label'    => 'Chaque evènement à un nombre de place',
            ]
        );
        $builder->add(
            'evenement_etat',
            OuiNonType::class,
            [
                'label'    => 'Activer etat',
            ]
        );
        $builder->add(
            'evenement_couleur',
            Type\TextType::class,
            [
                'label'    => 'Couleur disponible',
                'required' => false,
            ]
        );
        unset($options);
    }
}
