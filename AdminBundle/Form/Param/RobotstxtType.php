<?php

namespace Mkk\AdminBundle\Form\Param;

use Mkk\AdminBundle\Lib\ParamType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;

class RobotstxtType extends ParamType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'robotstxt',
            Type\TextareaType::class,
            [
                'label'    => ' ',
                'required' => false,
                'attr'     => [
                    'rows' => 10,
                ],
            ]
        );
    }
}
