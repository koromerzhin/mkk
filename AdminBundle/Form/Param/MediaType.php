<?php

namespace Mkk\AdminBundle\Form\Param;

use Mkk\AdminBundle\Lib\ParamType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;

class MediaType extends ParamType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'filelogobackend',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Logo',
                'attr'     => [
                    'data-upload' => 'beparam_uploadlogobackend',
                ],
            ]
        );
        $builder->add(
            'fileavatar',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Avatar',
                'attr'     => [
                    'data-upload' => 'beparam_uploadavatar',
                ],
            ]
        );
        $builder->add(
            'filegooglemaps',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Pointer Google Maps',
                'attr'     => [
                    'data-upload' => 'beparam_uploadgooglemaps',
                ],
            ]
        );
        $builder->add(
            'filelogo',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Logo backend',
                'attr'     => [
                    'data-upload' => 'beparam_uploadlogo',
                ],
            ]
        );
        $builder->add(
            'filefavicon',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Favicon',
                'attr'     => [
                    'data-upload' => 'beparam_uploadfavicon',
                ],
            ]
        );
    }
}
