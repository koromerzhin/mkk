<?php

namespace Mkk\AdminBundle\Form\Param;

use Mkk\AdminBundle\Lib\ParamType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;

class UploadType extends ParamType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'upload',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.upload'),
                'allow_add'    => false,
                'delete_empty' => true,
                'allow_delete' => false,
                'label'        => 'Upload',
            ]
        );
    }
}
