<?php

namespace Mkk\AdminBundle\Form\Param;

use Mkk\AdminBundle\Lib\ParamType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;

class ListingType extends ParamType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'longueurliste',
            Type\ChoiceType::class,
            [
                'label'   => 'Admin',
                'choices' => [
                    5   => 5,
                    10  => 10,
                    15  => 15,
                    20  => 20,
                    25  => 25,
                    50  => 50,
                    100 => 100,
                    150 => 150,
                    200 => 200,
                ],
            ]
        );
        $builder->add(
            'publicliste',
            Type\IntegerType::class,
            [
                'label' => 'Publique',
                'attr'  => ['min' => 1],
            ]
        );
        $builder->add(
            'module_listing',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.listing'),
                'allow_add'    => false,
                'delete_empty' => true,
                'allow_delete' => false,
                'label'        => 'Par modules',
            ]
        );
        unset($options);
    }
}
