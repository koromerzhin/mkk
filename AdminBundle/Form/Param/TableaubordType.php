<?php

namespace Mkk\AdminBundle\Form\Param;

use Mkk\AdminBundle\Lib\ParamType;
use Mkk\SiteBundle\Type\MceEditorType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;

class TableaubordType extends ParamType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $manager    = $this->container->get('bdd.group_manager');
        $repository = $manager->getRepository();
        $groups     = $repository->findAll();
        if (isset($this->params['group_connect'])) {
            $connect = $this->params['group_connect'];
            foreach ($connect as $idGroup) {
                $group = '';
                foreach ($groups as $row) {
                    if ($row->getId() == $idGroup) {
                        $group = $row->getNom();
                        break;
                    }
                }
                $builder->add(
                    'groupbord_' . $idGroup . '_accueil',
                    Type\TextType::class,
                    [
                        'label'   => $group,
                    ]
                );
            }
        }

        $builder->add(
            'messageaccueil',
            MceEditorType::class,
            [
                'label'    => "Message d'accueil",
                'required' => false,
            ]
        );
    }
}
