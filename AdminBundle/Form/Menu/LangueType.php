<?php

namespace Mkk\AdminBundle\Form\Menu;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Mkk\SiteBundle\Type\MceEditorType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LangueType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'libelle',
            Type\TextType::class,
            [
                'label'    => 'Libellé',
                'required' => false,
            ]
        );
        $builder->add(
            'description',
            MceEditorType::class,
            [
                'label'    => 'Description',
                'required' => false,
            ]
        );
        $builder->add(
            'url',
            Type\TextType::class,
            [
                'label'    => 'url',
                'required' => false,
                'attr'     => ['data-url' => 'bemenu_searchurl'],
            ]
        );
        unset($options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                    'data_class'      => BUNDLEU . '\SiteBundle\Entity\Menu',
                    'csrf_protection' => false,
                ]
        );
    }

    public function getBlockPrefix()
    {
        return 'menu';
    }
}
