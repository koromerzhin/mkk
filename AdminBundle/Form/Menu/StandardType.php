<?php

namespace Mkk\AdminBundle\Form\Menu;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Mkk\SiteBundle\Type\OuiNonType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StandardType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'envoyer',
            Type\SubmitType::class
        );
        $choicecible = [
            ''        => 'Non renseigné',
            '_self'   => 'Cette fenètre',
            '_blank'  => 'Nouvelle fenètre',
            '_parent' => 'Cadre parent',
            '_top'    => 'Cadre racine',
        ];
        $builder->add(
            'clef',
            Type\TextType::class,
            [
                'label'    => 'Clef',
                'required' => false,
            ]
        );
        $builder->add(
            'separateur',
            OuiNonType::class,
            [
                'label' => 'Séparateur',
            ]
        );
        $builder->add(
            'cible',
            Type\ChoiceType::class,
            [
                'choices'  => $choicecible,
                'label'    => 'Cible',
                'required' => false,
                'attr'     => ['placeholder' => 'Choisir la cible'],
            ]
        );
        $builder->add(
            'image_vignette',
            Type\HiddenType::class,
            [
                'label'    => 'Vignette',
                'mapped'   => false,
                'required' => false,
            ]
        );
        $builder->add(
            'icon',
            Type\TextType::class,
            [
                'label'    => 'Icon',
                'required' => false,
            ]
        );
        unset($options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                    'data_class'      => BUNDLEU . '\SiteBundle\Entity\Menu'
                ]
        );
    }

    public function getBlockPrefix()
    {
        return 'menu';
    }
}
