<?php

namespace Mkk\AdminBundle\Form\Partenaire;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StandardType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'envoyer',
            Type\SubmitType::class
        );
        $builder->add(
            'fileimage',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Photo',
                'attr'     => [
                    'data-upload' => 'bepartenaire_uploadimage',
                ],
            ]
        );
        $builder->add(
            'categorie',
            Type\TextType::class,
            [
                'label'    => 'Catégorie',
                'required' => true,
                'attr'     => [
                    'placeholder' => 'Catégorie',
                    'data-url'    => 'bepartenaire_searchcategorie',
                ],
            ]
        );
        $builder->add(
            'nom',
            Type\TextType::class,
            [
                'label' => 'Nom',
            ]
        );
        $builder->add(
            'url',
            Type\UrlType::class,
            [
                'label' => 'Site internet',
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'      => BUNDLEU . '\SiteBundle\Entity\Partenaire'
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'partenaire';
    }
}
