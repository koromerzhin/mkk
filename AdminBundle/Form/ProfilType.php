<?php

namespace Mkk\AdminBundle\Form;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Mkk\SiteBundle\Type\OuiNonType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfilType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'submit',
            Type\SubmitType::class
        );
        $builder->add(
            'fileavatar',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Image Profil',
                'attr'     => [
                    'data-upload' => 'bemoncompte_uploadavatar',
                ],
            ]
        );
        $builder->add(
            'nom',
            Type\TextType::class,
            [
                 'label'    => 'Nom',
                 'required' => false,
             ]
        );
        $civilite = [];
        foreach ($this->params['civilite'] as $data) {
            $code            = $data['code'];
            $nom             = $data['nom'];
            $civilite[$code] = $nom;
        }

        $builder->add(
            'civilite',
            Type\ChoiceType::class,
            [
                'expanded' => true,
                'label'    => 'Civilité',
                'choices'  => $civilite,
            ]
        );
        $builder->add(
            'adresses',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.adresse'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label'        => 'Adresses',
            ]
        );
        $builder->add(
            'liens',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.lien'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label'        => 'Liens',
            ]
        );
        $builder->add(
            'emails',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.email'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label'        => 'Emails',
            ]
        );
        $builder->add(
            'telephones',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.telephone'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label'        => 'Téléphones',
            ]
        );
        $builder->add(
            'prenom',
            Type\TextType::class,
            [
                'label'    => 'Prénom',
                'required' => false,
            ]
        );
        $builder->add(
            'naissance',
            Type\TextType::class,
            [
                'label'    => 'Date de naissance',
                'required' => false,
                'attr'     => [
                    'class'       => 'DateNaissance',
                    'placeholder' => 'dd/mm/YYYY',
                    'data-max'    => date('d/m/Y'),
                ],
            ]
        );
        $builder->add(
            'username',
            Type\TextType::class,
            [
                'required' => false,
                 'label'   => 'Identifiant',
             ]
        );
        $builder->add(
            'plainPassword',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Mot de passe',
            ]
        );
        $builder->add(
            'pays',
            Type\ChoiceType::class,
            [
                'choices' => [
                    'fr' => 'Français',
                    'en' => 'English',
                ],
                'label' => 'Langue utilisée',
            ]
        );
        $builder->add(
            'langue',
            Type\ChoiceType::class,
            [
                'choices' => [
                    'fr' => 'Français',
                    'en' => 'English',
                ],
                'label' => 'Langue du site',
            ]
        );
        $builder->add(
            'contactsms',
            OuiNonType::class,
            [
                'label'    => 'Communication par SMS',
                'attr'     => ['data-rel' => 'CheckboxSmsContact'],
            ]
        );
        $builder->add(
            'contactemail',
            OuiNonType::class,
            [
                'label'    => 'Communication par Email',
                'attr'     => ['data-rel' => 'CheckboxEmailContact'],
            ]
        );

        unset($options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'      => BUNDLEU . '\SiteBundle\Entity\User',
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'moncompte';
    }
}
