<?php

namespace Mkk\AdminBundle\Form\Evenement;

use Mkk\AdminBundle\Form\Evenement\Collection\LieuxType;
use Mkk\AdminBundle\Form\Evenement\Collection\PrixsType;
use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Mkk\SiteBundle\Type\MceEditorType;
use Mkk\SiteBundle\Type\OuiNonType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StandardType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'envoyer',
            Type\SubmitType::class
        );
        $builder->add(
            'filevignette',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Image',
                'attr'     => [
                    'data-upload' => 'beevenement_uploadvignette',
                ],
            ]
        );
        $builder->add(
            'filegalerie',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Galerie',
                'attr'     => [
                    'data-upload' => 'beevenement_uploadgalerie',
                ],
            ]
        );
        $builder->add(
            'copyright',
            MceEditorType::class,
            [
                'label'    => 'Copyright',
                'required' => false,
            ]
        );
        if ($options['adresses'] == 1) {
            $builder->add(
                'emplacementadresses',
                Type\CollectionType::class,
                [
                    'required'     => false,
                    'entry_type'   => $this->container->get('mkk_admin.form.collection.adresse'),
                    'allow_add'    => false,
                    'delete_empty' => false,
                    'allow_delete' => false,
                    'by_reference' => false,
                ]
            );
            $builder->add(
                'etablissements',
                Type\CollectionType::class,
                [
                    'required'     => false,
                    'entry_type'   => $this->container->get('mkk_admin.form.collection.lieu'),
                    'allow_add'    => false,
                    'delete_empty' => false,
                    'allow_delete' => false,
                    'by_reference' => false,
                ]
            );
        }

        if ($options['place'] == 1) {
            $builder->add(
                'type',
                Type\ChoiceType::class,
                [
                    'expanded' => true,
                    'label'    => 'Type de programmation',
                    'choices'  => [
                        '0' => 'Créneaux horaires',
                        '1' => 'PASS',
                    ],
                ]
            );
            $builder->add(
                'totalnbplace',
                Type\IntegerType::class,
                [
                    'label'    => 'Place',
                    'attr'     => ['min' => 0],
                ]
            );
            $builder->add(
                'placeillimite',
                OuiNonType::class,
                [
                    'label'    => 'Place illimité',
                ]
            );
        }
        $builder->add(
            'categorie',
            Type\TextType::class,
            [
                'label'    => 'Catégorie',
                'required' => true,
                'attr'     => [
                    'placeholder' => 'Catégorie',
                    'data-url'    => 'beevenement_searchcategorie',
                ],
            ]
        );
        $builder->add(
            'prixs',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.prix'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label'        => 'Prix',
            ]
        );
        $builder->add(
            'telephones',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.telephone'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label'        => 'Téléphones',
            ]
        );
        $builder->add(
            'liens',
            Type\CollectionType::class,
            [
               'required'     => false,
               'entry_type'   => $this->container->get('mkk_admin.form.collection.lien'),
               'allow_add'    => true,
               'delete_empty' => true,
               'allow_delete' => true,
               'by_reference' => false,
               'label'        => 'Liens',
            ]
        );
        $builder->add(
            'emails',
            Type\CollectionType::class,
            [
               'required'     => false,
               'entry_type'   => $this->container->get('mkk_admin.form.collection.email'),
               'allow_add'    => true,
               'delete_empty' => true,
               'allow_delete' => true,
               'by_reference' => false,
               'label'        => 'Emails',
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'         => BUNDLEU . '\SiteBundle\Entity\Evenement',
                'adresses'           => 0,
                'place'              => 0,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'evenement';
    }
}
