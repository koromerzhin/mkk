<?php

namespace Mkk\AdminBundle\Form\Evenement;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Mkk\SiteBundle\Type\MceEditorType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LangueType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'titre',
            Type\TextType::class,
            [
                'label'    => 'Titre',
                'attr'     => ['placeholder' => 'Titre'],
                'required' => false,
            ]
        );
        $builder->add(
            'description',
            MceEditorType::class,
            [
                'label'    => 'Description',
                'required' => false,
            ]
        );
        $builder->add(
            'alias',
            Type\TextType::class,
            [
                'label'    => 'Alias',
                'attr'     => ['placeholder' => 'Alias'],
                'required' => false,
            ]
        );
        unset($options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'      => BUNDLEU . '\SiteBundle\Entity\Evenement',
                'csrf_protection' => false,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'evenement';
    }
}
