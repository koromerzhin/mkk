<?php

namespace Mkk\AdminBundle\Form\Noteinterne;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Mkk\SiteBundle\Type\OuiNonType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StandardType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'envoyer',
            Type\SubmitType::class
        );
        $builder->add(
            'publier',
            OuiNonType::class,
            [
                'label'     => 'Publier',
            ]
        );
        $builder->add(
            'tous',
            OuiNonType::class,
            [
                'label'    => 'Envoyer à tous',
                'mapped'   => false,
            ]
        );
        $builder->add(
            'groupes',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Envoyer à ces groupes',
                'mapped'   => false,
                'attr'     => [
                    'data-url'      => 'benoteinterne_searchgroup',
                    'data-multiple' => true,
                    'disabled'      => 'disabled',
                ],
            ]
        );
        $builder->add(
            'entreprises',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Envoyer à ces entreprises',
                'mapped'   => false,
                'attr'     => [
                    'data-url'      => 'benoteinterne_searchetablissement',
                    'data-multiple' => true,
                    'disabled'      => 'disabled',
                ],
            ]
        );
        $builder->add(
            'users',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Envoyer à ces personnes',
                'mapped'   => false,
                'attr'     => [
                    'data-url'      => 'benoteinterne_searchuser',
                    'data-multiple' => true,
                    'disabled'      => 'disabled',
                ],
            ]
        );
        $md5 = md5(uniqid());
        $builder->add(
            'datedebut',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Du ',
                'attr'     => [
                    'placeholder'     => 'Date de début',
                    'data-md5'        => $md5,
                    'class'           => 'datepicker',
                    'data-datepicker' => 'min',
                ],
            ]
        );
        $builder->add(
            'datefin',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => ' au ',
                'attr'     => [
                    'placeholder'     => 'Date de fin',
                    'data-md5'        => $md5,
                    'class'           => 'datepicker',
                    'data-datepicker' => 'max',
                ],
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'datedebut'       => '',
                'datefin'         => '',
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'noteinterne';
    }
}
