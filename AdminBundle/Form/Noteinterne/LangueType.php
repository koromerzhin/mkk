<?php

namespace Mkk\AdminBundle\Form\Noteinterne;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Mkk\SiteBundle\Type\MceEditorType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LangueType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'titre',
            Type\TextType::class,
            [
                'required' => true,
                'label'    => 'Titre',
            ]
        );
        $builder->add(
            'lien',
            Type\UrlType::class,
            [
                'required' => false,
                'label'    => 'Lien pour en savoir plus',
            ]
        );
        $builder->add(
            'contenu',
            MceEditorType::class,
            [
                'label'    => 'Contenu',
                'required' => false,
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'csrf_protection' => false,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'noteinterne';
    }
}
