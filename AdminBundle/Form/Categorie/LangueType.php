<?php

namespace Mkk\AdminBundle\Form\Categorie;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LangueType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'alias',
            Type\TextType::class,
            [
                'label'    => 'Alias',
                'required' => false,
            ]
        );
        $builder->add(
            'nom',
            Type\TextType::class,
            [
                'label'    => 'Nom',
                'required' => false,
            ]
        );
        $builder->add(
            'meta_titre',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Titre',
            ]
        );
        $builder->add(
            'meta_description',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Description',
            ]
        );
        $builder->add(
            'meta_keywords',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Mots clefs',
            ]
        );
        unset($options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'      => BUNDLEU . '\SiteBundle\Entity\Categorie',
                'csrf_protection' => false,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'categorie';
    }
}
