<?php

namespace Mkk\AdminBundle\Form\RevolutionSlider;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SliderType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'titre',
            Type\TextType::class,
            [
                'label' => 'Titre',
                'attr'  => ['placeholder' => 'Titre'],
            ]
        );
        $builder->add(
            'code',
            Type\TextType::class,
            [
                'label' => 'Code',
                'attr'  => ['placeholder' => 'Code'],
            ]
        );
        $builder->add(
            'langue',
            Type\LanguageType::class,
            [
                'label'       => 'Langue',
                'placeholder' => 'Langue',
                'choices'     => $options['languedispo'],
            ]
        );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'      => BUNDLEU . '\SiteBundle\Entity\RevolutionSlider',
                'csrf_protection' => false,
                'languedispo'     => [],
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'slider';
    }
}
