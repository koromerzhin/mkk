<?php

namespace Mkk\AdminBundle\Form\RevolutionSlider;

use Mkk\AdminBundle\Form\RevolutionSlider\Navigation\NavigationType;
use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ParamType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'delay',
            Type\TextType::class,
            [
                'label'    => 'Délai',
                'attr'     => ['placeholder' => 'Temps de chaque slide (millisecondes)'],
                'required' => false,
            ]
        );
        $builder->add(
            'sliderType',
            Type\ChoiceType::class,
            [
                'label'       => 'Type',
                'placeholder' => 'Choisir le type',
                'choices'     => [
                    'standard' => 'Standard',
                    'hero'     => 'Hero',
                    'carousel' => 'Carousel',
                ],
                'required' => false,
            ]
        );
        $builder->add(
            'sliderLayout',
            Type\ChoiceType::class,
            [
                'label'       => 'Disposition',
                'placeholder' => 'Choisir la disposition',
                'choices'     => [
                    'auto'       => 'Automatique',
                    'fullwidth'  => 'Pleine largeur',
                    'fullscreen' => 'Plein écran',
                ],
                'required' => false,
                'expanded' => true,
            ]
        );
        $builder->add(
            'gridwidth',
            Type\TextType::class,
            [
                'label'    => 'Largeur de la grille',
                'attr'     => ['placeholder' => 'Chiffres uniquement'],
                'required' => false,
            ]
        );
        $builder->add(
            'gridheight',
            Type\TextType::class,
            [
                'label'    => 'Hauteur de la grille',
                'attr'     => ['placeholder' => 'Chiffres uniquement'],
                'required' => false,
            ]
        );
        $builder->add(
            'fullScreenOffsetContainer',
            Type\TextType::class,
            [
                'label'    => 'Container',
                'attr'     => ['placeholder' => 'Container'],
                'required' => false,
            ]
        );
        $builder->add(
            $builder->create(
                'navigation',
                NavigationType::class,
                ['by_reference' => true]
            )
        );
        unset($options);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'csrf_protection' => false,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'sliderparam';
    }
}
