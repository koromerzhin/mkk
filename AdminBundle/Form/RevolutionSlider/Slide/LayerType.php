<?php

namespace Mkk\AdminBundle\Form\RevolutionSlider\Slide;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LayerType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'nom',
            Type\TextType::class,
            [
                'label' => 'Nom',
                'attr'  => ['placeholder' => 'Nom'],
            ]
        );
        $builder->add(
            'texthtml',
            Type\TextareaType::class,
            [
                'label'    => 'Text / html',
                'attr'     => ['placeholder' => 'Text / html', 'rows' => 10],
                'required' => false,
            ]
        );
        $builder->add(
            'x',
            Type\TextType::class,
            [
                'label' => 'X',
                'attr'  => ['placeholder' => 'X'],
            ]
        );
        $builder->add(
            'y',
            Type\TextType::class,
            [
                'label' => 'Y',
                'attr'  => ['placeholder' => 'Y'],
            ]
        );
        $builder->add(
            'start',
            Type\TextType::class,
            [
                'label'    => 'Start',
                'attr'     => ['placeholder' => 'Start'],
                'required' => false,
            ]
        );
        $builder->add(
            'class',
            Type\TextType::class,
            [
                'label'    => 'Classe(s)',
                'attr'     => ['placeholder' => 'Classe(s)'],
                'required' => false,
            ]
        );
        $builder->add(
            'whitespace',
            Type\ChoiceType::class,
            [
                'choices' => [
                    'normal' => 'Normal',
                    'nowrap' => 'Nowrap',
                    'pre'    => 'Pre',
                ],
                'placeholder' => 'Whitespace',
                'label'       => 'Whitespace',
                'required'    => false,
            ]
        );
        $builder->add(
            'color',
            Type\TextType::class,
            [
                'attr'     => ['placeholder' => 'Code couleur html'],
                'label'    => 'Couleur',
                'required' => false,
            ]
        );
        $builder->add(
            'width',
            Type\TextType::class,
            [
                'attr'     => ['placeholder' => 'Chiffre uniquement'],
                'label'    => 'Largeur',
                'required' => false,
            ]
        );
        $builder->add(
            'transform_in',
            Type\TextType::class,
            [
                'attr'     => ['placeholder' => 'Transform in'],
                'label'    => 'Transform in',
                'required' => false,
            ]
        );
        unset($options);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'csrf_protection' => false,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'slidelayer';
    }
}
