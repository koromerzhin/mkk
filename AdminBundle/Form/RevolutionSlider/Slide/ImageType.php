<?php

namespace Mkk\AdminBundle\Form\RevolutionSlider\Slide;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ImageType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'bgrepeat',
            Type\ChoiceType::class,
            [
                'label'   => 'Repeat',
                'choices' => [
                    'no-repeat' => 'no-repeat',
                    'repeat'    => 'repeat',
                    'repeat-x'  => 'repeat-x',
                    'repeat-y'  => 'repeat-y',
                ],
                'required' => false,
            ]
        );
        $builder->add(
            'bgfit',
            Type\ChoiceType::class,
            [
                'label'   => 'Fit',
                'choices' => [
                    'cover'   => 'cover',
                    'contain' => 'contain',
                    'normal'  => 'normal',
                ],
                'required' => false,
            ]
        );
        $builder->add(
            'bgposition',
            Type\ChoiceType::class,
            [
                'label'   => 'Position',
                'choices' => [
                    'left top'      => 'left top',
                    'left center'   => 'left center',
                    'left bottom'   => 'left bottom',
                    'center top'    => 'center top',
                    'center center' => 'center center',
                    'center bottom' => 'center bottom',
                    'right top'     => 'right top',
                    'right center'  => 'right center',
                    'right bottom'  => 'right bottom',
                ],
                'required' => false,
            ]
        );
        unset($options);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'csrf_protection' => false,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'slideimage';
    }
}
