<?php

namespace Mkk\AdminBundle\Form\RevolutionSlider\Slide;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ParamType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'transition',
            Type\ChoiceType::class,
            [
                'label'    => 'Transition',
                'attr'     => ['placeholder' => 'Transition'],
                'required' => false,
                'choices'  => [
                    'Basics' => [
                        'notransition'           => 'No Transition',
                        'fade'                   => 'Fade',
                        'crossfade'              => 'Cross Fade',
                        'fadethroughdark'        => 'Fade through Dark BG',
                        'fadethroughlight'       => 'Fade through Light BG',
                        'fadethroughtransparent' => 'Fade through predefined BG',
                    ],
                    'Slide simple' => [
                        'slideup'         => 'Slide To Top',
                        'slidedown'       => 'Slide To Bottom',
                        'slideright'      => 'Slide To Right',
                        'slideleft'       => 'Slide To Left',
                        'slidehorizontal' => 'Slide Horizontal (depending on Next/Previous)',
                        'slidevertical'   => 'Slide Vertical (depending on Next/Previous)',
                    ],
                    'Slide over' => [
                        'slideoverup'         => 'Slide Over To Top',
                        'slideoverdown'       => 'Slide Over To Bottom',
                        'slideoverright'      => 'Slide Over To Right',
                        'slideoverleft'       => 'Slide Over To Left',
                        'slideoverhorizontal' => 'Slide Over Horizontal (Next/Previous)',
                        'slideoververtical'   => 'Slide Over Vertical (Next/Previous)',
                    ],
                    'Slots and boxes' => [
                        'boxslide'             => 'Slide Boxes',
                        'slotslide-horizontal' => 'Slide Slots Horizontal',
                        'slotslide-vertical'   => 'Slide Slots Vertical',
                        'boxfade'              => 'Fade Boxes',
                        'slotfade-horizontal'  => 'Fade Slots Horizontal',
                        'slotfade-vertical'    => 'Fade Slots Vertical',
                    ],
                    'Fade & slide' => [
                        'fadefromright'           => 'Fade and Slide from Right',
                        'fadefromleft'            => 'Fade and Slide from Left',
                        'fadefromtop'             => 'Fade and Slide from Top',
                        'fadefrombottom'          => 'Fade and Slide from Bottom',
                        'fadetoleftfadefromright' => 'To Left From Right',
                        'fadetorightfadefromleft' => 'To Right From Left',
                        'fadetotopfadefrombottom' => 'To Top From Bottom',
                        'fadetobottomfadefromtop' => 'To Bottom From Top',
                    ],
                    'Parallax' => [
                        'parallaxtoright'    => 'Parallax to Right',
                        'parallaxtoleft'     => 'Parallax to Left',
                        'parallaxtotop'      => 'Parallax to Top',
                        'parallaxtobottom'   => 'Parallax to Bottom',
                        'parallaxhorizontal' => 'Parallax Horizontal',
                        'parallaxvertical'   => 'Parallax Vertical',
                    ],
                    'Sliding overlays' => [
                        'slidingoverlayup'         => 'Sliding Overlays to the Top',
                        'slidingoverlaydown'       => 'Sliding Overlays to the Bottom',
                        'slidingoverlayright'      => 'Sliding Overlays to the Right',
                        'slidingoverlayleft'       => 'Sliding Overlays to the Left',
                        'slidingoverlayhorizontal' => 'Sliding Overlays Horizontal',
                        'slidingoverlayvertical'   => 'Sliding Overlays Vertical',
                    ],
                    'Zoom transitions' => [
                        'scaledownfromright'  => 'Zoom Out and Fade From Right',
                        'scaledownfromleft'   => 'Zoom Out and Fade From Left',
                        'scaledownfromtop'    => 'Zoom Out and Fade From Top',
                        'scaledownfrombottom' => 'Zoom Out and Fade From Bottom',
                        'zoomout'             => 'ZoomOut',
                        'zoomin'              => 'ZoomIn',
                        'slotzoom-horizontal' => 'Zoom Slots Horizontal',
                        'slotzoom-vertical'   => 'Zoom Slots Vertical',
                    ],
                    'Curtain transitions' => [
                        'curtain-1' => 'Curtain from Left',
                        'curtain-2' => 'Curtain from Right',
                        'curtain-3' => 'Curtain from Middle',
                    ],
                    'Premium transitions' => [
                        '3dcurtain-horizontal' => '3D Curtain Horizontal',
                        '3dcurtain-vertical'   => '3D Curtain Vertical',
                        'cube'                 => 'Cube Vertical',
                        'cube-horizontal'      => 'Cube Horizontal',
                        'incube'               => 'In Cube Vertical',
                        'incube-horizontal'    => 'In Cube Horizontal',
                        'turnoff'              => 'TurnOff Horizontal',
                        'turnoff-vertical'     => 'TurnOff Vertical',
                        'papercut'             => 'Paper Cut',
                        'flyin'                => 'Fly In',
                    ],
                    'Slide remove' => [
                        'slideremoveup'         => 'Slide Remove To Top',
                        'slideremovedown'       => 'Slide Remove To Bottom',
                        'slideremoveright'      => 'Slide Remove To Right',
                        'slideremoveleft'       => 'Slide Remove To Left',
                        'slideremovehorizontal' => 'Slide Remove Horizontal (Next/Previous)',
                        'slideremovevertical'   => 'Slide Remove Vertical (Next/Previous)',
                    ],
                    'Random' => [
                        'random-selected' => 'Random of Selected',
                        'random-static'   => 'Random Flat',
                        'random-premium'  => 'Random Premium',
                        'random'          => 'Random Flat and Premium',
                    ],
                ],
            ]
        );
        // $builder->add(
        //     'slotamount',
        //     Type\TextType::class,
        //     [
        //         'label' => 'Slotamount',
        //         'attr' => ['placeholder' => 'Slotamount'],
        //         'required' => false
        //     ]
        // );
        // $builder->add(
        //     'masterspeed',
        //     Type\TextType::class,
        //     [
        //         'label' => 'Masterspeed',
        //         'attr' => ['placeholder' => 'Masterspeed'],
        //         'required' => false
        //     ]
        // );
        unset($options);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'csrf_protection' => false,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'slideparam';
    }
}
