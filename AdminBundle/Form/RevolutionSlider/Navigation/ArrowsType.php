<?php

namespace Mkk\AdminBundle\Form\RevolutionSlider\Navigation;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Mkk\SiteBundle\Type\OuiNonType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ArrowsType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'enable',
            OuiNonType::class,
            [
                'label'    => 'Flèches',
            ]
        );
        $builder->get('enable')->addModelTransformer(
            new CallbackTransformer(
                function ($boolEnable) {
                    $boolVal = boolval($boolEnable);

                    return $boolVal;
                },
                function ($boolEnable) {
                    $boolVal = boolval($boolEnable);

                    return $boolVal;
                }
            )
        );
        $builder->add(
            'hide_onmobile',
            Type\ChoiceType::class,
            [
                'choices' => [
                    'true'  => 'Oui',
                    'false' => 'Non',
                ],
                'placeholder' => 'Choisir une valeur',
                'label'       => 'Cacher sur mobile',
                'required'    => false,
            ]
        );
        $builder->add(
            'hide_onleave',
            Type\ChoiceType::class,
            [
                'choices' => [
                    'true'  => 'Oui',
                    'false' => 'Non',
                ],
                'placeholder' => 'Choisir une valeur',
                'label'       => 'Cacher lors de la perte du focus',
                'required'    => false,
            ]
        );
        unset($options);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'csrf_protection' => false,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'arrows';
    }
}
