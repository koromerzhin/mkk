<?php

namespace Mkk\AdminBundle\Form\RevolutionSlider\Navigation;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Mkk\SiteBundle\Type\OuiNonType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BulletsType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'enable',
            OuiNonType::class,
            [
                'label'    => 'Puces',
            ]
        );
        $builder->get('enable')->addModelTransformer(
            new CallbackTransformer(
                function ($boolEnable) {
                    $boolVal = boolval($boolEnable);

                    return $boolVal;
                },
                function ($boolEnable) {
                    $boolVal = boolval($boolEnable);

                    return $boolVal;
                }
            )
        );
        $builder->add(
            'hide_onmobile',
            Type\ChoiceType::class,
            [
                'choices' => [
                    'true'  => 'Oui',
                    'false' => 'Non',
                ],
                'placeholder' => 'Choisir une valeur',
                'label'       => 'Cacher sur mobile',
                'required'    => false,
            ]
        );
        $builder->add(
            'hide_onleave',
            Type\ChoiceType::class,
            [
                'choices' => [
                    'true'  => 'Oui',
                    'false' => 'Non',
                ],
                'placeholder' => 'Choisir une valeur',
                'label'       => 'Cacher lors de la perte du focus',
                'required'    => false,
            ]
        );
        $builder->add(
            'direction',
            Type\ChoiceType::class,
            [
                'choices' => [
                    'horizontal' => 'Horizontal',
                    'vertical'   => 'Vertical',
                ],
                'placeholder' => 'Choisir une valeur',
                'label'       => 'Direction',
                'required'    => false,
            ]
        );
        $builder->add(
            'space',
            Type\TextType::class,
            [
                'attr' => [
                    'placeholder' => 'Espace entre les puces',
                ],
                'label'    => 'Espace entre les puces',
                'required' => false,
            ]
        );
        $builder->add(
            'h_align',
            Type\ChoiceType::class,
            [
                'choices' => [
                    'left'   => 'A gauche',
                    'center' => 'Centrer',
                    'right'  => 'A droite',
                ],
                'placeholder' => 'Alignement horizontal',
                'label'       => 'Choisir une valeur',
                'required'    => false,
            ]
        );
        $builder->add(
            'v_align',
            Type\ChoiceType::class,
            [
                'choices' => [
                    'top'    => 'En haut',
                    'center' => 'Centrer',
                    'bottom' => 'En bas',
                ],
                'placeholder' => 'Choisir une valeur',
                'label'       => 'Alignement vertical',
                'required'    => false,
            ]
        );
        $builder->add(
            'h_offset',
            Type\TextType::class,
            [
                'attr' => [
                    'placeholder' => 'Décalage horizontal',
                ],
                'label'    => 'Décalage horizontal',
                'required' => false,
            ]
        );
        $builder->add(
            'v_offset',
            Type\TextType::class,
            [
                'attr' => [
                    'placeholder' => 'Décalage vertical',
                ],
                'label'    => 'Décalage vertical',
                'required' => false,
            ]
        );
        $builder->add(
            'style',
            Type\TextType::class,
            [
                'attr' => [
                    'placeholder' => 'Nom de la classe',
                ],
                'label'    => 'Style',
                'required' => false,
            ]
        );
        unset($options);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'csrf_protection' => false,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'bullets';
    }
}
