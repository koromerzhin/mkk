<?php

namespace Mkk\AdminBundle\Form\RevolutionSlider\Navigation;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NavigationType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            $builder->create(
                'arrows',
                ArrowsType::class,
                ['by_reference' => true]
            )
        );
        $builder->add(
            $builder->create(
                'bullets',
                BulletsType::class,
                ['by_reference' => true]
            )
        );
        unset($options);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'csrf_protection' => false,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'navigation';
    }
}
