<?php

namespace Mkk\AdminBundle\Form\Edito;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Mkk\SiteBundle\Type\DatePickerType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StandardType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'envoyer',
            Type\SubmitType::class
        );
        $builder->add(
            'datedebut',
            DatePickerType::class,
            [
                'required' => false,
                'label'    => 'Date de début de publication',
                'attr'     => ['placeholder' => 'Date de publication'],
            ]
        );
        $builder->add(
            'datefin',
            DatePickerType::class,
            [
                'required' => false,
                'label'    => 'Date de fin de publication',
                'attr'     => ['placeholder' => 'Date de publication'],
            ]
        );
        $builder->add(
            'user',
            Type\TextType::class,
            [
                'label'  => 'Rédacteur',
                'attr'   => [
                    'placeholder' => 'Rédacteur',
                    'data-url'    => 'beedito_searchredacteur',
                ],
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'      => BUNDLEU . '\SiteBundle\Entity\Edito'
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'edito';
    }
}
