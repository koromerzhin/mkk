<?php

namespace Mkk\AdminBundle\Form;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Mkk\SiteBundle\Type\MceEditorType;
use Mkk\SiteBundle\Type\OuiNonType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Intl\Intl;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BlogType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'envoyer',
            Type\SubmitType::class
        );
        $builder->add(
            'filevignette',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Vignette',
                'attr'     => [
                    'data-upload' => 'beblog_uploadvignette',
                ],
            ]
        );
        $builder->add(
            'filegalerie',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Vignette',
                'attr'     => [
                    'data-upload' => 'beblog_uploadgalerie',
                ],
            ]
        );
        $builder->add(
            'fileimage',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Image',
                'attr'     => [
                    'data-upload' => 'beblog_uploadimage',
                ],
            ]
        );
        $builder->add(
            'titre',
            Type\TextType::class,
            [
                'label' => 'Titre',
                'attr'  => ['placeholder' => 'Titre'],
            ]
        );
        $languedispo = [];
        $langues     = Intl::getLanguageBundle()->getLanguageNames('fr');
        foreach ($this->params['languesite'] as $code) {
            $name               = $langues[$code];
            $languedispo[$name] = $code;
        }
        $builder->add(
            'langue',
            Type\LanguageType::class,
            [
                'label'       => 'Langue',
                'placeholder' => 'Langue',
                'choices'     => $languedispo,
            ]
        );
        $builder->add(
            'commentaire',
            OuiNonType::class,
            [
                'label'    => 'Commentaire activé',
            ]
        );
        $builder->add(
            'actif_public',
            OuiNonType::class,
            [
                'label'    => 'Article publié',
            ]
        );
        $builder->add(
            'datepublication',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Date de publication',
                'attr'     => ['placeholder' => 'Date de publication'],
            ]
        );
        $builder->add(
            'alias',
            Type\TextType::class,
            [
                'label'    => 'Alias',
                'attr'     => ['placeholder' => 'Alias'],
                'required' => false,
            ]
        );
        $builder->add(
            'contenu',
            MceEditorType::class,
            [
                'label'    => 'Contenu',
                'required' => false,
            ]
        );
        $builder->add(
            'intro',
            MceEditorType::class,
            [
                'label'    => 'Intro',
                'required' => false,
            ]
        );
        $builder->add(
            'video',
            Type\UrlType::class,
            [
                'label'    => 'Lien vidéo (Youtube / Viméo / Dailymotion)',
                'required' => false,
            ]
        );
        $builder->add(
            'tag',
            Type\TextType::class,
            [
                'label'    => 'Tags',
                'attr'     => ['placeholder' => 'Tags'],
                'mapped'   => false,
                'required' => false,
            ]
        );
        $builder->add(
            'categorie',
            Type\TextType::class,
            [
                'label'    => 'Catégorie',
                'required' => true,
                'attr'     => [
                    'placeholder' => 'Catégorie',
                    'data-url'    => 'beblog_searchcategorie',
                ],
            ]
        );
        $builder->add(
            'user',
            Type\TextType::class,
            [
                'label'  => 'Rédacteur',
                'attr'   => [
                    'placeholder' => 'Rédacteur',
                    'data-url'    => 'beblog_searchredacteur',
                ],
            ]
        );
        $builder->add(
            'redacteur',
            OuiNonType::class,
            [
                'label'    => 'Afficher le rédacteur',
            ]
        );
        $builder->add(
            'meta_titre',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Titre',
            ]
        );
        $builder->add(
            'meta_description',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Description',
            ]
        );
        $builder->add(
            'meta_keywords',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Mots clefs',
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => BUNDLEU . '\SiteBundle\Entity\Blog',
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'blog';
    }
}
