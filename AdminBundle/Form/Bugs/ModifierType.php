<?php

namespace Mkk\AdminBundle\Form\Bugs;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModifierType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (count($options['status']) != 0) {
            $builder->add(
                'status_id',
                Type\ChoiceType::class,
                [
                    'required' => true,
                    'label'    => 'Status',
                    'choices'  => $data['status'],
                    'data'     => $data['idstatus'],
                ]
            );
        }

        $builder->add(
            'notes',
            Type\TextareaType::class,
            [
                'label'    => 'Notes',
                'required' => false,
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'csrf_protection' => false,
                'status'          => [],
                'idstatus'        => 0,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'bugs';
    }
}
