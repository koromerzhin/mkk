<?php

namespace Mkk\AdminBundle\Form\Bugs;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AjouterType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'tracker',
            Type\ChoiceType::class,
            [
                'required' => true,
                'label'    => 'Type de demande',
                'choices'  => $options['trackers'],
            ]
        );
        $builder->add(
            'sujet',
            Type\TextType::class,
            [
                'label'    => 'Sujet',
                'required' => true,
            ]
        );
        $builder->add(
            'description',
            Type\TextareaType::class,
            [
                'label'    => 'Description',
                'required' => true,
            ]
        );
        $builder->add(
            'priorite',
            Type\ChoiceType::class,
            [
                'required' => true,
                'label'    => 'Priorité',
                'choices'  => $options['priorities'],
            ]
        );
        $builder->add(
            'url',
            Type\UrlType::class,
            [
                'label'    => 'url',
                'required' => true,
            ]
        );
        $builder->add(
            'pour',
            Type\TextType::class,
            [
                'label'    => 'Utilisateur',
                'required' => true,
                'data'     => $options['pour'],
                'attr'     => [
                    'data-url' => 'bebugs_searchuser',
                ],
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'csrf_protection' => false,
                'pour'            => '',
                'trackers'        => [],
                'priorities'      => [],
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'bugs';
    }
}
