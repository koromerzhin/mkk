<?php

namespace Mkk\AdminBundle\Form;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TagType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'submit',
            Type\SubmitType::class
        );
        $builder->add(
            'nom',
            Type\TextType::class,
            ['label' => 'Nom']
        );
        unset($options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'  => BUNDLEU . '\SiteBundle\Entity\Tag',
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'tag';
    }
}
