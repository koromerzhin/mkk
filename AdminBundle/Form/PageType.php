<?php

namespace Mkk\AdminBundle\Form;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Mkk\SiteBundle\Type\MceEditorType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PageType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'envoyer',
            Type\SubmitType::class
        );
        $builder->add(
            'titre',
            Type\TextType::class,
            [
                'label' => 'Titre',
            ]
        );
        $builder->add(
            'filevideo',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Video',
                'attr'     => [
                    'data-upload' => 'bepage_uploadvideo',
                ],
            ]
        );
        $builder->add(
            'fileimage',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Image',
                'attr'     => [
                    'data-upload' => 'bepage_uploadimage',
                ],
            ]
        );
        $builder->add(
            'filefondimage',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Fond Image',
                'attr'     => [
                    'data-upload' => 'bepage_uploadfondimage',
                ],
            ]
        );
        $builder->add(
            'filefiligramme',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Filigramme',
                'attr'     => [
                    'data-upload' => 'bepage_uploadfiligramme',
                ],
            ]
        );
        $builder->add(
            'filegalerie',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Galerie',
                'attr'     => [
                    'data-upload' => 'bepage_uploadgalerie',
                ],
            ]
        );
        $builder->add(
            'url',
            Type\TextType::class,
            [
                'label' => 'url',
            ]
        );
        $builder->add(
            'contenu',
            MceEditorType::class,
            [
                'label'    => 'Contenu',
                'required' => false,
            ]
        );
        $builder->add(
            'css',
            Type\TextareaType::class,
            [
                'label'    => 'Css',
                'required' => false,
                'attr'     => [
                    'rows' => 40,
                ],
            ]
        );
        $builder->add(
            'meta_titre',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Titre',
            ]
        );
        $builder->add(
            'meta_description',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Description',
            ]
        );
        $builder->add(
            'meta_keywords',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Mots clefs',
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'      => BUNDLEU . '\SiteBundle\Entity\Page',
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'page';
    }
}
