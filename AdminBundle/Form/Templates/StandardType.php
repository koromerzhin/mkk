<?php

namespace Mkk\AdminBundle\Form\Templates;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StandardType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'envoyer',
            Type\SubmitType::class
        );
        $builder->add(
            'code',
            Type\TextType::class,
            ['label' => 'Code']
        );
        $builder->add(
            'type',
            Type\ChoiceType::class,
            [
                'expanded' => true,
                'choices'  => [
                    'email' => 'email',
                    'sms'   => 'sms',
                ],
                'label' => 'Type',
            ]
        );
        unset($options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'      => BUNDLEU . '\SiteBundle\Entity\Templates',
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'templates';
    }
}
