<?php

namespace Mkk\AdminBundle\Form\Metariane;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StandardType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'envoyer',
            Type\SubmitType::class
        );
        $builder->add(
            'route',
            Type\TextType::class,
            [
                'label' => 'Route',
                'attr'  => ['disabled' => 'disabled'],
            ]
        );
        unset($options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'      => BUNDLEU . '\SiteBundle\Entity\Metariane'
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'metariane';
    }
}
