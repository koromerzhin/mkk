<?php

namespace Mkk\AdminBundle\Form\Metariane;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LangueType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'titre',
            Type\TextType::class,
            [
                'label'    => 'Titre',
                'required' => false,
            ]
        );
        $builder->add(
            'description',
            Type\TextType::class,
            [
                'label'    => 'Metatags description',
                'required' => false,
            ]
        );
        $builder->add(
            'keywords',
            Type\TextType::class,
            [
                'label'    => 'Metatags keywords',
                'required' => false,
            ]
        );
        $builder->add(
            'ariane',
            Type\TextType::class,
            [
                'label'    => "Fil d'ariane",
                'required' => false,
            ]
        );
        unset($options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                    'data_class'      => BUNDLEU . '\SiteBundle\Entity\Metariane',
                    'csrf_protection' => false,
                ]
        );
    }

    public function getBlockPrefix()
    {
        return 'metariane';
    }
}
