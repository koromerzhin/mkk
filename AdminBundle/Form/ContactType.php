<?php

namespace Mkk\AdminBundle\Form;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Mkk\SiteBundle\Type\OuiNonType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'envoyer',
            Type\SubmitType::class
        );
        $builder->add(
            'nom',
            Type\TextType::class,
            [
                'label'    => 'Nom',
                'required' => false,
            ]
        );
        $builder->add(
            'prenom',
            Type\TextType::class,
            [
                'label'    => 'Prénom',
                'required' => false,
            ]
        );
        $builder->add(
            'societe',
            Type\TextType::class,
            [
                'label'    => 'Société',
                'required' => false,
            ]
        );
        foreach ($this->params['civilite'] as $data) {
            $code            = $data['code'];
            $nom             = $data['nom'];
            $civilite[$code] = $nom;
        }

        $builder->add(
            'civilite',
            Type\ChoiceType::class,
            [
                'expanded' => true,
                'label'    => 'Civilité',
                'choices'  => $civilite,
            ]
        );
        $builder->add(
            'username',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Identifiant',
            ]
        );
        $builder->add(
            'plainPassword',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Mot de passe',
            ]
        );
        $builder->add(
            'group',
            Type\TextType::class,
            [
                'label'    => 'Groupe',
                'attr'     => [
                    'placeholder' => 'Groupe',
                    'data-url'    => 'becontact_searchgroup',
                ],
            ]
        );
        $builder->add(
            'raison',
            Type\ChoiceType::class,
            [
                'label'       => 'Forme juridique',
                'placeholder' => 'Indiquez la forme juridique',
                'required'    => false,
                'choices'     => [
                    'SARL'        => 'SARL',
                    'SAS'         => 'SAS',
                    'EURL'        => 'EURL',
                    'ASSOCIATION' => 'ASSOCIATION',
                    'SCOP'        => 'SCOP',
                    'SA'          => 'SA',
                    'Autre'       => 'Autre',
                ],
            ]
        );
        $builder->add(
            'pays',
            Type\ChoiceType::class,
            [
                'choices' => [
                    'fr' => 'Français',
                    'en' => 'English',
                ],
                'label' => 'Langue utilisée',
            ]
        );
        $builder->add(
            'langue',
            Type\ChoiceType::class,
            [
                'choices' => [
                    'fr' => 'Français',
                    'en' => 'English',
                ],
                'label' => 'Langue du site',
            ]
        );
        $builder->add(
            'observations',
            Type\TextareaType::class,
            [
                'label'    => 'Observations',
                'required' => false,
                'attr'     => ['rows' => 6],
            ]
        );
        $builder->add(
            'enabled',
            OuiNonType::class,
            [
                'label'    => 'Utilisateur activé',
            ]
        );
        $builder->add(
            'contactsms',
            OuiNonType::class,
            [
                'label'    => 'Communication par SMS',
                'attr'     => ['data-rel' => 'CheckboxSmsContact'],
            ]
        );
        $builder->add(
            'contactemail',
            OuiNonType::class,
            [
                'label'    => 'Communication par Email',
                'attr'     => ['data-rel' => 'CheckboxEmailContact'],
            ]
        );
        $builder->add(
            'type',
            Type\ChoiceType::class,
            [
                'label'    => 'Type de contact',
                'required' => false,
                'choices'  => [
                    'prospect' => 'Prospect',
                    'client'   => 'client',
                ],
            ]
        );
        $builder->add(
            'tags',
            Type\TextType::class,
            [
                'label'    => 'Tags',
                'required' => false,
            ]
        );
        $builder->add(
            'douanier',
            Type\ChoiceType::class,
            [
                'label'       => 'Statut douanier',
                'required'    => false,
                'placeholder' => 'choisissez le statut douanier',
                'choices'     => [
                    'france'             => 'France',
                    'intracommunautaire' => 'Intra-communautaire',
                    'export'             => 'Export',
                ],
            ]
        );
        $builder->add(
            'telephones',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.telephone'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label'        => 'Téléphones',
            ]
        );
        $builder->add(
            'adresses',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.adresse'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label'        => 'Adresses',
            ]
        );
        $builder->add(
            'emails',
            Type\CollectionType::class,
            [
               'required'     => false,
               'entry_type'   => $this->container->get('mkk_admin.form.collection.email'),
               'allow_add'    => true,
               'delete_empty' => true,
               'allow_delete' => true,
               'by_reference' => false,
               'label'        => 'Emails',
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'      => BUNDLEU . '\SiteBundle\Entity\User',
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'contact';
    }
}
