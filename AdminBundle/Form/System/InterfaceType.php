<?php

namespace Mkk\AdminBundle\Form\System;

use Mkk\AdminBundle\Lib\SystemType;
use Mkk\SiteBundle\Type\OuiNonType;
use Symfony\Component\Form\FormBuilderInterface;

class InterfaceType extends SystemType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'modal_keyboard',
            OuiNonType::class,
            [
                'label'    => "Lorsque l'on clique sur la toile de fond",
            ]
        );
        $builder->add(
            'modal_backdrop',
            OuiNonType::class,
            [
                'label'    => "Lorsque la touche d'échappement est pressé",
            ]
        );
        unset($options);
    }
}
