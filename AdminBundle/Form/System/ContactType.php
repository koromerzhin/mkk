<?php

namespace Mkk\AdminBundle\Form\System;

use Mkk\AdminBundle\Lib\SystemType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;

class ContactType extends SystemType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $groupscontact = [];
        if (isset($this->params['group_contacts'])) {
            $groupscontact = $this->params['group_contacts'];
        }

        $groupscontact = implode(',', $groupscontact);

        $builder->add(
            'groupcontactdefault',
            Type\TextType::class,
            [
                'label'    => 'Groupe de contact par défaut',
                'attr'     => [
                    'placeholder' => 'Choisir le groupe contact par défaut',
                    'data-url'    => 'besystem_searchgroup',
                ],
            ]
        );
    }
}
