<?php

namespace Mkk\AdminBundle\Form\System;

use Mkk\AdminBundle\Lib\SystemType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;

class BddType extends SystemType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'tags_telephone',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.tag'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'label'        => 'Utilisateur',
            ]
        );
        $builder->add(
            'tags_email',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.tag'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'label'        => 'Utilisateur',
            ]
        );
        $builder->add(
            'tags_adresse',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.tag'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'label'        => 'Utilisateur',
            ]
        );
        $builder->add(
            'tags_client',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.tag'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'label'        => 'Tags client',
            ]
        );
    }
}
