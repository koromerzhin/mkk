<?php

namespace Mkk\AdminBundle\Form\Search;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EtablissementType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'recherche',
            Type\TextType::class,
            [
                'label'    => 'Recherche',
                'required' => false,
                'attr'     => [
                    'placeholder' => 'Nom',
                ],
            ]
        );
        if ($options['secteur'] == 1) {
            $builder->add(
                'nafsousclasse',
                Type\TextType::class,
                [
                    'label'    => 'Code NAF',
                    'required' => false,
                    'attr'     => [
                        'placeholder' => 'Code NAF',
                        'data-url'    => 'beetablissement_searchnafsousclasse',
                    ],
                ]
            );
            $builder->add(
                'activite',
                Type\TextType::class,
                [
                    'label'    => 'Activité',
                    'required' => false,
                    'attr'     => [
                        'placeholder' => 'Activité',
                    ],
                ]
            );
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'csrf_protection' => false,
                'secteur'         => false,
                'activite'        => false,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
