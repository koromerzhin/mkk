<?php

namespace Mkk\AdminBundle\Form\Search;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NoteinterneType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'publier',
            Type\ChoiceType::class,
            [
            'label'   => 'Choix note interne',
            'choices' => [
                '1' => 'Publiée',
                '0' => 'Non publiée',
            ],
            'attr'        => ['placeholder' => 'Visibilité'],
            'placeholder' => 'Visibilité sur le site',
            'required'    => false,
            ]
        );
        $builder->add(
            'recherche',
            Type\TextType::class,
            [
                'label'    => 'Recherche',
                'required' => false,
                'attr'     => [
                    'placeholder' => 'Nom',
                ],
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['csrf_protection' => false, 'publier' => ['data' => '']]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
