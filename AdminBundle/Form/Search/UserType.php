<?php

namespace Mkk\AdminBundle\Form\Search;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'recherche',
            Type\TextType::class,
            [
                'label'    => 'Recherche',
                'required' => false,
                'attr'     => [
                    'placeholder' => 'Nom, Prénom, Société, Ville',
                ],
            ]
        );
        $builder->add(
            'refgroup',
            Type\TextType::class,
            [
                'label'    => 'Groupe',
                'required' => false,
                'attr'     => [
                    'placeholder' => 'Groupe',
                    'data-url'    => 'beuser_searchgroup',
                ],
            ]
        );
        $builder->add(
            'etablissement',
            Type\TextType::class,
            [
                'label'    => 'Etablissement',
                'required' => false,
                'attr'     => [
                    'placeholder' => 'Etablissement',
                    'data-url'    => 'beuser_searchetablissement',
                ],
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                    'type' => [
                        'data' => 'user',
                    ],
                    'csrf_protection' => false,
                ]
        );
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
