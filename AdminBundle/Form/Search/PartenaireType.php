<?php

namespace Mkk\AdminBundle\Form\Search;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PartenaireType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'recherche',
            Type\TextType::class,
            [
                'label'    => 'Recherche',
                'required' => false,
                'attr'     => [
                    'placeholder' => 'Nom',
                ],
            ]
        );
        $builder->add(
            'categorie',
            Type\TextType::class,
            [
                'label'    => 'Catégorie',
                'required' => true,

                'attr'     => [
                    'placeholder' => 'Catégorie',
                    'data-url'    => 'bepartenaire_searchcategorie',
                ],
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'csrf_protection' => false,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
