<?php

namespace Mkk\AdminBundle\Form\Search;

use Mkk\AdminBundle\Lib\SearchContactType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;

class ContactType extends SearchContactType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'lettre',
            Type\TextType::class,
            [
                'label'    => 'Recherche',
                'required' => false,
                'attr'     => [
                    'placeholder' => 'Nom, Prenom',
                ],
            ]
        );
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
