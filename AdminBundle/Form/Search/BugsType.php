<?php

namespace Mkk\AdminBundle\Form\Search;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BugsType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'tracker_id',
            Type\ChoiceType::class,
            [
                'required'    => false,
                'label'       => 'Type de demande',
                'choices'     => $options['trackers'],
                'placeholder' => 'Type de demande',
            ]
        );
        $builder->add(
            'status_id',
            Type\ChoiceType::class,
            [
                'required'    => false,
                'label'       => 'Status',
                'choices'     => $options['status'],
                'placeholder' => 'Status',
            ]
        );
        $builder->add(
            'priority_id',
            Type\ChoiceType::class,
            [
                'required'    => false,
                'label'       => 'Priorité',
                'choices'     => $options['priority'],
                'placeholder' => 'priorité',
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'trackers'        => [],
                'status'          => [],
                'priority'        => [],
                'csrf_protection' => false,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
