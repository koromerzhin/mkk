<?php

namespace Mkk\AdminBundle\Form\Search;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BlogType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'recherche',
            Type\TextType::class,
            [
                'label'    => 'Recherche',
                'required' => false,
                'attr'     => [
                    'placeholder' => 'Nom',
                ],
            ]
        );
        $languedispo = [];
        $langues     = Intl::getLanguageBundle()->getLanguageNames('fr');
        foreach ($this->params['languesite'] as $code) {
            $name               = $langues[$code];
            $languedispo[$name] = $code;
        }
        $builder->add(
            'langue',
            Type\LanguageType::class,
            [
                'label'       => 'Langue',
                'required'    => false,
                'placeholder' => 'Langue',
                'choices'     => $languedispo,
            ]
        );
        $builder->add(
            'categorie',
            Type\TextType::class,
            [
                'label'    => 'Catégorie',
                'required' => true,
                'attr'     => [
                    'placeholder' => 'Catégorie',
                    'data-url'    => 'beblog_searchcategorie',
                ],
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'csrf_protection' => false,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
