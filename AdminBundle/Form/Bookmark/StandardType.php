<?php

namespace Mkk\AdminBundle\Form\Bookmark;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StandardType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'envoyer',
            Type\SubmitType::class
        );
        $builder->add(
            'fileimage',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Image',
                'attr'     => [
                    'data-upload' => 'bebookmark_uploadimage',
                ],
            ]
        );
        $builder->add(
            'url',
            Type\UrlType::class,
            [
                'label'    => 'Url',
                'attr'     => ['placeholder' => 'Alias'],
                'required' => false,
            ]
        );
        $builder->add(
            'tag',
            Type\TextType::class,
            [
                'label'    => 'Tags',
                'attr'     => ['placeholder' => 'Tags'],
                'mapped'   => false,
                'required' => false,
            ]
        );
        unset($options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'      => BUNDLEU . '\SiteBundle\Entity\Bookmark'
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'bookmark';
    }
}
