<?php

namespace Mkk\AdminBundle\Form\Traits;

use Mkk\SiteBundle\Type\MceEditorType;
use Mkk\SiteBundle\Type\OuiNonType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;

trait EtablissementTrait
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'envoyer',
            Type\SubmitType::class
        );
        $builder->add(
            'adresses',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.adresse'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label'        => 'Adresses',
            ]
        );
        $builder->add(
            'users',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.user'),
                'allow_add'    => false,
                'delete_empty' => true,
                'allow_delete' => false,
                'by_reference' => false,
                'label'        => 'Utilisateurs',
            ]
        );
        $builder->add(
            'liens',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.lien'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label'        => 'Liens',
            ]
        );
        $builder->add(
            'horaires',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.horaire'),
                'allow_add'    => true,
                'delete_empty' => false,
                'allow_delete' => true,
                'by_reference' => false,
                'label'        => 'Horaires',
            ]
        );
        $builder->add(
            'emails',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.email'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label'        => 'Emails',
            ]
        );
        $builder->add(
            'telephones',
            Type\CollectionType::class,
            [
                'required'     => false,
                'entry_type'   => $this->container->get('mkk_admin.form.collection.telephone'),
                'allow_add'    => true,
                'delete_empty' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label'        => 'Téléphones',
            ]
        );
        $builder->add(
            'directeur',
            Type\TextType::class,
            [
                'label'    => 'Directeur',
                'required' => false,
            ]
        );
        $builder->add(
            'nom',
            Type\TextType::class,
            ['label' => 'Enseigne']
        );
        $builder->add(
            'prefix',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Prefix',
            ]
        );
        $builder->add(
            'alias',
            Type\TextType::class,
            [
                'label'    => 'Alias',
                'required' => false,
            ]
        );
        $builder->add(
            'actif',
            OuiNonType::class,
            [
                'label'    => 'Activation sur le site publique',
            ]
        );
        $builder->add(
            'video',
            Type\UrlType::class,
            [
                'label'    => 'Lien vidéo (Youtube / Viméo / Dailymotion)',
                'required' => false,
            ]
        );
        $builder->add(
            'videoiframe360',
            MceEditorType::class,
            [
                'label'    => 'Video 360',
                'required' => false,
            ]
        );
        $builder->add(
            'descriptionactivite',
            MceEditorType::class,
            [
                'label'    => 'Description',
                'required' => false,
            ]
        );
        if (isset($this->params['etablissement_mediaphotoequipe']) && 1 == $this->params['etablissement_mediaphotoequipe']) {
            $builder->add(
                'filevuesequipe',
                Type\TextType::class,
                [
                    'required' => false,
                    'label'    => "L'équipe",
                    'attr'     => [
                        'data-upload' => 'beetablissement_uploadvuesequipe',
                    ],
                ]
            );
        }
        if (isset($this->params['etablissement_mediaphotoenexterieur']) && 1 == $this->params['etablissement_mediaphotoenexterieur']) {
            $builder->add(
                'filevuesexterne',
                Type\TextType::class,
                [
                    'required' => false,
                    'label'    => 'En extérieur',
                    'attr'     => [
                        'data-upload' => 'beetablissement_uploadvuesexterne',
                    ],
                ]
            );
        }
        if (isset($this->params['etablissement_mediaphotoeninterieur']) && 1 == $this->params['etablissement_mediaphotoeninterieur']) {
            $builder->add(
                'filevuesinterne',
                Type\TextType::class,
                [
                    'required' => false,
                    'label'    => 'En intérieur',
                    'attr'     => [
                        'data-upload' => 'beetablissement_uploadvuesinterne',
                    ],
                ]
            );
        }
        if (isset($this->params['etablissement_mediaimages']) && 1 == $this->params['etablissement_mediaimages']) {
            $builder->add(
                'filegalerie',
                Type\TextType::class,
                [
                    'required' => false,
                    'label'    => 'Galerie',
                    'attr'     => [
                        'data-upload' => 'beetablissement_uploadgalerie',
                    ],
                ]
            );
        }
        if (isset($this->params['etablissement_medialogo']) && 1 == $this->params['etablissement_medialogo']) {
            $builder->add(
                'filevignette',
                Type\TextType::class,
                [
                    'required' => false,
                    'label'    => 'Photo de couverture',
                    'attr'     => [
                        'data-upload' => 'beetablissement_uploadvignette',
                    ],
                ]
            );
        }
        if (isset($this->params['etablissement_factures']) && 1 == $this->params['etablissement_factures']) {
            $builder->add(
                'filepdf',
                Type\TextType::class,
                [
                    'required' => false,
                    'label'    => 'Pdf',
                    'attr'     => [
                        'data-upload' => 'beetablissement_uploadpdf',
                    ],
                ]
            );
        }
        if (isset($this->params['etablissement_mediaimages']) and 1 == $this->params['etablissement_mediaimages']) {
            $builder->add(
                'copyright',
                MceEditorType::class,
                [
                    'label'    => 'Copyright',
                    'required' => false,
                ]
            );
        }
        if (isset($this->params['etablissement_secteur']) and 1 == $this->params['etablissement_secteur']) {
            $builder->add(
                'nafsousclasse',
                Type\TextType::class,
                [
                    'label'    => 'Code NAF',
                    'required' => false,
                    'attr'     => [
                        'placeholder' => 'Code NAF',
                        'data-url'    => 'beetablissement_searchnafsousclasse',
                    ],
                ]
            );
            $builder->add(
                'activite',
                Type\TextType::class,
                [
                    'label'    => 'Activité',
                    'required' => false,
                    'attr'     => [
                        'placeholder' => 'Activité',
                    ],
                ]
            );
        }

        if (isset($this->params['etablissement_nbrsalarie']) and 1 == $this->params['etablissement_nbrsalarie']) {
            $builder->add(
                'nbsalarie',
                Type\IntegerType::class,
                [
                    'attr'     => ['min' => 0],
                    'label'    => 'Nombre de salariés',
                    'required' => false,
                ]
            );
        }

        if (isset($this->params['etablissement_ca']) and 1 == $this->params['etablissement_ca']) {
            $builder->add(
                'ca',
                Type\TextType::class,
                [
                    'label'    => 'Chiffre d\'affaire',
                    'required' => false,
                ]
            );
        }

        if (isset($this->params['etablissement_siret']) and 1 == $this->params['etablissement_siret']) {
            $builder->add(
                'siret',
                Type\TextType::class,
                [
                    'label'    => 'N° de SIRET',
                    'required' => false,
                ]
            );
        }

        if (isset($this->params['etablissement_ape']) and 1 == $this->params['etablissement_ape']) {
            $builder->add(
                'ape',
                Type\TextType::class,
                [
                    'label'    => 'APE',
                    'required' => false,
                ]
            );
        }

        if (isset($this->params['etablissement_tva']) and 1 == $this->params['etablissement_tva']) {
            $builder->add(
                'tvaintra',
                Type\TextType::class,
                [
                    'label'    => 'N° TVA intracommunautaire',
                    'required' => false,
                ]
            );
        }
    }
}
