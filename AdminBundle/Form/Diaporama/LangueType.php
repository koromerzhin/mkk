<?php

namespace Mkk\AdminBundle\Form\Diaporama;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LangueType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'nom',
            Type\TextType::class,
            ['label' => 'Nom', 'attr' => ['placeholder' => 'Nom']]
        );
        unset($options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                    'data_class'      => BUNDLEU . '\SiteBundle\Entity\Diaporama',
                    'csrf_protection' => false,
                ]
        );
    }

    public function getName()
    {
        return 'diaporama';
    }
}
