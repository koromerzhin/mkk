<?php

namespace Mkk\AdminBundle\Form\Diaporama;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StandardType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'envoyer',
            Type\SubmitType::class
        );
        $builder->add(
            'fileimages',
            Type\TextType::class,
            [
                'required' => false,
                'label'    => 'Galerie',
                'attr'     => [
                    'data-upload' => 'bediaporama_uploadimages',
                ],
            ]
        );
        unset($options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                    'data_class'      => BUNDLEU . '\SiteBundle\Entity\Diaporama'
                ]
        );
    }

    public function getName()
    {
        return 'diaporama';
    }
}
