<?php

namespace Mkk\PublicBundle\Controller;

use Mkk\PublicBundle\Controller\Bookmark\AffichageTrait;
use Mkk\PublicBundle\Controller\Bookmark\BddTrait;
use Symfony\Component\HttpFoundation\Request;

trait BookmarkTrait
{
    use AffichageTrait;
    use BddTrait;

    public function bookmarkListingAction(Request $request)
    {
        $actuelroute  = $request->get('_route');
        list($module) = explode('_', $actuelroute);
        $this->bookmarkBddIndex($request);
        if (count($this->paginator) == 0 && $request->get('page') != 0) {
            $url      = $this->generateUrl($module . '_index');
            $redirect = $this->redirect($url, 301);
        } elseif (count($this->paginator) == 0) {
            $url      = $this->generateUrl('public_index');
            $redirect = $this->redirect($url, 301);
        }

        $twig = $this->bookmarkAffichageIndex($request);

        return $twig;
    }

    public function bookmarkLienAction(Request $request, $alias)
    {
        $actuelroute  = $request->get('_route');
        list($module) = explode('_', $actuelroute);
        $bookmark     = $this->bookmarkBddLien($request, $alias);
        if (! $bookmark) {
            $url = $this->generateUrl($module . '_index');
        }

        $url      = $bookmark->getUrl();
        $redirect = $this->redirect($url, 301);

        return $redirect;
    }

    public function bookmarkTagsAction(Request $request, $tag)
    {
        $actuelroute  = $request->get('_route');
        list($module) = explode('_', $actuelroute);
        $this->bookmarkBddTags($request, $tag);
        if (count($this->paginator) == 0 && $request->get('page') != 0) {
            $url      = $this->generateUrl($module . '_index');
            $redirect = $this->redirect($url, 301);
        } elseif (count($this->paginator) == 0) {
            $url      = $this->generateUrl('public_index');
            $redirect = $this->redirect($url, 301);
        }

        $twig = $this->bookmarkAffichageIndex($request);

        return $twig;
    }
}
