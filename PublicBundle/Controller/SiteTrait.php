<?php

namespace Mkk\PublicBundle\Controller;

trait SiteTrait
{
    use BlogTrait,
        BookmarkTrait;
}
