<?php

namespace Mkk\PublicBundle\Controller;

use Mkk\PublicBundle\Controller\Blog\AffichageTrait;
use Mkk\PublicBundle\Controller\Blog\BddTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

trait BlogTrait
{
    use AffichageTrait;
    use BddTrait;

    /**
     * @Route("/blog/",name="blog_index" , defaults={"page": 0, "_locale": "fr"})
     * @Route("/blog/{page}.html",name="blog_page", defaults={"_locale": "fr"})
     *
     * @param mixed $page
     */
    public function blogListingAction(Request $request)
    {
        $dataBDD = $this->blogBddIndex($request);
        $twig    = $this->blogAffichageIndex($request, $dataBDD);

        return $twig;
    }

    /**
     * @Route("/blog/{alias}", name="blog_article", defaults={"_locale": "fr"})
     *
     * @param mixed $alias
     */
    public function blogArticleAction(Request $request, $alias)
    {
        $article = $this->blogBddArticle($request, $alias);
        if (! $article) {
            $url      = $this->generateUrl('blog_index');
            $redirect = $this->redirect($url, 301);

            return $redirect;
        }

        $twig = $this->blogAffichageArticle($request, $article);

        return $twig;
    }

    /**
     * @Route("/blog/archive/", name="blog_archiveindex", defaults={"date": "", "_locale": "fr"})
     * @Route("/blog/archive/{date}", name="blog_archivedate", defaults={"_locale": "fr"})
     *
     * @param mixed $date
     */
    public function blogArchiveAction(Request $request, $date)
    {
        $dataBDD = $this->blogBddArchive($request, $date);
        $twig    = $this->blogAffichageArchive($request, $dataBDD, ['dateListing' => $date]);

        return $twig;
    }

    /**
     * @Route("/blog/{categorie}/",name="blog_categorieindex" , defaults={"page": 0, "_locale": "fr"})
     * @Route("/blog/{categorie}/{page}.html",name="blog_categoriepage", defaults={"_locale": "fr"})
     *
     * @param mixed $categorie
     * @param mixed $page
     */
    public function blogCategorieAction(Request $request, $categorie, $page)
    {
        $dataBDD = $this->blogBddTags($request, $categorie);
        if ($dataBDD['pagination']['total'] == 0) {
            $dataBDD = $this->blogBddCategories($request, $categorie, $page);
            if ($dataBDD['pagination']['total'] == 0) {
                $url      = $this->generateUrl('blog_index');
                $redirect = $this->redirect($url, 301);

                return $redirect;
            }
        }

        $twig = $this->blogAffichageIndex($request, $dataBDD);

        return $twig;
    }
}
