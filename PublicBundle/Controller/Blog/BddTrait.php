<?php

namespace Mkk\PublicBundle\Controller\Blog;

use Symfony\Component\HttpFoundation\Request;

trait BddTrait
{
    private function blogBddArchive(Request $request, $date)
    {
        $locale         = $request->getLocale();
        $blogManager    = $this->get('bdd.blog_manager');
        $blogRepository = $blogManager->getRepository();
        $search         = [
            'langue' => $locale,
        ];
        if ($date == '') {
            $dataBDD = $blogRepository->publicgetArchiveListing($search);
        } else {
            $dataBDD = $blogRepository->publicgetArchiveListingDate($search, $date);
        }

        return $dataBDD;
    }

    private function blogBddIndex(Request $request)
    {
        $locale         = $request->getLocale();
        $blogManager    = $this->get('bdd.blog_manager');
        $blogRepository = $blogManager->getRepository();
        $search         = [
            'langue' => $locale,
        ];

        $order = [
            'datepublication' => 'DESC',
        ];

        $dataBDD = $blogRepository->publicgetListing($search, $order);

        return $dataBDD;
    }

    private function blogBddArticle($alias)
    {
        $blogManager    = $this->get('bdd.blog_manager');
        $blogRepository = $blogManager->getRepository();
        $search         = [
            'alias' => $alias,
        ];
        $article        = $blogRepository->publicgetArticle($search);

        return $article;
    }

    private function blogBddTags($request, $tags)
    {
        $locale         = $request->getLocale();
        $blogManager    = $this->get('bdd.blog_manager');
        $blogRepository = $blogManager->getRepository();
        $search         = [
                'langue' => $locale,
            ];

        $order = [
                'datepublication' => 'DESC',
            ];

        $dataBDD = $blogRepository->publicgetTagsListing($search, $order, $tags);

        return $dataBDD;
    }

    private function blogBddCategories($request, $categorie)
    {
        $locale         = $request->getLocale();
        $blogManager    = $this->get('bdd.blog_manager');
        $blogRepository = $blogManager->getRepository();
        $search         = [
                'langue' => $locale,
            ];

        $order = [
                'datepublication' => 'DESC',
            ];

        $dataBDD = $blogRepository->publicgetCategoriesListing($search, $order, $categorie);

        return $dataBDD;
    }
}
