<?php

namespace Mkk\PublicBundle\Controller\Blog;

use Symfony\Component\HttpFoundation\Request;

trait AffichageTrait
{
    private function blogAffichageArchive(Request $request, $dataBDD, $paramData = [])
    {
        $fichier     = $this->trouverFichierTwig('Blog', 'archive.html.twig');
        $paramGlobal = [
            'listing' => $dataBDD,
        ];
        $paramRender = array_merge($paramGlobal, $paramData);
        $twig        = $this->renderMkk(
            $request,
            $fichier,
            $paramRender
        );

        return $twig;
    }

    private function blogAffichageIndex(Request $request, $dataBDD, $paramData = [])
    {
        $fichier     = $this->trouverFichierTwig('Blog', 'index.html.twig');
        $paramGlobal = [
            'articles' => $dataBDD,
        ];
        $paramRender = array_merge($paramGlobal, $paramData);
        $twig        = $this->renderMkk(
            $request,
            $fichier,
            $paramRender
        );

        return $twig;
    }

    private function blogAffichageArticle(Request $request, $article, $paramData = [])
    {
        $fichier     = $this->trouverFichierTwig('Blog', 'article.html.twig');
        $paramGlobal = [
            'article'     => $article,
            'commentaire' => 1,
        ];
        $paramGlobal = $this->setMetatags($paramGlobal, $article);

        $paramRender = array_merge($paramGlobal, $paramData);
        $twig        = $this->renderMkk(
            $request,
            $fichier,
            $paramRender
        );

        return $twig;
    }

    private function setMetatags($paramGlobal, $article)
    {
        if (! isset($paramGlobal['metatags'])) {
            $paramGlobal['metatags'] = [];
        }

        $metatags                 = $paramGlobal['metatags'];
        $metatags['twitter:card'] = 'summary_large_image';
        if ($article->getImage() != '') {
            $url = $this->generateUrl('public_index', [], true) . $article->getImage();

            $metatags['twitter:image'] = $url;
        }

        $title         = $article->getTitre();
        $user          = $article->getRefUser();
        $author        = $user->getAppel();
        $publishedDate = $article->getDatepublication();
        if ($publishedDate != '') {
            $publishedDate = date('c', $publishedDate);
        }

        $modifiedDate = $article->getDatemodif();
        if ($modifiedDate != '') {
            $modifiedDate = date('c', $modifiedDate);
        }

        $categorie = $article->getRefCategorie();
        $categorie = $categorie->getNom();
        $tags      = $article->getTags();

        $metatags['twitter:title'] = $title;
        $metatags['og:type']       = 'article';
        if ($publishedDate != '') {
            $metatags['article:published_time'] = $publishedDate;
        }

        if ($modifiedDate != '') {
            $metatags['article:modified_time'] = $modifiedDate;
        }

        $metatags['article:author']  = $author;
        $metatags['article:section'] = $categorie;
        $metatags['og:title']        = $title;
        foreach ($tags as $tag) {
            $metatags['article:tag'] = $tag->getNom();
        }

        $liens = $user->getLiens();
        foreach ($liens as $lien) {
            if (substr_count($lien->getAdresse(), 'twitter') != 0) {
                $twitterUrl = $lien->getAdresse();
                $twitterUrl = '@' . substr($twitterUrl, strrpos($twitterUrl, '/') + 1);

                $metatags['twitter:creator'] = $twitterUrl;
            }
        }

        $metaDescription = $article->getMetadescription();

        $metatags['description']         = $metaDescription;
        $metatags['twitter:description'] = $metaDescription;
        $metatags['og:description']      = $metaDescription;

        $metaKeywords = $article->getMetakeywords();

        $metatags['keywords'] = $metaKeywords;

        $paramGlobal['metatags'] = $metatags;

        return $paramGlobal;
    }
}
