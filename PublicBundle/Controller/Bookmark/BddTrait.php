<?php

namespace Mkk\PublicBundle\Controller\Bookmark;

trait BddTrait
{
    private function bookmarkBddIndex()
    {
        $listingService = $this->get('mkk.listing_service');
        $listingService->setRequest('bdd.bookmark_manager', 'publicgetListing');
    }

    private function bookmarkBddLien($alias)
    {
        $bookmarkManager    = $this->get('bdd.bookmark_manager');
        $bookmarkRepository = $bookmarkManager->getRepository();
        $search             = [
            'alias' => $alias,
        ];
        $article            = $bookmarkRepository->publicgetLien($search);

        return $article;
    }

    private function bookmarkBddTags($tags)
    {
        $listingService = $this->get('mkk.listing_service');
        $listingService->setRequest('bdd.bookmark_manager', 'publicgetTagsListing', $tags);
    }
}
