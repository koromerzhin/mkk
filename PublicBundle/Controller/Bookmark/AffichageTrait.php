<?php

namespace Mkk\PublicBundle\Controller\Bookmark;

use Symfony\Component\HttpFoundation\Request;

trait AffichageTrait
{
    private function bookmarkAffichageIndex(Request $request, $paramData = [])
    {
        $fichier = $this->trouverFichierTwig('Bookmark', 'index.html.twig');
        $twig    = $this->renderMkk(
            $request,
            $fichier,
            $paramData
        );

        return $twig;
    }
}
