<?php

namespace Mkk\PublicBundle\Controller;

use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use hybridauth\hybridauth\Hybrid\Auth as Hybrid_Auth;
use Mkk\PublicBundle\Form\ResettingType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;

trait ScriptTrait
{
    /**
     * @Route("/reset/{token}", name="fos_user_resetting_reset")
     * Reset user password.
     *
     * @param Request $request
     * @param string  $token
     *
     * @return Response
     */
    public function resetAction(Request $request, $token)
    {
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->findUserByConfirmationToken($token);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with "confirmation token" does not exist for value "%s"', $token));
        }

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::RESETTING_RESET_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $formResetting = $this->createForm(
            ResettingType::class,
            $user
        );
        $formResetting->handleRequest($request);

        if ($formResetting->isSubmitted() && $formResetting->isValid()) {
            $event = new FormEvent($formResetting, $request);
            $dispatcher->dispatch(FOSUserEvents::RESETTING_RESET_SUCCESS, $event);

            $userManager->updateUser($user);

            if (null === $response = $event->getResponse()) {
                $url      = $this->generateUrl('user_login');
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(
                FOSUserEvents::RESETTING_RESET_COMPLETED,
                new FilterUserResponseEvent($user, $request, $response)
            );

            return $response;
        }

        $twigHtml = $this->renderMkk(
            $request,
            'MkkPublicBundle:Default:reset.html.twig',
            [
                'token'         => $token,
                'form'          => $formResetting->createView(),
            ]
        );

        return $twigHtml;
    }

    /**
     * @Route("/check-email", name="fos_user_resetting_check_email")
     * Tell the user to check his email provider.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function checkEmailAction(Request $request)
    {
        $username = $request->query->get('username');

        if (empty($username)) {
            // the user does not come from the sendEmail action
            return new RedirectResponse($this->generateUrl('user_login'));
        }

        $twigHtml = $this->renderMkk(
            $request,
            'MkkPublicBundle:Default:check_email.html.twig',
            [
                'bancookie'     => false,
                'tokenLifetime' => ceil($this->container->getParameter('fos_user.resetting.retry_ttl') / 3600),
            ]
        );

        return $twigHtml;
    }

    /**
     * @Route("/login", name="user_login")
     */
    public function userLoginAction(Request $request)
    {
        $session         = $request->getSession();
        $authErrorKey    = Security::AUTHENTICATION_ERROR;
        $lastUsernameKey = Security::LAST_USERNAME;
        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has($authErrorKey)) {
            $error = $request->attributes->get($authErrorKey);
        } elseif (null !== $session && $session->has($authErrorKey)) {
            $error = $session->get($authErrorKey);
            $session->remove($authErrorKey);
        } else {
            $error = null;
        }

        if (! $error instanceof AuthenticationException) {
            $error = null; // The value does not come from the security component.
        }

        $lastUsername = (null === $session) ? '' : $session->get($lastUsernameKey);
        $formFactory  = $this->get('form.factory');
        $formService  = $this->get('mkk.authenticated.form.login');
        $formLogin    = $formFactory->create(
            $formService,
            null,
            [
                'last_username' => $lastUsername,
            ]
        );

        $formService   = $this->get('mkk.authenticated.form.forgot');
        $formForgot    = $formFactory->create($formService);

        $parameters = [
            'error'           => $error,
            'bancookie'       => false,
            'formLogin'       => $formLogin->createView(),
            'formForgot'      => $formForgot->createView(),
        ];

        $twigHtml = $this->renderMkk(
            $request,
            'MkkPublicBundle:Default:login.html.twig',
            $parameters
        );

        return $twigHtml;
    }

    /**
     * @Route("/showtel/{id}", name="script_showtel")
     *
     * @param mixed $id
     */
    public function showtelAction($id)
    {
        $tab                 = '';
        $telephoneManager    = $this->get('bdd.telephone_manager');
        $telephoneRepository = $telephoneManager->getRepository();
        $telephone           = $telephoneRepository->find($id);
        if ($telephone) {
            $this->get('mkk.stats_service')->set('telephone', $telephone->getChiffre());
            $tab = $telephone->getChiffre();
        }
        $jsonresponse = new JsonResponse($tab);

        return $jsonresponse;
    }

    /**
     * @Route("/showlien/{id}", name="script_showlien")
     *
     * @param mixed $id
     */
    public function showlienAction($id)
    {
        $lienManager    = $this->get('bdd.lien_manager');
        $lienRepository = $lienManager->getRepository();
        $tab            = [];
        $lien           = $lienRepository->find($id);
        if ($lien) {
            $this->get('mkk.stats_service')->set('lien', $lien->getAdresse());

            return $this->redirect($lien->getAdresse());
        }
        $jsonresponse = new JsonResponse($tab);

        return $jsonresponse;
    }

    /**
     * @Route("/tinymce", name="script_tinymce")
     */
    public function tinymceAction(Request $request)
    {
        $generateurl = 'public_index';
        $filename    = $request->server->get('SCRIPT_FILENAME');
        $url         = str_replace(['app.php', 'app_dev.php', '/web/'], '', $filename);
        $url         = substr($url, strrpos($url, '/') + 1);
        $filemanager = 'plugins/responsivefilemanager/plugin.min.js';
        $json        = [
            'filemanager_title'      => 'Gestion des fichiers',
            'filemanager_access_key' => md5('48tp6QNp' . $url . date('m/Y')),
            'url'                    => $this->generateUrl($generateurl) . 'filemanager/',
            'external_plugins'       => [
                'filemanager' => $filemanager,
            ],
        ];

        $param            = $this->get('mkk.param_service')->listing();
        $filemanagerDroit = 0;
        if (isset($param['tinymce_filemanageracces'])) {
            $filemanageracces     = $param['tinymce_filemanageracces'];
            $tokenStorage         = $this->get('security.token_storage')->getToken();
            $authorizationChecker = $this->get('security.authorization_checker');
            if (null != $tokenStorage && $authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
                $idGroup = $tokenStorage->getUser()->getRefGroup()->getId();
                foreach ($filemanageracces as $groupId) {
                    if ($idGroup == $groupId) {
                        $filemanagerDroit = 1;
                        break;
                    }
                }
            }
        }

        if ($filemanagerDroit == 1) {
            $json['filemanager_access_key']    = md5('48tp6QNp' . $url . date('m/Y'));
            $json['external_filemanager_path'] = $this->generateUrl($generateurl, [], true) . 'filemanager/';
            $json['external_plugins']          = [
                'filemanager' => $filemanager,
            ];
        }

        $fichiers = glob('scripts/tinymce/langs/*');
        $lang     = $request->request->get('language');
        if ($lang == '') {
            $param = $this->get('mkk.param_service')->listing();
            $lang  = $param['langueprincipal'];
        }

        foreach ($fichiers as $file) {
            $fichier = str_replace('scripts/tinymce/langs/', '', $file);
            $fichier = str_replace('.js', '', $fichier);
            if (substr_count($fichier, $lang) != 0) {
                $json['language'] = $fichier;
                break;
            }
        }

        if (isset($json['filemanager_access_key'])) {
            $json['plugins'] = [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern responsivefilemanager',
            ];
        } else {
            $json['plugins'] = [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern',
            ];
        }

        $json = new JsonResponse($json);

        return $json;
    }

    /**
     * @Route("/disable/", name="script_disable")
     */
    public function disableAction(Request $request)
    {
        $param = $this->get('mkk.param_service')->listing();
        $html  = '';
        if (isset($param['desactivation_raison'])) {
            $html = $param['desactivation_raison'];
        }

        $parameters = [
            'html'      => $html,
            'bancookie' => false,
        ];

        $twigHtml = $this->renderMkk(
            $request,
            'MkkPublicBundle:Default:disable.html.twig',
            $parameters
        );

        return $twigHtml;
    }

    public function socialAction()
    {
        $url    = $this->generateUrl('script_auth', [], true);
        $config = $this->get('mkk.param_service')->social(
            $url,
            [
            'Facebook' => ['scope' => 'email, user_about_me, user_birthday, user_hometown, user_website, offline_access, read_stream, publish_stream, read_friendlists,manage_pages'],
            ]
        );
        new Hybrid_Auth($config);
        Hybrid_Endpoint::process();
    }

    /**
     * @Route("/upload", name="script_upload")
     */
    public function uploadAction(Request $request)
    {
        return $this->renderMkk($request, 'MkkPublicBundle:Default:result.html.twig');
    }

    /**
     * @Route("/cpville", name="script_cpville")
     */
    public function cpvilleAction(Request $request)
    {
        $json            = [];
        $requete         = [
            'pays'  => $request->get('pays'),
            'cp'    => $request->get('cp'),
            'ville' => $request->get('ville'),
            'gps'   => $request->get('gps'),
        ];
        $json['reponse'] = $this->get('mkk.geodata_service')->getcpville($requete);
        $responsejson    = new JsonResponse($json);

        return $responsejson;
    }

    /**
     * @Route("/javascript", name="script_javascript")
     */
    public function javascriptAction(Request $request)
    {
        return $this->renderMkk($request, 'MkkPublicBundle:Default:nojs.html.twig');
    }

    /**
     * @Route("/langue/{langue}", name="user_langue")
     *
     * @param null|mixed $langue
     */
    public function userLangueAction(Request $request, $langue = null)
    {
        $container = $this->get('service_container');
        if ($langue != null) {
            $container->get('session')->setLocale($langue);
        }

        $routes      = $this->getRoutes();
        $generateurl = 'bord_index';
        foreach (array_keys($routes) as $url) {
            if ($url == 'public_index') {
                $generateurl = $url;
                break;
            }
        }

        // on tente de rediriger vers la page d'origine
        $url = $request->headers->get('referer');
        if (empty($url)) {
            $url = $this->generate($generateurl);
        }

        $redirect = $this->redirect($url, 301);

        return $redirect;
    }

    /**
     * @Route("/change", name="user_change")
     */
    public function userChangeAction()
    {
        $userManager    = $this->get('bdd.user_manager');
        $userRepository = $userManager->getRepository();
        $user           = $userRepository->findoneby(['salt' => $id]);
        $token          = $this->get('security.token_storage')->getToken();
        $testsalt       = ($id != $token->getUser()->getSalt() && ! is_numeric($id));
        $generateurl    = 'public_index';
        if ($user && $testsalt) {
            $token = new UsernamePasswordToken($user, null, 'main', ['ROLE_ADMIN']);
            $this->get('security.token_storage')->setToken($token);
        }

        $url      = $this->generateUrl($generateurl);
        $redirect = $this->redirect($url, 301);

        return $redirect;
    }

    /**
     * @Route("/telephone", name="script_telephone")
     */
    public function telephoneAction(Request $request)
    {
        $serviceTel = $this->get('mkk.telephone_service');
        $numero     = $request->request->get('telephone');
        $md5        = $request->request->get('md5');
        if ($numero != '') {
            if (substr_count($numero, ':') != 0) {
                list($code, $chaine) = explode(':', $numero);
                unset($code);
            } else {
                $chaine = $numero;
            }

            $responsejson = $serviceTel->verif($chaine);
        } else {
            $responsejson = [];
        }

        $responsejson['md5'] = $md5;

        $json = new JsonResponse($responsejson);

        return $json;
    }
}
