<?php

namespace Mkk\PublicBundle\Form;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LoginType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            '_username',
            Type\TextType::class,
            [
                'label' => 'Pseudo / E-mail',
                'data'  => $options['last_username'],
                'attr'  => [
                    'autocomplete'     => 'off',
                    'placeholder'      => 'Pseudo / E-mail',
                    'data-labelactive' => 1,
                ],
            ]
        );
        $builder->add(
            '_password',
            Type\PasswordType::class,
            [
                'label' => 'Password',
                'attr'  => [
                    'autocomplete' => 'off',
                    'placeholder'  => 'Password',
                ],
            ]
        );
        $builder->add(
            '_remember_me',
            Type\CheckboxType::class,
            [
                'label'    => 'Se souvenir de moi',
                'required' => false,
                'mapped'   => false,
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'last_username'   => 'aa',
                'csrf_protection' => false,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
