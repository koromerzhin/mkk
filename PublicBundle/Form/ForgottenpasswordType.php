<?php

namespace Mkk\PublicBundle\Form;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ForgottenpasswordType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'username',
            Type\TextType::class,
            [
                'label' => 'Pseudo / E-mail',
                'attr'  => [
                    'placeholder'  => 'Pseudo / E-mail',
                ],
            ]
        );
        unset($options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'csrf_protection' => false,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
