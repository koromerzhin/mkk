<?php

namespace Mkk\PublicBundle\Form;

use Mkk\SiteBundle\Lib\AbstractTypeLib;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ResettingType extends AbstractTypeLib
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'plainPassword',
            Type\RepeatedType::class,
            [
                'type'            => PasswordType::class,
                'invalid_message' => 'fos_user.password.mismatch',
                'first_options'   => [
                    'attr' => [
                        'placeholder' => 'Nouveau mot de passe',
                    ],
                ],
                'second_options'  => [
                    'attr' => [
                        'placeholder' => 'Répéter le nouveau mot de passe',
                    ],
                ],
                'label'           => 'Password',
            ]
        );
        unset($options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'last_username'   => 'aa',
                'csrf_protection' => false,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
