<?php

namespace Mkk\SiteBundle\Handler;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;

class UploadHandler
{
    private $accessor;
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->accessor  = PropertyAccess::createPropertyAccessor();
    }

    public function uploadFile($entity, $property, $annotation)
    {
        $md5       = $this->accessor->getValue($entity, $property);
        $mkkupload = $this->container->get('mkk.upload_service');
        $recuperer = $mkkupload->get($md5);
        $data      = $mkkupload->move($recuperer, $entity, $this->accessor, $annotation);
        $this->accessor->setValue($entity, $annotation->getFilename(), $data);
    }

    /**
     * Supprimer les anciens fichiers présent sur le site (WIP).
     *
     * @param mixed $entity
     * @param mixed $annotation
     */
    public function removeOldFile($entity, $annotation): void
    {
        $filename = $this->accessor->getValue($entity, $annotation->getFilename());
        if ('' == $filename) {
            return;
        }
        if (is_array($filename)) {
            $data = $filename;
        } else {
            $data = json_decode($filename, true);
            if (! is_array($data)) {
                $data = [$filename];
            }
        }
    }

    public function setFileFromFilename($entity, $property, $annotation)
    {
        $md5 = $this->getFileFromFilename($entity, $annotation);
        $this->accessor->setValue($entity, $property, $md5);
    }

    private function getFileFromFilename($entity, $annotation)
    {
        $filename = $this->accessor->getValue($entity, $annotation->getFilename());
        $md5      = md5(uniqid(null, true));
        $this->container->get('mkk.upload_service')->init($md5, $filename);

        return $md5;
    }
}
