<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mkk\SiteBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\FlattenException;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;

/**
 * ExceptionController renders error or exception pages for a given
 * FlattenException.
 */
class ExceptionController extends ContainerAware
{
    protected $twig;

    /**
     * @var bool Show error (false) or exception (true) pages by default
     */
    protected $debug;

    /**
     * Converts an Exception to a Response.
     *
     * A "showException" request parameter can be used to force display of an error page (when set to false) or
     * the exception page (when true). If it is not present, the "debug" value passed into the constructor will
     * be used.
     *
     * @param Request              $request   The request
     * @param FlattenException     $exception A FlattenException instance
     * @param DebugLoggerInterface $logger    A DebugLoggerInterface instance
     *
     * @throws \InvalidArgumentException When the exception template does not exist
     *
     * @return Response
     */
    public function showAction(Request $request, FlattenException $exception, DebugLoggerInterface $logger = null)
    {
        $currentContent = $this->getAndCleanOutputBuffering($request->headers->get('X-Php-Ob-Level', -1));
        $showException  = $request->attributes->get('showException', $this->debug); // As opposed to an additional parameter, this maintains BC

        $code   = $exception->getStatusCode();
        $render = $this->container->get('templating')->render(
            (string)$this->findTemplate($request, $code, $showException),
            [
                'status_code'    => $code,
                'status_text'    => isset(Response::$statusTexts[$code]) ? Response::$statusTexts[$code] : '',
                'exception'      => $exception,
                'logger'         => $logger,
                'currentContent' => $currentContent,
            ]
        );

        $response = new Response($render);

        return $response;
    }

    /**
     * @param int $startObLevel
     *
     * @return string
     */
    protected function getAndCleanOutputBuffering($startObLevel)
    {
        if (ob_get_level() <= $startObLevel) {
            return '';
        }

        Response::closeOutputBuffers($startObLevel + 1, true);

        return ob_get_clean();
    }

    /**
     * @param Request $request
     * @param string  $format
     * @param int     $code          An HTTP response status code
     * @param bool    $showException
     *
     * @return string
     */
    protected function findTemplate(Request $request, $code)
    {
        $format     = $request->getRequestFormat();
        $templating = $this->container->get('templating');
        $engine     = 'twig';

        $names = [
           'error' . $code . '.' . $format . '.' . $engine,
           'error.' . $format . '.' . $engine,
           'error.html.' . $engine,
        ];

        $bundles = [
           'MkkSiteBundle',
        ];

        array_merge($bundles, ['TwigBundle']);

        foreach ($bundles as $bundle) {
            foreach ($names as $name) {
                $template = $bundle . ':Exception:' . $name;

                if ($templating->exists($template)) {
                    return $template;
                }
            }
        }

        return 'TwigBundle:Exception:error.html.twig';
    }
}
