<?php

namespace Mkk\SiteBundle\Annotation;

use Doctrine\Common\Annotations\CachedReader;

class AnnotationReader
{
    /**
     * @var AnnotationReader
     */
    private $reader;

    public function __construct(CachedReader $reader)
    {
        $this->reader = $reader;
    }

    public function getUploadableFields($entity): array
    {
        $tab        = [];
        $reflection = new \ReflectionClass(get_class($entity));
        foreach ($reflection->getProperties() as $property) {
            $annotation = $this->reader->getPropertyAnnotation($property, UploadableField::class);
            if ($annotation != null) {
                $name       = $property->getName();
                $tab[$name] = $annotation;
            }
        }

        return $tab;
    }
}
