<?php

namespace Mkk\SiteBundle\Type\Date;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DatePickerType extends AbstractType
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
            'widget'      => 'single_text',
            'format'      => 'dd/MM/yyyy',
            ]
        );
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $attr = $options['attr'];
        if (! isset($attr['class'])) {
            $attr['class'] = '';
        }

        $attr['class']      = trim($attr['class'] . ' datepicker');
        $view->vars['attr'] = $attr;
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return DateType::class;
    }
}
