<?php

namespace Mkk\SiteBundle\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class DatePickerType extends AbstractType
{
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $attr = $options['attr'];
        if (! isset($attr['class'])) {
            $attr['class'] = '';
        }

        $attr['class']      = trim($attr['class'] . ' datepicker');
        $view->vars['attr'] = $attr;
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return TextType::class;
    }
}
