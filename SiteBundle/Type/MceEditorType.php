<?php

namespace Mkk\SiteBundle\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class MceEditorType extends AbstractType
{
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $attr = $options['attr'];
        if (! isset($attr['class'])) {
            $attr['class'] = '';
        }
        $attr['data-url'] = 'script_tinymce';
        $attr['class']    = trim($attr['class'] . ' mceEditor');
        if (! isset($attr['rows'])) {
            $attr['rows'] = 10;
        }
        $view->vars['attr'] = $attr;
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return TextareaType::class;
    }
}
