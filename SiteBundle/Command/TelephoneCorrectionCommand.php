<?php

namespace Mkk\SiteBundle\Command;

use Mkk\SiteBundle\Lib\ContainerAwareCommandLib;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TelephoneCorrectionCommand extends ContainerAwareCommandLib
{
    protected function configure()
    {
        $this->setName('mkk:correction:telephone');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Vérification des telephones');
        $container           = $this->getContainer();
        $telephoneManager    = $container->get('bdd.telephone_manager');
        $telephoneRepository = $telephoneManager->getRepository();
        $telephoneEntity     = $telephoneManager->getTable();
        $telephone           = new $telephoneEntity();
        $tab                 = [];
        $methods             = get_class_methods($telephone);
        $code                = 'getRef';
        foreach ($methods as $method) {
            if (substr($method, 0, strlen($code)) == $code) {
                $tab[$method] = strtolower(str_replace($code, 'ref', $method));
            }
        }

        $tabsupp = [];
        foreach ($tab as $method => $field) {
            $telephones = $telephoneRepository->commandFind($field);
            if (count($telephones) != 0) {
                $supp    = $this->telephoneVerifDoublon($telephones, $method, $tabsupp);
                $tabsupp = array_merge($tabsupp, $supp);
            }
        }

        $output->writeln('Suppression de ' . count($tabsupp) . ' telephone(s)');
        foreach ($tabsupp as $supp) {
            $telephoneManager->remove($supp);
        }

        $telephoneManager->flush();
        unset($input);
    }

    private function telephoneVerifDoublon($telephones, $field)
    {
        $tab  = [];
        $supp = [];
        foreach ($telephones as $telephone) {
            $idfield = $telephone->$field()->getId();
            $data    = $telephone->getChiffre();
            if (! isset($tab[$field][$idfield][$data])) {
                $tab[$field][$idfield][$data] = $telephone->getId();
            } else {
                $supp[] = $telephone;
            }
        }

        return $supp;
    }
}
