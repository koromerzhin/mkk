<?php

namespace Mkk\SiteBundle\Command;

use Mkk\SiteBundle\Lib\ContainerAwareCommandLib;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DebugPhpmdCommand extends ContainerAwareCommandLib
{
    protected function configure()
    {
        $this->setName('mkk:debug:phpmd');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('PHPMD');
        $phpmdBin = 'bin/phpmd';
        if (! is_file($phpmdBin)) {
            $output->writeln('PHPMD non installé');

            return false;
        }

        $folders = glob('src/*');
        foreach ($folders as $folder) {
            $output->writeln('Traitement du dossier ' . $folder);
            if (file_exists($folder . '/phpmd.xml')) {
                unlink($folder . '/phpmd.xml');
            }
            exec('php ' . $phpmdBin . ' ' . $folder . ' xml src/Mkk/SiteBundle/rules/phpmd.xml >> ' . $folder . '/phpmd.xml');
        }
        unset($input);
    }
}
