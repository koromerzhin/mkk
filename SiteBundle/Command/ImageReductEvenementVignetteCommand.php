<?php

namespace Mkk\SiteBundle\Command;

use Mkk\SiteBundle\Lib\ContainerAwareCommandLib;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImageReductEvenementVignetteCommand extends ContainerAwareCommandLib
{
    protected function configure()
    {
        $this->setName('mkk:image:reduct-evenementvignette');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $output->writeln('Reduction image evenement vignette');
        $paramManager    = $container->get('bdd.param_manager');
        $paramRepository = $paramManager->getRepository();
        $tab             = [
            'max_width' => [
                'code' => 'beevenement_uploadvignette-image-max_width',
                'val'  => '',
            ],
            'max_height' => [
                'code' => 'beevenement_uploadvignette-image-max_height',
                'val'  => '',
            ],
        ];
        foreach ($tab as $key => $data) {
            $param = $paramRepository->findoneby(['code' => $data['code']]);
            if ($param) {
                $tab[$key]['val'] = $param->getValeur();
            }
        }

        $this->redimensionImage('evenement/vignette', $tab);
        unset($input);
    }
}
