<?php

namespace Mkk\SiteBundle\Command;

use Mkk\SiteBundle\Lib\ContainerAwareCommandLib;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DebugPhpcsCommand extends ContainerAwareCommandLib
{
    protected function configure()
    {
        $this->setName('mkk:debug:phpcs');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('PHPCS');
        $phpcsBin = 'bin/phpcs';
        if (! is_file($phpcsBin)) {
            $output->writeln('PHPCS non installé');

            return false;
        }

        $folders = glob('src/*');
        foreach ($folders as $folder) {
            $output->writeln('Traitement du dossier ' . $folder);
            if (file_exists($folder . '/phpcs.json')) {
                unlink($folder . '/phpcs.json');
            }
            exec('php ' . $phpcsBin . ' ' . $folder . '  --standard=src/Mkk/SiteBundle/rules/phpcs.xml --report=json --extensions=php >> ' . $folder . '/phpcs.json');
        }
        unset($input);
    }
}
