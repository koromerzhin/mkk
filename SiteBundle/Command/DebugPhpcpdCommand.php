<?php

namespace Mkk\SiteBundle\Command;

use Mkk\SiteBundle\Lib\ContainerAwareCommandLib;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DebugPhpcpdCommand extends ContainerAwareCommandLib
{
    protected function configure()
    {
        $this->setName('mkk:debug:phpcpd');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('PHPCPD');
        $phpcpdBin = 'bin/phpcpd';
        if (! is_file($phpcpdBin)) {
            $output->writeln('PHPCPD non installé');

            return false;
        }

        $folders = glob('src/*');
        foreach ($folders as $folder) {
            $output->writeln('Traitement du dossier ' . $folder);
            if (file_exists($folder . '/phpcpd.log')) {
                unlink($folder . '/phpcpd.log');
            }

            exec('php ' . $phpcpdBin . ' --fuzzy -v ' . $folder . '  >> ' . $folder . '/phpcpd.log');
        }
        unset($input);
    }
}
