<?php

namespace Mkk\SiteBundle\Command;

use Mkk\SiteBundle\Lib\ContainerAwareCommandLib;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DebugPhpcsFixerCommand extends ContainerAwareCommandLib
{
    protected function configure()
    {
        $this->setName('mkk:debug:php-cs-fixer');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('PHP-CS-FIXER');
        $phpcsfixerBin = 'bin/php-cs-fixer';
        if (! is_file($phpcsfixerBin)) {
            $output->writeln('PHP-CS-FIXER non installé');

            return false;
        }

        $folders = glob('src/*');
        foreach ($folders as $folder) {
            $output->writeln('Traitement du dossier ' . $folder);
            $command = 'php ' . $phpcsfixerBin . ' fix --config=src/Mkk/SiteBundle/rules/.php_cs.dist ';
            $command = $command . $folder . ' --path-mode=override --using-cache=no --no-interaction';
            exec($command);
            if (file_exists($folder . '/.php_cs.cache')) {
                unlink($folder . '/.php_cs.cache');
            }
        }
        unset($input);
    }
}
