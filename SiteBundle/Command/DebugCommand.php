<?php

namespace Mkk\SiteBundle\Command;

use Mkk\SiteBundle\Lib\ContainerAwareCommandLib;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DebugCommand extends ContainerAwareCommandLib
{
    protected function configure()
    {
        $this->setName('mkk:debug');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Débug');
        $tab = [
            'mkk:debug:php-cs-fixer',
            'mkk:debug:phpmd',
            'mkk:debug:phpcpd',
            'mkk:debug:phpcs',
        ];
        foreach ($tab as $code) {
            $this->executeCommand($code, $input, $output);
        }
        $output->writeln('Fin Débug');
    }
}
