<?php

namespace Mkk\SiteBundle\Command;

use Mkk\SiteBundle\Lib\ContainerAwareCommandLib;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImageReductCommand extends ContainerAwareCommandLib
{
    protected function configure()
    {
        $this->setName('mkk:image:reduct');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $tab = [
            'mkk:image:reduct-evenementvignette',
        ];
        foreach ($tab as $code) {
            $this->executeCommand($code, $input, $output);
        }
    }
}
