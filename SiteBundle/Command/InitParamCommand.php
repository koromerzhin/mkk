<?php

namespace Mkk\SiteBundle\Command;

use Mkk\SiteBundle\Command\Init\NafTrait;
use Mkk\SiteBundle\Lib\ContainerAwareCommandLib;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InitParamCommand extends ContainerAwareCommandLib
{
    use NafTrait;

    protected function configure()
    {
        $this->setName('mkk:param:init');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Initialisation des paramètres');
        $this->setDeleteParamModuleListing();
        $this->setParam();
        $this->setAddParamModuleListing();
        $this->setParamUpload();
        $container   = $this->getContainer();
        $param       = $container->get('mkk.param_service');
        $data        = $param->listing();
        $siteService = $container->get('mkk.site_service');
        $options     = [];
        $forms       = $siteService->getFormSystemParam('param', [], $options);
        foreach ($forms as $nom => $form) {
            foreach ($form as $id => $input) {
                if (! isset($data[$id])) {
                    $val        = '';
                    $config     = $input->getConfig();
                    $name       = $config->getType()->getName();
                    $attributes = $config->getAttributes();
                    if (isset($attributes['data_collector/passed_options'])) {
                        if (isset($attributes['data_collector/passed_options']['multiple'])) {
                            $val = [];
                        }
                    }

                    $param->save($id, $val);
                }
            }
        }
    }

    private function setParam()
    {
        $container       = $this->getContainer();
        $paramManager    = $container->get('bdd.param_manager');
        $paramRepository = $paramManager->getRepository();
        $paramEntity     = $paramManager->getTable();
        $fichier         = 'web/fichiers/data/param.json';
        if (is_file($fichier)) {
            $data = json_decode(file_get_contents($fichier), true);
            foreach ($data as $code => $val) {
                $entity = $paramRepository->findOneByCode($code);
                if (! $entity) {
                    $entity = new $paramEntity();
                    $entity->setCode($code);
                    $entity->setValeur($val);
                    $paramManager->persistAndFlush($entity);
                }
            }
            unset($fichier);
        }

        $publicliste = $paramRepository->findOneByCode('publicliste');
        if (! $publicliste) {
            $publicliste = new $paramEntity();
            $publicliste->setCode('publicliste');
            $publicliste->setValeur(50);
            $paramManager->persistAndFlush($publicliste);
        }

        $longueurliste = $paramRepository->findOneByCode('longueurliste');
        if (! $longueurliste) {
            $longueurliste = new $paramEntity();
            $longueurliste->setCode('longueurliste');
            $longueurliste->setValeur(50);
            $paramManager->persistAndFlush($longueurliste);
        }
        $this->ParamCivilite($paramEntity, $paramRepository, $paramManager);
        $this->ParamEtablissement($paramEntity, $paramRepository, $paramManager);

        $dataTelephone = [
            'code' => 'Personnel',
            'code' => 'Professionnel',
        ];
        $this->paramTags($dataTelephone, 'tags_telephone', $paramEntity, $paramRepository, $paramManager);
        $this->paramTags($dataTelephone, 'tags_telephone_etablissement', $paramEntity, $paramRepository, $paramManager);
        $this->paramTags($dataTelephone, 'tags_telephone_evenement', $paramEntity, $paramRepository, $paramManager);
        $dataEmail = [
            'code' => 'Personnel',
            'code' => 'Professionnel',
        ];
        $this->paramTags($dataEmail, 'tags_email', $paramEntity, $paramRepository, $paramManager);
        $this->paramTags($dataEmail, 'tags_email_etablissement', $paramEntity, $paramRepository, $paramManager);
        $this->paramTags($dataEmail, 'tags_email_evenement', $paramEntity, $paramRepository, $paramManager);
        $dataAdresse = [
            'code' => 'Personnel',
            'code' => 'Professionnel',
        ];
        $this->paramTags($dataAdresse, 'tags_adresse', $paramEntity, $paramRepository, $paramManager);
        $this->paramTags($dataAdresse, 'tags_adresse_etablissement', $paramEntity, $paramRepository, $paramManager);
        $this->paramTags($dataAdresse, 'tags_adresse_evenement', $paramEntity, $paramRepository, $paramManager);
    }

    private function ParamCivilite($paramEntity, $paramRepository, $paramManager)
    {
        $tags    = $paramRepository->findOneByCode('civilite');
        $ajouter = 0;
        $data    = [
            ['code' => 'M.', 'nom' => 'Monsieur'],
            ['code' => 'Mme', 'nom' => 'Madame'],
        ];
        if (! $tags) {
            $tags    = new $paramEntity();
            $ajouter = 1;
            $tags->setCode('civilite');
        } else {
            $valeur = $tags->getValeur();
            if ($valeur == '[]' || $valeur == '') {
                $ajouter = 1;
            }
        }

        if ($ajouter == 1) {
            $tags->setValeur(json_encode($data));
            $paramManager->persistAndFlush($tags);
        }
    }

    private function setDeleteParamModuleListing()
    {
        $container    = $this->getContainer();
        $serviceParam = $container->get('mkk.param_service');
        $listing      = $serviceParam->listing();
        $delete       = [];
        foreach (array_keys($listing) as $code) {
            if (substr_count($code, '_listing') != 0 && $code != 'module_listing') {
                $delete[] = $code;
                $code     = str_replace('module_', '', $code);
                $code     = str_replace('_listing', '', $code);
            }
        }

        if (count($delete) != 0) {
            foreach ($delete as $code) {
                $serviceParam->delete($code);
            }
        }
    }

    private function ParamEtablissement($paramEntity, $paramRepository, $paramManager)
    {
        $tags    = $paramRepository->findOneByCode('type_etablissement');
        $data    = [];
        $ajouter = 0;
        if (! $tags) {
            $tags    = new $paramEntity();
            $ajouter = 1;
            $tags->setCode('type_etablissement');
        } else {
            $valeur = $tags->getValeur();
            if ($valeur == '[]' || $valeur == '') {
                $ajouter = 1;
            }
        }

        if ($ajouter == 1) {
            $tags->setValeur(json_encode($data));
            $paramManager->persistAndFlush($tags);
        }
    }

    private function paramTags($setData, $code, $paramEntity, $paramRepository, $paramManager)
    {
        $tags    = $paramRepository->findOneByCode($code);
        $ajouter = 0;
        if (! $tags) {
            $tags    = new $paramEntity();
            $ajouter = 1;
            $tags->setCode($code);
        } else {
            $valeur = $tags->getValeur();
            if ($valeur == '[]' || $valeur == '') {
                $ajouter = 1;
            } else {
                $setData = json_decode($valeur, true);
                if (count($setData) != 0 && ! isset($setData[0]['code'])) {
                    $newdata = [];
                    foreach ($setData as $val) {
                        if ($val != '') {
                            $newdata[]['code'] = $val;
                        }
                    }

                    $setData = $newdata;
                    $ajouter = 1;
                }
            }
        }

        if ($ajouter == 1) {
            $tags->setValeur(json_encode($setData));
            $paramManager->persistAndFlush($tags);
        }
    }

    private function setAddParamModuleListing()
    {
        $container    = $this->getContainer();
        $serviceParam = $container->get('mkk.param_service');
        $listing      = $serviceParam->listing();
        $module       = [];
        if (isset($listing['module_listing'])) {
            $module = $listing['module_listing'];
        }

        $routes     = [];
        $appManager = $container->get('mkk.app_manager');
        $listroutes = $appManager->getRoutes($container);
        foreach ($listroutes as $code => $tab) {
            list($module, $action) = explode('_', $code);
            unset($action);
            if (! isset($routes[$module])) {
                $routes[$module] = [
                    'module' => $module,
                    'val'    => 50,
                ];
            }
        }
        $newroutes = [];
        foreach ($routes as $tab) {
            $newroutes[] = $tab;
        }

        ksort($routes);
        $serviceParam->save('module_listing', $newroutes);
    }

    private function setParamUpload()
    {
        $container    = $this->getContainer();
        $serviceParam = $container->get('mkk.param_service');
        $listing      = $serviceParam->listing();
        $appManager   = $container->get('mkk.app_manager');
        $listroutes   = $appManager->getRoutes($container);
        $delete       = [];
        $upload       = [];
        if (isset($listing['upload']) && is_array($listing['upload'])) {
            foreach ($listing['upload'] as $code => $tab) {
                $upload[$code] = $tab;
            }
        }

        foreach ($listroutes as $code => $tab) {
            list($module, $action) = explode('_', $code);
            if (substr_count($action, 'upload') != 0) {
                if (! isset($upload[$code])) {
                    $upload[$code] = [
                        'url'              => $code,
                        'min_height'       => 0,
                        'max_height'       => 0,
                        'min_width'        => 0,
                        'max_width'        => 0,
                        'image-max_width'  => 0,
                        'image-max_height' => 0,
                    ];
                }
                foreach ($listing as $id => $val) {
                    if (substr_count($id, $code) != 0) {
                        $param                 = str_replace($code . '-', '', $id);
                        $upload[$code][$param] = $val;
                        $delete[]              = $id;
                    }
                }
            }
        }

        $newupload = [];
        foreach ($upload as $code => $tab) {
            $newupload[] = $tab;
        }

        foreach ($delete as $code) {
            $serviceParam->delete($code);
        }
        $listing = $serviceParam->listing();
        foreach ($listing as $id => $val) {
            if (substr_count($id, 'upload') != 0 && $id != 'upload') {
                $serviceParam->delete($id);
            }
        }

        $serviceParam->save('upload', $newupload);
    }
}
