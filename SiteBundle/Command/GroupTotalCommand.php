<?php

namespace Mkk\SiteBundle\Command;

use Mkk\SiteBundle\Lib\ContainerAwareCommandLib;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GroupTotalCommand extends ContainerAwareCommandLib
{
    protected function configure()
    {
        $this->setName('mkk:group:total');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Sauvegarde du nombre d'utilisateur par groupes");
        $container       = $this->getContainer();
        $mkkGroupManager = $container->get('mkk.group_manager');
        $mkkGroupManager->setTotalUser();
        unset($input);
    }
}
