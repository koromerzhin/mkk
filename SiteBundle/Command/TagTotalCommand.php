<?php

namespace Mkk\SiteBundle\Command;

use Mkk\SiteBundle\Lib\ContainerAwareCommandLib;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TagTotalCommand extends ContainerAwareCommandLib
{
    protected function configure()
    {
        $this->setName('mkk:tag:total');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container     = $this->getContainer();
        $mkkTagManager = $container->get('mkk.tag_manager');
        $output->writeln('Sauvegarde du nombre de blogs par tag');
        $mkkTagManager->setTotalBlogs();
        $output->writeln('Sauvegarde du nombre de bookmarks par tag');
        $mkkTagManager->setTotalBookmarks();
        unset($input);
    }
}
