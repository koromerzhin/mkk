<?php

namespace Mkk\SiteBundle\Command;

use Mkk\SiteBundle\Lib\ContainerAwareCommandLib;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DroitInitCommand extends ContainerAwareCommandLib
{
    protected function configure()
    {
        $this->setName('mkk:droit:init');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Sauvegarde des droits d'utilisateurs");
        $container       = $this->getContainer();
        $groupManager    = $container->get('bdd.group_manager');
        $appManager      = $container->get('mkk.app_manager');
        $listroutes      = $appManager->getRoutes($container);
        $routes          = $this->generateRoute($listroutes);
        $groupRepository = $groupManager->getRepository();
        $groups          = $groupRepository->findby([], ['nom' => 'ASC']);
        $this->saveNewRoute($routes, $groups);
        unset($input);
    }

    /**
     * Sauvegarde les nouvelles routes.
     *
     * @param array $routes liste des routes
     * @param array $groups liste des groups
     *
     * @return bool
     */
    private function saveNewRoute($routes, $groups)
    {
        $container        = $this->getContainer();
        $actionManager    = $container->get('bdd.action_manager');
        $newaction        = $actionManager->getTable();
        $actionRepository = $actionManager->getRepository();
        $traitement       = 0;
        $batchSize        = 20;
        $countI           = 0;
        foreach ($routes as $tab) {
            foreach ($groups as $group) {
                $action = $actionRepository->findBy(
                    [
                        'module'   => $tab['module'],
                        'refgroup' => $group->getId(),
                    ]
                );
                if (count($action) == 0) {
                    $action = new $newaction();
                    $action->setModule($tab['module']);
                    $action->setCodes(implode(',', $tab['action']));
                    $action->setRefGroup($group);
                    $actionManager->persist($action);
                    ++$countI;
                    if (($countI % $batchSize) == 0) {
                        $actionManager->flush();
                    }

                    $traitement = 1;
                }
            }
        }

        $actionManager->flush();

        return $traitement;
    }
}
