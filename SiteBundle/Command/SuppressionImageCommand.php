<?php

namespace Mkk\SiteBundle\Command;

use Mkk\SiteBundle\Lib\ContainerAwareCommandLib;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class non terminé.
 */
class SuppressionImageCommand extends ContainerAwareCommandLib
{
    protected function configure()
    {
        $this->setName('mkk:image:supp');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Suppression des images inutiles sur le serveur');
        // $folders = glob('web/fichiers/*');
        // foreach ($folders as $row) {
        //     if (is_dir($row)) {
        //         $entity = $row;
        //         $this->traitementEntity($entity);
        //     }
        // }
        unset($input);
    }

    private function traitementEntity($entity)
    {
        $folders = glob($entity . '/*');
        foreach ($folders as $row) {
            if (is_dir($row)) {
                $field = $row;
                $this->traitementField($entity, $field);
            }
        }
    }

    private function traitementField($entity, $field)
    {
        $folders   = glob($field . '/*');
        $champs    = ucfirst(str_replace($entity . '/', '', $field));
        $entite    = str_replace('web/fichiers/', '', $entity);
        $container = $this->getContainer();
        $code      = 'bdd.' . $entite . '_manager';
        if (! $container->has($code)) {
            return false;
        }

        $entityManager    = $container->get($code);
        $entityTable      = $entityManager->getTable();
        $entityRepository = $entityManager->getRepository();
        if (! $entityRepository) {
            return false;
        }

        $entity  = new $entityTable();
        $methods = get_class_methods($entity);
        $table   = $entityManager->getNameSpace() . 'SiteBundle:' . ucfirst($entite);
        if (! in_array('set' . $champs, $methods)) {
            return false;
        }

        $champs = strtolower($champs);
        foreach ($folders as $file) {
            if (is_file($file)) {
                $this->champsVerifier($file, $table, $champs)();
            }
        }
    }

    private function champsVerifier($file, $table, $champs)
    {
        $container = $this->getContainer();
        $em        = $container->get('doctrine')->getManager();
        $fichier   = str_replace('web/', '', $file);
        $params    = [
            'file'  => '%' . $fichier . '%',
            'file2' => '%' . json_encode($fichier) . '%',
        ];

        $dql    = 'SELECT p FROM ' . $table . ' p WHERE p.' . $champs . ' LIKE :file OR p.' . $champs . ' LIKE :file2';
        $query  = $em->createQuery($dql);
        $params = $query->setParameters($params);
        $result = $params->getResult();
        if (count($result) == 0) {
            //unlink($file);
        }
    }
}
