<?php

namespace Mkk\SiteBundle\Command;

use Mkk\SiteBundle\Lib\ContainerAwareCommandLib;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImageChangerCommand extends ContainerAwareCommandLib
{
    protected function configure()
    {
        $this->setName('mkk:image:changer');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Modification des noms des fichiers');
        $container  = $this->getContainer();
        $appManager = $container->get('mkk.app_manager');
        $routes     = $appManager->getRoutes($container);
        foreach (array_keys($routes) as $route) {
            echo $route . "\n";
        }
        unset($input);
    }
}
