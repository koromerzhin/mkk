<?php

namespace Mkk\SiteBundle\Command;

use Mkk\SiteBundle\Lib\ContainerAwareCommandLib;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OptimizeAssetsCommand extends ContainerAwareCommandLib
{
    protected function configure()
    {
        $this->setName('mkk:optimize:assets');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('optimization assets');
        $application = $this->getApplication();
        $command     = $application->find('assets:install');
        $arguments   = [
            'command'    => 'assets:install',
            '--symlink'  => true,
        ];

        $greetInput = new ArrayInput($arguments);
        $command->run($greetInput, $output);
        copy('src/Mkk/SiteBundle/Data/.htaccess', 'web/bundles/.htaccess');
        copy('src/Mkk/SiteBundle/Data/.htaccess', 'web/scripts/.htaccess');
        unset($input);
    }
}
