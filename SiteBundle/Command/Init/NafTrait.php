<?php

namespace Mkk\SiteBundle\Command\Init;

trait NafTrait
{
    public function recuperDataXls($output, $fichier, $ligne)
    {
        $container = $this->getContainer();
        $phpexcel  = $container->get('phpexcel');
        $data      = [];
        $fichier   = 'src/Mkk/SiteBundle/Data/naf/' . $fichier . '.xls';
        if (is_file($fichier)) {
            $phpExcelObject = $phpexcel->createPHPExcelObject($fichier);
            $sheet          = $phpExcelObject->getActiveSheet();
            $output->writeln($fichier);
            foreach ($sheet->getRowIterator() as $i => $row) {
                if ($i > $ligne) {
                    $tab = [];
                    foreach ($row->getCellIterator() as $cell) {
                        $val   = $cell->getValue();
                        $tab[] = $val;
                    }
                    $data[] = $tab;
                }
            }
        }

        return $data;
    }

    private function setInitNaf($output)
    {
        $this->setNafSection($output);
        $this->setNafDivision($output);
        $this->setNafGroupe($output);
        $this->setNafClasse($output);
        $this->setNafSousClasse($output);
        $this->setNaf($output);
    }

    private function remplirNafDefault($data, $nom)
    {
        $container  = $this->getContainer();
        $manager    = $container->get('bdd.' . $nom . '_manager');
        $repository = $manager->getRepository();
        $all        = $repository->findAll();
        if (count($all) != count($data)) {
            foreach ($data as $row) {
                if ($row[0] != '') {
                    $entity = $repository->findOneBy(['code' => $row[0]]);
                    if (! $entity) {
                        $table  = $manager->getTable();
                        $entity = new $table();
                        $entity->setCode($row[0]);
                        $entity->setLibelle($row[1]);
                        $manager->persistAndFlush($entity);
                    }
                }
            }
        }
    }

    private function setNafSection($output)
    {
        $fichier = 'naf2008_liste_n1';
        $data    = $this->recuperDataXls($output, $fichier, 3);
        $this->remplirNafDefault($data, 'nafsection');
    }

    private function setNafDivision($output)
    {
        $fichier = 'naf2008_liste_n2';
        $data    = $this->recuperDataXls($output, $fichier, 3);
        $this->remplirNafDefault($data, 'nafdivision');
    }

    private function setNafGroupe($output)
    {
        $fichier = 'naf2008_liste_n3';
        $data    = $this->recuperDataXls($output, $fichier, 3);
        $this->remplirNafDefault($data, 'nafgroupe');
    }

    private function setNafClasse($output)
    {
        $fichier = 'naf2008_liste_n4';
        $data    = $this->recuperDataXls($output, $fichier, 3);
        $this->remplirNafDefault($data, 'nafclasse');
    }

    private function setNafSousClasse($output)
    {
        $fichier = 'naf2008_liste_n5';
        $data    = $this->recuperDataXls($output, $fichier, 3);
        $this->remplirNafDefault($data, 'nafsousclasse');
    }

    private function setNaf($output)
    {
        $fichier = 'naf2008_5_niveaux';
        $data    = $this->recuperDataXls($output, $fichier, 1);
    }
}
