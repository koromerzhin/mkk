<?php

namespace Mkk\SiteBundle\Command;

use Mkk\SiteBundle\Lib\ContainerAwareCommandLib;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MenuConfigCommand extends ContainerAwareCommandLib
{
    protected function configure()
    {
        $this->setName('mkk:menu:config');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Configuration du menu admin');
        $container      = $this->getContainer();
        $menuManager    = $container->get('bdd.menu_manager');
        $menuRepository = $menuManager->getRepository();
        $menuEntity     = $menuManager->getTable();
        $aside          = $menuRepository->findOneByClef('aside');
        if (! $aside) {
            $aside = new $menuEntity();
            $aside->setClef('aside');
            $menuManager->persistAndFlush($aside);
        }

        $this->verifierContenuMoncompte($aside);

        $menus = $menuRepository->findByUrl('bestats_index');
        foreach ($menus as $menu) {
            $menuManager->remove($menu);
        }

        $menus = $menuRepository->findByClef('adminaccueil');
        foreach ($menus as $menu) {
            $menuManager->remove($menu);
        }

        $menus = $menuRepository->findByClef('moncomptemenu');
        foreach ($menus as $menu) {
            $menuManager->remove($menu);
        }

        $menus = $menuRepository->findBySeparateur(1);
        foreach ($menus as $menu) {
            $menuManager->remove($menu);
        }

        $menuManager->flush();

        unset($input);
    }

    private function verifierContenuMoncompte($moncompte)
    {
        $container   = $this->getContainer();
        $appManager  = $container->get('mkk.app_manager');
        $routes      = $appManager->getRoutes($container);
        $tab         = [];

        $tab['configuration'] = [
            'libelle'  => 'Configuration',
            'position' => count($tab),
            'child'    => [
                'param' => [
                    'libelle'  => 'Paramètres',
                    'url'      => 'beparam_index',
                    'position' => 0,
                ],
                'fichiers' => [
                    'libelle'  => 'Gestion des fichiers',
                    'url'      => 'befichier_index',
                    'position' => 1,
                ],
                'separateurconfig1' => [
                    'separateur' => 1,
                    'position'   => 2,
                ],
                'templates' => [
                    'libelle'  => 'Templates',
                    'url'      => 'betemplates_index',
                    'position' => 3,
                ],
                'page' => [
                    'libelle'  => 'Pages publiques',
                    'url'      => 'bepage_index',
                    'position' => 4,
                ],
                'group' => [
                    'libelle'  => 'Groupe utilisateurs',
                    'url'      => 'begroup_index',
                    'position' => 5,
                ],
                'user' => [
                    'libelle'  => 'Utilisateurs',
                    'url'      => 'beuser_index',
                    'position' => 6,
                ],
                'droit' => [
                    'libelle'  => 'Droits utilisateurs',
                    'url'      => 'bedroit_index',
                    'position' => 7,
                ],
                'metariane' => [
                    'libelle'  => "Metatags /  fil d'ariane",
                    'url'      => 'bemetariane_index',
                    'position' => 8,
                ],
                'menu' => [
                    'libelle'  => 'Menus',
                    'url'      => 'bemenu_index',
                    'position' => 9,
                ],
                'separateurconfig2' => [
                    'separateur' => 1,
                    'position'   => 10,
                ],
            ],
        ];
        foreach ($routes as $url => $lien) {
            if ($url == 'public_index') {
                $tab['siteaccueil'] = [
                    'libelle'  => 'Accueil',
                    'url'      => $url,
                    'position' => count($tab),
                    'cible'    => '_blank',
                ];
                break;
            }
        }

        $tab['bugs']          = [
            'libelle'  => 'Bugs',
            'url'      => 'bebugs_index',
            'position' => count($tab),
            'cible'    => '_blank',
        ];

        $childs = $moncompte->getChild();
        $this->traitementVerifContenuMenu($tab, $childs, $moncompte);
    }

    private function traitementVerifContenuMenu($tab, $childs, $parent)
    {
        $container   = $this->getContainer();
        $menuManager = $container->get('bdd.menu_manager');
        $newmenu     = $menuManager->getTable();
        foreach ($tab as $code => $verifmenu) {
            $trouver = 0;
            if (count($childs) != 0) {
                foreach ($childs as $child) {
                    if ($child->getClef() == $code) {
                        $menu    = $child;
                        $trouver = 1;
                        break;
                    } elseif (isset($verifmenu['url']) && $child->getUrl() == $verifmenu['url']) {
                        $menu    = $child;
                        $trouver = 1;
                        break;
                    }
                }
            }

            $modifier = 0;
            if ($trouver == 0) {
                $menu = new $newmenu();
                $menu->setRefMenu($parent);
                $menu->setClef($code);
                $modifier = 1;
            }

            $libelle = $menu->getLibelle();
            if ($libelle == '' && isset($verifmenu['libelle'])) {
                $menu->setLibelle($verifmenu['libelle']);
                $modifier = 1;
            }

            $url = $menu->getUrl();
            if ($url == '' && isset($verifmenu['url'])) {
                $menu->setUrl($verifmenu['url']);
                $modifier = 1;
            }

            $position = $menu->getPosition();
            if ($position == '' && isset($verifmenu['position'])) {
                $menu->setPosition($verifmenu['position']);
                $modifier = 1;
            }

            $cible = $menu->getCible();
            if ($cible == '' && isset($verifmenu['cible'])) {
                $menu->setCible($verifmenu['cible']);
                $modifier = 1;
            }

            $data = $menu->getData();
            if (isset($verifmenu['ariane']) && (! isset($data['ariane']) || $data['ariane'] == '')) {
                $data['ariane'] = $verifmenu['ariane'];
                $modifier       = 1;
                $menu->setData($data);
            }

            if (isset($verifmenu['separateur'])) {
                $menu->setSeparateur(1);
            }

            if ($modifier) {
                $menuManager->persistAndFlush($menu);
            }

            if (isset($verifmenu['child'])) {
                $this->traitementVerifContenuMenu($verifmenu['child'], $menu->getChild(), $menu);
            }
        }
    }
}
