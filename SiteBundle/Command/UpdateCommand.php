<?php

namespace Mkk\SiteBundle\Command;

use Mkk\SiteBundle\Lib\ContainerAwareCommandLib;
use Symfony\Component\Console\Descriptor\ApplicationDescription;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateCommand extends ContainerAwareCommandLib
{
    protected function configure()
    {
        $this->setName('mkk:update');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $application = $this->getApplication();
        $output->writeln('Update');
        $tab = [
            'mkk:param:init',
            'mkk:site:init',
            'mkk:group:total',
            'mkk:metariane:init',
            'mkk:droit:init',
            'mkk:menu:config',
            'mkk:entity:init',
            'mkk:image:supp',
            'mkk:image:reduct',
            'mkk:correction:lien',
            'mkk:correction:telephone',
            'mkk:correction:adresse',
            'mkk:optimize:assets',
        ];

        $application = new ApplicationDescription($application);
        $namespaces  = $application->getNamespaces();
        foreach ($namespaces as $namespace) {
            foreach ($namespace['commands'] as $command) {
                $tabcommand = explode(':', $command);
                if (count($tabcommand) == 2 && $tabcommand[1] == 'update' && ! in_array($command, [$this->getName(), 'translation:update'])) {
                    $tab[] = $command;
                }
            }
        }

        foreach ($tab as $code) {
            $this->executeCommand($code, $input, $output);
        }
        $output->writeln('Fin Update MKK');
    }
}
