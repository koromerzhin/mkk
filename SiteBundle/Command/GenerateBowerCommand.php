<?php

namespace Mkk\SiteBundle\Command;

use Mkk\SiteBundle\Lib\ContainerAwareCommandLib;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class GenerateBowerCommand extends ContainerAwareCommandLib
{
    protected function configure()
    {
        $this->setName('mkk:generate:bower')

        ->setDefinition(
            [
            new InputOption(
                'auto',
                null,
                InputOption::VALUE_NONE
            ),
            ]
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $questionHelper = $this->getQuestionHelper();
        $pseudo         = '';
        $email          = '';
        $name           = '';
        $description    ='';
        $version        = '';
        $license        ='';
        $homepage       = '';
        $principal      = 'bower.json';
        if (file_exists($principal)) {
            $contents = file_get_contents($principal);
            $data     = json_decode($contents, true);
            if (isset($data['name'])) {
                $name = $data['name'];
            }
            if (isset($data['description'])) {
                $description = $data['description'];
            }
            if (isset($data['version'])) {
                $version = $data['version'];
            }
            if (isset($data['license'])) {
                $license = $data['license'];
            }
            if (isset($data['homepage'])) {
                $homepage = $data['homepage'];
            }
            if (isset($data['authors'][0])) {
                $auteur               = $data['authors'][0];
                list($pseudo, $email) = explode('<', $auteur);
                $pseudo               = trim($pseudo);
                $email                = str_replace('>', '', $email);
            }
        }
        if (! $input->getOption('auto')) {
            $questionHelper->writeSection($output, 'Génération du fichier bower.json');
            $question    = new Question($questionHelper->getQuestion('Pseudo', $pseudo), $pseudo);
            $pseudo      = $questionHelper->ask($input, $output, $question);
            $question    = new Question($questionHelper->getQuestion('Email', $email), $email);
            $email       = $questionHelper->ask($input, $output, $question);
            $question    = new Question($questionHelper->getQuestion("Nom  de l'application", $name), $name);
            $name        = $questionHelper->ask($input, $output, $question);
            $question    = new Question($questionHelper->getQuestion('Description', $description), $description);
            $description = $questionHelper->ask($input, $output, $question);
            $question    = new Question($questionHelper->getQuestion('Version', $version), $version);
            $version     = $questionHelper->ask($input, $output, $question);
            $question    = new Question($questionHelper->getQuestion('License', $license), $license);
            $license     = $questionHelper->ask($input, $output, $question);
            $question    = new Question($questionHelper->getQuestion('Homepage', $homepage), $homepage);
            $homepage    = $questionHelper->ask($input, $output, $question);
        }

        $this->getJsonBower($pseudo, $email, $name, $description, $version, $license, $homepage);
        $questionHelper->writeSection($output, 'Fin de génération, merci de lancer bower install');
    }

    private function getJsonBower($pseudo, $email, $name, $description, $version, $license, $homepage)
    {
        $principal = 'src/Mkk/SiteBundle/Data/default-bower.json';
        $auteur    = '';
        if ($pseudo != '' && $email != '') {
            $auteur = $pseudo . ' <' . $email . '>';
        }

        if (! file_exists($principal)) {
            return false;
        }

        $contents            = file_get_contents($principal);
        $data                = json_decode($contents, true);
        $data['name']        = $name;
        $data['version']     = $version;
        $data['description'] = $description;
        $data['license']     = $license;
        $trouver             = 0;
        foreach ($data['authors'] as $authors) {
            if ($authors == $auteur) {
                $trouver = 1;
                break;
            }
        }
        if ($trouver == 0 && $auteur != '') {
            $data['authors'][] = $auteur;
        }

        $folders          = glob('src/*');
        $data['homepage'] = strtolower($homepage);
        foreach ($folders as $folder) {
            $file = $folder . '/SiteBundle/Data/bower.json';
            if (is_file($file)) {
                $contents             = file_get_contents($file);
                $dependencies         = json_decode($contents, true);
                $data['dependencies'] = array_merge($dependencies, $data['dependencies']);
            }
        }
        ksort($data['dependencies']);
        $json = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        file_put_contents('bower.json', $json);
    }
}
