<?php

namespace Mkk\SiteBundle\Command;

use Mkk\SiteBundle\Lib\ContainerAwareCommandLib;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MetarianeInitCommand extends ContainerAwareCommandLib
{
    protected function configure()
    {
        $this->setName('mkk:metariane:init');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Initialisation des metatags / fil d'ariane");
        $container           = $this->getContainer();
        $metarianeData       = $container->get('mkk.metariane_data');
        $tabnotroute         = $metarianeData->getTab();
        $metarianeManager    = $container->get('bdd.metariane_manager');
        $newMetariane        = $metarianeManager->getTable();
        $metarianeRepository = $metarianeManager->getRepository();
        $metarianes          = $metarianeRepository->findall();
        $i                   = 0;
        $appManager          = $container->get('mkk.app_manager');
        $listroutes          = $appManager->getRoutes($container);
        foreach ($metarianes as $metariane) {
            $dataRoute = $metariane->getRoute();
            $trouver   = 0;
            if (substr_count($dataRoute, '_') == 0) {
                foreach (array_keys($listroutes) as $route) {
                    list($code, $page) = explode('_', $route);
                    if ($code == $dataRoute) {
                        $trouver = 1;
                        break;
                    }
                }
            } else {
                foreach (array_keys($listroutes) as $route) {
                    if ($route == $dataRoute) {
                        $trouver = 1;
                        break;
                    }
                }
            }

            if ($trouver == 0) {
                $metarianeManager->removeAndFlush($metariane);
            }
        }

        foreach ($tabnotroute as $route => $filariane) {
            $entity = $metarianeRepository->findoneby(['route' => $route]);
            if (! $entity) {
                $entity = new $newMetariane();
                $entity->setRoute($route);
            }

            $entity->setAriane(ucfirst($filariane));
            $metarianeManager->persistAndFlush($entity);
        }

        $i = 0;
        foreach (array_keys($listroutes) as $route) {
            list($code, $page) = explode('_', $route);

            $entity = $metarianeRepository->findoneby(['route' => $code]);
            if (! $entity) {
                $entity = new $newMetariane();
                $entity->setRoute($code);
                $metarianeManager->persistAndFlush($entity);
            }

            $entity        = $metarianeRepository->findoneby(['route' => $route]);
            $trouverjson   = substr_count($page, 'json');
            $trouverupload = substr_count($page, 'upload');
            $trouverpage   = substr_count($page, 'page');
            $trouverdelete = substr_count($page, 'delete');
            $trouver       = $trouverjson + $trouverupload + $trouverpage + $trouverdelete;
            if (! $entity && $trouver == 0) {
                $entity = new $newMetariane();
                $entity->setRoute($route);
                $metarianeManager->persistAndFlush($entity);
            } elseif ($entity && $trouver != 0) {
                $metarianeManager->removeAndFlush($entity);
            }
        }
        unset($input);
    }
}
