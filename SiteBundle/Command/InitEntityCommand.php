<?php

namespace Mkk\SiteBundle\Command;

use Mkk\SiteBundle\Command\Init\NafTrait;
use Mkk\SiteBundle\Lib\ContainerAwareCommandLib;
use Symfony\Component\Console\Descriptor\ApplicationDescription;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InitEntityCommand extends ContainerAwareCommandLib
{
    use NafTrait;

    protected function configure()
    {
        $this->setName('mkk:entity:init');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $application = $this->getApplication();
        $output->writeln('Initialisation des entités');
        $this->setInitNaf($output);
        $output->writeln('aa');
        $this->setGroup();
        $output->writeln('bb');
        //$this->setEnseigne();
        $output->writeln('cc');
        $this->setAction();
        $output->writeln('dd');
        $this->setCategorieBlog();
        $output->writeln('ee');
        $this->correctionMenu();
        $output->writeln('ff');
        //$this->correctionEtablissement();
        $output->writeln('gg');
        $this->correctionCategorie();
        $application = new ApplicationDescription($application);
        $namespaces  = $application->getNamespaces();
        $tab         = [];
        foreach ($namespaces as $namespace) {
            foreach ($namespace['commands'] as $command) {
                if (substr_count($command, 'entity:init') != 0 && $command != $this->getName()) {
                    $tab[] = $command;
                }
            }
        }

        foreach ($tab as $code) {
            $this->executeCommand($code, $input, $output);
        }
    }

    private function correctionMenu()
    {
        $container      = $this->getContainer();
        $menuManager    = $container->get('bdd.menu_manager');
        $menuRepository = $menuManager->getRepository();
        $menus          = $menuRepository->findAll();
        foreach ($menus as $menu) {
            $parent = $menu->getParent();
            if ($parent != null) {
                $menu->setRefMenu($parent);
                $menu->setParent(null);
                $menuManager->persistAndFlush($menu);
            }
        }

        $menus = $menuRepository->findBySeparateur(1);
        foreach ($menus as $menu) {
            $menuManager->removeAndFlush($menu);
        }
    }

    private function correctionEtablissement()
    {
        $container               = $this->getContainer();
        $etablissementManager    = $container->get('bdd.etablissement_manager');
        $etablissementRepository = $etablissementManager->getRepository();
        $etablissements          = $etablissementRepository->findBy(['refetablissement' => null]);
        foreach ($etablissements as $etablissement) {
            $parent = $etablissement->getParent();
            if ($parent != null) {
                $etablissement->setRefEtablissement($parent);
                $etablissementManager->persistAndFlush($etablissement);
            }
        }
    }

    private function correctionCategorie()
    {
        $container           = $this->getContainer();
        $categorieManager    = $container->get('bdd.categorie_manager');
        $categorieRepository = $categorieManager->getRepository();
        $categories          = $categorieRepository->findBy(['refcategorie' => null]);
        foreach ($categories as $categorie) {
            $parent = $categorie->getParent();
            if ($parent != null) {
                $categorie->setRefCategorie($parent);
                $categorieManager->persistAndFlush($categorie);
            }
        }
    }

    private function setCategorieBlog()
    {
        $container           = $this->getContainer();
        $categorieManager    = $container->get('bdd.categorie_manager');
        $categorieRepository = $categorieManager->getRepository();
        $categories          = $categorieRepository->findBy(['totalnbblog' => 0]);
        foreach ($categories as $categorie) {
            $totalnbblog = count($categorie->getBlogs());
            $categorie->setTotalnbblog($totalnbblog);
            $categorieManager->persist($categorie);
        }

        $categorieManager->flush();
    }

    private function getRoutes()
    {
        $container  = $this->getContainer();
        $listroutes = [];
        $router     = $container->get('router');
        foreach ($router->getRouteCollection()->all() as $name => $route) {
            $pattern = $route->getPath();
            foreach ($route->getDefaults() as $code => $val) {
                if ($code != '_controller') {
                    $pattern = str_replace('{' . $code . '}', $val, $pattern);
                }
            }

            if (substr($name, 0, 1) != '_' && substr_count($name, '_') != 0) {
                $listroutes[$name] = $pattern;
            }
        }

        return $listroutes;
    }

    private function setAction()
    {
        $routes = [];
        foreach (array_keys($this->getRoutes()) as $tab) {
            list($module, $action) = explode('_', $tab);
            if (! isset($routes[$module])) {
                $routes[$module] = '';
            } else {
                $routes[$module] = $routes[$module] . ',';
            }

            $routes[$module] = $routes[$module] . $action;
        }

        $this->supprimerAction($routes);
    }

    private function supprimerAction($routes)
    {
        $container        = $this->getContainer();
        $actionManager    = $container->get('bdd.action_manager');
        $actionRepository = $actionManager->getRepository();
        $actions          = $actionRepository->findall();
        $batchSize        = 20;
        $i                = 0;
        foreach ($actions as $action) {
            $module = $action->getModule();
            if (! isset($routes[$module])) {
                ++$i;
                $actionManager->remove($action);
                if (($i % $batchSize) == 0) {
                    $actionManager->flush();
                }
            }
        }

        $actionManager->flush();
    }

    private function setEnseigne()
    {
        $container               = $this->getContainer();
        $etablissementManager    = $container->get('bdd.etablissement_manager');
        $etablissementEntity     = $etablissementManager->getTable();
        $etablissementRepository = $etablissementManager->getRepository();
        $enseigne                = $etablissementRepository->findOneByType('enseigne');
        if (! $enseigne) {
            $enseigne = new $etablissementEntity();
            $enseigne->setType('enseigne');
            $etablissementManager->persistAndFlush($enseigne);
        }
    }

    private function setGroup()
    {
        $container       = $this->getContainer();
        $groupManager    = $container->get('bdd.group_manager');
        $groupRepository = $groupManager->getRepository();
        $groupEntity     = $groupManager->getTable();
        $visiteur        = $groupRepository->findOneByCode('visiteur');
        if (! $visiteur) {
            $group = new $groupEntity();
            $group->setCode('visiteur');
            $group->setNom('visiteur');
            $groupManager->persistAndFlush($group);
        }

        $superadmin = $groupRepository->findOneByCode('superadmin');
        if (! $superadmin) {
            $group = new $groupEntity();
            $group->setCode('superadmin');
            $group->setNom('superadmin');
            $groupManager->persistAndFlush($group);
        }
    }
}
