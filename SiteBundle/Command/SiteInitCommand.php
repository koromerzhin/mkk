<?php

namespace Mkk\SiteBundle\Command;

use Mkk\SiteBundle\Lib\ContainerAwareCommandLib;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SiteInitCommand extends ContainerAwareCommandLib
{
    protected function configure()
    {
        $this->setName('mkk:site:init');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Initialisation des dossiers');
        if (! is_dir('web/tmp')) {
            mkdir('web/tmp');
        }

        if (! is_dir('web/tmp/geonames')) {
            mkdir('web/tmp/geonames');
        }

        if (! is_dir('web/fichiers')) {
            mkdir('web/fichiers');
        }

        if (! is_dir('web/fichiers/mailer')) {
            mkdir('web/fichiers/mailer');
        }

        if (! is_dir('web/scripts')) {
            mkdir('web/scripts');
        }

        if (! is_dir('web/fichiers/user')) {
            mkdir('web/fichiers/user');
        }

        if (! is_dir('web/fichiers/user/avatar')) {
            mkdir('web/fichiers/user/avatar');
        }

        if (! is_dir('web/fichiers/geonames')) {
            $this->rrmdir('web/fichiers/geonames');
        }

        if (is_dir('web/fichiers/tmp')) {
            $this->rrmdir('fichiers/tmp');
        }

        if (is_dir('web/fichiers/thumbnail')) {
            $this->rrmdir('web/fichiers/thumbnail');
        }

        if (! is_dir('web/filemanager') && is_dir('../public/filemanager')) {
            mkdir('web/filemanager');
            $this->cpy(
                '../public/filemanager',
                'web/filemanager'
            );
        }

        if (is_dir('web/scripts/tinymce/')) {
            if (! is_dir('web/scripts/tinymce/plugins/responsivefilemanager')) {
                mkdir('web/scripts/tinymce/plugins/responsivefilemanager');
            }

            $dossierSource      = 'src/Mkk/SiteBundle/Resources/public/js/responsivefilemanager';
            $dossierDestination = 'web/scripts/tinymce/plugins/responsivefilemanager';
            if (is_dir($dossierSource)) {
                $this->cpy(
                    $dossierSource,
                    $dossierDestination
                );
            }

            if (! is_dir('web/scripts/tinymce/langs')) {
                mkdir('web/scripts/tinymce/langs');
                $dossierSource      = 'src/Mkk/SiteBundle/Resources/public/js/tinymcelangs';
                $dossierDestination = 'web/scripts/tinymce/langs';
                if (is_dir($dossierSource)) {
                    $this->cpy(
                        'src/Mkk/SiteBundle/Resources/public/js/tinymcelangs',
                        'web/scripts/tinymce/langs'
                    );
                }
            }
        }

        $container       = $this->getContainer();
        $paramManager    = $container->get('bdd.param_manager');
        $paramRepository = $paramManager->getRepository();
        $paramEntity     = $paramManager->getTable();
        $robots          = $paramRepository->findOneByCode('robotstxt');
        if (! $robots) {
            $robots = new $paramEntity();
            $robots->setCode('robotstxt');
            $valeur = file_get_contents('src/Mkk/SiteBundle/Data/robots.txt');
            $robots->setValeur($valeur);
            $paramManager->persistAndFlush($robots);
        } else {
            $valeur = $robots->getValeur();
        }

        file_put_contents('web/robots.txt', $valeur);

        $robots = $paramRepository->findOneByCode('crontab');
        if (! $robots) {
            $crontab = new $paramEntity();
            $crontab->setCode('crontab');
            $crontab->setValeur('');
            $paramManager->persistAndFlush($crontab);
        } else {
            $valeur = $robots->getValeur();
        }

        file_put_contents('web/crontab.sh', $valeur);

        $menuManager    = $container->get('mkk.menu_manager');
        $menuBddManager = $container->get('bdd.menu_manager');
        $menuRepository = $menuBddManager->getRepository();
        $menuList       = $menuRepository->findby(['parent' => null]);

        unset($input, $output);
    }

    private function cpy($source, $dest)
    {
        if (is_dir($source)) {
            $dirHandle = opendir($source);
            while ($file = readdir($dirHandle)) {
                if ($file != '.' && $file != '..') {
                    if (is_dir($source . '/' . $file)) {
                        if (! is_dir($dest . '/' . $file)) {
                            mkdir($dest . '/' . $file);
                        }

                        $this->cpy($source . '/' . $file, $dest . '/' . $file);
                    } else {
                        copy($source . '/' . $file, $dest . '/' . $file);
                    }
                }
            }

            closedir($dirHandle);
        } else {
            copy($source, $dest);
        }
    }

    private function rrmdir($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != '.' && $object != '..') {
                    if (filetype($dir . '/' . $object) == 'dir') {
                        $this->rrmdir($dir . '/' . $object);
                    } else {
                        unlink($dir . '/' . $object);
                    }
                }
            }

            reset($objects);
            rmdir($dir);
        }
    }
}
