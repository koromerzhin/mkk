<?php

namespace Mkk\SiteBundle\Data;

class MetarianeData
{
    public function getTab()
    {
        $tabnotroute = [
            'bedebug'      => 'Debug Application',
            'bemailer'     => 'Envoi de mail',
            'beedito'      => 'Editorial',
            'beapropos'    => 'A propos',
            'bebugs'       => 'Signalisation des bugs',
            'bedocument'   => 'Documents',
            'becontact'    => 'contacts',
            'bebackground' => 'backgrounds',
            'bedroit'      => 'droits des utilisateurs',
            'befichier'    => 'fichiers',
            'begroup'      => "groupes d'utilisateurs",
            'bemail'       => 'emails',
            'bemenu'       => 'menus',
            'bemetariane'  => "metatags / fils d'ariane",
            'bepartenaire' => 'Gestion des partenaires',
            'bemoncompte'  => 'Mon compte',
            'benewsletter' => 'newsletter',
            'beparam'      => 'paramètres du site',
            'besystem'     => 'paramètres du site',
            'besearch'     => '',
            'betemplates'  => 'templates email / S.M.S.',
            'bepage'       => 'pages',
            'beuser'       => 'utilisateurs',
            'bord'         => 'Tableau de bord',
            'beenseigne'   => "Gestion de l'enseigne",
            'script'       => '',
            'user'         => '',
        ];

        return $tabnotroute;
    }
}
