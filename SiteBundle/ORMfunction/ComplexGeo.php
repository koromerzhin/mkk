<?php

namespace Mkk\SiteBundle\ORMfunction;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

class ComplexGeo extends FunctionNode
{
    /**
     * @var \Doctrine\ORM\Query\AST\SimpleArithmeticExpression
     */
    private $latitude1;

    /**
     * @var \Doctrine\ORM\Query\AST\SimpleArithmeticExpression
     */
    private $longitude1;

    /**
     * @var \Doctrine\ORM\Query\AST\ArithmeticExpression
     */
    private $latitude2;

    /**
     * @var \Doctrine\ORM\Query\AST\ArithmeticExpression
     */
    private $longitude2;

    /**
     * Parse DQL Function.
     *
     * @param Parser $parser
     */
    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->latitude1 = $parser->SimpleArithmeticExpression();
        $parser->match(Lexer::T_COMMA);
        $this->longitude1 = $parser->SimpleArithmeticExpression();
        $parser->match(Lexer::T_COMMA);
        $this->latitude2 = $parser->ArithmeticExpression();
        $parser->match(Lexer::T_COMMA);
        $this->longitude2 = $parser->ArithmeticExpression();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    /**
     * get SQL.
     *
     * @param SqlWalker $sqlWalker
     *
     * @return string
     */
    public function getSql(SqlWalker $sqlWalker)
    {
        return sprintf('((ACOS(SIN(%s * PI() / 180) * SIN(%s * PI() / 180) + COS(%s * PI() / 180) * COS(%s * PI() / 180) * COS((%s - %s) * PI() / 180)) * 180 / PI()) * 60 * %s)', $this->latitude1->dispatch($sqlWalker), $this->latitude2->dispatch($sqlWalker), $this->latitude1->dispatch($sqlWalker), $this->latitude2->dispatch($sqlWalker), $this->longitude1->dispatch($sqlWalker), $this->longitude2->dispatch($sqlWalker), '1.1515 * 1.609344');
    }
}
