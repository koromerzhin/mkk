<?php

namespace Mkk\SiteBundle\ORMfunction;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * DateFunction ::= "HOUR" "(" ArithmeticPrimary ")".
 */
class Oldpassword extends FunctionNode
{
    public $stringExpression;

    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->stringExpression = $parser->StringPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(SqlWalker $sqlWalker)
    {
        return 'OLD_PASSWORD(' .
                $this->stringExpression->dispatch($sqlWalker) .
                ')';
    }
}
