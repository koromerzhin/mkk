<?php

namespace Mkk\SiteBundle\Listener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class RequestListener
{
    public function __construct(ContainerInterface $container, Session $session, \Twig_Environment $twig)
    {
        $this->session   = $session;
        $this->container = $container;
        $this->router    = $container->get('router');
        $this->twig      = $twig;
    }

    public function onKernelPageRequest(GetResponseEvent $event)
    {
        $route   = $event->getRequest()->attributes->get('_route');
        $routes  = $this->getRoutes();
        $request = $event->getRequest();
        if (substr_count($route, 'index') != 0) {
            $routeTest = str_replace('index', 'page', $route);
            if (isset($routes[$routeTest])) {
                $query    = $request->query->all();
                $args     = array_merge($query, ['page' => 1]);
                $url      = $this->router->generate($routeTest, $args);
                $redirect = new RedirectResponse($url);
                $event->setResponse($redirect);
            }
        }
    }

    public function onKernelDesactiverRequest(GetResponseEvent $event)
    {
        $route    = $event->getRequest()->attributes->get('_route');
        $param    = $this->container->get('mkk.param_service')->listing();
        $disable  = isset($param['desactivation_etat']) ? $param['desactivation_etat'] : 0;
        $tabroute = [
            'user_login',
            '',
            '_wdt',
            'bord_index',
        ];
        if (in_array($route, $tabroute)) {
            return false;
        }

        $tokenStorage         = $this->container->get('security.token_storage')->getToken();
        $authorizationChecker = $this->container->get('security.authorization_checker');
        if (null != $tokenStorage && $authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return false;
        }

        if ($route != 'script_disable' && $disable == 1) {
            $param = $this->container->get('mkk.param_service')->listing();
            $url   = $this->router->generate('script_disable');
            $event->setResponse(new RedirectResponse($url));
        } elseif ($route == 'script_disable' && $disable == 0) {
            $url = $this->router->generate('public_index');
        }

        if (isset($url)) {
            $redirect = new RedirectResponse($url);
            $event->setResponse($redirect);
        }
    }

    public function onKernelSiteRequest(GetResponseEvent $event)
    {
        $container            = $this->container;
        $groupManager         = $container->get('bdd.group_manager');
        $groupRepository      = $groupManager->getRepository();
        $visiteurEntity       = $groupRepository->findOneByCode('visiteur');
        $idgroup              = $visiteurEntity->getId();
        $tokenStorage         = $container->get('security.token_storage')->getToken();
        $authorizationChecker = $container->get('security.authorization_checker');
        if (null != $tokenStorage && $authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $user    = $tokenStorage->getUser();
            $idgroup = $user->getRefGroup()->getId();
            $this->twig->addGlobal('user', $user);
            if ($user->getPrenom() != '') {
                $textmoncomptemenu = $user->getPrenom();
                if ($user->getNom() != '') {
                    $textmoncomptemenu = $textmoncomptemenu . ' ' . substr($user->getNom(), 0, 1) . '.';
                }
            } elseif ($user->getSociete() != '') {
                $textmoncomptemenu = $user->getSociete();
            }

            if (isset($textmoncomptemenu)) {
                $this->twig->addGlobal('textmoncomptemenu', $textmoncomptemenu);
            }
        } else {
            $this->twig->addGlobal('user', null);
        }

        $this->twig->addGlobal('idgroup', $idgroup);
        $bundles = $container->getParameter('kernel.bundles');
        $dir     = '../';
        if (is_dir('web')) {
            $dir = '';
        }

        foreach ($bundles as $code => $bundle) {
            if (substr_count($code, 'SiteBundle') != 0) {
                $dossier                    = str_replace('\\', '/', $bundle);
                $dossier                    = substr($dossier, 0, strrpos($dossier, '/'));
                list($emplacement, $partie) = explode('/', $dossier);
                unset($partie);

                $fichier = $dir . 'src/' . $emplacement . '/SiteBundle/Resources/views/backend.html.twig';
                if (is_file($fichier)) {
                    $this->twig->addGlobal('dossiersite', $code);
                }
            }
        }

        $emplacementadmin = strtolower(BUNDLEU) . 'admin';
        $this->twig->addGlobal('emplacementadmin', $emplacementadmin);

        $payslangue = Intl::getRegionBundle()->getCountryNames('fr');
        $this->twig->addGlobal('payslangue', $payslangue);
        $manifesteFile = 'assets/manifest.json';
        $manifest      = [];
        if (is_file($manifesteFile)) {
            $manifest = json_decode(file_get_contents($manifesteFile), true);
        }
        $this->twig->addGlobal('manifest', $manifest);
        $languelangue = Intl::getLanguageBundle()->getLanguageNames('fr');
        $this->twig->addGlobal('languelangue', $languelangue);
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        if (is_file('favicon.png')) {
            $favicon = [
                'mime' => finfo_file($finfo, 'favicon.png'),
                'url'  => 'favicon.png',
            ];
        } elseif (is_file('favicon.ico')) {
            $favicon = [
                'mime' => finfo_file($finfo, 'favicon.ico'),
                'url'  => 'favicon.ico',
            ];
        }

        if (isset($favicon)) {
            $this->twig->addGlobal('favicon', $favicon);
        }

        $container = $this->container;
        $route     = $event->getRequest()->attributes->get('_route');

        $urltab      = [
            'user_login',
            'script_disable',
            'fos_user_resetting_check_email',
            'fos_user_resetting_reset',
        ];
        $generateurl = 'public_index';
        $this->twig->addGlobal('generateurl', $generateurl);
        if (in_array($route, $urltab)) {
            $tabfichiers = glob('fichiers/login/*.*');
            if ($tabfichiers != false) {
                foreach ($tabfichiers as $key => $filename) {
                    $tabaccueil[$key] = $filename;
                }

                $imgaccueil = array_rand($tabaccueil, 1);
                $imgaccueil = $this->router->generate($generateurl) . $tabaccueil[$imgaccueil];
                $this->twig->addGlobal('imgaccueil', $imgaccueil);
            }
        }

        $routes = $this->getRoutes();
        $this->twig->addGlobal('routesapps', $routes);
        $routeadresse = substr($route, 0, strrpos($route, '_')) . '_adresse';
        if (isset($routes[$routeadresse])) {
            $this->twig->addGlobal('urladresse', $routeadresse);
        }

        $routeparam = $event->getRequest()->attributes->get('_route_params');
        if (isset($routeparam['_locale'])) {
            $this->twig->addGlobal('_locale', $routeparam['_locale']);
        }

        $locale = $event->getRequest()->getLocale();
        $this->twig->addGlobal('languesite', $locale);

        $routeavatar = substr($route, 0, strrpos($route, '_')) . '_avatar';
        if (isset($routes[$routeavatar])) {
            $this->twig->addGlobal('urlavatar', $routeavatar);
        }

        list($module) = explode('_', $route);
        $this->twig->addGlobal('moduleactuel', $module);
        if (isset($routes[$route])) {
            $this->twig->addGlobal('moduleroute', $routes[$route]);
        }

        $locale = $event->getRequest()->getLocale();
        $this->twig->addGlobal('locale', $locale);
        $payslangue = Intl::getRegionBundle()->getCountryNames($locale);
        $this->twig->addGlobal('payslangue', $payslangue);
        $languagelangue = Intl::getLanguageBundle()->getLanguageNames($locale);
        $this->twig->addGlobal('languagelangue', $languagelangue);

        if (is_file('logobackend.png')) {
            $isimagelogobackend = 1;
        } else {
            $isimagelogobackend = 0;
        }

        $this->twig->addGlobal('isimagelogobackend', $isimagelogobackend);
        $etablissementManager    = $container->get('bdd.etablissement_manager');
        $etablissementRepository = $etablissementManager->getRepository();
        $enseigne                = $etablissementRepository->findoneBy(['type' => 'enseigne']);
        $this->twig->addGlobal('enseigne', $enseigne);
        $folder = '';
        if (is_dir('web')) {
            $folder = 'web/';
        }
        $this->twig->addGlobal('javaScriptsRequirejs', []);

        $folder = '../';
        if (is_dir('web')) {
            $folder = '';
        }

        $routes = $this->getRoutes();
        $this->twig->addGlobal('routessites', $routes);

        $params    = $this->container->get('mkk.param_service')->listing();
        $titresite = "Nous n'avons pas encore de titre";
        if (isset($params['meta_titre']) && $params['meta_titre'] != '') {
            $titresite = $params['meta_titre'];
        }

        $this->twig->addGlobal('titresite', $titresite);

        $formtagsclient = '';
        if (isset($params['tags_client'])) {
            $formtagsclient = $params['tags_client'];
            $formtagsclient = implode(',', $formtagsclient);
        }

        $this->twig->addGlobal('formtagsclient', $formtagsclient);

        if (isset($params['tags_telephone'])) {
            $this->twig->addGlobal('tagstelephone', $params['tags_telephone']);
        }

        if (isset($params['tags_email'])) {
            $this->twig->addGlobal('tagsemail', $params['tags_email']);
        }

        if (isset($params['tags_adresse'])) {
            $this->twig->addGlobal('tagsadresse', $params['tags_adresse']);
        }

        if (isset($params['type_etablissement'])) {
            $this->twig->addGlobal('type_etablissement', $params['type_etablissement']);
        }

        //
        // $formCommande = $formFactory->createBuilder(CommandeType::class);
        // $formCommande = $formCommande->getForm()->createView();
        // $this->twig->addGlobal('formcommande', $formCommande);
    }

    public function onKernelAdminRequest(GetResponseEvent $event)
    {
        $container            = $this->container;
        $route                = $event->getRequest()->attributes->get('_route');
        $param                = $container->get('mkk.param_service')->listing();
        $groupManager         = $container->get('bdd.group_manager');
        $groupRepository      = $groupManager->getRepository();
        $visiteurEntity       = $groupRepository->findOneByCode('visiteur');
        $idgroup              = $visiteurEntity->getId();
        $tokenStorage         = $container->get('security.token_storage')->getToken();
        $authorizationChecker = $container->get('security.authorization_checker');
        if (null != $tokenStorage && $authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $idgroup = $tokenStorage->getUser()->getRefGroup()->getId();
        }

        if ($route == 'bord_index' || substr($route, 0, 2) == 'be') {
            $request = $event->getRequest();
            $this->verifierMenuAdmin($request, $idgroup);
        }

        if ($route == 'bord_index' && null != $tokenStorage && $authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            if ($tokenStorage->getUser() != null && $tokenStorage->getUser() != 'anon.') {
                $user = $tokenStorage->getUser();
            }

            if (isset($param['messageaccueil'])) {
                $messageaccueil = $param['messageaccueil'];
                $messageaccueil = str_replace('%appel%', $user->getAppel(), $messageaccueil);
                $messageaccueil = str_replace('%date%', date('d/m/Y'), $messageaccueil);
                $this->twig->addGlobal('accueilmessage', $messageaccueil);
            }

            $noteinternelectureManager    = $container->get('bdd.noteinternelecture_manager');
            $noteinternelectureRepository = $noteinternelectureManager->getRepository();
            $noteinternes                 = $noteinternelectureRepository->getNew($user);
            $this->twig->addGlobal('noteinternes', $noteinternes);
        }
    }

    public function onKernelPaginationRequest(GetResponseEvent $event)
    {
        $route      = $event->getRequest()->attributes->get('_route');
        $param      = $this->container->get('mkk.param_service')->listing();
        $controller = $event->getRequest()->attributes->get('_controller');
        if (! defined('LONGUEURLISTE')) {
            if (substr_count($controller, 'PublicBundle') != 0) {
                if (isset($param['publicliste'])) {
                    define('LONGUEURLISTE', $param['publicliste']);
                } else {
                    define('LONGUEURLISTE', 10);
                }
            } elseif ($route != '') {
                list($module, $routing) = explode('_', $route);
                unset($routing);
                if (isset($param['module_listing'])) {
                    foreach ($param['module_listing'] as $tab) {
                        if ($tab['module'] == $module) {
                            define('PAGELONGUEURLISTE', $tab['val']);
                        }
                    }
                }

                if (isset($param['longueurliste'])) {
                    define('LONGUEURLISTE', $param['longueurliste']);
                } else {
                    define('LONGUEURLISTE', 50);
                }
            }
        }
    }

    public function onKernelRoutingRequest(GetResponseEvent $event)
    {
        $viderVerif     = $this->verifUrlVider($event);
        $jsonVerif      = $this->verifUrlJson($event);
        $searchVerif    = $this->verifUrlSearchUpload($event);
        $searchVerif    = $this->verifUrlDatabase($event);
        $searchNonDroit = $this->verifUrlNonDroit($event);
        if ($viderVerif) {
            $text = "Le serveur a compris la requête, mais refuse de l'exécuter.";
            $text = $text . " Contrairement à l'erreur 401, s'authentifier ne fera aucune différence.";
            $text = $text . " Sur les serveurs où l'authentification est requise,";
            $text = $text . " cela signifie généralement que l'authentification a été acceptée mais";
            $text = $text . " que les droits d'accès ne permettent pas au client d'accéder à la ressource";
            throw new HttpException(403, $text);
        }

        if ($jsonVerif || $searchVerif || $searchVerif || $searchNonDroit) {
            return false;
        }

        $route = $event->getRequest()->attributes->get('_route');
        if ($route != '') {
            $error = $this->verifDroit($event);
            if ($error == 401) {
                $request = $event->getRequest();
                if ($request->isXmlHttpRequest()) {
                    throw new HttpException(401, 'Une authentification est nécessaire pour accéder à la ressource');
                }

                throw new AccessDeniedException('no acces');
            } elseif ($error == 403) {
                $text = "Le serveur a compris la requête, mais refuse de l'exécuter.";
                $text = $text . " Contrairement à l'erreur 401, s'authentifier ne fera aucune différence.";
                $text = $text . " Sur les serveurs où l'authentification est requise,";
                $text = $text . " cela signifie généralement que l'authentification a été acceptée mais";
                $text = $text . " que les droits d'accès ne permettent pas au client d'accéder à la ressource";
                throw new HttpException(403, $text);
            }
        }
    }

    private function getIndexSite()
    {
        $route       = $this->getRoutes();
        $generateurl = 'bord_index';
        $trouver     = 0;
        foreach (array_keys($route) as $url) {
            if ($url == 'public_index') {
                $trouver     = 1;
                $generateurl = $url;
                break;
            }
        }

        return $generateurl;
    }

    /**
     * Verifie les Droits utilisateurs suivant la page.
     *
     * @param mixed $event
     */
    private function verifDroit($event)
    {
        $container            = $this->container;
        $actionManager        = $container->get('bdd.action_manager');
        $actionRepository     = $actionManager->getRepository();
        $tokenStorage         = $container->get('security.token_storage')->getToken();
        $authorizationChecker = $container->get('security.authorization_checker');
        $groupManager         = $container->get('bdd.group_manager');
        $groupRepository      = $groupManager->getRepository();
        $superadminEntity     = $groupRepository->findOneByCode('superadmin');
        $superadmin           = $superadminEntity->getId();
        $visiteurEntity       = $groupRepository->findOneByCode('visiteur');
        $visiteur             = $visiteurEntity->getId();
        $group                = $visiteur;
        if (null != $tokenStorage && $authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $user  = $tokenStorage->getUser();
            $group = $user->getRefGroup()->getId();
        }

        $route                 = $event->getRequest()->attributes->get('_route');
        list($module, $action) = explode('_', $route);
        $actiongroup           = $actionRepository->getActionGroup($group);
        $annuler               = 0;
        if (substr($route, 0, 2) == 'be' || $route == 'bord_index' || $route == 'script_tinymce') {
            if ($group == $visiteur) {
                $annuler = 1;
            } elseif ($group != $superadmin && $route != 'beapropos_index') {
                if (! isset($actiongroup[$module])) {
                    $annuler = 1;
                } else {
                    $codes   = explode(',', $actiongroup[$module]);
                    $annuler = 1;
                    foreach ($codes as $code) {
                        if ($code == $action) {
                            $annuler = 0;
                            break;
                        }
                    }
                }
            }
        } else {
            if (! isset($actiongroup[$module])) {
                $annuler = 1;
            } else {
                $codes   = explode(',', $actiongroup[$module]);
                $annuler = 1;
                foreach ($codes as $code) {
                    if ($code == $action) {
                        $annuler = 0;
                        break;
                    }
                }
            }
        }

        $error = '';
        if ($annuler == 1) {
            if (null != $tokenStorage && $authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
                $error = 403;
            } else {
                $error = 401;
            }
        }

        return $error;
    }

    /**
     * Pour ne pas checker des liens inutiles.
     *
     * @param mixed $event
     */
    private function verifUrlNonDroit($event)
    {
        $container         = $this->container;
        $route             = $event->getRequest()->attributes->get('_route');
        $retour            = 0;
        $urlanepasverifier = [
            'script_image',
            'user_login',
            'user_check',
            'beimport_set',
        ];
        $kernel            = $container->get('kernel');
        $urlanepasverifier = in_array($route, $urlanepasverifier);
        $urlanepasverifier = ($urlanepasverifier || substr_count($route, 'fos_') != 0);
        $urlanepasverifier = ($urlanepasverifier || substr_count($route, 'script_') != 0);
        if ($kernel->isDebug()) {
            $urlanepasverifier = ($urlanepasverifier || substr($route, 0, 1) == '_');
        }
        if ($urlanepasverifier) {
            $retour = 1;
        }

        return $retour;
    }

    /**
     * Les url vider sont accessible uniquement aux supradmin.
     *
     * @param mixed $event
     */
    private function verifUrlVider($event)
    {
        $container            = $this->container;
        $route                = $event->getRequest()->attributes->get('_route');
        $retour               = 0;
        $tokenStorage         = $container->get('security.token_storage')->getToken();
        $authorizationChecker = $container->get('security.authorization_checker');
        if (substr_count($route, 'vider') && null != $tokenStorage && $authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $groupManager    = $container->get('bdd.group_manager');
            $groupRepository = $groupManager->getRepository();
            $superadmin      = $groupRepository->findOneByCode('superadmin');
            $user            = $tokenStorage->getUser();
            $group           = $user->getRefGroup()->getId();
            if ($group != $superadmin->getId()) {
                $retour = 1;
            }
        }

        return $retour;
    }

    /**
     * Verification des liens search et upload.
     *
     * @param
     * @param mixed $event
     *
     * @author
     * @copyright
     */
    private function verifUrlSearchUpload($event)
    {
        $container            = $this->container;
        $route                = $event->getRequest()->attributes->get('_route');
        $groupManager         = $container->get('bdd.group_manager');
        $groupRepository      = $groupManager->getRepository();
        $visiteur             = $groupRepository->findOneByCode('visiteur');
        $retour               = 0;
        $urlsearch1           = substr_count($route, 'besearch_');
        $urlsearch2           = substr_count($route, 'search');
        $urlupload            = substr_count($route, 'upload');
        $tokenStorage         = $container->get('security.token_storage')->getToken();
        $authorizationChecker = $container->get('security.authorization_checker');
        $verifsearch          = ($urlsearch1 == 1 || $urlsearch2 == 1 || $urlupload == 1);
        if ($verifsearch && null != $tokenStorage && $authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $user  = $tokenStorage->getUser();
            $group = $user->getRefGroup()->getId();
            if ($group != $visiteur->getId()) {
                $retour = 1;
            }
        }

        if ($urlsearch2 == 1) {
            $retour = 1;
        }

        return $retour;
    }

    /**
     * Lien database à ne pas vérifier.
     *
     * @param mixed $event
     */
    private function verifUrlDatabase($event)
    {
        $route  = $event->getRequest()->attributes->get('_route');
        $retour = 0;
        if (substr_count($route, 'database') == 1) {
            $retour = 1;
        }

        return $retour;
    }

    /**
     * Liens json à ne pas vérifier.
     *
     * @param mixed $event
     */
    private function verifUrlJson($event)
    {
        $route  = $event->getRequest()->attributes->get('_route');
        $retour = 0;
        if (substr_count($route, 'json') == 1) {
            $retour = 1;
        }

        return $retour;
    }

    private function getRoutes()
    {
        $listroutes = [];
        $router     = $this->router;
        foreach ($router->getRouteCollection()->all() as $name => $route) {
            $pattern = $route->getPath();
            foreach ($route->getDefaults() as $code => $val) {
                if ($code != '_controller') {
                    $pattern = str_replace('{' . $code . '}', $val, $pattern);
                }
            }

            if (substr($name, 0, 1) != '_' && substr_count($name, '_') != 0) {
                $listroutes[$name] = $pattern;
            }
        }

        return $listroutes;
    }

    private function verifierMenuAdmin($request, $idgroup)
    {
        $parameters = [
            'idgroup' => $idgroup,
        ];

        $container      = $this->container;
        $menuManager    = $container->get('mkk.menu_manager');
        $menuBddManager = $container->get('bdd.menu_manager');
        $menuRepository = $menuBddManager->getRepository();
        $menu           = $menuRepository->findby(['refmenu' => null]);
        $this->twig->addGlobal('menusite', $menu);
    }

    private function getDirContents($dir, &$results = [])
    {
        $files = scandir($dir);
        foreach ($files as $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (! is_dir($path)) {
                $results[] = $path;
            } elseif ($value != '.' && $value != '..') {
                $this->getDirContents($path, $results);
                $results[] = $path;
            }
        }

        return $results;
    }
}
