<?php

namespace Mkk\SiteBundle\Listener;

use Doctrine\ORM\Event\OnFlushEventArgs;
use Mkk\SiteBundle\Lib\LibDatabaseListener;
use Mkk\SiteBundle\Listener\Database\BlogTrait;
use Mkk\SiteBundle\Listener\Database\BookmarkTrait;
use Mkk\SiteBundle\Listener\Database\DateTrait;
use Mkk\SiteBundle\Listener\Database\DiaporamaTrait;
use Mkk\SiteBundle\Listener\Database\EmplacementTrait;
use Mkk\SiteBundle\Listener\Database\ParamTrait;
use Mkk\SiteBundle\Listener\Database\UserTrait;

class DatabaseListener extends LibDatabaseListener
{
    use BlogTrait;
    use BookmarkTrait;
    use DateTrait;
    use DiaporamaTrait;
    use EmplacementTrait;
    use UserTrait;
    use ParamTrait;

    public function onFlush(OnFlushEventArgs $args)
    {
        $this->setVarargs($args);
        $this->onTableBlog();
        $this->onTableBookmark();
        $this->onTableDate();
        $this->onTableDiaporama();
        $this->onTableEmplacement();
        $this->onTableUser();
        $this->onTableUser();
        $this->onTableParam();
    }
}
