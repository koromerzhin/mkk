<?php

namespace Mkk\SiteBundle\Listener\Database;

trait UserTrait
{
    public function onTableUser()
    {
        $container   = $this->container;
        $userManager = $container->get('bdd.user_manager');
        $userEntity  = $userManager->getTable();
        if ($this->entityInstanceofUpdate($userEntity)) {
            $this->onFlushUser($this->entity);
        }
        if ($this->entityInstanceofDelete($userEntity)) {
            $this->onDeleteUser($this->entity);
        }
    }

    private function onDeleteUser($entity)
    {
        $em      = $this->em;
        $uow     = $this->uow;
        $group   = $entity->getRefGroup();
        $methods = get_class_methods($group);
        if (is_array($methods) && in_array('getUsers', $methods)) {
            $users = $group->getUsers();
            $total = count($users) - 1;
            $group->setTotalnbuser($total);
            $em->persist($group);
            $md = $em->getClassMetadata(get_class($group));
            $uow->computeChangeSet($md, $group);
        }
    }

    private function onFlushUser($entity)
    {
        $em            = $this->em;
        $uow           = $this->uow;
        $group         = $entity->getRefGroup();
        $methods       = get_class_methods($group);
        $plainPassword = $entity->getPlainPassword();

        if (0 != strlen($plainPassword)) {
            $salt = rtrim(str_replace('+', '.', base64_encode(random_bytes(32))), '=');
            $entity->setSalt($salt);
            $encoder        = $this->container->get('security.encoder_factory')->getEncoder($entity);
            $hashedPassword = $encoder->encodePassword($plainPassword, $entity->getSalt());
            $entity->setPassword($hashedPassword);
            $entity->eraseCredentials();
        }
        if (is_array($methods) && in_array('getUsers', $methods)) {
            $users = $group->getUsers();
            $total = count($users);
            $group->setTotalnbuser($total);
            $em->persist($group);
            $md = $em->getClassMetadata(get_class($group));
            $uow->computeChangeSet($md, $group);
        }
    }
}
