<?php

namespace Mkk\SiteBundle\Listener\Database;

trait ParamTrait
{
    public function onTableParam()
    {
        $container    = $this->container;
        $paramManager = $container->get('bdd.param_manager');
        $paramEntity  = $paramManager->getTable();
        if ($this->entityInstanceofUpdate($paramEntity)) {
            $this->onFlushParam($this->entity);
        }
    }

    private function onFlushParam($entity)
    {
        $code   = $entity->getCode();
        $valeur = $entity->getValeur();
        if ($code == 'robotstxt') {
            file_put_contents('robots.txt', $valeur);
        } elseif ($code == 'crontab') {
            file_put_contents('crontab.sh', $valeur);
        }
    }
}
