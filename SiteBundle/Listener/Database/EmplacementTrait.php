<?php

namespace Mkk\SiteBundle\Listener\Database;

trait EmplacementTrait
{
    public function onTableEmplacement()
    {
        $container          = $this->container;
        $emplacementManager = $container->get('bdd.emplacement_manager');
        $emplacementEntity  = $emplacementManager->getTable();
        if ($this->entityInstanceofUpdate($emplacementEntity)) {
            $this->onFlushEmplacement($this->entity);
        }
        if ($this->entityInstanceofDelete($emplacementEntity)) {
            $this->onDeleteEmplacement($this->entity);
        }
    }

    private function onDeleteEmplacement($entity)
    {
        $em           = $this->em;
        $uow          = $this->uow;
        $evenement    = $entity->getRefEvenement();
        $emplacements = $evenement->getEmplacements();
        $mindate      = 0;
        $maxdate      = 0;
        $place        = 0;
        $illimite     = 0;
        foreach ($emplacements as $emplacement) {
            if ($emplacement->getId() != $entity->getId()) {
                $etatPlaceillimite = $emplacement->getPlaceillimite();
                if ($etatPlaceillimite == 0) {
                    $illimite = 0;
                } else {
                    $illimite = 1;
                }
                $totalplace = $emplacement->getTotalnbplace();
                $tempsmin   = $emplacement->getMindate();
                if ($mindate == 0 || $mindate > $tempsmin) {
                    $mindate = $tempsmin;
                }

                $tempsmax = $emplacement->getMaxdate();
                if ($maxdate == 0 || $maxdate < $tempsmax) {
                    $maxdate = $tempsmax;
                }
                $place = $place + $totalplace;
            }
        }

        $type = $evenement->getType();
        if ($type == 1) {
            $illimite = $evenement->getPlaceillimite();
            $place    = $evenement->getTotalnbplace();
        }

        $total = count($emplacements);
        $evenement->setPlaceillimite($illimite);
        $evenement->setTotalnbplace($place);
        $evenement->setMindate($mindate);
        $evenement->setMaxdate($maxdate);
        $evenement->setTotalnbemplacement($total);
        $em->persist($evenement);
        $md = $em->getClassMetadata(get_class($evenement));
        $uow->computeChangeSet($md, $evenement);
    }

    private function onFlushEmplacement($entity)
    {
        $em           = $this->em;
        $uow          = $this->uow;
        $evenement    = $entity->getRefEvenement();
        $emplacements = $evenement->getEmplacements();
        $mindate      = 0;
        $maxdate      = 0;
        $place        = 0;
        $illimite     = 0;
        foreach ($emplacements as $emplacement) {
            $etatPlaceillimite = $emplacement->getPlaceillimite();
            if ($etatPlaceillimite == 0) {
                $illimite = 0;
            } else {
                $illimite = 1;
            }
            $totalplace = $emplacement->getTotalnbplace();
            $tempsmin   = $emplacement->getMindate();
            if ($mindate == 0 || $mindate > $tempsmin) {
                $mindate = $tempsmin;
            }

            $tempsmax = $emplacement->getMaxdate();
            if ($maxdate == 0 || $maxdate < $tempsmax) {
                $maxdate = $tempsmax;
            }
            $place = $place + $totalplace;
        }

        $type = $evenement->getType();
        if ($type == 1) {
            $illimite = $evenement->getPlaceillimite();
            $place    = $evenement->getTotalnbplace();
        }

        $total = count($emplacements);
        $evenement->setPlaceillimite($illimite);
        $evenement->setTotalnbplace($place);
        $evenement->setMindate($mindate);
        $evenement->setMaxdate($maxdate);
        $evenement->setTotalnbemplacement($total);
        $em->persist($evenement);
        $md = $em->getClassMetadata(get_class($evenement));
        $uow->computeChangeSet($md, $evenement);
    }
}
