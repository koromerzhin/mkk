<?php

namespace Mkk\SiteBundle\Listener\Database;

trait DiaporamaTrait
{
    public function onTableDiaporama()
    {
        $container        = $this->container;
        $diaporamaManager = $container->get('bdd.diaporama_manager');
        $diaporamaEntity  = $diaporamaManager->getTable();
        if ($this->entityInstanceofUpdate($diaporamaEntity)) {
            $this->onFlushDiaporama($this->entity);
        }
        if ($this->entityInstanceofDelete($diaporamaEntity)) {
            $this->onDeleteDiaporama($this->entity);
        }
    }

    private function onDeleteDiaporama($entity)
    {
        $em     = $this->em;
        $uow    = $this->uow;
        $images = $entity->getImages();
        $total  = count($images);
        $entity->setTotalnbimage($total);
        $em->persist($entity);
        $md = $em->getClassMetadata(get_class($entity));
        $uow->computeChangeSet($md, $entity);
    }

    private function onFlushDiaporama($entity)
    {
        $em     = $this->em;
        $uow    = $this->uow;
        $images = $entity->getImages();
        $total  = count($images);
        $entity->setTotalnbimage($total);
        $em->persist($entity);
        $md = $em->getClassMetadata(get_class($entity));
        $uow->computeChangeSet($md, $entity);
    }
}
