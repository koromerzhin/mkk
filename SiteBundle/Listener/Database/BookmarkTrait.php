<?php

namespace Mkk\SiteBundle\Listener\Database;

trait BookmarkTrait
{
    public function onTableBookmark()
    {
        $container       = $this->container;
        $bookmarkManager = $container->get('bdd.bookmark_manager');
        $bookmarkEntity  = $bookmarkManager->getTable();
        if ($this->entityInstanceofUpdate($bookmarkEntity)) {
            $this->onFlushBookmark($this->entity);
        }
        if ($this->entityInstanceofDelete($bookmarkEntity)) {
            $this->onDeleteBookmark($this->entity);
        }
    }

    private function onDeleteBookmark($entity)
    {
        $em   = $this->em;
        $uow  = $this->uow;
        $tags = $entity->getTags();
        foreach ($tags as $tag) {
            $total = count($tag->getBookmarks()) - 1;
            $tag->setTotalnbbookmark($total);
            $em->persist($tag);
            $md = $em->getClassMetadata(get_class($tag));
            $uow->computeChangeSet($md, $tag);
        }
    }

    private function onFlushBookmark($entity)
    {
        $em   = $this->em;
        $uow  = $this->uow;
        $tags = $entity->getTags();
        foreach ($tags as $tag) {
            $total = count($tag->getBookmarks());
            $tag->setTotalnbbookmark($total);
            $em->persist($tag);
            $md = $em->getClassMetadata(get_class($tag));
            $uow->computeChangeSet($md, $tag);
        }
    }
}
