<?php

namespace Mkk\SiteBundle\Listener\Database;

trait BlogTrait
{
    public function onTableBlog()
    {
        $container   = $this->container;
        $blogManager = $container->get('bdd.blog_manager');
        $blogEntity  = $blogManager->getTable();
        if ($this->entityInstanceofUpdate($blogEntity)) {
            $this->onFlushBlog($this->entity);
        }
        if ($this->entityInstanceofDelete($blogEntity)) {
            $this->onDeleteBlog($this->entity);
        }
    }

    private function onDeleteBlog($entity)
    {
        $em   = $this->em;
        $uow  = $this->uow;
        $tags = $entity->getTags();
        foreach ($tags as $tag) {
            $total = count($tag->getBlogs()) - 1;
            $tag->setTotalnbblog($total);
            $em->persist($tag);
            $md = $em->getClassMetadata(get_class($tag));
            $uow->computeChangeSet($md, $tag);
        }
    }

    private function onFlushBlog($entity)
    {
        $em   = $this->em;
        $uow  = $this->uow;
        $tags = $entity->getTags();
        foreach ($tags as $tag) {
            $total = count($tag->getBlogs());
            $tag->setTotalnbblog($total);
            $em->persist($tag);
            $md = $em->getClassMetadata(get_class($tag));
            $uow->computeChangeSet($md, $tag);
        }
        $categorie   = $entity->getRefCategorie();
        $totalnbblog = count($categorie->getBlogs());
        $categorie->setTotalnbblog($totalnbblog);
        $em->persist($categorie);
        $md = $em->getClassMetadata(get_class($categorie));
        $uow->computeChangeSet($md, $categorie);
    }
}
