<?php

namespace Mkk\SiteBundle\Listener\Database;

trait DateTrait
{
    public function onTableDate()
    {
        $container   = $this->container;
        $dateManager = $container->get('bdd.date_manager');
        $dateEntity  = $dateManager->getTable();
        if ($this->entityInstanceofUpdate($dateEntity)) {
            $this->onFlushDate($this->entity);
        }

        if ($this->entityInstanceofDelete($dateEntity)) {
            $this->onDeleteDate($this->entity);
        }
    }

    private function onDeleteDate($entity)
    {
        $em          = $this->em;
        $uow         = $this->uow;
        $emplacement = $entity->getRefEmplacement();
        $dates       = $emplacement->getDates();
        $mindate     = 0;
        $maxdate     = 0;
        $total       = count($dates);
        $place       = 0;
        $illimite    = 0;
        foreach ($dates as $date) {
            if ($date->getId() != $entity->getId()) {
                $etatPlaceillimite = $date->getPlaceillimite();
                if ($etatPlaceillimite == 0) {
                    $illimite = 0;
                } else {
                    $illimite = 1;
                }

                $totalplace = $date->getPlace();
                $temps      = $date->getDebut();
                if ($mindate == 0 || $mindate > $temps) {
                    $mindate = $temps;
                }

                if ($maxdate == 0 || $maxdate < $temps) {
                    $maxdate = $temps;
                }

                $place = $place + $totalplace;
            }
        }

        $emplacement->setPlaceillimite($illimite);
        $emplacement->setTotalnbplace($place);
        $emplacement->setMindate($mindate);
        $emplacement->setMaxdate($maxdate);
        $emplacement->setTotalnbdate($total);
        $em->persist($emplacement);
        $md = $em->getClassMetadata(get_class($emplacement));
        $uow->computeChangeSet($md, $emplacement);
        $this->onFlushEmplacement($emplacement);
    }

    private function onFlushDate($entity)
    {
        $em          = $this->em;
        $uow         = $this->uow;
        $emplacement = $entity->getRefEmplacement();
        $dates       = $emplacement->getDates();
        $mindate     = 0;
        $maxdate     = 0;
        $total       = count($dates);
        $place       = 0;
        $illimite    = 0;
        foreach ($dates as $date) {
            $etatPlaceillimite = $date->getPlaceillimite();
            if ($etatPlaceillimite == 0) {
                $illimite = 0;
            } else {
                $illimite = 1;
            }

            $totalplace = $date->getPlace();
            $temps      = $date->getDebut();
            if ($mindate == 0 || $mindate > $temps) {
                $mindate = $temps;
            }

            if ($maxdate == 0 || $maxdate < $temps) {
                $maxdate = $temps;
            }

            $place = $place + $totalplace;
        }

        $emplacement->setPlaceillimite($illimite);
        $emplacement->setTotalnbplace($place);
        $emplacement->setMindate($mindate);
        $emplacement->setMaxdate($maxdate);
        $emplacement->setTotalnbdate($total);
        $em->persist($emplacement);
        $md = $em->getClassMetadata(get_class($emplacement));
        $uow->computeChangeSet($md, $emplacement);
        $this->onFlushEmplacement($emplacement);
    }
}
