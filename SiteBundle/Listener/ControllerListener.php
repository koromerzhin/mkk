<?php

namespace Mkk\SiteBundle\Listener;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class ControllerListener
{
    protected $twig;
    protected $container;

    public function __construct(Container $container, \Twig_Environment $twig)
    {
        $this->container = $container;
        $this->twig      = $twig;
    }

    public function before(FilterControllerEvent $event)
    {
        if (HttpKernelInterface::MASTER_REQUEST == $event->getRequestType()) {
            // Récupération du controller
            $controller = $event->getController();
            if (isset($controller[0])) {
                $request    = $event->getRequest();
                $controller = $controller[0];
                // On vérifie que le controller implémente la méthode preExecute
                if (method_exists($controller, 'AccesDroitAdmin')) {
                    $controller->accesDroitAdmin($request);
                }
            }
        }
    }

    public function onKernelAdminResponse(FilterControllerEvent $event)
    {
        // $attributes = $event->getRequest()->attributes;
        // $route      = $attributes->get('_route');
        // $controller = $event->getController();
        // if ($route == 'bord_index' || substr($route, 0, 2) == 'be') {
        //     if (isset($controller[0])) {
        //         $controller = $controller[0];
        //         if (method_exists($controller, 'get')) {
        //             if ($controller->get('kernel')->isDebug()) {
        //                 $twig    = $this->twig;
        //                 $globals = $twig->getGlobals();
        //             }
        //         }
        //     }
        // }
    }

    public function onKernelSiteResponse(FilterControllerEvent $event)
    {
        // $container = $this->container;
    }
}
