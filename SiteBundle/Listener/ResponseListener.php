<?php

namespace Mkk\SiteBundle\Listener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\Intl\Intl;

class ResponseListener
{
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->router    = $container->get('router');
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        $container           = $this->container;
        $bundles             = $container->getParameter('kernel.bundles');
        $fichierTel          = 'MkkSiteBundle:Include:telephone.html.twig';
        $fichierAdminTel     = 'MkkAdminBundle:Include:telephone.html.twig';
        $fichierCountry      = 'MkkSiteBundle:Include:country.html.twig';
        $fichierAdminCountry = 'MkkAdminBundle:Include:country.html.twig';
        $fichierDiaporama    = 'MkkSiteBundle:Include:diaporama.html.twig';
        $fichierEmail        = 'MkkSiteBundle:Include:email.html.twig';
        $fichierAdminEmail   = 'MkkAdminBundle:Include:email.html.twig';
        foreach ($bundles as $code => $bundle) {
            if (0 != substr_count($code, 'SiteBundle')) {
                $dossier                    = str_replace('\\', '/', $bundle);
                $dossier                    = substr($dossier, 0, strrpos($dossier, '/'));
                list($emplacement, $partie) = explode('/', $dossier);
                unset($partie);

                $fichier = '../src/' . $emplacement . '/SiteBundle/Resources/views/Include/telephone.html.twig';
                if (is_file($fichier)) {
                    $fichierTel = $emplacement . 'SiteBundle:Include:telephone.html.twig';
                    break;
                }

                $fichier = '../src/' . $emplacement . '/AdminBundle/Resources/views/Include/telephone.html.twig';
                if (is_file($fichier)) {
                    $fichierAdminTel = $emplacement . 'AdminBundle:Include:telephone.html.twig';
                    break;
                }

                $fichier = '../src/' . $emplacement . '/SiteBundle/Resources/views/Include/country.html.twig';
                if (is_file($fichier)) {
                    $fichierCountry = $emplacement . 'SiteBundle:Include:country.html.twig';
                    break;
                }

                $fichier = '../src/' . $emplacement . '/AdminBundle/Resources/views/Include/country.html.twig';
                if (is_file($fichier)) {
                    $fichierAdminCountry = $emplacement . 'AdminBundle:Include:country.html.twig';
                    break;
                }

                $fichier = '../src/' . $emplacement . '/SiteBundle/Resources/views/Include/diaporama.html.twig';
                if (is_file($fichier)) {
                    $fichierDiaporama = $emplacement . 'SiteBundle:Include:diaporama.html.twig';
                    break;
                }

                $fichier = '../src/' . $emplacement . '/SiteBundle/Resources/views/Include/email.html.twig';
                if (is_file($fichier)) {
                    $fichierEmail = $emplacement . 'SiteBundle:Include:email.html.twig';
                    break;
                }

                $fichier = '../src/' . $emplacement . '/AdminBundle/Resources/views/Include/email.html.twig';
                if (is_file($fichier)) {
                    $fichierAdminEmail = $emplacement . 'AdminBundle:Include:email.html.twig';
                    break;
                }
            }
        }

        $request         = $event->getRequest();
        $route           = $request->get('_route');
        $response        = $event->getResponse();
        $content         = $response->getContent();
        $templateTel     = $fichierTel;
        $templateCountry = $fichierCountry;
        $templateEmail   = $fichierEmail;
        if ('bord_index' == $route || 'be' == substr($route, 0, 2)) {
            $templateTel     = $fichierAdminTel;
            $templateCountry = $fichierAdminCountry;
            $templateEmail   = $fichierAdminEmail;
        } else {
            $content = $this->modifVideo($content);
            $content = $this->modifGalerie($content, $fichierDiaporama);
        }

        $content = $this->modifThSelection($content);
        $content = $this->modifierBoolean($content);
        $content = $this->modifierDay($content, $request->getLocale());
        $content = $this->modifierTemps($content, $request->getLocale());
        $content = $this->modifierHeure($content, $request->getLocale());
        $content = $this->modifTelephone($content, $request->getLocale(), $templateTel);
        $content = $this->modifCountry($content, $request->getLocale(), $templateCountry);
        $content = $this->modifEmail($content, $templateEmail);
        $content = $this->modifForm($content);
        $content = $this->addUploadFormTmpl($content);
        $content = $this->addModal($content);
        $response->setContent($content);
        $event->setResponse($response);
    }

    private function addModal($content)
    {
        $templating = $this->container->get('templating');
        if (0 != substr_count($content, 'required')) {
            $html       = $templating->render(
                'MkkSiteBundle:Include:modal/required.html.twig'
            );
            $html    = str_replace(["\t", "\n"], '', $html);
            $content = str_replace('</body>', $html . '</body>', $content);
        }

        return $content;
    }

    private function addUploadFormTmpl($content)
    {
        $templating = $this->container->get('templating');
        if (0 != substr_count($content, 'EmplacementUpload')) {
            $twiguploadtmpl = [
                'upload'   => '',
                'download' => '',
            ];
            $folder         = '../';
            $file           = $folder . 'src/Mkk/SiteBundle/Resources/views/Tmpl/upload.tmpl';
            if (is_file($file)) {
                $twiguploadtmpl['upload'] = str_replace(["\n", "\t"], '', file_get_contents($file));
            }

            $file = $folder . 'src/Mkk/SiteBundle/Resources/views/Tmpl/download.tmpl';
            if (is_file($file)) {
                $twiguploadtmpl['download'] = str_replace(["\n", "\t"], '', file_get_contents($file));
            }

            $html    = $templating->render(
                'MkkSiteBundle:Include:upload.html.twig',
                [
                    'twiguploadtmpl' => $twiguploadtmpl,
                ]
            );
            $content = str_replace('</body>', $html . '</body>', $content);
        }

        return $content;
    }

    private function modifForm($content)
    {
        if (0 != substr_count($content, '<body')) {
            $doc = new \DOMDocument();
            libxml_use_internal_errors(true);
            $doc->loadHTML($content);
            $forms = $doc->getElementsByTagName('form');
            foreach ($forms as $form) {
                $class = $form->getAttribute('class');
                if ('' == $class || (0 == substr_count($class, 'form-horizontal') && 0 == substr_count($class, 'form-inline') && 0 == substr_count($class, 'form-vertical'))) {
                    $class = $class . ' form-horizontal';
                }
                if ('form-vertical' == $class) {
                    $class = str_replace('form-vertical', '', $class);
                }

                $class = trim($class);
                $form->setAttribute('class', $class);
            }

            $content =  $doc->saveHTML();
        }

        return $content;
    }

    private function modifierBoolean($content)
    {
        $tab        = explode('{boolean}', $content);
        $templating = $this->container->get('templating');
        foreach ($tab as $row) {
            if (0 != substr_count($row, '{/boolean}')) {
                $code    = substr($row, 0, strpos($row, '{/boolean}'));
                $html    = $templating->render(
                    'MkkSiteBundle:Include:boolean.html.twig',
                    [
                        'code'     => $code,
                    ]
                );
                $content = str_replace('{boolean}' . $code . '{/boolean}', $html, $content);
            }
        }

        return $content;
    }

    private function modifierDay($content, $locale)
    {
        $tab        = explode('{day}', $content);
        $templating = $this->container->get('templating');
        foreach ($tab as $row) {
            if (0 != substr_count($row, '{/day}')) {
                $code    = substr($row, 0, strpos($row, '{/day}'));
                $html    = $templating->render(
                    'MkkSiteBundle:Include:day.html.twig',
                    [
                        'code'     => $code,
                        'locale'   => $locale,
                    ]
                );
                $content = str_replace('{day}' . $code . '{/day}', $html, $content);
            }
        }

        return $content;
    }

    private function modifierTemps($content, $locale)
    {
        $tab        = explode('{temps}', $content);
        $templating = $this->container->get('templating');
        foreach ($tab as $row) {
            if (0 != substr_count($row, '{/temps}')) {
                $code    = substr($row, 0, strpos($row, '{/temps}'));
                $html    = $templating->render(
                    'MkkSiteBundle:Include:temps.html.twig',
                    [
                        'code'     => $code,
                        'locale'   => $locale,
                    ]
                );
                $content = str_replace('{temps}' . $code . '{/temps}', $html, $content);
            }
        }

        return $content;
    }

    private function modifierHeure($content, $locale)
    {
        $tab        = explode('{heure}', $content);
        $templating = $this->container->get('templating');
        foreach ($tab as $row) {
            if (0 != substr_count($row, '{/heure}')) {
                $code    = substr($row, 0, strpos($row, '{/heure}'));
                $html    = $templating->render(
                    'MkkSiteBundle:Include:heure.html.twig',
                    [
                        'code'     => $code,
                        'locale'   => $locale,
                    ]
                );
                $content = str_replace('{heure}' . $code . '{/heure}', $html, $content);
            }
        }

        return $content;
    }

    private function modifThSelection($content)
    {
        $html    = '<th class="table-checkbox"><input data-rel="allselect" type="checkbox" /></th>';
        $content = str_replace('{thselection}', $html, $content);

        return $content;
    }

    private function modifCountry($content, $locale, $twig)
    {
        $tab        = explode('{country}', $content);
        $templating = $this->container->get('templating');
        foreach ($tab as $row) {
            if (0 != substr_count($row, '{/country}')) {
                $code    = substr($row, 0, strpos($row, '{/country}'));
                $intl    = Intl::getRegionBundle()->getCountryNames($locale);
                $country = isset($intl[$code]) ? $intl[$code] : '';
                $html    = $templating->render(
                    $twig,
                    [
                        'code'     => $code,
                        'country'  => $country,
                    ]
                );
                $content = str_replace('{country}' . $code . '{/country}', $html, $content);
            }
        }

        return $content;
    }

    private function modifEmail($content, $twig)
    {
        $tab        = explode('{lienemail}', $content);
        $templating = $this->container->get('templating');
        foreach ($tab as $row) {
            if (0 != substr_count($row, '{/lienemail}')) {
                $adresse = substr($row, 0, strpos($row, '{/lienemail}'));
                $html    = $templating->render(
                    $twig,
                    [
                        'mail'  => $adresse,
                    ]
                );
                $content = str_replace('{lienemail}' . $adresse . '{/lienemail}', $html, $content);
            }
        }

        return $content;
    }

    private function modifTelephone($content, $locale, $twig)
    {
        $telService = $this->container->get('mkk.telephone_service');
        $tab        = explode('{lientel}', $content);
        $templating = $this->container->get('templating');
        $telService = $this->container->get('mkk.telephone_service');
        foreach ($tab as $row) {
            if (0 != substr_count($row, '{/lientel}')) {
                $numero = substr($row, 0, strpos($row, '{/lientel}'));
                $json   = $telService->verif($numero, $locale);
                $num    = $numero;
                if (isset($json['num'])) {
                    $num = $json['num'];
                }

                $html    = $templating->render(
                    $twig,
                    [
                        'locale'  => strtoupper($locale),
                        'country' => Intl::getRegionBundle()->getCountryNames(),
                        'locale'  => strtoupper($locale),
                        'data'    => $json,
                    ]
                );
                $content = str_replace('{lientel}' . $numero . '{/lientel}', $html, $content);
            }
        }

        return $content;
    }

    private function modifVideo($content)
    {
        $tab = explode('{video}', $content);
        foreach ($tab as $row) {
            if (0 != substr_count($row, '{/video}')) {
                $url  = substr($row, 0, strpos($row, '{/video}'));
                $json = $this->oembed($url);
                $html = '';
                if (isset($json['html'])) {
                    $html = $json['html'];
                }

                $html = str_replace(['width="480"', 'height="270"'], '', $html);
                $html = str_replace('<iframe', "<iframe class='embed-responsive-item'", $html);

                $content = str_replace('{video}' . $url . '{/video}', $html, $content);
            }
        }

        return $content;
    }

    /**
     * Transforme un lien en code html oembed.
     *
     * @param string $lien URL
     *
     * @return array
     */
    private function oembed($lien)
    {
        $tab = [];
        if (1 == substr_count($lien, 'vimeo')) {
            $url = 'http://vimeo.com/api/oembed.json?url=';
        } elseif (1 == substr_count($lien, 'youtube') || 1 == substr_count($lien, 'youtu.be')) {
            $url = 'http://www.youtube.com/oembed?format=json&url=';
        } elseif (1 == substr_count($lien, 'ifttt')) {
            $url = 'https://ifttt.com/oembed.json?url=';
        } elseif (1 == substr_count($lien, 'flickr')) {
            $url = 'https://www.flickr.com/services/oembed.json?url=';
        }

        if (isset($url)) {
            $url = $url . urlencode($lien);
            $tab = json_decode(file_get_contents($url), true);
        }

        return $tab;
    }

    private function modifGalerie($content, $twig)
    {
        $tab        = explode('{video}', $content);
        $container  = $this->container;
        $templating = $container->get('templating');
        foreach ($tab as $row) {
            if (0 != substr_count($row, '{/galerie}')) {
                $value = substr($row, 0, strpos($row, '{/galerie}'));
                $html  = '';
                if (is_numeric($value)) {
                    $diaporamaManager    = $container->get('bdd.diaporama_manager');
                    $diaporamaRepository = $diaporamaManager->getRepository();
                    $diaporama           = $diaporamaRepository->find($value);
                    if ($diaporama) {
                        $data = [
                            'images' => $diaporama->getImages(),
                        ];
                    }
                } elseif (is_dir($value)) {
                    $fichiers = glob($value . '/*');
                    foreach ($fichiers as $i => $file) {
                        $file = str_replace('//', '/', $file);
                        if (! is_file($file)) {
                            unset($fichiers[$i]);
                        } else {
                            $fichiers[$i] = substr($file, strpos($file, $value));
                        }
                    }
                    $data = [
                        'images' => $fichiers,
                    ];
                }

                if (isset($data) && 0 != count($data['images'])) {
                    $data['md5'] = md5(uniqid());
                    $html        = $templating->render(
                        $twig,
                        $data
                    );
                }

                $content = str_replace('{galerie}' . $value . '{/galerie}', $html, $content);
            }
        }

        return $content;
    }
}
