<?php

namespace Mkk\SiteBundle\Listener;

use Doctrine\Common\EventArgs;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Mkk\SiteBundle\Annotation\AnnotationReader;
use Mkk\SiteBundle\Handler\UploadHandler;

class UploadSubscriber implements EventSubscriber
{
    /**
     * @var AnnotationReader
     */
    private $reader;

    /**
     * @var UploadHandler
     */
    private $handler;

    public function __construct(AnnotationReader $reader, UploadHandler $handler)
    {
        $this->handler = $handler;
        $this->reader  = $reader;
    }

    public function getSubscribedEvents(): array
    {
        return [
            'prePersist',
            'preUpdate',
            'postLoad',
        ];
    }

    public function prePersist(EventArgs $event)
    {
        $this->preEvent($event);
    }

    public function preUpdate(EventArgs $event)
    {
        $this->preEvent($event);
    }

    public function postLoad(LifecycleEventArgs $event)
    {
        $entity           = $event->getEntity();
        $uploadableFields = $this->reader->getUploadableFields($entity);
        foreach ($uploadableFields as $property => $annotation) {
            $this->handler->setFileFromFilename($entity, $property, $annotation);
        }
    }

    private function preEvent(EventArgs $event)
    {
        $entity           = $event->getEntity();
        $uploadableFields = $this->reader->getUploadableFields($entity);
        foreach ($uploadableFields as $property => $annotation) {
            $this->handler->removeOldFile($entity, $annotation);
            $this->handler->uploadFile($entity, $property, $annotation);
        }
    }
}
