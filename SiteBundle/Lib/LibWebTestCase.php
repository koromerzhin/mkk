<?php

namespace Mkk\SiteBundle\Lib;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class LibWebTestCase extends WebTestCase
{
    public function setUp()
    {
        $this->client = static::createClient();
    }

    protected function setUrl($route, $param = [])
    {
        if (substr($route, 0, 2) == 'be' || $route == 'bord_index') {
            $this->logIn();
        }
        $container = $this->client->getContainer();
        $router    = $container->get('router');
        if (substr_count($route, '_upload') != 0) {
            $param['md5'] = md5(uniqid());
        }

        $url = $router->generate($route, $param);

        return $url;
    }

    protected function verifStatusCode($method, $url, $param, $code)
    {
        $this->client->request($method, $url, $param);
        $statutcode = $this->client->getResponse()->getStatusCode();
        $this->assertSame($code, $statutcode);
    }

    protected function logIn()
    {
        $container       = $this->client->getContainer();
        $session         = $container->get('session');
        $groupManager    = $container->get('bdd.group_manager');
        $groupRepository = $groupManager->getRepository();
        $group           = $groupRepository->findOneBy(['code' => 'superadmin']);
        if (! $group) {
            return;
        }
        $userManager    = $container->get('bdd.user_manager');
        $userRepository = $userManager->getRepository();
        $user           = $userRepository->findOneBy(['enabled' => 1, 'refgroup' => $group]);
        if (! $user) {
            return;
        }

        // the firewall context defaults to the firewall name
        $firewallContext = 'main';

        $token = new UsernamePasswordToken($user, null, $firewallContext, $user->getRoles());
        $session->set('_security_' . $firewallContext, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }
}
