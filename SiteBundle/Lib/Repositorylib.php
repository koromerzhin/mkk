<?php

namespace Mkk\SiteBundle\Lib;

use Doctrine\ORM\EntityRepository;

class Repositorylib extends EntityRepository
{
    private $data;

    public function setSearchResult($dql, $param = [])
    {
        $em    = $this->getEntityManager();
        $query = $em->createQuery($dql);
        if (count($param) != 0) {
            $query->setParameters($param);
        }
        if (count($param) != 0) {
        }

        $methods = get_class_methods($this);
        if (in_array('getResultEntityManager', $methods)) {
            $result = $this->getResultEntityManager($query);
        } else {
            $result = $query;
        }

        return $result;
    }

    public function getRowTable($recherche, $param, $entity)
    {
        $em     = $this->getEntityManager();
        $sql    = "SELECT entity FROM $entity entity";
        $sql    = $this->searchImplode($recherche, $sql);
        $query  = $em->createQuery($sql);
        $param  = $query->setParameters($param);
        $result = $param->getOneOrNullResult();

        return $result;
    }

    public function getResultPagination($recherche, $param, $order, $sql)
    {
        $em  = $this->getEntityManager();
        $sql = $this->searchImplode($recherche, $sql);
        if (count($order) != 0) {
            foreach (array_keys($order) as $position => $code) {
                if ($position == 0) {
                    $sql = $sql . ' ORDER BY ';
                } else {
                    $sql = $sql . ',';
                }

                $sql = $sql . "entity.$code ";
            }
        }
        $query  = $em->createQuery($sql);
        $param  = $query->setParameters($param);
        $result = $param->getResult();

        return $result;
    }

    public function getListingTable($recherche, $param, $order, $entity)
    {
        $sql = "SELECT entity FROM $entity entity";
        $tab = $this->getResultPagination($recherche, $param, $order, $sql);

        return $tab;
    }

    public function getEtablissement($id)
    {
        $tab         = [];
        $param       = [];
        $em          = $this->getEntityManager();
        $sql         = 'SELECT e FROM ' . BUNDLESITE . ':Etablissement e WHERE e.id=:id';
        $param['id'] = $id;
        $tab         = $em->createQuery($sql)->setParameters($param)->getSingleResult();

        return $tab;
    }

    public function searchImplode($recherche, $sql)
    {
        if (count($recherche) != 0) {
            $sqlrecherche = '';
            foreach ($recherche as $search) {
                if ($sqlrecherche != '') {
                    $sqlrecherche = $sqlrecherche . ' AND ';
                }

                $sqlrecherche = $sqlrecherche . ' ' . $search;
            }

            $sql = $sql . ' WHERE ' . trim($sqlrecherche);
        }

        return $sql;
    }

    public function concat($separateur, $tab)
    {
        $texte  = '';
        $newtab = [];
        foreach ($tab as $key => $val) {
            if (count($newtab) != 0) {
                $newtab[] = "' '";
            }
            $newtab[] = $val;
        }
        foreach ($newtab as $key => $valeur) {
            if ($texte != '') {
                $texte = $texte . ',';
            }
            if (count($newtab) != $key + 1) {
                $texte = $texte . 'CONCAT(';
            }
            $texte = $texte . $valeur;
        }
        $texte = $texte . str_repeat(')', count($newtab) - 1);

        return $texte;
    }

    public function getRegion($id, $user = 'toutoul')
    {
        $url = 'http://api.geonames.org/childrenJSON?geonameId=' . $id;
        $md5 = md5($url);
        if (is_file('tmp/geonames/' . $md5)) {
            $contents = file_get_contents('tmp/geonames/' . $md5);
        } else {
            $contents = file_get_contents($url . '&username=' . $user);
            file_put_contents('tmp/geonames/' . $md5, $contents);
        }
        $tab = json_decode($contents, true);

        return $tab['geonames'];
    }

    public function getInfoGeoname($id, $user = 'toutoul')
    {
        $url = 'http://api.geonames.org/getJSON?geonameId=' . $id;
        $md5 = md5($url);
        if (is_file('tmp/geonames/' . $md5)) {
            $contents = file_get_contents('tmp/geonames/' . $md5);
        } else {
            $contents = file_get_contents($url . '&username=' . $user);
            file_put_contents('tmp/geonames/' . $md5, $contents);
        }

        $tab = json_decode($contents, true);

        return $tab;
    }

    public function getcpville($request, $user = 'toutoul')
    {
        $response = [];
        $pays     = $request['pays'];
        $cp       = $request['cp'];
        $ville    = $request['ville'];
        $gps      = $request['gps'];
        $url      = '';
        if ($cp != '') {
            $url = 'http://api.geonames.org/postalCodeSearchJSON?country=' . $pays . '&postalcode=' . urlencode($cp);
        } elseif ($ville != '') {
            $url = 'http://api.geonames.org/searchJSON?&name_startsWith=' . urlencode($ville) . '&country=' . $pays . '&style=FULL&featureClass=A&featureCode=ADM4';
        }
        if (! is_dir('tmp')) {
            mkdir('tmp');
        }

        if (! is_dir('tmp/geonames')) {
            mkdir('tmp/geonames');
        }

        if ($url != '') {
            $md5 = md5($url);
            if (is_file('tmp/geonames/' . $md5)) {
                $contents = file_get_contents('tmp/geonames/' . $md5);
            } else {
                $contents = file_get_contents($url . '&username=' . $user);
                file_put_contents('tmp/geonames/' . $md5, $contents);
            }
            $tab = json_decode($contents, true);
            if (isset($tab['postalCodes'])) {
                $response = $tab['postalCodes'];
                if ($ville != '') {
                    $tab      = $response;
                    $response = [];
                    foreach ($tab as $key => $data) {
                        if (mb_strtoupper($ville, 'UTF-8') == mb_strtoupper($data['placeName'], 'UTF-8')) {
                            if ($gps != '') {
                                list($data['lat'], $data['lng']) = explode(',', $gps);
                            }

                            $response[$key] = $data;
                        }
                    }
                }
            } elseif (isset($tab['geonames'])) {
                $response = $tab['geonames'];
            } else {
                $response = $tab;
                if (isset($tab['status']['message'])) {
                    if (is_file('tmp/geonames/' . $md5)) {
                        unlink('tmp/geonames/' . $md5);
                    }
                }
            }
            foreach ($response as $code => $val) {
                if (isset($val['postalCode'])) {
                    $response[$code]['id'] = $val['postalCode'];
                }
            }
        }

        return $response;
    }

    public function paginationTraitement($total, $page, $limit, $data = [])
    {
        $tab = [
            'max'       => $total,
            'data'      => $data,
            'limit'     => $limit,
            'offset'    => 0,
            'courant'   => $page,
            'precedent' => [],
            'suivant'   => [],
            'total'     => ceil($total / $limit),
        ];
        if ($page <= 1) {
            $tab['courant'] = 1;
            $tab['offset']  = 0;
        } elseif ($page >= ceil($total / $limit)) {
            $tab['offset']  = (ceil($total / $limit) - 1) * $limit;
            $tab['courant'] = ceil($total / $limit);
        } else {
            $tab['offset'] = ($page - 1) * $limit;
        }

        $avant = $tab['courant'] - 5;
        for ($i = $avant; $i < $tab['courant']; ++$i) {
            if ($i >= 1) {
                $tab['precedent'][$i] = $i;
            }
        }

        for ($i = $tab['courant'] + 1; $i <= $tab['total']; ++$i) {
            if ($i - $tab['courant'] <= 5) {
                $tab['suivant'][$i] = $i;
            } else {
                break;
            }
        }

        if ($tab['max'] == 0) {
            $tab['offset'] = 0;
        }

        return $tab;
    }

    public function randomPassword($number)
    {
        $alphabet    = 'abcdefghijklmnopqrstuwxyz';
        $pass        = []; //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $number; ++$i) {
            $n      = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }

        return implode($pass);
    }

    public function like($nom, $variables, $param)
    {
        $sql = '';
        if (! is_array($variables)) {
            $variables = [
                $variables,
            ];
        }
        foreach ($variables as $variable) {
            if ($sql != '') {
                $sql = $sql . ' OR ';
            }
            $md51         = $this->randomPassword(8);
            $md52         = $this->randomPassword(8);
            $param[$md51] = '%' . $variable . '%';
            $param[$md52] = $variable . '%';
            $sql          = $sql . $nom . ' LIKE :' . $md51 . ' OR ' . $nom . ' LIKE :' . $md52;
        }

        return [$sql, $param];
    }

    public function pagination($sql, $page, $data = [])
    {
        // C'est quoi encore ce délire ???
        $limit = 50;
        if (constant('LONGUEURLISTE') != null) {
            $limit = LONGUEURLISTE;
        }

        if (defined('PAGELONGUEURLISTE') && constant('PAGELONGUEURLISTE') != null) {
            $limit = PAGELONGUEURLISTE;
        }

        if (isset($_GET['combien'])) {
            $limit = $_GET['combien'];
        } elseif (isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }

        if (in_array('getQuery', get_class_methods($sql))) {
            $sql = $sql->getQuery();
        }

        $req = $sql->getSql();
        if (substr_count($req, 'GROUP BY') != 0) {
            $tadada = $sql->getResult();
            if (count($tadada) == 0) {
                $total = 0;
            } else {
                $total = count($tadada);
            }
        } else {
            $total = $sql->getSingleScalarResult();
        }
        $tab = $this->paginationTraitement($total, $page, $limit, $data);

        return $tab;
    }

    public function arrayTabEtablissement($user, $tab = [])
    {
        $methods = get_class_methods($user);
        if (in_array('getEtablissements', $methods)) {
            if (count($user->getEtablissements()) != 0) {
                foreach ($user->getEtablissements() as $etablissement) {
                    $id       = $etablissement->getId();
                    $tab[$id] = $id;
                    $tab      = $this->arrayTabEtablissement($etablissement, $tab);
                }
            }
        } elseif (in_array('getChild', $methods)) {
            if (count($user->getChild()) != 0) {
                foreach ($user->getChild() as $child) {
                    $id       = $child->getId();
                    $tab[$id] = $id;
                    $tab      = $this->arrayTabEtablissement($child, $tab);
                }
            }
        }

        ksort($tab);

        return $tab;
    }

    public function arrayTabEtablissementUsers($user, $tab = [])
    {
        $methods = get_class_methods($user);
        if (in_array('getUsers', $methods)) {
            if (count($user->getUsers()) != 0) {
                foreach ($user->getUsers() as $row) {
                    $tab[] = $row->getId();
                }
            }
        }

        if (in_array('getEtablissements', $methods)) {
            if (count($user->getEtablissements()) != 0) {
                foreach ($user->getEtablissements() as $etablissement) {
                    $tab = $this->arrayTabEtablissementUsers($etablissement, $tab);
                }
            }
        } elseif (in_array('getChild', $methods)) {
            if (count($user->getChild()) != 0) {
                foreach ($user->getChild() as $child) {
                    $tab = $this->arrayTabEtablissementUsers($child, $tab);
                }
            }
        }
        ksort($tab);

        return $tab;
    }
}
