<?php

namespace Mkk\SiteBundle\Lib;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Intl\Intl;

abstract class LibController extends Controller
{
    public $paginator;
    private $datasite = [];

    public function traitementFilAriane($menu, $ariane = [])
    {
        $parent = $menu->getParent();
        if ($parent) {
            $ariane = $this->traitementFilAriane($parent, $ariane);
        }

        $routes = $this->getRoutes();
        $data   = $menu->getData();
        if (isset($data['ariane']) && $data['ariane'] != '') {
            $url = $menu->getUrl();
            if (isset($routes[$menu->getUrl()]) && substr_count('{', $routes[$menu->getUrl()]) != 0) {
                $url = $this->generateUrl($menu->getUrl());
            } else {
                $url = '';
            }

            $ariane[] = [
                'url'  => $url,
                'text' => $data['ariane'],
            ];
        }

        return $ariane;
    }

    public function traitementMetatags($menu, $meta = [])
    {
        if (count($meta) == 0) {
            $meta['titre']       = '';
            $meta['keywords']    = '';
            $meta['description'] = '';
        }

        if ($menu) {
            $data   = $menu->getData();
            $parent = $menu->getParent();
            if (isset($data['meta'])) {
                if (isset($data['meta']['titre']) && $data['meta']['titre'] != '' && $meta['titre'] == '') {
                    $meta['titre'] = $data['meta']['titre'];
                }

                if (isset($data['meta']['keywords']) && $data['meta']['keywords'] != '' && $meta['keywords'] == '') {
                    $meta['keywords'] = $data['meta']['keywords'];
                }

                if (isset($data['meta']['description'])
                    && $data['meta']['description'] != ''
                    && $meta['description'] == ''
                ) {
                    $meta['description'] = $data['meta']['description'];
                }
            }

            if ($parent) {
                $new = $this->traitementMetatags($parent);
                if (isset($new['titre']) && $new['titre'] != '' && $meta['titre'] == '') {
                    $meta['titre'] = $new['titre'];
                }

                if (isset($new['keywords']) && $new['keywords'] != '' && $meta['keywords'] == '') {
                    $meta['keywords'] = $new['keywords'];
                }

                if (isset($new['description']) && $new['description'] != '' && $meta['description'] == '') {
                    $meta['description'] = $new['description'];
                }
            }
        }

        return $meta;
    }

    public function traitementLienActuel($menu, $lienactuel = [])
    {
        $parent = $menu->getParent();
        if ($parent) {
            $lienactuel = $this->traitementLienActuel($parent, $lienactuel);
        }

        $lienactuel[] = $menu->getId();

        return $lienactuel;
    }

    public function initPage($request, $parameters)
    {
        $this->getBundle();
        $uploadService                  = $this->get('mkk.upload_service');
        $actionService                  = $this->get('mkk.action_service');
        $boutons                        = $actionService->getBouton();
        $parameters['boutonsAffichage'] = $boutons;
        $parameters['md5']              = $uploadService->getMd5();
        $param                          = $this->get('mkk.param_service')->listing();
        $parameters['param']            = $param;
        $token                          = $this->get('security.token_storage')->getToken();
        $parameters['idgroup']          = $this->getDataGroupUser($token);
        $actionService                  = $this->get('mkk.action_service');
        $actionService->traiter();
        $parameters['actions']['data'] = $actionService->get();
        if ($this->has('searchform')) {
            $parameters['search'] = $this->get('searchform');
        }

        $parameters = $this->getMetatagsArianeLienActuel($parameters);
        $route      = $request->attributes->get('_route');
        $meta       = ['description' => '', 'titre' => '', 'keywords' => ''];
        if (substr($route, 0, 2) != 'be' && $route != 'bord_index') {
            $tabmeta = ['Titre', 'Description', 'Keywords'];
            foreach ($parameters as $val) {
                if (is_object($val)) {
                    $methods = get_class_methods($val);
                    foreach ($tabmeta as $key) {
                        if (in_array('getMeta' . $key, $methods)) {
                            $fonction = 'getMeta' . $key;
                            $titre    = strtolower($key);
                            if ($val->$fonction() != '') {
                                $meta[$titre] = $val->$fonction();
                            }
                        }
                    }
                }
            }
        }

        foreach ($meta as $titre => $val) {
            if (! isset($parameters['metatags'][$titre])) {
                $parameters['metatags'][$titre] = '';
            }

            if (isset($meta[$titre]) && $val != '' && $parameters['metatags'][$titre] == '') {
                $parameters['metatags'][$titre] = $val;
            } elseif (isset($parameters['seo_titre'])
                && $parameters['seo_titre'] == 1
                && $titre == 'titre' && $val != ''
            ) {
                if (substr_count($val, $parameters['metatags'][$titre]) == 0) {
                    $parameters['metatags'][$titre] = $val . ' - ' . $parameters['metatags'][$titre];
                }
            }
        }

        if (isset($parameters['metatags']) && isset($parameters['google_webmastertool'])) {
            $parameters['metatags']['google-site-verification'] = $parameters['google_webmastertool'];
        }

        $parameters['controller'] = $this;

        $titresite = "Nous n'avons pas encore de titre";
        if (isset($parameters['param']['meta_titre']) && $parameters['param']['meta_titre'] != '') {
            $titresite = $parameters['param']['meta_titre'];
        }

        $parameters['titresite'] = $titresite;
        ksort($parameters['metatags']);

        return $parameters;
    }

    public function setListing(Request $request, $managerCode, $fonction)
    {
        $manager               = $this->get($managerCode);
        $data                  = $request->query->all();
        $data['user']          = $this->get('security.token_storage')->getToken()->getUser();
        $data['params_config'] = $this->get('mkk.param_service')->listing();
        $repository            = $manager->getRepository();
        $query                 = $repository->$fonction($data);

        return $query;
    }

    public function setMailer($tab)
    {
        $subject       = isset($tab['subject']) ? $tab['subject'] : '';
        $from          = isset($tab['from']) ? $tab['from'] : '';
        $to            = isset($tab['to']) ? $tab['to'] : '';
        $body          = isset($tab['content']) ? $tab['content'] : '';
        $fichiers      = isset($tab['fichiers']) ? $tab['fichiers'] : [];
        $mailerManager = $this->get('bdd.mailer_manager');
        $mailerEntity  = $mailerManager->getTable();
        $mailer        = new $mailerEntity();
        $mailer->setSubject($subject);
        $mailer->setFrom($from);
        if (isset($tab['reply'])) {
            $mailer->setReply($tab['reply']);
        }

        if (isset($tab['cc'])) {
            $mailer->setCc($tab['cc']);
        }

        $mailer->setTo($to);
        if ($body == '') {
            return false;
        }

        $mailer->setBody($body);
        $mailerManager->persistAndFlush($mailer);
        if (count($fichiers) != 0) {
            $id = $mailer->getId();
            if (! is_dir('fichiers/mailer/' . $id)) {
                mkdir('fichiers/mailer/' . $id);
            }

            $newFile = [];
            foreach ($fichiers as $nom => $tmpFile) {
                $file = 'fichiers/mailer/' . $id . '/' . $nom;
                copy($tmpFile, $file);
                $newFile[] = $file;
            }

            $mailer->setFichiers($newFile);
            $mailerManager->persistAndFlush($mailer);
        }

        $param = $this->get('mkk.param_service')->listing();
        $titre = '';
        if (isset($param['meta_titre'])) {
            $titre = $param['meta_titre'];
        }
        $mail = \Swift_Message::newInstance();
        $mail->setSubject($mailer->getSubject());
        if (isset($param['meta_titre'])) {
            $from = [$mailer->getFrom() => $param['meta_titre']];
        } else {
            $from = $mailer->getFrom();
        }

        $mail->setFrom($from);
        if ($mailer->getReply() != '') {
            $mail->setReplyTo($mailer->getReply());
        }

        $cc = $mailer->getCc();
        if (count($cc) != 0) {
            $mail->setCc($cc);
        }

        $mail->setTo($mailer->getTo());
        $body = $mailer->getBody();
        $mail->setBody(strip_tags($body));
        $mail->addPart($body, 'text/html');
        $fichiers = $mailer->getFichiers();
        if (count($fichiers) != 0) {
            foreach ($fichiers as $file) {
                $nom        = substr($file, strrpos($file, '/'));
                $attachment = \Swift_Attachment::fromPath($file)->setFilename($nom);
                $mail->attach($attachment);
            }
        }

        $this->get('mailer')->send($mail);
    }

    public function modifierColorFileXml($fichier)
    {
        if (is_file($fichier)) {
            $html = file_get_contents($fichier);
            preg_match_all("/rgb\s*\(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*\)/i", $html, $matches);
            $tab = [];
            foreach ($matches[0] as $match) {
                if (! isset($tab[$match])) {
                    preg_match_all('/\\d+/', $match, $colors);
                    $r = dechex($colors[0][0]);
                    if (strlen($r) != 2) {
                        $r = '0' . $r;
                    }

                    $g = dechex($colors[0][1]);
                    if (strlen($g) != 2) {
                        $g = '0' . $g;
                    }

                    $b = dechex($colors[0][2]);
                    if (strlen($b) != 2) {
                        $b = '0' . $b;
                    }

                    $color       = "#$r$g$b";
                    $tab[$match] = $color;
                }

                //$html  = str_replace($match, $color, $html);
            }

            foreach ($tab as $match => $color) {
                $html = str_replace($match, $color, $html);
            }

            file_put_contents($fichier, $html);
        }
    }

    public function trouverFichierTwig($folder, $twig)
    {
        $repertoire = $this->getBundlePublicRepertoire();
        $fichier    = '';
        $fichier1   = '../src/' . $repertoire . '/Resources/views/' . $folder . '/' . $twig;
        $fichier2   = '../src/Mkk/PublicBundle/Resources/views/' . $folder . '/' . $twig;
        if (is_file($fichier1)) {
            $fichier = $fichier1;
        } elseif (is_file($fichier2)) {
            $fichier = $fichier2;
        }
        $fichier = str_replace(
            [
                '../src/',
                '/PublicBundle',
                '/Resources/views/',
                '/',
            ],
            [
                '',
                'PublicBundle',
                ':',
                ':',
            ],
            $fichier
        );

        return $fichier;
    }

    public function getBundlePublicRepertoire()
    {
        $bundle     = $this->getBundlePublic();
        $repertoire = str_replace('Public', '/Public', $bundle);

        return $repertoire;
    }

    public function getBundlePublic()
    {
        $bundle = $this->getBundle('PublicBundle');

        return $bundle;
    }

    public function getRoutes()
    {
        if (! isset($this->listroutes)) {
            $this->listroutes = [];
            $router           = $this->get('router');
            foreach ($router->getRouteCollection()->all() as $name => $route) {
                $pattern = $route->getPath();
                foreach ($route->getDefaults() as $code => $val) {
                    if ($code != '_controller') {
                        $pattern = str_replace('{' . $code . '}', $val, $pattern);
                    }
                }

                if (substr($name, 0, 1) != '_' && substr_count($name, '_') != 0) {
                    $this->listroutes[$name] = $pattern;
                }
            }
        }

        return $this->listroutes;
    }

    public function arrayTabCategorie($categories, $tab = [])
    {
        if (is_array($categories)) {
            foreach ($categories as $categorie) {
                $tab = $this->arrayTabCategorie($categorie, $tab);
            }

            ksort($tab);

            return $tab;
        }

        $methods = get_class_methods($categories);
        if (in_array('getChild', $methods)) {
            $id       = $categories->getId();
            $tab[$id] = $id;
            if (count($categories->getChild()) != 0) {
                foreach ($categories->getChild() as $child) {
                    $id       = $child->getId();
                    $tab[$id] = $id;
                    $tab      = $this->arrayTabCategorie($child, $tab);
                }
            }
        }

        ksort($tab);

        return $tab;
    }

    public function debugAdmin()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($this->get('kernel')->isDebug() || ($user != 'anon.' && $user->getRefGroup()->getCode() == 'superadmin')) {
            return true;
        }

        return false;
    }

    public function sendmail($mail, $fichiers = [])
    {
        $param = $this->get('mkk.param_service')->listing();
        if (isset($param['signature'])) {
            $content = $mail->getBody();
            $content = str_replace('%signature%', $param['signature'], $content);
            $mail->setBody($content);
        }

        if (isset($param['emailsite']) && $param['emailsite'] != '') {
            list($codesite, $domainsite) = explode('@', $param['emailsite']);
            unset($codesite);
            $from    = $mail->getFrom();
            $changer = 0;
            if ($from != '') {
                if (count($from) == 1) {
                    foreach ($from as $email => $nom) {
                        list($codefrom, $domainfrom) = explode('@', $email);
                        unset($codefrom);
                        if ($domainfrom != $domainsite) {
                            $changer = 1;
                        }
                    }
                }
            } else {
                $changer = 1;
            }

            if ($changer == 1) {
                $mail->setFrom($param['emailsite']);
            }
        }

        if (isset($param['emailreply']) && $param['emailreply'] != '') {
            if ($mail->getReplyTo() == '') {
                $mail->setReplyTo($param['emailreply']);
            }
        }

        if (count($fichiers) != 0) {
            foreach ($fichiers as $file => $nom) {
                $attachment = \Swift_Attachment::fromPath($file)->setFilename($nom);
                $mail->attach($attachment);
            }
        }

        $mail->setContentType('text/html');
        $this->get('mailer')->send($mail);
    }

    public function getUrlSite($request)
    {
        $url = $request->getSchemeAndHttpHost() . $this->get('router')->getContext()->getBaseUrl();

        return $url;
    }

    public function getBundle($type = '')
    {
        $fichier = get_class($this);
        $bundle  = str_replace(
            ['AdminBundle', 'PublicBundle', '\\'],
            ['SiteBundle', 'SiteBundle', ''],
            substr($fichier, 0, strpos($fichier, "\Controller"))
        );
        if (! defined('BUNDLESITE')) {
            define('BUNDLESITE', $bundle);
        }

        if (! defined('BUNDLEU')) {
            define('BUNDLEU', str_replace(['AdminBundle', 'PublicBundle', 'SiteBundle'], '', $bundle));
        }

        if ($type != '') {
            $bundle = str_replace('SiteBundle', $type, $bundle);
        }

        return $bundle;
    }

    public function cpy($source, $dest)
    {
        if (is_dir($source)) {
            $dirHandle = opendir($source);
            while ($file = readdir($dirHandle)) {
                if ($file != '.' && $file != '..') {
                    if (is_dir($source . '/' . $file)) {
                        if (! is_dir($dest . '/' . $file)) {
                            mkdir($dest . '/' . $file);
                        }

                        $this->cpy($source . '/' . $file, $dest . '/' . $file);
                    } else {
                        copy($source . '/' . $file, $dest . '/' . $file);
                    }
                }
            }

            closedir($dirHandle);
        } else {
            copy($source, $dest);
        }
    }

    public function VerifRecaptcha($request)
    {
        $param     = $this->get('mkk.param_service')->listing();
        $recaptcha = 0;
        $post      = $request->request->all();
        if (isset($post['g-recaptcha-response'])) {
            $recaptcha = 1;
        }

        if (isset($param['recaptcha_clef']) && $param['recaptcha_clef'] != '') {
            $recaptcha = $recaptcha + 1;
        }

        if (isset($param['recaptcha_secret']) && $param['recaptcha_secret'] != '') {
            $recaptcha = $recaptcha + 1;
        }

        if ($recaptcha == 3) {
            $url     = 'https://www.google.com/recaptcha/api/siteverify?secret=';
            $url     = $url . $post['g-recaptcha-response'];
            $url     = $url . '&response=' . $request->request->get('g-recaptcha-response');
            $url     = $url . '&remoteip=' . $request->server->get('REMOTE_ADDR');
            $content = file_get_contents($url);
            $json    = json_decode($content, true);
            if ($json['success'] == false) {
                return false;
            }
        }

        return true;
    }

    public function isPostAjax($request)
    {
        $resultat = ($request->isXmlHttpRequest() && $request->isMethod('POST') && $this->VerifRecaptcha($request));

        return $resultat;
    }

    public function getMetatagsArianeLienActuel($parameters)
    {
        $tab = explode(',', 'metatags,ariane,lienactuel');
        foreach ($tab as $id) {
            if ((! isset($parameters[$id]) || count($parameters[$id]) == 0) && isset($this->datasite[$id])) {
                $parameters[$id] = $this->datasite[$id];
            } else {
                if (isset($parameters[$id]) && is_array($parameters[$id]) && isset($this->datasite[$id])) {
                    foreach ($parameters[$id] as $key => $val) {
                        if ($val == '' && isset($this->datasite[$id][$key])) {
                            $parameters[$id][$key] = $this->datasite[$id][$key];
                        }
                    }
                }
            }
        }

        if (! isset($parameters['lienactuel'])) {
            $parameters['lienactuel'] = [];
        }

        return $parameters;
    }

    public function renderMkk($request, $view, array $parameters = [], Response $response = null)
    {
        if ($this->has('breadcrumb')) {
            $parameters['breadcrumb'] = $this->get('breadcrumb');
        }

        if ($this->has('paginator')) {
            $parameters['paginator'] = $this->get('paginator');
        }

        if ($this->has('translations')) {
            $parameters['translations'] = $this->get('translations');
        }

        $parameters               = $this->initPage($request, $parameters);
        $parameters['controller'] = $this;

        if ($this->get('kernel')->isDebug()) {
            $last                = end($parameters['ariane']);
            $parameters['titre'] = $last['text'];
            if (! isset($parameters['titre']) || $parameters['titre'] == '') {
                $parameters['titre'] = 'Sans titre :D';
            }
        }

        $parameters['metatags']['og:site_name'] = $parameters['titresite'];
        if (isset($parameters['twitter_url'])) {
            $twitterUrl = $parameters['twitter_url'];
            $twitterUrl = '@' . substr($twitterUrl, strrpos($twitterUrl, '/') + 1);

            $parameters['metatags']['og:site'] = $twitterUrl;
        }

        $twigHtml = $this->render($view, $parameters, $response);

        return $twigHtml;
    }

    public function setMetatagsArianeLienActuel($request)
    {
        $tab       = ['metatags', 'lienactuel', 'ariane'];
        $continuer = 1;
        foreach ($tab as $key) {
            if (isset($this->datasite[$key])) {
                $continuer = 0;
                break;
            }
        }

        if ($continuer) {
            $this->datasite['metatags'] = [
                'titre'       => '',
                'description' => '',
                'keywords'    => '',
            ];

            $this->datasite['ariane'] = [];
            $em                       = $this->getDoctrine()->getManager();
            if (! $request->isXmlHttpRequest()) {
                $tab = ['titre'];

                $param = $this->get('mkk.param_service')->listing();
                foreach ($tab as $code) {
                    if (isset($param['meta_' . $code])) {
                        $this->datasite['metatags'][$code] = $param['meta_' . $code];
                    }
                }

                $route = $request->attributes->get('_route');

                $tab                 = [
                    $route,
                    str_replace('page', 'index', $route),
                    substr($route, 0, strpos($route, '_')) . '_index',
                ];
                $continuer           = 0;
                list($module, $view) = explode('_', $route);
                unset($view);
                try {
                    $em     = $this->getDoctrine()->getManager();
                    $bundle = $this->getBundle();

                    $param = [
                        'module' => $module,
                        'route'  => $route,
                    ];

                    $sql    = 'SELECT meta FROM ' . $bundle . ':Metariane meta WHERE';
                    $sql    = $sql . ' meta.route=:module OR meta.route=:route ORDER BY meta.route ASC';
                    $query  = $em->createQuery($sql)->setParameters($param);
                    $result = $query->getResult();
                    foreach ($result as $metariane) {
                        $titre = $metariane->getTitre();
                        if ($titre != '') {
                            $this->datasite['metatags']['titre'] = $titre;
                        }

                        $description = $metariane->getDescription();
                        if ($description != '') {
                            $this->datasite['metatags']['description'] = $description;
                        }

                        $keywords = $metariane->getKeywords();
                        if ($keywords != '') {
                            $this->datasite['metatags']['keywords'] = $keywords;
                        }

                        $ariane = $metariane->getAriane();
                        if ($ariane != '') {
                            $this->datasite['ariane'] = [
                                [
                                    'text' => $ariane,
                                ],
                            ];
                        }
                    }
                } catch (Exception $e) {
                }
            }
        }
    }

    public function accesDroitAdmin($request)
    {
        $this->setMetatagsArianeLienActuel($request);

        $this->params = $this->get('mkk.param_service')->listing();
    }

    public function arrayTabEtablissement($user, $tab = [])
    {
        $methods = get_class_methods($user);
        if (in_array('getEtablissements', $methods)) {
            if (count($user->getEtablissements()) != 0) {
                foreach ($user->getEtablissements() as $etablissement) {
                    $id       = $etablissement->getId();
                    $tab[$id] = $id;
                    $tab      = $this->arrayTabEtablissement($etablissement, $tab);
                }
            }
        } elseif (in_array('getChild', $methods)) {
            if (count($user->getChild()) != 0) {
                foreach ($user->getChild() as $child) {
                    $id       = $child->getId();
                    $tab[$id] = $id;
                    $tab      = $this->arrayTabEtablissement($child, $tab);
                }
            }
        }
        ksort($tab);

        return $tab;
    }

    private function getDataGroupUser($token)
    {
        $groupManager          = $this->get('bdd.group_manager');
        $groupRepository       = $groupManager->getRepository();
        $param                 = [];
        $param['visiteur']     = 'visiteur';
        $param['superadmin']   = 'superadmin';
        $dataGroup             = $groupRepository->getGroupSuperVisiteur($param);
        $this->groupvisiteur   = $dataGroup['visiteur'];
        $this->groupsuperadmin = $dataGroup['superadmin'];

        if ($token == null) {
            $idgroup = $this->groupvisiteur->getId();
        } else {
            $user = $token->getUser();
            if (is_string($user)) {
                $idgroup = $this->groupvisiteur->getId();
            } else {
                $idgroup = $user->getRefGroup()->getId();
            }
        }

        return $idgroup;
    }
}
