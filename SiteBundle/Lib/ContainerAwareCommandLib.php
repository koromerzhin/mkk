<?php

namespace Mkk\SiteBundle\Lib;

use Imagine;
use Sensio\Bundle\GeneratorBundle\Command\Helper\QuestionHelper;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class ContainerAwareCommandLib extends ContainerAwareCommand
{
    public function executeCommand($code, InputInterface $input, OutputInterface $output)
    {
        $application = $this->getApplication();
        $command     = $application->find($code);
        $command->run($input, $output);
    }

    public function getService($container)
    {
        $tab = $container->getServiceIds();
        foreach ($tab as $key) {
            $this->testgetService($key);
        }
    }

    protected function getQuestionHelper()
    {
        $question = $this->getHelperSet()->get('question');
        if (! $question || get_class($question) !== 'Sensio\Bundle\GeneratorBundle\Command\Helper\QuestionHelper') {
            $this->getHelperSet()->set($question = new QuestionHelper());
        }

        return $question;
    }

    protected function redimensionImage($dossier, $tab)
    {
        $imagine = new Imagine\Gd\Imagine();
        $dir     = glob('web/fichiers/' . $dossier . '/*.*');
        foreach ($dir as $fichier) {
            $this->foreachredimensionImage($imagine, $fichier, $tab);
        }
    }

    /**
     * Génère la liste des routes.
     *
     * @param mixed $listroute
     *
     * @return array
     */
    protected function generateRoute($listroute)
    {
        $routes = [];

        foreach (array_keys($listroute) as $tab) {
            list($module, $action) = explode('_', $tab);
            if ($module != 'besearch'
                && substr_count($action, 'search') == 0
                && substr_count($action, 'upload') == 0
                && substr_count($action, 'vider') == 0
                && substr_count($action, 'page') == 0
            ) {
                if (! isset($routes[$module])) {
                    $routes[$module] = '';
                } else {
                    $routes[$module] = $routes[$module] . ',';
                }

                $routes[$module] = $routes[$module] . $action;
            }
        }

        $list   = $routes;
        $routes = [];
        foreach ($list as $module => $action) {
            $routes[] = [
                'module' => $module,
                'action' => explode(',', $action),
            ];
        }

        ksort($routes);

        return $routes;
    }

    private function foreachredimensionImage($imagine, $fichier, $tab)
    {
        $image     = $imagine->open($fichier);
        $size      = $image->getSize();
        $maxWidth  = $imgWidth   = $size->getWidth();
        $maxHeight = $imgHeight = $size->getHeight();
        if (! empty($tab['max_width']['val']) && $tab['max_width']['val'] != 0) {
            $maxWidth = $tab['max_width']['val'];
        }

        if (! empty($tab['max_height']['val']) && $tab['max_height']['val'] != 0) {
            $maxHeight = $tab['max_height']['val'];
        }

        if ($imgWidth > $maxWidth || $imgHeight > $maxHeight) {
            if (($imgWidth / $imgHeight) >= ($maxWidth / $maxHeight)) {
                $newWidth  = $imgWidth / ($imgHeight / $maxHeight);
                $newHeight = $maxHeight;
            } else {
                $newWidth  = $maxWidth;
                $newHeight = $imgHeight / ($imgWidth / $maxWidth);
            }

            $size = new Imagine\Image\Box($newWidth, $newHeight);
            $image->resize($size);
            $image->save($fichier);
        }
    }

    private function testgetService($key)
    {
        if (substr_count($key, 'bdd.') != 0) {
            $this->traiterService($key);
        }
    }

    private function traiterService($key)
    {
        $container        = $this->getContainer();
        $entityManager    = $container->get($key);
        $entityRepository = $entityManager->getRepository();
        if ($entityRepository) {
            $entity = $entityManager->getTable();
            $class  = new $entity();
            $class  = get_class($class);
        }
    }
}
