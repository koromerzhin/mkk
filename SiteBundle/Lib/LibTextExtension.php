<?php

namespace Mkk\SiteBundle\Lib;

abstract class LibTextExtension extends \Twig_Extension
{
    public function menuTraiter($listemenu, $code)
    {
        $tab = [];
        foreach ($listemenu as $row) {
            if ($row['data']['clef'] == $code) {
                $tab = $row;
            }
        }

        return $tab;
    }

    public function md5($string)
    {
        return md5($string);
    }

    public function getName()
    {
        return 'mkk';
    }

    public function boolean($val)
    {
        if ($val) {
            $data = 1;
        } else {
            $data = 0;
        }

        return '{boolean}' . $data . '{/boolean}';
    }

    public function day($temps)
    {
        if (is_object($temps)) {
            $timestamp = $temps->getTimestamp();
        } else {
            $timestamp = $temps;
        }
        // d/m/Y
        return '{day}' . $timestamp . '{/day}';
    }

    public function temps($temps)
    {
        if (is_object($temps)) {
            $timestamp = $temps->getTimestamp();
        } else {
            $timestamp = $temps;
        }
        // d/m y H:i:s
        return '{temps}' . $timestamp . '{/temps}';
    }

    public function heure($temps)
    {
        if (is_object($temps)) {
            $timestamp = $temps->getTimestamp();
        } else {
            $timestamp = $temps;
        }
        // H:s
        return '{heure}' . $timestamp . '{/heure}';
    }

    public function mustache($mot)
    {
        return '{{' . $mot . '}}';
    }

    public function parseTel($chiffre)
    {
        $numero = str_replace([' ', '-', '.'], '', $chiffre);
        $json   = $this->verifTel($numero);
        if (! isset($json['num'])) {
            $json['num'] = $numero;
        }

        return $json['num'];
    }

    public function verifierMime($mime, $verifier)
    {
        $etat = false;
        if (substr_count($mime, $verifier) != 0) {
            $etat = true;
        }

        return $etat;
    }

    public function lienActuel($code, $lienactuel)
    {
        if (in_array($code, $lienactuel)) {
            return true;
        }

        return false;
    }

    public function actifMenu($listgroup, $group)
    {
        $data = explode(',', $listgroup);
        if (in_array($group, $data)) {
            $etat = 1;
        } else {
            $etat = 0;
        }

        return $etat;
    }

    public function afficherEmail($users)
    {
        $texte = '';
        foreach ($users as $user) {
            if (filter_var($user->getEmail(), FILTER_VALIDATE_EMAIL)) {
                if ($texte != '') {
                    $texte = $texte . ',';
                }
                $texte = $texte . $user->getEmail();
            }
        }

        return $texte;
    }

    public function imgActifDesactifDroit($action, $module, $group, $data)
    {
        $actif    = 'success';
        $desactif = 'danger';
        $text     = $desactif;
        foreach ($data as $row) {
            if ($row->getModule() == $module && $row->getRefGroup()->getId() == $group) {
                $list = explode(',', $row->getCodes());
                if (in_array($action, $list)) {
                    $text = $actif;
                }
                break;
            }
        }
        if ($action == 'all') {
            $text = 'info';
        }

        return $text;
    }

    public function imgActifDesactif($listgroup, $group)
    {
        $actif    = 'success';
        $desactif = 'danger';
        $text     = $desactif;
        $data     = explode(',', $listgroup);
        if (in_array($group, $data)) {
            $text = $actif;
        } else {
            $text = $desactif;
        }

        return $text;
    }

    public function joursemaine($i)
    {
        $texte = '';
        switch ($i) {
            case 1:
                $texte = 'lundi';
break;
            case 2:
                $texte = 'mardi';
break;
            case 3:
                $texte = 'mercredi';
break;
            case 4:
                $texte = 'jeudi';
break;
            case 5:
                $texte = 'vendredi';
break;
            case 6:
                $texte = 'samedi';
break;
            case 7:
                $texte = 'dimanche';
break;
        }

        return $texte;
    }

    public function afficherPlan($plan)
    {
        ob_start();
        if (count($plan) != 0) {
            ?><ul><?php
foreach ($plan as $lien) {
                if (isset($lien['url']) || (isset($lien['enfant']) && count($lien['enfant']) != 0)) {
                    ?><li><?php if (isset($lien['url'])) {
                        ?><a href="<?php echo $lien['url']; ?>"><?php
                    } ?><?php echo $lien['libelle']; ?><?php
if (isset($lien['description'])) {
                        echo ' ' . $lien['description'];
                    }
                    if (isset($lien['url'])) {
                        ?></a><?php
                    }
                    if (isset($lien['enfant']) && count($lien['enfant']) != 0) {
                        echo $this->afficherPlan($lien['enfant']);
                    } ?></li><?php
                }
            } ?></ul><?php
        }
        $codehtml = ob_get_contents();
        ob_end_clean();

        return $codehtml;
    }

    public function affichageBase64($data, $tab = [])
    {
        foreach ($tab as $key => $value) {
            $data[$key] = $value;
        }

        return base64_encode(json_encode($data));
    }

    public function afficherAdresseSansPays($data)
    {
        $texte = '<adress>';
        if ($data->getAdresseInfo() != '') {
            $texte = $texte . str_replace("\n", '<br />', $data->getAdresseInfo()) . '<br/>';
        }
        $cp    = $data->getAdresseCp();
        $ville = $data->getAdresseVille();
        $texte = $texte . $cp . ' ' . $ville;
        $texte = $texte . '</address>';

        return $texte;
    }

    public function affichageAriane($ariane)
    {
        $affichage = '';
        $i         = 0;
        foreach ($ariane as $tab) {
            if ($affichage == '') {
                $affichage = '<ul class="breadcrumb">';
            }
            if ($i == count($ariane) - 1) {
                $affichage = $affichage . '<li class="actif">' . $tab['text'] . '</li>';
            } else {
                $affichage = $affichage . '<li><a href="' . $tab['url'] . '">' . $tab['text'] . '</a> <span class="divider">/</span></li>';
            }
            ++$i;
        }
        if ($affichage != '') {
            $affichage = $affichage . '</ul>';
        }

        return $affichage;
    }

    public function traitementData($input)
    {
        print_r($input);
    }

    public function inputMeta($form)
    {
        $input = [];
        foreach ($form->getChildren() as $key => $child) {
            if (substr_count($child->get('name'), 'meta_') != 0) {
                $input[$key] = $child;
            }
        }

        return $input;
    }

    public function getTypeInput($input)
    {
        $variable = $input->getVars();
        $data     = $variable['block_prefixes'];
        $type     = $data[2];
        if ($type == 'text') {
            if (count($data) == 5 && $data[3] == 'textarea') {
                $type = 'textarea';
            }
        }

        return $type;
    }

    public function actifMenuChild($child, $group)
    {
        $retour = 0;
        foreach ($child as $menu) {
            $actif = $this->actifMenu($menu->getRefGroup(), $group);
            if ($actif == 1) {
                $retour = 1;
                break;
            }
        }

        return $retour;
    }

    public function jsonDecode($code)
    {
        $tab = json_decode($code, true);

        return $tab;
    }

    public function jsonEncode($code)
    {
        $tab = json_encode($code);

        return $tab;
    }

    public function filegetContent($file)
    {
        $folder = '';
        if (is_dir('web')) {
            $folder = 'web/';
        }

        if (is_file($folder . $file)) {
            return file_get_contents($folder . $file);
        }

        return '';
    }

    public function hexdec($hex)
    {
        $hex = str_replace('#', '', $hex);
        if ($hex == '00FF') {
            $hex = '0000FF';
        }

        if ($hex == 'FF00') {
            $hex = 'FF00';
        }

        if (strlen($hex) == 3) {
            $r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
            $g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
            $b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
        } else {
            $r = hexdec(substr($hex, 0, 2));
            $g = hexdec(substr($hex, 2, 2));
            $b = hexdec(substr($hex, 4, 2));
        }

        $rgb = [$r, $g, $b];
        //return implode(",", $rgb); // returns the rgb values separated by commas
        return json_encode($rgb); // returns an array with the rgb values
    }

    public function isFileImage($file)
    {
        $trouver = 0;
        $folder  = '';
        if (is_dir('web')) {
            $folder = 'web/';
        }

        $isfile = is_file($folder . $file);
        if ($isfile) {
            $mime = mime_content_type($folder . $file);
            if (substr_count($mime, 'image/') != 0) {
                $taille = getimagesize($folder . $file);
                if (isset($taille[0])) {
                    $trouver = 1;
                }
            }
        }

        return $trouver;
    }

    public function isFile($file)
    {
        $folder = '';
        if (is_dir('web')) {
            $folder = 'web/';
        }

        $isfile = is_file($folder . $file);

        return $isfile;
    }

    public function getImageSize($filename)
    {
        $tab           = [];
        $imageSize     = getimagesize($filename);
        $tab['width']  = $imageSize[0];
        $tab['height'] = $imageSize[1];

        return $tab;
    }

    public function findTranslations($entity, $translations)
    {
        $tab = $translations->findTranslations($entity);

        return $tab;
    }

    public function getAllMethods($entity)
    {
        $methods = get_class_methods($entity);
        print_r($methods);
    }

    public function getMethods($entity, $method)
    {
        $text    = '';
        $methods = get_class_methods($entity);
        foreach ($methods as $row) {
            $row = strtolower($row);
            if ($row == 'get' . $method) {
                $text = $entity->$row();
                break;
            }
        }

        return $text;
    }

    public function substrCount($str, $char)
    {
        return substr_count($str, $char);
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('boolean', [$this, 'boolean']),
            new \Twig_SimpleFilter('day', [$this, 'day']),
            new \Twig_SimpleFilter('temps', [$this, 'temps']),
            new \Twig_SimpleFilter('heure', [$this, 'heure']),
            new \Twig_SimpleFilter('findTranslations', [$this, 'findTranslations']),
            new \Twig_SimpleFilter('getAllMethods', [$this, 'getAllMethods']),
            new \Twig_SimpleFilter('substrCount', [$this, 'substrCount']),
            new \Twig_SimpleFilter('md5', [$this, 'md5']),
            new \Twig_SimpleFilter('hexdec', [$this, 'hexdec']),
            new \Twig_SimpleFilter('isFileImage', [$this, 'isFileImage']),
            new \Twig_SimpleFilter('isFile', [$this, 'isFile']),
            new \Twig_SimpleFilter('getMethods', [$this, 'getMethods']),
            new \Twig_SimpleFilter('filegetContent', [$this, 'filegetContent']),
            new \Twig_SimpleFilter('menuTraiter', [$this, 'menuTraiter']),
            new \Twig_SimpleFilter('jsonDecode', [$this, 'jsonDecode']),
            new \Twig_SimpleFilter('jsonEncode', [$this, 'jsonEncode']),
            new \Twig_SimpleFilter('mustache', [$this, 'mustache']),
            new \Twig_SimpleFilter('parseTel', [$this, 'parseTel']),
            new \Twig_SimpleFilter('verifierMime', [$this, 'verifierMime']),
            new \Twig_SimpleFilter('lienActuel', [$this, 'lienActuel']),
            new \Twig_SimpleFilter('actifMenu', [$this, 'actifMenu']),
            new \Twig_SimpleFilter('afficherEmail', [$this, 'afficherEmail']),
            new \Twig_SimpleFilter('imgActifDesactif', [$this, 'imgActifDesactif']),
            new \Twig_SimpleFilter('imgActifDesactifDroit', [$this, 'imgActifDesactifDroit']),
            new \Twig_SimpleFilter('joursemaine', [$this, 'joursemaine']),
            new \Twig_SimpleFilter('afficherPlan', [$this, 'afficherPlan']),
            new \Twig_SimpleFilter('affichageBase64', [$this, 'affichageBase64']),
            new \Twig_SimpleFilter('afficherAdresseSansPays', [$this, 'afficherAdresseSansPays']),
            new \Twig_SimpleFilter('affichageAriane', [$this, 'affichageAriane']),
            new \Twig_SimpleFilter('traitementData', [$this, 'traitementData']),
            new \Twig_SimpleFilter('getTypeInput', [$this, 'getTypeInput']),
            new \Twig_SimpleFilter('inputMeta', [$this, 'inputMeta']),
            new \Twig_SimpleFilter('actifMenu', [$this, 'actifMenu']),
            new \Twig_SimpleFilter('actifMenuChild', [$this, 'actifMenuChild']),
            new \Twig_SimpleFilter('getImageSize', [$this, 'getImageSize']),
        ];
    }

    /**
     * Initialise le tableau pour getFilters.
     *
     * @param array $tab    tableau des anciennes fonctions twig
     * @param array $newtab tableau des nouvelles fonctions twig
     *
     * @return array
     */
    protected function setTabFilters($tab, $newtab)
    {
        foreach ($newtab as $val) {
            $code = $val->getName();
            if (! isset($tab[$code])) {
                $tab[$code] = $val;
            }
        }

        return $tab;
    }

    private function verifTel($numero)
    {
        $numero = str_replace([' ', '-', '.'], '', $numero);
        try {
            $phoneUtil   = \libphonenumber\PhoneNumberUtil::getInstance();
            $trouver     = 0;
            $numberProto = $phoneUtil->parseAndKeepRawInput($numero, 'FR');
            if ($phoneUtil->isViablePhoneNumber($numero)) {
                $trouver = 1;
            } else {
                $listepays = $phoneUtil->getSupportedRegions();
                foreach ($listepays as $pays) {
                    $numberProto = $phoneUtil->parseAndKeepRawInput($numero, $pays);
                    if ($phoneUtil->isViablePhoneNumber($numberProto)) {
                        $trouver = 1;
                        break;
                    }
                }
            }

            if ($trouver) {
                $json['country']       = $phoneUtil->getRegionCodeForNumber($numberProto);
                $json['international'] = $phoneUtil->format($numberProto, 0);
                $json['num']           = $phoneUtil->format($numberProto, 2);
                $json['type']          = $phoneUtil->getNumberType($numberProto);
            }
        } catch (\NumberParseException $e) {
            $json['error'] = $e->__toString();
        }

        if ($json['type'] == 1) {
            $json['icontypetel'] = 'glyphicon-phone';
            $json['type']        = 'mobile';
        } elseif ($json['type'] == 0 || $json['type'] == 6) {
            $json['icontypetel'] = 'glyphicon-phone-alt';
            $json['type']        = 'fixe';
        }

        if (isset($json['country']) && $json['country'] != '') {
            $json['iconpays'] = 'bundles/mkksite/img/country/' . $json['country'] . '.png';
        }

        return $json;
    }
}
