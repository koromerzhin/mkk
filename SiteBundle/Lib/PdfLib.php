<?php

namespace Mkk\SiteBundle\Lib;

use HTML2PDF;

class PdfLib extends HTML2PDF
{
    /**
     * tag : DIV
     * mode : OPEN
     * Celui qui modifie le nom de cette fonction n'aura pas compris
     * le principe de l'héritage et qu'il aille se faire foutre alors (bisou)
     * J'ai aussi pas l'intention de modifier la doc pour lui.
     *
     * @param array  $param
     * @param string $other name of tag that used the div tag
     *
     * @return bool
     */
    protected function _tag_open_DIV($param, $other = 'div')
    {
        // verifie la hauteur du PDF pour faire un retour a la ligne
        // ou afficher directement le tableau (d'après carine)

        if (isset($param['id']) && $param['id'] == 'RepereDevis') {
            $test1 = ($this->pdf->gety() > 0 && $this->pdf->gety() < 41);
            $test2 = ($this->pdf->gety() > 240 && $this->pdf->gety() < 273);
            if ($test1 || $test2) {
                $param['style']['height'] = '200mm';
            }
        } elseif (isset($param['id']) && $param['id'] == 'RepereCommande') {
            $mm = 74 + (4.5 * $param['nbreglement']);
            if ((283 - $this->pdf->gety()) < $mm) {
                $param['style']['height'] = (248 - $mm) . 'mm';
                $this->_setNewPage();
            }
        }

        $return = parent::_tag_open_DIV($param, $other);

        return $return;
    }
}
