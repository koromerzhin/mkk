<?php

namespace Mkk\SiteBundle\Lib;

use Doctrine\Common\EventSubscriber;

class LibDatabaseListener implements EventSubscriber
{
    protected $kernel;
    protected $container;
    protected $em;
    protected $entities;

    public function __construct($kernel)
    {
        $this->kernel    = $kernel;
        $this->container = $kernel->getContainer();
    }

    public function getSubscribedEvents()
    {
        return [
            'onFlush',
        ];
    }

    public function entityInstanceof($entity)
    {
        $retour = false;
        $total  = count($this->entities);
        if ($total != 0) {
            foreach ($this->entities as $row) {
                if (get_class($row) == $entity) {
                    $this->entity = $row;
                    $retour       = true;
                    break;
                }
            }
        }

        return $retour;
    }

    public function entityInstanceofDelete($entity)
    {
        $retour = false;
        $total  = count($this->delete);
        if ($total != 0) {
            foreach ($this->delete as $row) {
                if (get_class($row) == $entity) {
                    $this->entity = $row;
                    $retour       = true;
                    break;
                }
            }
        }

        return $retour;
    }

    public function entityInstanceofUpdate($entity)
    {
        $retour = false;
        $total  = count($this->update);
        if ($total != 0) {
            foreach ($this->update as $row) {
                if (get_class($row) == $entity) {
                    $this->entity = $row;
                    $retour       = true;
                    break;
                }
            }
        }

        return $retour;
    }

    public function setVarargs($args)
    {
        $em        = $args->getEntityManager();
        $this->em  = $em;
        $uow       = $em->getUnitOfWork();
        $this->uow = $uow;
        $update    = array_merge(
            $uow->getScheduledEntityInsertions(),
            $uow->getScheduledEntityUpdates()
        );

        $this->update = $update;
        $delete       = $uow->getScheduledEntityDeletions();
        $this->delete = $delete;
        $entities     = array_merge(
            $update,
            $delete
        );

        $this->entities = $entities;
    }
}
