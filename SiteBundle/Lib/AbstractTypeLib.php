<?php

namespace Mkk\SiteBundle\Lib;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;

abstract class AbstractTypeLib extends AbstractType
{
    protected $name       = '';
    protected $datachoice = [];
    protected $dataxml    = [];
    protected $data       = [];
    protected $marketik   = '';
    protected $em;
    protected $container;
    protected $params;

    public function __construct(ContainerInterface $container, EntityManager $em)
    {
        $this->em        = $em;
        $this->container = $container;
        $this->params    = $this->container->get('mkk.param_service')->listing();
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getBlockPrefix()
    {
        return 'denteal';
    }
}
