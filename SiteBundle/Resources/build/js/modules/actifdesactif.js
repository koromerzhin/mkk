
import { postXHRJson } from '../../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
export class ActifDesactif {
    Selection() {
        let selection = ''
        let checked   = $('input[type=\'checkbox\']:checked')
        $.each(
            checked,
            function () {
                if ($(this).val() != '' && $(this).val() != 'on' && $(this).closest('table').html() != undefined) {
                    if (selection != '') {
                        selection += ','
                    }
                    selection += $(this).val()
                }
            }
        )
    return selection
    }

    AllAllSelectOnClick() {
        // clic sur la case cocher/decocher
        let allchecked = $(this).is(':checked')
        let tbody      = $(this).closest('table').find('tbody')
        if (allchecked == true) {
            if ($('#BoutonActiver').length) {
                $('#BoutonActiver, #BoutonDesactiver').removeClass('disabled')
            }
            $(tbody).find('input[type=checkbox]').each(
                function () {
                    $(this).prop('checked', true)
                }
            )
        } else {
            if ($('#BoutonActiver').length) {
                $('#BoutonActiver, #BoutonDesactiver').addClass('disabled')
            }
            $(tbody).find('input[type=checkbox]').each(
                function () {
                    $(this).prop('checked', false)
                }
            )
        }
    }

    InputCheckboxOnClick() {
        let active    = $(this).is(':checked')
        let table     = $(this).closest('tbody')
        let selection = $(table).find('input[type=\'checkbox\']:checked')
        if (selection.length) {
            if ($('#BoutonActiver').length) {
                $('#BoutonActiver, #BoutonDesactiver').removeClass('disabled')
            }
        } else {
            if ($('#BoutonActiver').length) {
                $('#BoutonActiver, #BoutonDesactiver').addClass('disabled')
            }
        }
    }
    CheckedAll() {
        let parent = this
        $('[data-rel=\'allselect\']').each(
            function () {
                $(this).on("click", parent.AllAllSelectOnClick)
                if ($(this).attr('data-onselect') == undefined) {
                    let table = $(this).closest('table')
                    $(table).find('input[type=checkbox]').each(
                        function () {
                            $(this).prop('checked', false)
                        }
                    )
                }
            }
        )
    $('table>tbody').each(
            function () {
                $(this).find('input[type=\'checkbox\']').each(
                    function () {
                        $(this).on("click", parent.InputCheckboxOnClick)
                    }
                )
            }
        )
    }
    BoutonActiverOnClick(e) {
        let selection = this.Selection()
        if (selection != '') {
            $('#PopupActiver').modal('show')
        }
        e.preventDefault()
    }
    PopupActiverRelSaveOnClick() {
        let selection = this.Selection()
        this.Executer(selection, 1, $('#BoutonActiver').attr('href'))
    }
    PopupDesactifRelSaveOnClick() {
        let selection = this.Selection()
        this.Executer(selection, 0, $('#BoutonDesactiver').attr('href'))
    }
    BoutonDesactiverOnClick(e) {
        let selection = this.Selection()
        if (selection != '') {
            $('#PopupDesactiver').modal('show')
        }
        e.preventDefault()
    }
    constructor() {
        this.CheckedAll()
        $('#PopupActiver').find('[data-rel=\'save\']').on("click", this.PopupActiverRelSaveOnClick.bind(this))
        $('#BoutonActiver').on("click", this.BoutonActiverOnClick.bind(this))
        $('#PopupDesactiver').find('[data-rel=\'save\']').on("click", this.PopupDesactifRelSaveOnClick.bind(this))
        $('#BoutonDesactiver').on("click", this.BoutonDesactiverOnClick.bind(this))
    }
    async Executer(selection, etat, url) {
        let datapost = {
            etat: etat,
            selection: selection
        }
        let response = await postXHRJson(url, datapost)
        document.location.reload()
    }
}
