

import { postXHRJson } from '../xhr/postXHRJson'
class Telephone {
    constructor() {}
    tableau(e) {
        let collection = $(e).find(".CollectionRow")
        let parent     = this
        $(collection).each(
            function () {
                let row = this
                parent.row(row)
            }
        )
    }
    verifTel(e) {
        this.asyncVerifTel(e.currentTarget)
    }
    async asyncVerifTel(e) {
        let val = $(e).val()
        let tr  = $(e).closest("tr")
        if ($(tr).data('md5') == undefined) {
            let md5 = Math.random()
            $(tr).attr('data-md5', md5)
        }

        let md5      = $(tr).attr('data-md5')
        let datapost = {
            telephone: val,
            md5: md5
        }

        let fieldset = $(e).closest("fieldset")
        let url      = $(fieldset).data('route')
        let response = await postXHRJson(url, datapost, 0)
        tr           = $("tr[data-md5='" + md5 + "']")
        if ($(tr).length) {
            let input = $(tr).find(".InputTelChiffre:first")
            if (typeof response.iconpays != undefined) {
                let drapeau = $(tr).find('.drapeaupays:first')
                if (response.iconpays == 'glyphicon-question-sign') {
                    $(drapeau).attr('class', 'drapeaupays glyphicon ' + response.iconpays);
                } else {
                    $(tr).find('.InputTelPays:first').val(response.country)
                    $(drapeau).attr(
                        {
                            'class': '',
                            'src': response.iconpays
                        }
                    )
                }
                let typetel = $(tr).find('.typetel:first')
                $(typetel).attr('class', 'typetel glyphicon ' + response.icontypetel)
                $(tr).find('.InputTelType:first').val(response.icontypetel)
            }
        }
    }
    onkeyUp(event) {
        if (event.which == 13) {
            $(this).blur()
        }
    }
    row(e) {
        let type = $(e).find('.InputTelType:first')
        let span = $(e).find('.typetel:first')
        $(span).addClass($(type).val())
        let pays = $(e).find('.InputTelPays:first')
        let img  = $(e).find('.drapeaupays:first')
        if ($(pays).val() != '') {
            let image = $(img).data('src') + $(pays).val() + ".png"
            $(img).attr('src', image)
        }

        let chiffre       = $(e).find('.InputTelChiffre:first')
        let utilisation   = $(e).find('.InputTelUtilisation:first')
        let tagstelephone = []
        $(e).closest("fieldset").find('.TagsCollection').find('li').each(
            function () {
                tagstelephone.push($(this).html());
            }
        )
    $(utilisation).select2(
            {
                tags: tagstelephone,
                allowClear: true,
                placeholder: $(utilisation).attr('placeholder'),
                maximumSelectionSize: 1,
                containerCssClass: 'select2tel'
            }
        )
    $(chiffre).on('blur', this.verifTel.bind(this))
    //$(chiffre).on('onkeyup', this.onkeyUp)
    }
}
export { Telephone }
