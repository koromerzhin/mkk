

class Email {
    constructor() {}
    tableau(e) {
        let collection = $(e).find(".CollectionRow")
        let parent     = this
        $(collection).each(
            function () {
                let row = this
                parent.row(row)
            }
        )
    }
    row(e) {
        let utilisation = $(e).find('.InputEmailUtilisation:first')
        let tagsemail   = [];
        $(e).closest("fieldset").find('.TagsCollection').find('li').each(
            function () {
                tagsemail.push($(this).html())
            }
        )
    $(utilisation).select2(
            {
                tags: tagsemail,
                allowClear: true,
                placeholder: $(utilisation).attr('placeholder'),
                maximumSelectionSize: 1,
                containerCssClass: 'select2email'
            }
        )
    }
}
export { Email }
