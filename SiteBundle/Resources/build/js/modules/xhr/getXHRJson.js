
function getXHRJson(url)
{
    return new Promise(
        function (resolve, reject) {
            $.get(
                url,
                resolve,
                'json'
            )
        }
    )
}
export { getXHRJson }
