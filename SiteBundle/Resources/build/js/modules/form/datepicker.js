import 'jquery-ui/ui/i18n/datepicker-af'
import 'jquery-ui/ui/i18n/datepicker-ar'
import 'jquery-ui/ui/i18n/datepicker-ar-DZ'
import 'jquery-ui/ui/i18n/datepicker-az'
import 'jquery-ui/ui/i18n/datepicker-be'
import 'jquery-ui/ui/i18n/datepicker-bg'
import 'jquery-ui/ui/i18n/datepicker-bs'
import 'jquery-ui/ui/i18n/datepicker-ca'
import 'jquery-ui/ui/i18n/datepicker-cs'
import 'jquery-ui/ui/i18n/datepicker-cy-GB'
import 'jquery-ui/ui/i18n/datepicker-da'
import 'jquery-ui/ui/i18n/datepicker-de'
import 'jquery-ui/ui/i18n/datepicker-el'
import 'jquery-ui/ui/i18n/datepicker-en-AU'
import 'jquery-ui/ui/i18n/datepicker-en-GB'
import 'jquery-ui/ui/i18n/datepicker-en-NZ'
import 'jquery-ui/ui/i18n/datepicker-eo'
import 'jquery-ui/ui/i18n/datepicker-es'
import 'jquery-ui/ui/i18n/datepicker-et'
import 'jquery-ui/ui/i18n/datepicker-eu'
import 'jquery-ui/ui/i18n/datepicker-fa'
import 'jquery-ui/ui/i18n/datepicker-fi'
import 'jquery-ui/ui/i18n/datepicker-fo'
import 'jquery-ui/ui/i18n/datepicker-fr'
import 'jquery-ui/ui/i18n/datepicker-fr-CA'
import 'jquery-ui/ui/i18n/datepicker-fr-CH'
import 'jquery-ui/ui/i18n/datepicker-gl'
import 'jquery-ui/ui/i18n/datepicker-he'
import 'jquery-ui/ui/i18n/datepicker-hi'
import 'jquery-ui/ui/i18n/datepicker-hr'
import 'jquery-ui/ui/i18n/datepicker-hu'
import 'jquery-ui/ui/i18n/datepicker-hy'
import 'jquery-ui/ui/i18n/datepicker-id'
import 'jquery-ui/ui/i18n/datepicker-is'
import 'jquery-ui/ui/i18n/datepicker-it'
import 'jquery-ui/ui/i18n/datepicker-it-CH'
import 'jquery-ui/ui/i18n/datepicker-ja'
import 'jquery-ui/ui/i18n/datepicker-ka'
import 'jquery-ui/ui/i18n/datepicker-kk'
import 'jquery-ui/ui/i18n/datepicker-km'
import 'jquery-ui/ui/i18n/datepicker-ko'
import 'jquery-ui/ui/i18n/datepicker-ky'
import 'jquery-ui/ui/i18n/datepicker-lb'
import 'jquery-ui/ui/i18n/datepicker-lt'
import 'jquery-ui/ui/i18n/datepicker-lv'
import 'jquery-ui/ui/i18n/datepicker-mk'
import 'jquery-ui/ui/i18n/datepicker-ml'
import 'jquery-ui/ui/i18n/datepicker-ms'
import 'jquery-ui/ui/i18n/datepicker-nb'
import 'jquery-ui/ui/i18n/datepicker-nl'
import 'jquery-ui/ui/i18n/datepicker-nl-BE'
import 'jquery-ui/ui/i18n/datepicker-nn'
import 'jquery-ui/ui/i18n/datepicker-no'
import 'jquery-ui/ui/i18n/datepicker-pl'
import 'jquery-ui/ui/i18n/datepicker-pt'
import 'jquery-ui/ui/i18n/datepicker-pt-BR'
import 'jquery-ui/ui/i18n/datepicker-rm'
import 'jquery-ui/ui/i18n/datepicker-ro'
import 'jquery-ui/ui/i18n/datepicker-ru'
import 'jquery-ui/ui/i18n/datepicker-sk'
import 'jquery-ui/ui/i18n/datepicker-sl'
import 'jquery-ui/ui/i18n/datepicker-sq'
import 'jquery-ui/ui/i18n/datepicker-sr'
import 'jquery-ui/ui/i18n/datepicker-sr-SR'
import 'jquery-ui/ui/i18n/datepicker-sv'
import 'jquery-ui/ui/i18n/datepicker-ta'
import 'jquery-ui/ui/i18n/datepicker-th'
import 'jquery-ui/ui/i18n/datepicker-tj'
import 'jquery-ui/ui/i18n/datepicker-tr'
import 'jquery-ui/ui/i18n/datepicker-uk'
import 'jquery-ui/ui/i18n/datepicker-vi'
import 'jquery-ui/ui/i18n/datepicker-zh-CN'
import 'jquery-ui/ui/i18n/datepicker-zh-HK'
import 'jquery-ui/ui/i18n/datepicker-zh-TW'
export class DatePicker {
    constructor() {
        this.Timeout()
    }
    paques(year) {
        let a    = year % 19
        let b    = Math.floor(year / 100)
        let c    = year % 100
        let d    = Math.floor(b / 4)
        let e    = b % 4
        let f    = Math.floor((b + 8) / 25)
        let g    = Math.floor((b - f + 1) / 3)
        let h    = (19 * a + b - d - g + 15) % 30
        let i    = Math.floor(c / 4)
        let k    = c % 4
        let l    = (32 + 2 * e + 2 * i - h - k) % 7
        let m    = Math.floor((a + 11 * h + 22 * l) / 451)
        let n0   = (h + l + 7 * m + 114)
        let n    = Math.floor(n0 / 31) - 1
        let p    = n0 % 31 + 1
        let date = new Date(year, n, p)
        return date
    }
    VerifierJour(jour, country, ferier, joursemainedesactiver) {
        if (joursemainedesactiver != '') {
            let day = jour.getDay()
            tab     = joursemainedesactiver.split(',')
            for (a in tab) {
                if (tab[a] == day) {
                    return [false, 'holiday-weekend', '']
                }
            }
        }
        if (ferier) {
            year   = jour.getFullYear()
            paques = this.paques(year)
            jours  = new Array()
            jours.push(paques)
            jours.push(new Date(year, paques.getMonth(), paques.getDate() + 1))
            jours.push(new Date(year, paques.getMonth(), paques.getDate() + 39))
            jours.push(new Date(year, paques.getMonth(), paques.getDate() + 49))
            jours.push(new Date(year, paques.getMonth(), paques.getDate() + 50))
            jours.push(new Date(year, 0, 1))
            jours.push(new Date(year, 4, 1))
            jours.push(new Date(year, 4, 8))
            jours.push(new Date(year, 6, 14))
            jours.push(new Date(year, 7, 15))
            jours.push(new Date(year, 10, 1))
            jours.push(new Date(year, 10, 11))
            jours.push(new Date(year, 11, 25))
            for (a in jours) {
                testday = jours[a]
                if (jour.getMonth() == testday.getMonth()) {
                    if (jour.getDate() == testday.getDate()) {
                        return [false, 'holiday-ferier', '']
                    }
                }
            }
        }
        return [true]
    }
    CalendarMaxMin(locale, min, max) {
        let variable = $.datepicker.regional[locale]
        variable     = $.extend(
            variable,
            {
                yearRange: "c-100:c+0",
                showAnim: "slide",
                changeMonth: true,
                changeYear: true,
                showWeek: true,
                showOn: "focus",
                buttonText: '',
                dateFormat: 'dd/mm/yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: '',
                onSelect: function (selectedDate) {
                    let option = $(this).attr('id') == min ? "minDate" : "maxDate",
                    instance   = $(this).data("datepicker"),
                    date       = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings)
                    $("#" + min + ", #" + max).not(this).datepicker("option", option, date)
                }
            }
        )
    $("#" + min + ", #" + max).datepicker(variable)
    }
    Lancement(locale) {
        this.DateTimePicker(locale)
        this.InitDatePicker(locale)
        this.InputDateNaissance(locale)
    }
    InitDatePicker(locale) {
        let minmax   = []
        let variable = $.datepicker.regional[locale]
        variable     = $.extend(
            variable,
            {
                beforeShow: function (input) {
                    let modal = $(input).closest('.modal')
                    let zindex
                    if (modal.html() === undefined) {
                        zindex = 100
                    } else {
                        zindex = $(modal).css('z-index')
                    }
                    $(input).css(
                        {
                            position: 'relative',
                            'z-index': zindex + 100
                        }
                    )
                },
                yearRange: 'c-100:c+100',
                showAnim: 'slide',
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                showWeek: true,
                buttonText: '',
                buttonImageOnly: false,
                dateFormat: 'dd/mm/yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            }
        )
    parent = this
    $('.datepicker').each(
            function () {
                if ($(this).data('datepicker') != undefined) {
                    let md5 = $(this).data('md5')
                    if (minmax[md5] == undefined) {
                        minmax[md5] = {
                            min: '',
                            max: ''
                        }
                    }
                    let etat          = $(this).data('datepicker')
                    minmax[md5][etat] = $(this).attr('id')
                    if (minmax[md5].min != '' && minmax[md5].max != '') {
                        parent.CalendarMaxMin(locale, minmax[md5].min, minmax[md5].max)
                    }
                } else {
                    $(this).datepicker(variable)
                }
            }
        )
    }
    DateTimePicker(locale) {
        /*
        $('.HorairePicker').datetimepicker(
          {
            lang: locale,
            mask: true,
            datepicker: false,
            format: 'H:i'
          }
        )
        $('.HoraireAM,.HorairePM').each(
          function() {
            $(this).datetimepicker(
              {
                mask: true,
                datepicker: false,
                format: 'H:i'
              }
            )
          }
        )
        $(".datetimepicker").datetimepicker(
        {
          lang: locale,
          timepicker: true,
          format: 'd/m/Y H:i'
        }
        )
        */
    }
    Timeout() {
        let locale = $("html").attr('lang')
        if ($.datepicker !== undefined && $.datepicker.regional[locale] !== undefined) {
            this.Lancement(locale)
        }
    }
    InputDateNaissance(locale) {
        let variable = $.datepicker.regional[locale]
        variable     = $.extend(
            variable,
            {
                beforeShow: function (input) {
                    let modal = $(input).closest('.modal')
                    let zindex
                    if (modal.html() === undefined) {
                        zindex = 100
                    } else {
                        zindex = $(modal).css('z-index')
                    }
                    $(input).css(
                        {
                            position: 'relative',
                            'z-index': zindex + 100
                        }
                    )
                },
                yearRange: 'c-100:c+100',
                showAnim: 'slide',
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                showWeek: true,
                buttonText: '',
                buttonImageOnly: false,
                dateFormat: 'dd/mm/yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            }
        )
    $('.DateNaissance').datepicker(variable)
    }
}
