
import { postXHRJson } from '../../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
export class Supprimer {
    AllAllSelectOnClick() {
        // clic sur la case cocher/decocher
        let allchecked = $(this).is(':checked')
        let tbody      = $(this).closest('table').find('tbody')
        if (allchecked == true) {
            if ($('#BoutonSupprimer').length) {
                $('#BoutonSupprimer').removeClass('disabled')
            }
            $(tbody).find('input[type=checkbox]').each(
                function () {
                    $(this).prop('checked', true)
                }
            )
        } else {
            if ($('#BoutonSupprimer').length) {
                $('#BoutonSupprimer').addClass('disabled')
            }
            $(tbody).find('input[type=checkbox]').each(
                function () {
                    $(this).prop('checked', false)
                }
            )
        }
    }

    InputCheckboxOnClick() {
        let active    = $(this).is(':checked')
        let table     = $(this).closest('tbody')
        let selection = $(table).find('input[type=\'checkbox\']:checked')
        if (selection.length) {
            if ($('#BoutonSupprimer').length) {
                $('#BoutonSupprimer').removeClass('disabled')
            }
        } else {
            if ($('#BoutonSupprimer').length) {
                $('#BoutonSupprimer').addClass('disabled')
            }
        }
    }
    CheckedAll() {
        let parent = this
        $('[data-rel=\'allselect\']').each(
            function () {
                $(this).on("click", parent.AllAllSelectOnClick)
                if ($(this).attr('data-onselect') == undefined) {
                    let table = $(this).closest('table')
                    $(table).find('input[type=checkbox]').each(
                        function () {
                            $(this).prop('checked', false)
                        }
                    )
                }
            }
        )
    $('table>tbody').each(
            function () {
                $(this).find('input[type=\'checkbox\']').each(
                    function () {
                        $(this).on("click", parent.InputCheckboxOnClick)
                    }
                )
            }
        )
    }
    Selection() {
        let selection = ''
        let checked   = $('input[type=\'checkbox\']:checked')
        $.each(
            checked,
            function () {
                if ($(this).val() != '' && $(this).val() != 'on' && $(this).closest('table').html() != undefined) {
                    if (selection != '') {
                        selection += ','
                    }
                    selection += $(this).val()
                }
            }
        )
    return selection
    }
    PopupSupprimerRelSaveOnClick() {
        $('#PopupSupprimer').modal('hide')
        let selection = this.Selection()
        this.Executer(selection, $('#BoutonSupprimer').attr('href'))
    }
    PopupViderRelSaveOnClick(e) {
        $('#PopupVider').modal('hide')
        this.Executer({}, $('#BoutonVider').attr('href'))
        e.preventDefault()
    }
    BoutonSupprimerOnClick(e) {
        let selection = this.Selection()
        if (selection != '') {
            $('#PopupSupprimer').modal('show')
        }
        e.preventDefault()
    }
    BoutonViderOnClick(e) {
        $('#PopupVider').modal('show')
        e.preventDefault()
    }
    async Executer(selection, url) {
        let datapost = {
            selection: selection
        }
        let response = await postXHRJson(url, datapost)
    }
    constructor() {
        this.CheckedAll()
        $('#PopupSupprimer').find('[data-rel=\'save\']').on("click", this.PopupSupprimerRelSaveOnClick.bind(this))
        $('#PopupVider').find('[data-rel=\'save\']').on("click", this.PopupViderRelSaveOnClick.bind(this))
        $('#BoutonSupprimer').on("click", this.BoutonSupprimerOnClick.bind(this))
        $('#BoutonVider').on("click", this.BoutonViderOnClick)
    }
}
