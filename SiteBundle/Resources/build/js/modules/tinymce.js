import { postXHRJson } from '../../../../../SiteBundle/Resources/build/js/modules/xhr/postXHRJson'
class Tinymce {
    async sendTinymce(url, datapost) {
        if (this.recupererurl == 0) {
            let datatinymce   = await postXHRJson(url, datapost, 0)
            this.recupererurl = 1;
            this.datatinymce  = datatinymce;
            this.Remplir(datatinymce)
        } else {
            this.Remplir(this.datatinymce)
        }
    }
    Remplir(datatinymce) {
        let data = {
            selector: 'textarea',
            theme: 'modern',
            image_advtab: true,
            menubar: 'edit insert view tools',
            selector: '.mceEditor',
            relative_urls: true,
            remove_script_host: false,
            init_instance_callback: function (ed) {
                if ($('#' + ed.id).attr('maxlength')) {
                    let maxlength = $('#' + ed.id).attr('maxlength')
                    let total     = (maxlength - ed.getContent(
                        {
                            format: 'text'
                        }
                    ).length)
                    let statusbar = $(ed.getContainer()).find('.mce-statusbar').find('div')
                    statusbar.find('.mce-path').after($('.mce-lettercount'))
                    statusbar.find('.mce-lettercount').find('span').html(total)
                    if (total < 0) {
                              statusbar.find('.mce-lettercount').addClass('negatif')
                    }
                    ed.on(
                        'keyup',
                        function () {
                              let total = (maxlength - ed.getContent(
                                  {
                                        format: 'text'
                                  }
                              ).length)
                            if (total < 0) {
                                            statusbar.find('.mce-lettercount').addClass('negatif')
                            } else {
                                            statusbar.find('.mce-lettercount').removeClass('negatif')
                            }
                                statusbar.find('.mce-lettercount').find('span').html(total)
                        }
                    )
                }
            }
        }
        data = $.extend(data, datatinymce, {})
        tinyMCE.init(data)
    }
    constructor() {
        this.recupererurl = 0;
        this.datatinymce  = 0;
        this.setTinymce()
    }
    setTinymce() {
        if ($(".mceEditor").length != 0) {
            let textarea = $(".mceEditor")
            let url      = $(textarea[0]).data('url')
            let id       = $(textarea[0]).attr('id')
            let css      = $(textarea[0]).data('css')
            let form     = $(textarea[0]).closest('form').attr('id')
            let module   = $('body').data('module')
            let route    = $('body').data('route')
            let datapost = {
                'language': $("html").attr('lang'),
                'css': css,
                'url': url,
                'id': id,
                'form': form,
                'module': module,
                'route': route
            }
            this.sendTinymce(url, datapost)
        }
    }
}

export { Tinymce }
