import { Country } from './form/country'
import { DatePicker } from './form/datepicker'
import { Login } from './form/login'
import { Upload } from './form/upload'
import { Submit } from './form/submit'
import { SearchInput } from './form/SearchInput'
import { Select2 } from './form/select2'
export class Form {

    OnKeyUpInput(event) {
        if (event.which == 13) {
            $(this).trigger("blur")
        }
    }
    InputKeyUp() {
        $("input").on("keyup", this.OnKeyUpInput)
    }

    BoutonSaveAndCloseOnClick() {
        $('#BoutonSaveAndClose').attr('data-close', 1)
        $('#BoutonSave').trigger("click")
    }

    BoutonSaveAndClose() {
        $('#BoutonSaveAndClose').attr('data-close', 0)
        $('#BoutonSaveAndClose').on('click', this.BoutonSaveAndCloseOnClick)
    }
    ClickActionSearch() {
        let param = $('#FormSearch').serializeForm()
        let url   = $('#FormSearch').attr('action')
        if (param != '') {
            url               = url + '?' + this.param(param)
            document.location = url
        }
        return false
    }
    param(data) {
        let url = ''
        for (let a in data) {
            if (data[a] != "") {
                if (url != '') {
                    url = url + '&';
                }
                url = url + a + '=' + data[a]
            }
        }
        return url
    }
    Search() {
        let parent = this
        $("input").each(
            function () {
                if ($(this).attr('data-url') != undefined && $(this).closest(".modal").html() == undefined) {
                    parent.SearchInput.traiter(this)
                }
            }
        )
    $(".BtnSearch").on("click", this.ClickActionSearch.bind(this))
    }
    ChampsObligatoire() {
        $('label.required').each(
            function () {
                if ($(this).closest('.modal').html() === undefined) {
                    $('#ChampsObligatoireToolbar').show()
                }
            }
        )
    }
    CheckboxAll() {
        $('[data-rel=\'selectionall\']').on("click", this.ClickSelectAll)
    }
    OnclickInputNumber() {
        $(this).select()
    }
    SelectInputNumber() {
        $("input[type='number']").on("click", this.OnclickInputNumber)
    }
    getDatePicker() {
        return this.DatePicker
    }
    getSelect2() {
        return this.select2
    }
    constructor() {
        this.Submit      = new Submit()
        this.SearchInput = new SearchInput()
        this.select2     = new Select2()
        this.select2.Init()
        this.InputKeyUp()
        this.Country     = new Country()
        this.DatePicker  = new DatePicker()
        this.Login       = new Login()
        this.Upload      = new Upload()
        this.BoutonSaveAndClose()
        this.Search()
        this.ChampsObligatoire()
        this.CheckboxAll()
        this.SelectInputNumber()
    }
}
