
import { Adresse } from './collection/adresse'
import { Email } from './collection/email'
import { Telephone } from './collection/telephone'
import { Horaire } from './collection/horaire'
class Collection {
    Ajouter(e) {
        let type          = $(e).attr('data-type')
        let fieldset      = $(e).closest("fieldset")
        let prototype     = $(fieldset).attr('data-prototype')
        let index         = $(fieldset).find('tr').length
        let tags          = prototype.replace(/__name__/g, index)
        let tabCollection = $(fieldset).find('.TabCollection' + type)
        $(tabCollection).append(tags)
        let row           = $(tabCollection).find('.CollectionRow:last')
        if (type == "telephones") {
            this.Telephone.row(row)
        } else if (type == "emails") {
            this.Email.row(row)
        } else if (type == "adresses") {
            this.Adresse.row(row)
        }

        let parent = this;
        $(".BtnCollectionDeleteVal").each(
            function () {
                $(this).on(
                    'click',
                    function () {
                        $(this).closest('.CollectionRow').remove()
                    }
                )
                $(this).addClass('BtnCollectionDelete')
                $(this).removeClass('BtnCollectionDeleteVal')
            }
        )
    $("input,select").on(
            "keypress",
            function (event) {
                if (event.which == 13) {
                    $(this).blur()
                    event.preventDefault()
                }
            }
        )
    }
    Supprimer() {
        $(this).closest(".CollectionRow").remove()
    }
    constructor() {
        this.Telephone = new Telephone()
        this.Email     = new Email()
        this.Adresse   = new Adresse()
        this.Horaire   = new Horaire()
        let parent     = this
        $(".BtnCollectionAdd").on(
            "click",
            function () {
                parent.Ajouter(this)
            }
        )
    $(".BtnCollectionDelete").on("click", this.Supprimer).bind(this)
    $(".FieldCollection").each(
            function () {
                let type = $(this).attr('data-type')
                if (type == "telephones") {
                    parent.Telephone.tableau(this)
                } else if (type == "emails") {
                    parent.Email.tableau(this)
                } else if (type == "adresses") {
                    parent.Adresse.tableau(this)
                }
            }
        )
    }
}
export { Collection }
