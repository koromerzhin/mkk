
export class Navbar {
  Verifier() {
    if ($('a[data-toggle="tab"]').length) {
      $('a[data-toggle="tab"]').on('click', this.OnClick)
      let lastTab = localStorage.getItem('lastTab')
      if (lastTab) {
        if ($('a[href="' + lastTab + '"]').length) {
          $('a[href="' + lastTab + '"]').trigger("click")
        } else {
          localStorage.removeItem('lastTab')
        }
      }
    }
  }
  OnClick(e) {
    let ul = $(this).closest("ul")
    if ($(ul).attr('data-nolocalstorage') == undefined) {
      localStorage.setItem('lastTab', $(e.target).attr('href'))
    }

  }

  NavTAbsAOnClick(e) {
    if ($($(this).attr('href')).length != 0) {
      e.preventDefault()
      $(this).tab('show')
    }
  }
  PrevNext() {
    $(".NavPrev").on("click", function() {
      let li = $(this).closest(".nav-tabs").find("li")
      let nouveau
      $(li).each(
        function(i) {
          if (!$(this).hasClass("NavPrev") && !$(this).hasClass("NavNext")) {
            if ($(this).hasClass("active") && nouveau == undefined) {
              nouveau = i - 1
            }
          }
        }
      )
      if ($($(li).get(nouveau)) != undefined) {
        if (!$($(li).get(nouveau)).hasClass("NavPrev")) {
          $($(li).get(nouveau)).find('a').trigger('click')
        }
      }
    })
    $(".NavNext").on("click", function() {
      let li = $(this).closest(".nav-tabs").find("li")
      let nouveau
      $(li).each(
        function(i) {
          if (!$(this).hasClass("NavPrev") && !$(this).hasClass("NavNext")) {
            if ($(this).hasClass("active") && nouveau == undefined) {
              nouveau = i + 1
            }
          }
        }
      )
      if ($($(li).get(nouveau)) != undefined) {
        if (!$($(li).get(nouveau)).hasClass("NavNext")) {
          $($(li).get(nouveau)).find('a').trigger('click')
        }
      }
    })
  }
  constructor() {
    this.Verifier()
    this.PrevNext()
    $('.nav-tabs').find('a').on("click", this.NavTAbsAOnClick)
  }
}
