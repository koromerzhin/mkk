import { SearchInput } from '../modules/form/SearchInput'
import { Select2 } from '../modules/form/select2'
export class Modal {
  verifChampsRequired() {
    $('.modal').each(
      function() {
        let champsrequired = $(this).find('.ModalRequired')
        if ($(this).find('label.required').length != 0) {
          $(champsrequired).show()
        } else {
          $(champsrequired).hide()
        }
      }
    )
  }
  onShow(e) {
    let input = $(e.currentTarget).find('input')
    let parent = this
    $(input).each(
      function() {
        if ($(this).attr('type') != 'submit' && $(this).attr('data-url') != undefined) {
          parent.SearchInput.traiter(this)
        }
      }
    )
    if ($(input).length == 0) {
      $(this).focus()
    } else {
      $($(input).get(0)).focus()
    }
  }
  onHide(e) {
    let select = $(e.currentTarget).find('select,input')
    select.each(
      function() {
        $(this).select2('close')
      }
    )
  }
  onShown(e) {
    parent = this
    $(e.currentTarget).find('select').each(
      function() {
        parent.select2.Input(this)
      }
    )
    let footer = $(e.currentTarget).find('.modal-footer')
    let a = $(footer).find('a')
    if ($(a).length == 1 && $(e.currentTarget).data('autofocus') == true) {
      $(a).focus()
    }

    if ($(e.currentTarget).data('autoclose') != undefined) {
      footer = $(e.currentTarget).find('.modal-footer')
      a = $(footer).find('a')
      if ($(a).length == 1 && $(e.currentTarget).attr('data-autoclose') == "true") {
        console.log($(e.currentTarget).attr('data-autoclose'))
        setTimeout(
          function() {
            $($(a).get(0)).trigger("click")
          },
          1500
        )
      }
    }
  }
  constructor() {
    this.select2 = new Select2()
    this.SearchInput = new SearchInput()
    this.verifChampsRequired()
    $('.modal').on('show.bs.modal', this.onShow.bind(this))
    $('.modal').on('shown.bs.modal', this.onShown.bind(this))
    $('.modal').on('hide.bs.modal', this.onHide.bind(this))
  }
}
