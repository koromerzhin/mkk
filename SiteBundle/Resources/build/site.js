import { Modal } from './js/modules/modal'
import { Navbar } from './js/modules/navbar'
import { RevolutionSlider } from './js/modules/RevolutionSlider'
import { Collection } from './js/modules/collection'
import { Tinymce } from './js/modules/tinymce'
import { ActifDesactif } from './js/modules/actifdesactif'
import { Supprimer } from './js/modules/supprimer'
import { Dupliquer } from './js/modules/dupliquer'
import { Form } from './js/modules/form'
import { getXHRJson } from './js/modules/xhr/getXHRJson'
import { JqueryInit } from './js/modules/JqueryInit'
export class MkkSite {
    CacheTableHTML() {
        let nodata = $('#NoData').html()
        $('table').each(
            function () {
                if (!$(this).hasClass('NOCache')
                    && $(this).closest('.modal').html() === undefined
                    && $(this).closest('.EmplacementUpload').html() === undefined
                ) {
                    let tr = $(this).find('tbody').find('tr')
                    if ($(tr).length == 0) {
                        $(this).fadeOut(
                            'slow',
                            function () {
                                $(this).after(nodata)
                            }
                        )
                    }
                }
            }
        )
    }
    getModule() {
        return $('body').data('module')
    }
    getRoute() {
        return $('body').data('route')
    }
    ClickSelectAll() {
        checked = $(this).attr('checked')
        tableau = $(this).closest('table')
        if (checked == 'checked') {
            $(tableau).find('input[type=\'checkbox\']').each(
                function () {
                    $(this).attr('checked', 'checked')
                }
            )
        } else {
            $(tableau).find('input[type=\'checkbox\']').each(
                function () {
                    $(this).removeAttr('checked')
                }
            )
        }
    }

    OnClickLinkDisabled() {
        $('#PopupLienDesactiver').modal("toggle")
    }

    LinkDisabled() {
        $('a[data-rel=\'LienDesactiver\']').on("click", this.OnClickLinkDisabled)
    }

    Player() {
        if ($('.player').length == 1) {
            $('.player').mb_YTPlayer()
        }
    }
    OnClickShowTel(e) {
        this.CreationLienTel(e.currentTarget)
        e.preventDefault()
    }
    async CreationLienTel(lien) {
        let url      = $(lien).attr('href')
        let response = await getXHRJson(url)
        $(lien).html(response)
        if ($.browser != undefined && $.browser.mobile) {
            $(lien).attr('href', 'tel:' + response)
        } else {
            $(lien).removeAttr('href')
        }
    }
    Telephone() {
        $(".Showtel").on("click", this.OnClickShowTel.bind(this))
        if ($.browser != undefined && $.browser.mobile) {
            $('.LienTel').each(
                function () {
                    var a = document.createElement('a')
                    $(a).attr('href', 'tel:' + $(this).attr('data-tel')).html($(this).attr('data-tel'))
                    $(this).html(a)
                }
            )
        }
    }
    getForm() {
        return this.form
    }
    Accordion() {
        $(".accordion").each(
            function () {
                $(this).accordion(
                    {
                        heightStyle: 'content',
                        collapsible: true
                    }
                )
            }
        )
    }
    constructor() {
        console.log('mkk site')
        this.JqueryInit       = new JqueryInit()
        this.Modal            = new Modal()
        this.Accordion()
        this.Telephone()
        this.Navbar           = new Navbar()
        this.RevolutionSlider = new RevolutionSlider()
        this.ActifDesactif    = new ActifDesactif()
        this.Collection       = new Collection()
        this.Supprimer        = new Supprimer()
        this.Tinymce          = new Tinymce()
        this.Dupliquer        = new Dupliquer()
        this.form             = new Form()
        this.LinkDisabled()
        this.Player()
        this.CacheTableHTML()
    }
}
