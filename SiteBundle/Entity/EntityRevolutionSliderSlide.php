<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mkk\SiteBundle\Annotation\UploadableField;

class EntityRevolutionSliderSlide
{
    /**
     * @var int
     *
     * @ORM\Column(name="revolutionsliderslide_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="revolutionsliderslide_titre", type="string", length=255, nullable=true)
     */
    protected $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="revolutionsliderslide_image", type="string", length=255, nullable=true)
     */
    protected $image;

    /**
     * @UploadableField(filename="image", path="slide/image", unique=true, alias="nom")
     */
    protected $file_image;

    /**
     * @var int
     *
     * @ORM\Column(name="revolutionsliderslide_position", type="integer", nullable=true)
     */
    protected $position;

    /**
     * @var int
     *
     * @ORM\Column(name="revolutionsliderslide_actif_public", type="integer", nullable=true)
     */
    protected $actifPublic;

    /**
     * @var array
     *
     * @ORM\Column(name="revolutionsliderslide_param", type="array", nullable=true)
     */
    protected $param;

    /**
     * @var array
     *
     * @ORM\Column(name="revolutionsliderslide_paramimage", type="array", nullable=true)
     */
    protected $paramimage;

    /**
     * @var array
     *
     * @ORM\Column(name="revolutionsliderslide_layers", type="array", nullable=true)
     */
    protected $layers;

    /**
     * @ORM\ManyToOne(targetEntity="RevolutionSlider", inversedBy="revolutionssliderslides")
     * @ORM\JoinColumn(name="revolutionsliderslide_refrevolutionslider", referencedColumnName="revolutionslider_id")
     */
    protected $refrevolutionslider;

    /**
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre.
     *
     * @param string $titre
     *
     * @return Slide
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * get titre.
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set param.
     *
     * @param string $param
     *
     * @return Slide
     */
    public function setParam($param)
    {
        $this->param = $param;

        return $this;
    }

    /**
     * get param.
     *
     * @return string
     */
    public function getParam()
    {
        $this->param = $param;
    }

    /**
     * Set layers.
     *
     * @param string $layers
     *
     * @return Slide
     */
    public function setLayers($layers)
    {
        $this->layers = $layers;

        return $this;
    }

    /**
     * get layers.
     *
     * @return string
     */
    public function getLayers()
    {
        return $this->layers;
    }

    /**
     * Set refrevolutionslider.
     *
     * @param $refrevolutionslider
     *
     * @return RevolutionSlider
     */
    public function setRefRevolutionslider($refrevolutionslider = null)
    {
        $this->refrevolutionslider = $refrevolutionslider;

        return $this;
    }

    /**
     * get refrevolutionslider.
     *
     * @return RevolutionSlider
     */
    public function getRefRevolutionslider()
    {
        return $this->refrevolutionslider;
    }

    /**
     * Set paramimage.
     *
     * @param string $paramimage
     *
     * @return Slide
     */
    public function setParamimage($paramimage)
    {
        $this->paramimage = $paramimage;

        return $this;
    }

    /**
     * get paramimage.
     *
     * @return string
     */
    public function getParamimage()
    {
        return $this->paramimage;
    }

    /**
     * Set image.
     *
     * @param string $image
     *
     * @return Slide
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * get image.
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set position.
     *
     * @param int $position
     *
     * @return Slide
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * get position.
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set actif_public.
     *
     * @param int $actifPublic
     *
     * @return RevolutionSliderSlide
     */
    public function setActifpublic($actifPublic)
    {
        $this->actifPublic = $actifPublic;

        return $this;
    }

    /**
     * get actif_public.
     *
     * @return int
     */
    public function getActifpublic()
    {
        return $this->actifPublic;
    }

    /**
     * Get the value of File Image.
     *
     * @return mixed
     */
    public function getFileImage()
    {
        return $this->file_image;
    }

    /**
     * Set the value of File Image.
     *
     * @param mixed file_image
     * @param mixed            $file_image
     *
     * @return self
     */
    public function setFileImage($file_image)
    {
        $this->file_image = $file_image;

        return $this;
    }

    /**
     * Get the value of Updated At.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set the value of Updated At.
     *
     * @param \DateTime updatedAt
     *
     * @return self
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
