<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\GroupableInterface;
use FOS\UserBundle\Model\GroupInterface;
use FOS\UserBundle\Model\UserInterface;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Mkk\SiteBundle\Annotation\UploadableField;

class EntityUser implements UserInterface, GroupableInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var text
     *
     * @ORM\Column(name="user_temporaire", type="boolean", nullable=true)
     */
    protected $temporaire;

    /**
     * @var text
     *
     * @ORM\Column(name="user_avatar", type="text", nullable=true)
     */
    protected $avatar;

    /**
     * @UploadableField(filename="avatar", path="user/avatar", unique=true, alias="username")
     */
    protected $file_avatar;

    /**
     * @var text
     *
     * @ORM\Column(name="user_naissance", type="text", nullable=true)
     */
    protected $naissance;

    /**
     * @var text
     *
     * @ORM\Column(name="user_contactsms", type="boolean", nullable=true)
     */
    protected $contactsms;

    /**
     * @var text
     *
     * @ORM\Column(name="user_contactadresse", type="boolean", nullable=true)
     */
    protected $contactadresse;

    /**
     * @var text
     *
     * @ORM\Column(name="user_contactemail", type="boolean", nullable=true)
     */
    protected $contactemail;

    /**
     * @var string
     *
     * @ORM\Column(name="user_raison", type="string", length=255, nullable=true)
     */
    protected $raison;

    /**
     * @var string
     *
     * @ORM\Column(name="user_nom", type="string", length=255, nullable=true)
     */
    protected $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="user_civilite", type="string", length=255, nullable=true)
     */
    protected $civilite;

    /**
     * @var string
     *
     * @ORM\Column(name="user_type", type="string", length=255, nullable=true)
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="user_langue", type="string", length=255, nullable=true)
     */
    protected $langue;

    /**
     * @var string
     *
     * @ORM\Column(name="user_pays", type="string", length=255, nullable=true)
     */
    protected $pays;

    /**
     * @var string
     *
     * @ORM\Column(name="user_prenom", type="string", length=255, nullable=true)
     */
    protected $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="user_societe", type="text", nullable=true)
     */
    protected $societe;

    /**
     * @var string
     * @JMS\Exclude()
     * @ORM\Column(name="user_password", type="string", length=255, nullable=true)
     */
    protected $password;

    /**
     * @ORM\Column(name="user_salt", type="string", length=255)
     * @JMS\Exclude()
     *
     * @var string salt
     */
    protected $salt;

    /**
     * @ORM\ManyToOne(targetEntity="Group", inversedBy="users")
     * @ORM\JoinColumn(name="user_refgroup", referencedColumnName="group_id")
     * @JMS\Exclude()
     */
    protected $refgroup;

    /**
     * @ORM\ManyToMany(targetEntity="Etablissement",  mappedBy="users")
     */
    protected $etablissements;

    /**
     * @var bool
     *
     * @ORM\Column(name="user_active", type="boolean")
     */
    protected $enabled;

    /**
     * @var string
     *
     * @ORM\Column(name="user_username", type="text", nullable=true)
     */
    protected $username;

    /**
     * @var string
     *
     * @ORM\Column(name="user_username_canonical", type="text", nullable=true)
     */
    protected $usernameCanonical;

    /**
     * @var string
     *
     * @ORM\Column(name="user_email", type="text", nullable=true)
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(name="user_email_canonical", type="text", nullable=true)
     */
    protected $emailCanonical;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="user_dateinscription", type="datetime", nullable=true)
     */
    protected $dateinscription;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="user_last_login", type="datetime", nullable=true)
     */
    protected $lastLogin;

    /**
     * @var bool
     *
     * @ORM\Column(name="user_newsletter", type="boolean", nullable=true)
     */
    protected $newsletter;

    /**
     * @var text
     *
     * @ORM\Column(name="user_observations", type="text", nullable=true)
     */
    protected $observations;

    /**
     * @var array
     *
     * @ORM\Column(name="user_data", type="array", nullable=true)
     */
    protected $data;

    /**
     * @ORM\OneToMany(targetEntity="Adresse", mappedBy="refuser", cascade={"all"})
     */
    protected $adresses;

    /**
     * @ORM\OneToMany(targetEntity="Email", mappedBy="refuser", cascade={"all"})
     */
    protected $emails;

    /**
     * @ORM\OneToMany(targetEntity="Lien", mappedBy="refuser", cascade={"all"})
     */
    protected $liens;

    /**
     * @ORM\OneToMany(targetEntity="Telephone", mappedBy="refuser", cascade={"all"})
     */
    protected $telephones;

    /**
     * @ORM\OneToMany(targetEntity="Evenement", mappedBy="refuser", cascade={"all"})
     */
    protected $evenements;

    /**
     * @ORM\OneToMany(targetEntity="Config", mappedBy="refuser", cascade={"all"})
     */
    protected $configs;

    /**
     * @ORM\OneToMany(targetEntity="Noteinterne", mappedBy="refuser", cascade={"remove", "persist"})
     */
    protected $noteinternes;

    /**
     * @ORM\OneToMany(targetEntity="NoteinterneLecture", mappedBy="refuser", cascade={"remove", "persist"})
     */
    protected $noteinternelectures;

    /**
     * @ORM\OneToMany(targetEntity="Blog", mappedBy="refuser", cascade={"remove", "persist"})
     */
    protected $blogs;

    /**
     * @ORM\OneToMany(targetEntity="Edito", mappedBy="refuser", cascade={"remove", "persist"})
     */
    protected $editos;

    /**
     * @var string
     *
     * @ORM\Column(name="user_tags", type="string", length=255, nullable=true)
     */
    protected $tags;

    /**
     * @var string
     *
     * @ORM\Column(name="user_douanier", type="string", length=255, nullable=true)
     */
    protected $douanier;

    /**
     * Plain password. Used for model validation. Must not be persisted.
     *
     * @var string
     *
     * @ORM\Column(name="user_plain_password", type="string", length=255, nullable=true)
     */
    protected $plainPassword;

    /**
     * Random string sent to the user email address in order to verify it.
     *
     * @var string
     *
     * @ORM\Column(name="user_confirmation_token", type="string", length=255, nullable=true)
     */
    protected $confirmationToken;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="user_password_request_at", type="datetime", nullable=true)
     */
    protected $passwordRequestedAt;

    /**
     * @var Collection
     */
    protected $groups;

    /**
     * @var array
     *
     * @ORM\Column(name="user_roles", type="array", nullable=true)
     */
    protected $roles;

    /**
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    public function __construct()
    {
        $this->enabled        = true;
        $this->roles          = ['ROLE_ADMIN'];
        $this->salt           = md5(uniqid(null, true));
        $this->liens          = new ArrayCollection();
        $this->emails         = new ArrayCollection();
        $this->telephones     = new ArrayCollection();
        $this->etablissements = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getAppel();
    }

    public function serialize()
    {
        return serialize(
            [
            $this->password,
            $this->salt,
            $this->usernameCanonical,
            $this->username,
            $this->enabled,
            $this->id,
            $this->email,
            $this->emailCanonical,
            ]
        );
    }

    public function unserialize($serialized)
    {
        $data = unserialize($serialized);

        if (13 === count($data)) {
            // Unserializing a User object from 1.3.x
            unset($data[4], $data[5], $data[6], $data[9], $data[10]);
            $data = array_values($data);
        } elseif (11 === count($data)) {
            // Unserializing a User from a dev version somewhere between 2.0-alpha3 and 2.0-beta1
            unset($data[4], $data[7], $data[8]);
            $data = array_values($data);
        }

        list(
            $this->password,
            $this->salt,
            $this->usernameCanonical,
            $this->username,
            $this->enabled,
            $this->id,
            $this->email,
            $this->emailCanonical
        ) = $data;
    }

    public function addRole($role)
    {
        $role = strtoupper($role);
        if ($role === static::ROLE_DEFAULT) {
            return $this;
        }

        if (! in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     * gets an array of roles.
     *
     * @return array An array of Role objects
     */
    public function getRoles()
    {
        $roles = $this->roles;

        foreach ($this->getGroups() as $group) {
            $roles = array_merge($roles, $group->getRoles());
        }

        // we need to make sure to have at least one role
        $roles[] = static::ROLE_DEFAULT;

        return array_unique($roles);
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function hasRole($role)
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    /**
     * Compares this user to another to determine if they are the same.
     *
     * @param UserInterface $user The user
     *
     * @return bool True if equal, false othwerwise
     */
    public function equals(UserInterface $user)
    {
        return md5($this->getUsername()) == md5($user->getUsername());
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * gets the user password.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return $this->password;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    /**
     * Sets the user password.
     *
     * @param string $value    The password
     * @param mixed  $password
     */
    public function setPassword($value)
    {
        $this->password = $value;
    }

    public function getEmailCanonical()
    {
        return $this->emailCanonical;
    }

    public function setSuperAdmin($boolean)
    {
        if (true === $boolean) {
            $this->addRole(static::ROLE_SUPER_ADMIN);
        } else {
            $this->removeRole(static::ROLE_SUPER_ADMIN);
        }

        return $this;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;

        return $this;
    }

    public function setLastLogin(\DateTime $time = null)
    {
        $this->lastLogin = $time;

        return $this;
    }

    public function setConfirmationToken($confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    public function setRoles(array $roles)
    {
        $this->roles = [];

        foreach ($roles as $role) {
            $this->addRole($role);
        }

        return $this;
    }

    public function isPasswordRequestNonExpired($ttl)
    {
        return $this->getPasswordRequestedAt() instanceof \DateTime &&
               $this->getPasswordRequestedAt()->getTimestamp() + $ttl > time();
    }

    /**
     * Gets the timestamp that the user requested a password reset.
     *
     * @return null|\DateTime
     */
    public function getPasswordRequestedAt()
    {
        return $this->passwordRequestedAt;
    }

    public function setPasswordRequestedAt(\DateTime $date = null)
    {
        $this->passwordRequestedAt = $date;

        return $this;
    }

    /**
     * gets the user salt.
     *
     * @return string The salt
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Sets the user salt.
     *
     * @param string $value The salt
     */
    public function setSalt($value)
    {
        $this->salt = $value;
    }

    /**
     * gets the username.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Sets the username.
     *
     * @param string $value The username
     */
    public function setUsername($value)
    {
        $this->username = $value;
    }

    public function setUsernameCanonical($usernameCanonical)
    {
        $this->usernameCanonical = $usernameCanonical;

        return $this;
    }

    /**
     * get nom.
     *
     * @return string nom
     */
    public function getNom()
    {
        return mb_strtoupper($this->nom, 'UTF-8');
    }

    /**
     * Set nom.
     *
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = mb_strtoupper($nom, 'UTF-8');
    }

    /**
     * get prenom.
     *
     * @return string prenom
     */
    public function getPrenom()
    {
        return ucfirst(mb_strtolower($this->prenom, 'UTF-8'));
    }

    /**
     * Set prenom.
     *
     * @param string $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = ucfirst(mb_strtolower($prenom, 'UTF-8'));
    }

    /**
     * get societe.
     *
     * @return string societe
     */
    public function getSociete()
    {
        return mb_strtoupper($this->societe, 'UTF-8');
    }

    /**
     * Set societe.
     *
     * @param string $societe
     */
    public function setSociete($societe)
    {
        $this->societe = ucfirst(mb_strtolower($societe, 'UTF-8'));
    }

    /**
     * Set refgroup.
     *
     * @param Group $refgroup
     */
    public function setRefGroup($refgroup)
    {
        $this->refgroup = $refgroup;
    }

    /**
     * get refgroup.
     *
     * @return Group
     */
    public function getRefGroup()
    {
        return $this->refgroup;
    }

    /**
     * Erases the user credentials.
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /**
     * Set active.
     *
     * @param bool  $active
     * @param mixed $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * get active.
     *
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    public function isSuperAdmin()
    {
        return $this->hasRole(static::ROLE_SUPER_ADMIN);
    }

    public function removeRole($role)
    {
        if (false !== $key = array_search(strtoupper($role), $this->roles, true)) {
            unset($this->roles[$key]);
            $this->roles = array_values($this->roles);
        }

        return $this;
    }

    /**
     * Set email.
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function setEmailCanonical($emailCanonical)
    {
        $this->emailCanonical = $emailCanonical;

        return $this;
    }

    /**
     * get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set dateinscription.
     *
     * @return User
     */
    public function setDateinscription(\DateTime $dateinscription = null)
    {
        $this->dateinscription = $dateinscription;

        return $this;
    }

    /**
     * get dateinscription.
     *
     * @return int
     */
    public function getDateinscription()
    {
        return $this->dateinscription;
    }

    public function getTimestampDateinscription()
    {
        return $this->dateinscription;
    }

    /**
     * Set civilite.
     *
     * @param string $civilite
     *
     * @return User
     */
    public function setCivilite($civilite)
    {
        $this->civilite = $civilite;

        return $this;
    }

    /**
     * get civilite.
     *
     * @return string
     */
    public function getCivilite()
    {
        $civilite = $this->civilite;
        if (substr_count($civilite, 'Mme') != 0 || substr_count($civilite, 'Mlle') != 0) {
            $civilite = 'Mme';
        } else {
            $civilite = 'M.';
        }
        $this->setCivilite($civilite);

        return $civilite;
    }

    /**
     * Set observations.
     *
     * @param string $observations
     *
     * @return User
     */
    public function setObservations($observations)
    {
        $this->observations = $observations;

        return $this;
    }

    /**
     * get observations.
     *
     * @return string
     */
    public function getObservations()
    {
        return $this->observations;
    }

    /**
     * Add adresses.
     *
     * @param Adresse $adresses
     *
     * @return User
     */
    public function addAdresse($adresses)
    {
        $adresses->setRefUser($this);
        $this->adresses->add($adresses);

        return $this;
    }

    /**
     * Remove adresses.
     *
     * @param Adresse $adresses
     */
    public function removeAdresse($adresses)
    {
        $this->adresses->removeElement($adresses);
    }

    /**
     * get adresses.
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getAdresses()
    {
        return $this->adresses;
    }

    /**
     * Set langue.
     *
     * @param string $langue
     *
     * @return User
     */
    public function setLangue($langue)
    {
        $this->langue = $langue;

        return $this;
    }

    /**
     * get langue.
     *
     * @return string
     */
    public function getLangue()
    {
        return $this->langue;
    }

    /**
     * Set pays.
     *
     * @param string $pays
     *
     * @return User
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * get pays.
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Add telephones.
     *
     * @param Telephone $telephones
     *
     * @return User
     */
    public function addTelephone($telephones)
    {
        $telephones->setRefUser($this);
        $this->telephones->add($telephones);

        return $this;
    }

    /**
     * Remove telephones.
     *
     * @param Telephone $telephones
     */
    public function removeTelephone($telephones)
    {
        $this->telephones->removeElement($telephones);
    }

    /**
     * get telephones.
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getTelephones()
    {
        return $this->telephones;
    }

    /**
     * Set contactsms.
     *
     * @param bool $contactsms
     *
     * @return User
     */
    public function setContactsms($contactsms)
    {
        $this->contactsms = $contactsms;

        return $this;
    }

    /**
     * get contactsms.
     *
     * @return bool
     */
    public function getContactsms()
    {
        return $this->contactsms;
    }

    /**
     * Set contactemail.
     *
     * @param bool $contactemail
     *
     * @return User
     */
    public function setContactemail($contactemail)
    {
        $this->contactemail = $contactemail;

        return $this;
    }

    /**
     * get contactemail.
     *
     * @return bool
     */
    public function getContactemail()
    {
        return $this->contactemail;
    }

    /**
     * Set raison.
     *
     * @param string $raison
     *
     * @return User
     */
    public function setRaison($raison)
    {
        $this->raison = $raison;

        return $this;
    }

    /**
     * get raison.
     *
     * @return string
     */
    public function getRaison()
    {
        return $this->raison;
    }

    /**
     * Set data.
     *
     * @param text $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * get data.
     *
     * @return text
     */
    public function getData()
    {
        return $this->data;
        return $data;
    }

    /**
     * Set newsletter.
     *
     * @param bool $newsletter
     *
     * @return User
     */
    public function setNewsletter($newsletter)
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    /**
     * get newsletter.
     *
     * @return bool
     */
    public function getNewsletter()
    {
        return $this->newsletter;
    }

    public function getVille()
    {
        $ville = '';
        foreach ($this->getAdresses() as $adresse) {
            $ville = $adresse->getVille();
            if ($ville != '') {
                break;
            }
        }

        return $ville;
    }

    public function getAppelpdf()
    {
        $tab   = [
            $this->getPrenom(),
            $this->getNom(),
        ];
        $texte = trim(implode(' ', $tab));
        if ($texte != '') {
            $texte = $this->getCivilite() . ' ' . $texte;
        }

        return $texte;
    }

    public function getAppelListing()
    {
        $tab   = [
            $this->getPrenom(),
            $this->getNom(),
        ];
        $texte = trim(implode(' ', $tab));
        if ($texte == '') {
            foreach ($this->getEmails() as $email) {
                if ($email->getAdresse() != '') {
                    $texte = $email->getAdresse();
                    break;
                }
            }
            if ($texte == '') {
                $texte = 'non communiqué';
            }
        }

        return $texte;
    }

    public function getAppel()
    {
        $tab   = [
            $this->getPrenom(),
            $this->getNom(),
        ];
        $texte = trim(implode(' ', $tab));
        if ($this->getSociete() != '') {
            if ($texte != '') {
                $texte = $texte . ' - ' . $this->getSociete();
            } else {
                $texte = $this->getSociete();
            }
        }
        if ($texte == '') {
            foreach ($this->getEmails() as $email) {
                if ($email->getAdresse() != '') {
                    $texte = $email->getAdresse();
                    break;
                }
            }
            if ($texte == '') {
                $texte = 'non communiqué';
            }
        }

        return $texte;
    }

    /**
     * Add emails.
     *
     * @param Email $emails
     *
     * @return User
     */
    public function addEmail($emails)
    {
        $emails->setRefUser($this);
        $this->emails->add($emails);

        return $this;
    }

    /**
     * Remove emails.
     *
     * @param Email $emails
     */
    public function removeEmail($emails)
    {
        $this->emails->removeElement($emails);
    }

    /**
     * get emails.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * Add liens.
     *
     * @param Lien $liens
     *
     * @return User
     */
    public function addLien($liens)
    {
        $liens->setRefUser($this);
        $this->liens->add($liens);

        return $this;
    }

    /**
     * get liens.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLiens()
    {
        return $this->liens;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return User
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set avatar.
     *
     * @param string $avatar
     *
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * get avatar.
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set naissance.
     *
     * @param string $naissance
     *
     * @return User
     */
    public function setNaissance($naissance)
    {
        $this->naissance = $naissance;

        return $this;
    }

    /**
     * get naissance.
     *
     * @return string
     */
    public function getNaissance()
    {
        $data = $this->getData();
        if (isset($data['naissance'])) {
            $this->setNaissance($data['naissance']);
            unset($data['naissance']);
            $this->setData($data);
        }

        return $this->naissance;
    }

    /**
     * Remove liens.
     *
     * @param Lien $liens
     */
    public function removeLien($liens)
    {
        $this->liens->removeElement($liens);
    }

    /**
     * Set contactadresse.
     *
     * @param bool $contactadresse
     *
     * @return User
     */
    public function setContactadresse($contactadresse)
    {
        $this->contactadresse = $contactadresse;

        return $this;
    }

    /**
     * get contactadresse.
     *
     * @return bool
     */
    public function getContactadresse()
    {
        return $this->contactadresse;
    }

    public function getCoordonnees()
    {
        $data       = [];
        $telephones = $this->getTelephones();
        foreach ($telephones as $key => $row) {
            $data[$key] = $row;
        }

        $emails = $this->getEmails();
        foreach ($emails as $key => $row) {
            $data[$key] = $row;
        }

        $sites = $this->getLiens();
        foreach ($sites as $key => $row) {
            $data[$key] = $row;
        }

        return $data;
    }

    /**
     * Add etablissements.
     *
     * @param Etablissement $etablissements
     * @param mixed         $etablissement
     *
     * @return User
     */
    public function addEtablissement($etablissement)
    {
        $this->etablissements->add($etablissement);

        return $this;
    }

    /**
     * Remove etablissements.
     *
     * @param Etablissement $etablissements
     * @param mixed         $etablissement
     */
    public function removeEtablissement($etablissement)
    {
        $this->etablissements->removeElement($etablissement);
    }

    /**
     * get etablissements.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEtablissements()
    {
        return $this->etablissements;
    }

    /**
     * Add configs.
     *
     * @param Config $configs
     *
     * @return User
     */
    public function addConfig($configs)
    {
        $configs->setRefUser($this);
        $this->configs[] = $configs;

        return $this;
    }

    /**
     * Remove configs.
     *
     * @param Config $configs
     */
    public function removeConfig($configs)
    {
        $this->configs->removeElement($configs);
    }

    /**
     * get configs.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConfigs()
    {
        return $this->configs;
    }

    /**
     * Set temporaire.
     *
     * @param bool $temporaire
     *
     * @return User
     */
    public function setTemporaire($temporaire)
    {
        $this->temporaire = $temporaire;

        return $this;
    }

    /**
     * get temporaire.
     *
     * @return bool
     */
    public function getTemporaire()
    {
        return $this->temporaire;
    }

    /**
     * Add noteinternes.
     *
     * @param Noteinterne $noteinterne
     * @param mixed       $noteinternes
     *
     * @return User
     */
    public function addNoteinternes($noteinternes)
    {
        $noteinternes->setRefUser($this);
        $this->noteinternes[] = $noteinternes;

        return $this;
    }

    /**
     * Remove noteinternes.
     *
     * @param Noteinterne $noteinternes
     */
    public function removeNoteinternes($noteinternes)
    {
        $this->noteinternelectures->removeElement($noteinternes);
    }

    /**
     * get noteinternes.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNoteinternes()
    {
        return $this->noteinternes;
    }

    /**
     * Add noteinternelectures.
     *
     * @param NoteinterneLecture $lectures
     * @param mixed              $noteinternelectures
     *
     * @return User
     */
    public function addNoteinterneLectures($noteinternelectures)
    {
        $noteinternelectures->setRefUser($this);
        $this->noteinternelectures[] = $noteinternelectures;

        return $this;
    }

    /**
     * Remove noteinternelectures.
     *
     * @param NoteinterneLecture $noteinternelectures
     */
    public function removeNoteinterneLectures($noteinternelectures)
    {
        $this->noteinternelectures->removeElement($noteinternelectures);
    }

    /**
     * Add blogs.
     *
     * @param \Blog $blogs
     *
     * @return User
     */
    public function addBlog($blogs)
    {
        $blogs->setRefUser($this);
        $this->blogs[] = $blogs;

        return $this;
    }

    /**
     * Remove blogs.
     *
     * @param \Blog $blogs
     */
    public function removeBlog($blogs)
    {
        $this->blogs->removeElement($blogs);
    }

    /**
     * get blogs.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBlogs()
    {
        return $this->blogs;
    }

    /**
     * Add editos.
     *
     * @param \Edito $editos
     *
     * @return User
     */
    public function addEdito($editos)
    {
        $editos->setRefUser($this);
        $this->editos[] = $editos;

        return $this;
    }

    /**
     * Remove edito.
     *
     * @param \Edito $editos
     */
    public function removeEdito($editos)
    {
        $this->editos->removeElement($editos);
    }

    /**
     * get editos.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEditos()
    {
        return $this->editos;
    }

    public function getConfigTab()
    {
        $configs = $this->configs;
        $tab     = [];
        foreach ($configs as $config) {
            $code       = $config->getCode();
            $valeur     = $config->getValeur();
            $tab[$code] = $valeur;
        }

        ksort($tab);

        return $tab;
    }

    /**
     * Set tags.
     *
     * @param string $tags
     *
     * @return User
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * get tags.
     *
     * @return string
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set douanier.
     *
     * @param string $douanier
     *
     * @return User
     */
    public function setDouanier($douanier)
    {
        $this->douanier = $douanier;

        return $this;
    }

    /**
     * get douanier.
     *
     * @return string
     */
    public function getDouanier()
    {
        return $this->douanier;
    }

    /**
     * Set the value of Etablissements.
     *
     * @param mixed etablissements
     * @param mixed $etablissements
     *
     * @return self
     */
    public function setEtablissements($etablissements)
    {
        $this->etablissements = $etablissements;

        return $this;
    }

    /**
     * Set the value of Adresses.
     *
     * @param mixed adresses
     * @param mixed $adresses
     *
     * @return self
     */
    public function setAdresses($adresses)
    {
        $this->adresses = $adresses;

        return $this;
    }

    /**
     * Set the value of Emails.
     *
     * @param mixed emails
     * @param mixed $emails
     *
     * @return self
     */
    public function setEmails($emails)
    {
        $this->emails = $emails;

        return $this;
    }

    /**
     * Set the value of Liens.
     *
     * @param mixed liens
     * @param mixed $liens
     *
     * @return self
     */
    public function setLiens($liens)
    {
        $this->liens = $liens;

        return $this;
    }

    /**
     * Set the value of Telephones.
     *
     * @param mixed telephones
     * @param mixed $telephones
     *
     * @return self
     */
    public function setTelephones($telephones)
    {
        $this->telephones = $telephones;

        return $this;
    }

    /**
     * get the value of Evenements.
     *
     * @return mixed
     */
    public function getEvenements()
    {
        return $this->evenements;
    }

    /**
     * Set the value of Evenements.
     *
     * @param mixed evenements
     * @param mixed $evenements
     *
     * @return self
     */
    public function setEvenements($evenements)
    {
        $this->evenements = $evenements;

        return $this;
    }

    /**
     * Set the value of Configs.
     *
     * @param mixed configs
     * @param mixed $configs
     *
     * @return self
     */
    public function setConfigs($configs)
    {
        $this->configs = $configs;

        return $this;
    }

    /**
     * Set the value of Noteinternes.
     *
     * @param mixed noteinternes
     * @param mixed $noteinternes
     *
     * @return self
     */
    public function setNoteinternes($noteinternes)
    {
        $this->noteinternes = $noteinternes;

        return $this;
    }

    /**
     * get the value of Noteinternelectures.
     *
     * @return mixed
     */
    public function getNoteinternelectures()
    {
        return $this->noteinternelectures;
    }

    /**
     * Set the value of Noteinternelectures.
     *
     * @param mixed noteinternelectures
     * @param mixed $noteinternelectures
     *
     * @return self
     */
    public function setNoteinternelectures($noteinternelectures)
    {
        $this->noteinternelectures = $noteinternelectures;

        return $this;
    }

    /**
     * Set the value of Blogs.
     *
     * @param mixed blogs
     * @param mixed $blogs
     *
     * @return self
     */
    public function setBlogs($blogs)
    {
        $this->blogs = $blogs;

        return $this;
    }

    /**
     * Set the value of Editos.
     *
     * @param mixed editos
     * @param mixed $editos
     *
     * @return self
     */
    public function setEditos($editos)
    {
        $this->editos = $editos;

        return $this;
    }

    public function getUsernameCanonical()
    {
        return $this->usernameCanonical;
    }

    public function hasGroup($name)
    {
        return in_array($name, $this->getGroupNames());
    }

    public function removeGroup(GroupInterface $group)
    {
        if ($this->getGroups()->contains($group)) {
            $this->getGroups()->removeElement($group);
        }

        return $this;
    }

    public function addGroup(GroupInterface $group)
    {
        if (! $this->getGroups()->contains($group)) {
            $this->getGroups()->add($group);
        }

        return $this;
    }

    public function getGroupNames()
    {
        $names = [];
        foreach ($this->getGroups() as $group) {
            $names[] = $group->getName();
        }

        return $names;
    }

    public function getGroups()
    {
        return $this->groups ?: $this->groups = new ArrayCollection();
    }

    public function getSearchData()
    {
        $tab = [
            'id'   => $this->getId(),
            'nom'  => $this->getAppel(),
        ];

        return $tab;
    }

    public function getGroup()
    {
        return ! is_object($this->getRefGroup()) ? '' : $this->getRefGroup()->getId();
    }

    public function setGroup($v)
    {
        return $this;
    }

    public function getTabEtablissements()
    {
        $tab = [];
        if (count($this->getEtablissements()) != 0) {
            foreach ($this->getEtablissements() as $produit) {
                if ($produit) {
                    $tab[] = $produit->getId();
                }
            }
        }

        $retour = implode(',', $tab);

        return $retour;
    }

    public function setTabEtablissements($v)
    {
        return $this;
    }

    /**
     * Get the value of Description of what this does.
     *
     * @return mixed
     */
    public function getFileAvatar()
    {
        return $this->file_avatar;
    }

    /**
     * Set the value of Description of what this does.
     *
     * @param mixed file_avatar
     * @param mixed $file_avatar
     *
     * @return self
     */
    public function setFileAvatar($file_avatar)
    {
        $this->file_avatar = $file_avatar;

        return $this;
    }

    /**
     * Get the value of Updated At.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set the value of Updated At.
     *
     * @param \DateTime updatedAt
     *
     * @return self
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
