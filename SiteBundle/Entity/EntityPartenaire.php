<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use Mkk\SiteBundle\Annotation\UploadableField;

class EntityPartenaire implements Translatable
{
    /**
     * @var int
     *
     * @ORM\Column(name="partenaire_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="partenaire_nom", type="text", nullable=true)
     */
    protected $nom;

    /**
     * @var text
     * @Gedmo\Translatable
     * @ORM\Column(name="partenaire_description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @var string
     *
     * @ORM\Column(name="partenaire_slogan", type="text", nullable=true)
     */
    protected $slogan;

    /**
     * @var string
     *
     * @ORM\Column(name="partenaire_url", type="text", nullable=true)
     */
    protected $url;

    /**
     * @var string
     *
     * @ORM\Column(name="partenaire_image", type="text", nullable=true)
     */
    protected $image;

    /**
     * @UploadableField(filename="image", path="partenaire/image", unique=true, alias="nom")
     */
    protected $file_image;

    /**
     * @var int
     *
     * @ORM\Column(name="partenaire_actif_public", type="integer", nullable=true)
     */
    protected $actifPublic;

    /**
     * @var int
     *
     * @ORM\Column(name="partenaire_position", type="integer", nullable=true)
     */
    protected $position;

    /**
     * @ORM\ManyToOne(targetEntity="Categorie", inversedBy="partenaires")
     * @ORM\JoinColumn(name="partenaire_refcategorie", referencedColumnName="categorie_id")
     */
    protected $refcategorie;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     * and it is not necessary because globally locale can be set in listener
     */
    protected $locale;

    /**
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    public function __toString()
    {
        return $this->getNom();
    }

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return Partenaire
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set url.
     *
     * @param string $url
     *
     * @return Partenaire
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * get url.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set image.
     *
     * @param string $image
     *
     * @return Partenaire
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * get image.
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set refcategorie.
     *
     * @param $refcategorie
     *
     * @return Partenaire
     */
    public function setRefCategorie($refcategorie = null)
    {
        $this->refcategorie = $refcategorie;

        return $this;
    }

    /**
     * get refcategorie.
     *
     * @return Categorie
     */
    public function getRefCategorie()
    {
        return $this->refcategorie;
    }

    /**
     * Set position.
     *
     * @param int $position
     *
     * @return Partenaire
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * get position.
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set actif_public.
     *
     * @param int $actifPublic
     *
     * @return Partenaire
     */
    public function setActifpublic($actifPublic)
    {
        $this->actifPublic = $actifPublic;

        return $this;
    }

    /**
     * get actif_public.
     *
     * @return int
     */
    public function getActifpublic()
    {
        return $this->actifPublic;
    }

    /**
     * Set slogan.
     *
     * @param string $slogan
     *
     * @return Partenaire
     */
    public function setSlogan($slogan)
    {
        $this->slogan = $slogan;

        return $this;
    }

    /**
     * get slogan.
     *
     * @return string
     */
    public function getSlogan()
    {
        return $this->slogan;
    }

    public function getCategorie()
    {
        return ! is_object($this->getRefCategorie()) ? '' : $this->getRefCategorie()->getId();
    }

    public function setCategorie($v)
    {
        return $this;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * Get the value of Description.
     *
     * @return text
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of Description.
     *
     * @param text description
     * @param mixed $description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of File Image.
     *
     * @return mixed
     */
    public function getFileImage()
    {
        return $this->file_image;
    }

    /**
     * Set the value of File Image.
     *
     * @param mixed file_image
     * @param mixed $file_image
     *
     * @return self
     */
    public function setFileImage($file_image)
    {
        $this->file_image = $file_image;

        return $this;
    }

    /**
     * Get the value of Updated At.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set the value of Updated At.
     *
     * @param \DateTime updatedAt
     *
     * @return self
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
