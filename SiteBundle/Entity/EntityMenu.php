<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

class EntityMenu implements Translatable
{
    /**
     * @var int
     *
     * @ORM\Column(name="menu_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Menu", mappedBy="refmenu", cascade={"remove", "persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $menus;

    /**
     * @ORM\ManyToOne(targetEntity="Menu", inversedBy="menus")
     * @ORM\JoinColumn(name="menu_refmenu", referencedColumnName="menu_id", nullable=true)
     */
    protected $refmenu;

    /**
     * @ORM\ManyToOne(targetEntity="Menu", inversedBy="child")
     * @ORM\JoinColumn(name="menu_parent", referencedColumnName="menu_id", nullable=true)
     */
    protected $parent;

    /**
     * @var string
     *
     * @ORM\Column(name="menu_clef", type="string", length=255, nullable=true)
     */
    protected $clef;

    /**
     * @var bool
     *
     * @ORM\Column(name="menu_separateur", type="boolean")
     */
    protected $separateur;

    /**
     * @var text
     * @Gedmo\Translatable
     * @ORM\Column(name="menu_libelle", type="text", nullable=true)
     */
    protected $libelle;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="menu_description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @var array
     *
     * @ORM\Column(name="menu_data", type="array", nullable=true)
     */
    protected $data;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="menu_url", type="string", length=255, nullable=true)
     */
    protected $url;

    /**
     * @var string
     *
     * @ORM\Column(name="menu_cible", type="string", length=255, nullable=true)
     */
    protected $cible;

    /**
     * @var string
     *
     * @ORM\Column(name="menu_image", type="string", length=255, nullable=true)
     */
    protected $image;

    /**
     * @var int
     *
     * @ORM\Column(name="menu_position", type="integer", options={"default" = 0})
     */
    protected $position;

    /**
     * @var string
     *
     * @ORM\Column(name="menu_refgroup", type="string", length=255, nullable=true)
     */
    protected $refgroup;

    /**
     * @ORM\OneToMany(targetEntity="Menu", mappedBy="parent", cascade={"remove", "persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $child;

    /**
     * @var string
     *
     * @ORM\Column(name="menu_icon", type="string", length=255, nullable=true)
     */
    protected $icon;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     * and it is not necessary because globally locale can be set in listener
     */
    protected $locale;

    public function __construct()
    {
        $this->separateur = 0;
        $this->position   = 0;
    }

    public function __toString()
    {
        return $this->getLibelle();
    }

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set parent.
     *
     * @param Menu $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * get parent.
     *
     * @return Menu
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set clef.
     *
     * @param string $clef
     */
    public function setClef($clef)
    {
        $this->clef = $clef;
    }

    /**
     * get clef.
     *
     * @return string
     */
    public function getClef()
    {
        return $this->clef;
    }

    /**
     * Set separateur.
     *
     * @param bool $separateur
     */
    public function setSeparateur($separateur)
    {
        $this->separateur = $separateur;
    }

    /**
     * get separateur.
     *
     * @return bool
     */
    public function getSeparateur()
    {
        return $this->separateur;
    }

    /**
     * Set libelle.
     *
     * @param text $libelle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }

    /**
     * get libelle.
     *
     * @return text
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set url.
     *
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * get url.
     *
     * @return string
     */
    public function getUrl()
    {
        $url = $this->url;
        if ($url == 'user_logout') {
            $url = 'fos_user_security_logout';
        }

        return $url;
    }

    /**
     * Set cible.
     *
     * @param string $cible
     */
    public function setCible($cible)
    {
        $this->cible = $cible;
    }

    /**
     * get cible.
     *
     * @return string
     */
    public function getCible()
    {
        return $this->cible;
    }

    /**
     * Set image.
     *
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * get image.
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set position.
     *
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * get position.
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set refgroup.
     *
     * @param string $refgroup
     */
    public function setRefGroup($refgroup)
    {
        $this->refgroup = $refgroup;
    }

    /**
     * get refgroup.
     *
     * @return string
     */
    public function getRefGroup()
    {
        return $this->refgroup;
    }

    /**
     * Add child.
     *
     * @param Menu $child
     */
    public function addChild($child)
    {
        $child->setParent($this);
        $this->child->add($child);
    }

    /**
     * Add child.
     *
     * @param $childs
     */
    public function setChild($childs)
    {
        $this->child = $childs;
    }

    /**
     * get child.
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getChild()
    {
        return $this->child;
    }

    /**
     * Set description.
     *
     * @param text $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * get description.
     *
     * @return text
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set param.
     *
     * @param text  $param
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * get param.
     *
     * @return text
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Remove child.
     *
     * @param Menu $child
     */
    public function removeChild($child)
    {
        $this->child->removeElement($child);
    }

    /**
     * Set icon.
     *
     * @param string $icon
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    /**
     * get icon.
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * Add menu.
     *
     * @param Menu $menu
     *
     * @return Menu
     */
    public function addMenu($menu)
    {
        $menu->setRefmenu($this);
        $this->menus->add($menu);

        return $this;
    }

    /**
     * Remove menu.
     *
     * @param Menu $menu
     */
    public function removeMenu($menu)
    {
        $this->menus->removeElement($menu);
    }

    /**
     * get menus.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMenus()
    {
        return $this->menus;
    }

    /**
     * Set refmenu.
     *
     * @param Menu $refmenu
     *
     * @return Menu
     */
    public function setRefMenu($refmenu = null)
    {
        $this->refmenu = $refmenu;

        return $this;
    }

    /**
     * get refmenu.
     *
     * @return Menu
     */
    public function getRefMenu()
    {
        return $this->refmenu;
    }
}
