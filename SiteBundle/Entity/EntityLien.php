<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

class EntityLien
{
    /**
     * @var int
     *
     * @ORM\Column(name="lien_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="lien_adresse", type="text")
     */
    protected $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="lien_nom", type="string", length=255, nullable=true)
     */
    protected $nom;

    /**
     * @ORM\ManyToOne(targetEntity="Evenement", inversedBy="liens")
     * @ORM\JoinColumn(name="lien_refevenement", referencedColumnName="evenement_id", nullable=true, onDelete="CASCADE")
     */
    protected $refevenement;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="liens")
     * @ORM\JoinColumn(name="lien_refuser", referencedColumnName="user_id", nullable=true, onDelete="CASCADE")
     */
    protected $refuser;

    /**
     * @ORM\ManyToOne(targetEntity="Etablissement", inversedBy="liens")
     * @ORM\JoinColumn(name="lien_refetablissement", referencedColumnName="etablissement_id", nullable=true, onDelete="CASCADE")
     */
    protected $refetablissement;

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set adresse.
     *
     * @param string $adresse
     *
     * @return Email
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * get adresse.
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return Email
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    public function getFavicon()
    {
        return 'http://www.google.com/s2/favicons?domain=' . str_replace('http://', '', $this->getAdresse());
    }

    /**
     * Set refuser.
     *
     * @param User $refuser
     *
     * @return Lien
     */
    public function setRefUser($refuser = null)
    {
        $this->refuser = $refuser;

        return $this;
    }

    /**
     * get refuser.
     *
     * @return User
     */
    public function getRefUser()
    {
        return $this->refuser;
    }

    /**
     * Set refetablissement.
     *
     * @param Etablissement $refetablissement
     *
     * @return Lien
     */
    public function setRefEtablissement($refetablissement = null)
    {
        $this->refetablissement = $refetablissement;

        return $this;
    }

    /**
     * get refetablissement.
     *
     * @return Etablissement
     */
    public function getRefEtablissement()
    {
        return $this->refetablissement;
    }

    /**
     * @JMS\VirtualProperty()
     */
    public function getType()
    {
        $url  = $this->getAdresse();
        $tab  = [
            'dribbble'    => 'dribbble.com',
            'google +'    => 'plus.google.com',
            'youtube'     => ['youtube.com', 'youtu.be'],
            'github'      => 'github.com',
            'bitbucket'   => 'bitbucket.org',
            'twitter'     => 'twitter.com',
            'tripadvisor' => 'tripadvisor',
            'pinterest'   => 'pinterest.com',
            'facebook'    => 'facebook.com',
            'linkedin'    => 'linkedin.com',
            'instagram'   => 'instagram.com',
        ];
        $type = 'site';
        foreach ($tab as $key => $chaine) {
            if (is_array($chaine)) {
                foreach ($chaine as $code) {
                    if (substr_count($url, $code) != 0) {
                        $type = $key;
                        break;
                    }
                }
            } else {
                if (substr_count($url, $chaine) != 0) {
                    $type = $key;
                    break;
                }
            }
        }

        return $type;
    }

    /**
     * Set refevenement.
     *
     * @param User $refevenement
     *
     * @return Lien
     */
    public function setRefEvenement($refevenement = null)
    {
        $this->refevenement = $refevenement;

        return $this;
    }

    /**
     * get refevenement.
     *
     * @return Evenement
     */
    public function getRefEvenement()
    {
        return $this->refevenement;
    }
}
