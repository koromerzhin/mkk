<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

class EntityEdito implements Translatable
{
    /**
     * @var int
     *
     * @ORM\Column(name="edito_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var text
     * @Gedmo\Translatable
     * @ORM\Column(name="edito_titre", type="text", nullable=true)
     */
    protected $titre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="edito_datedebut", type="datetime", nullable=true)
     */
    protected $datedebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="edito_datefin", type="datetime", nullable=true)
     */
    protected $datefin;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="edito_contenu", type="text", nullable=true)
     */
    protected $contenu;

    /**
     * @var bool
     *
     * @ORM\Column(name="edito_publier", type="boolean",nullable=true)
     */
    protected $publier;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="editos")
     * @ORM\JoinColumn(name="edito_refuser", referencedColumnName="user_id")
     */
    protected $refuser;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     * and it is not necessary because globally locale can be set in listener
     */
    protected $locale;

    public function __construct()
    {
        $this->datedebut = null;
        $this->datefin   = null;
    }

    public function __toString()
    {
        return $this->getTitre();
    }

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre.
     *
     * @param text $titre
     *
     * @return Edito
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * get titre.
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set datedebut.
     *
     * @param \DateTime $datedebut
     *
     * @return Edito
     */
    public function setDatedebut($datedebut)
    {
        $this->datedebut = $datedebut;

        return $this;
    }

    /**
     * get datedebut.
     *
     * @var \DateTime
     */
    public function getDatedebut()
    {
        return $this->datedebut;
    }

    /**
     * Set datefin.
     *
     * @param \DateTime $datefin
     *
     * @return Edito
     */
    public function setDatefin($datefin)
    {
        $this->datefin = $datefin;

        return $this;
    }

    /**
     * get datefin.
     *
     * @var \DateTime
     */
    public function getDatefin()
    {
        return $this->datefin;
    }

    /**
     * Set contenu.
     *
     * @param string $contenu
     *
     * @return Edito
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * get contenu.
     *
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set publier.
     *
     * @param bool $publier
     *
     * @return Edito
     */
    public function setPublier($publier)
    {
        $this->publier = $publier;

        return $this;
    }

    /**
     * get publier.
     *
     * @return bool
     */
    public function getPublier()
    {
        return $this->publier;
    }

    /**
     * Set refuser.
     *
     * @param int $refuser
     *
     * @return Edito
     */
    public function setRefUser($refuser)
    {
        $this->refuser = $refuser;

        return $this;
    }

    /**
     * get refuser.
     *
     * @return int
     */
    public function getRefUser()
    {
        return $this->refuser;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    public function getUser()
    {
        return ! is_object($this->getRefUser()) ? '' : $this->getRefUser()->getId();
    }

    public function setUser($v)
    {
        return $this;
    }
}
