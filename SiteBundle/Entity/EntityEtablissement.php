<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use JMS\Serializer\Annotation as JMS;
use Mkk\SiteBundle\Annotation\UploadableField;

class EntityEtablissement implements Translatable
{
    /**
     * @var int
     *
     * @ORM\Column(name="etablissement_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Etablissement", mappedBy="refetablissement", cascade={"remove", "persist"})
     * @ORM\OrderBy({"position" = "ASC", "nom" = "ASC"})
     */
    protected $etablissements;

    /**
     * @ORM\ManyToOne(targetEntity="Etablissement", inversedBy="etablissements")
     * @ORM\JoinColumn(name="etablissement_refetablissement", referencedColumnName="etablissement_id", nullable=true)
     */
    protected $refetablissement;

    /**
     * @ORM\ManyToOne(targetEntity="NafSousClasse", inversedBy="etablissements")
     * @ORM\JoinColumn(name="etablissement_refnafsousclasse", referencedColumnName="nafsousclasse_id", nullable=true)
     */
    protected $refnafsousclasse;

    /**
     * @Gedmo\Slug(updatable=false,fields={"prefix","nom"})
     * @ORM\Column(name="etablissement_alias", type="string")
     */
    protected $alias;

    /**
     * @var text
     *
     * @ORM\Column(name="etablissement_prefix", type="text", nullable=true)
     */
    protected $prefix;

    /**
     * @var text
     *
     * @ORM\Column(name="etablissement_nom", type="text", nullable=true)
     */
    protected $nom;

    /**
     * @var text
     *
     * @ORM\Column(name="etablissement_directeur", type="text", nullable=true)
     */
    protected $directeur;

    /**
     * @var text
     *
     * @ORM\Column(name="etablissement_raisonsociale", type="text", nullable=true)
     */
    protected $raisonsociale;

    /**
     * @var text
     *
     * @ORM\Column(name="etablissement_formejuridique", type="text", nullable=true)
     */
    protected $formejuridique;

    /**
     * @var text
     *
     * @ORM\Column(name="etablissement_siret", type="text", nullable=true)
     */
    protected $siret;

    /**
     * @var text
     *
     * @ORM\Column(name="etablissement_ca", type="text", nullable=true)
     */
    protected $ca;

    /**
     * @var text
     *
     * @ORM\Column(name="etablissement_ape", type="text", nullable=true)
     */
    protected $ape;

    /**
     * @var text
     *
     * @ORM\Column(name="etablissement_tvaintra", type="text", nullable=true)
     */
    protected $tvaintra;

    /**
     * @var text
     * @Gedmo\Translatable
     * @ORM\Column(name="etablissement_descriptionactivite", type="text", nullable=true)
     */
    protected $descriptionactivite;

    /**
     * @ORM\Column(name="etablissement_copyright", type="text", nullable=true)
     */
    protected $copyright;

    /**
     * @var bool
     *
     * @ORM\Column(name="etablissement_actif", type="boolean", nullable=true)
     */
    protected $actif;

    /**
     * @var int
     *
     * @ORM\Column(name="etablissement_position", type="integer", nullable=true)
     */
    protected $position;

    /**
     * @var string
     *
     * @ORM\Column(name="etablissement_vignette", type="string", length=255, nullable=true)
     */
    protected $vignette;

    /**
     * @UploadableField(filename="vignette", path="etablissement/vignette", unique=true, alias="alias")
     */
    protected $file_vignette;

    /**
     * @var array
     *
     * @ORM\Column(name="etablissement_pdf", type="array", length=255, nullable=true)
     */
    protected $pdf;

    /**
     * @UploadableField(filename="pdf", path="etablissement/pdf", unique=false, alias="alias")
     */
    protected $file_pdf;

    /**
     * @var array
     *
     * @ORM\Column(name="etablissement_galerie", type="array", nullable=true)
     */
    protected $galerie;

    /**
     * @UploadableField(filename="galerie", path="etablissement/galerie", unique=false, alias="alias")
     */
    protected $file_galerie;

    /**
     * @var string
     *
     * @ORM\Column(name="etablissement_vuesinterne", type="array", nullable=true)
     */
    protected $vuesinterne;

    /**
     * @UploadableField(filename="vuesinterne", path="etablissement/vuesinterne", unique=false, alias="alias")
     */
    protected $file_vuesinterne;

    /**
     * @var string
     *
     * @ORM\Column(name="etablissement_vuesexterne", type="array", nullable=true)
     */
    protected $vuesexterne;

    /**
     * @UploadableField(filename="vuesexterne", path="etablissement/vuesexterne", unique=false, alias="alias")
     */
    protected $file_vuesexterne;

    /**
     * @var string
     *
     * @ORM\Column(name="etablissement_vuesequipe", type="array", nullable=true)
     */
    protected $vuesequipe;

    /**
     * @UploadableField(filename="vuesequipe", path="etablissement/vuesequipe", unique=false, alias="alias")
     */
    protected $file_vuesequipe;

    /**
     * @var string
     *
     * @ORM\Column(name="etablissement_meta_titre", type="string", length=255, nullable=true)
     */
    protected $metaTitre;

    /**
     * @var string
     *
     * @ORM\Column(name="etablissement_meta_description", type="string", length=255, nullable=true)
     */
    protected $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="etablissement_meta_keywords", type="string", length=255, nullable=true)
     */
    protected $metaKeywords;

    /**
     * @var int
     *
     * @ORM\Column(name="etablissement_nbsalarie", type="integer", nullable=true)
     */
    protected $nbsalarie;

    /**
     * @var string
     *
     * @ORM\Column(name="etablissement_videoiframe360", type="text", nullable=true)
     */
    protected $videoiframe360;

    /**
     * @var string
     *
     * @ORM\Column(name="etablissement_video", type="string", length=255, nullable=true)
     */
    protected $video;

    /**
     * @var string
     *
     * @ORM\Column(name="etablissement_activite", type="string", length=255, nullable=true)
     */
    protected $activite;

    /**
     * @var string
     *
     * @ORM\Column(name="etablissement_type", type="string", length=255, nullable=true)
     */
    protected $type;

    /**
     * @var text
     *
     * @ORM\Column(name="etablissement_accueil", type="boolean", nullable=true)
     */
    protected $accueil;

    /**
     * @JMS\Exclude()
     * @ORM\ManyToMany(targetEntity="User", inversedBy="etablissements", cascade={"remove", "persist"})
     * @ORM\JoinTable(
     *     joinColumns={@ORM\JoinColumn(name="refetablissement", referencedColumnName="etablissement_id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="refuser", referencedColumnName="user_id")}
     *     )
     */
    protected $users;

    /**
     * @ORM\OneToMany(targetEntity="Email", mappedBy="refetablissement", cascade={"all"})
     */
    protected $emails;

    /**
     * @ORM\OneToMany(targetEntity="Lien", mappedBy="refetablissement", cascade={"all"})
     */
    protected $liens;

    /**
     * @ORM\OneToMany(targetEntity="Horaire", mappedBy="refetablissement", cascade={"all"})
     * @ORM\OrderBy({"jour" = "ASC"})
     */
    protected $horaires;

    /**
     * @ORM\OneToMany(targetEntity="Telephone", mappedBy="refetablissement", cascade={"all"})
     */
    protected $telephones;

    /**
     * @ORM\OneToMany(targetEntity="Adresse", mappedBy="refetablissement", cascade={"all"})
     */
    protected $adresses;

    /**
     * @ORM\OneToMany(targetEntity="Etablissement", mappedBy="parent", cascade={"persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     * @JMS\Exclude()
     */
    protected $child;

    /**
     * @ORM\ManyToOne(targetEntity="Etablissement", inversedBy="child")
     * @ORM\JoinColumn(name="etablissement_parent", referencedColumnName="etablissement_id")
     */
    protected $parent;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     */
    protected $locale;

    /**
     * @ORM\OneToMany(targetEntity="Emplacement", mappedBy="refetablissement", cascade={"all"})
     */
    protected $emplacements;

    /**
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    public function __construct()
    {
        $this->actif      = 0;
        $this->liens      = new ArrayCollection();
        $this->emails     = new ArrayCollection();
        $this->telephones = new ArrayCollection();
        $this->horaires   = new ArrayCollection();
        $this->users      = new ArrayCollection();
    }

    /**
     * get id.
     *
     * @return int
     */
    public function __toString()
    {
        return $this->getNom();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alias.
     *
     * @param string $alias
     *
     * @return Etablissement
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * get alias.
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set accueil.
     *
     * @param string $accueil
     *
     * @return Etablissement
     */
    public function setAccueil($accueil)
    {
        $this->accueil = $accueil;

        return $this;
    }

    /**
     * get accueil.
     *
     * @return string
     */
    public function getAccueil()
    {
        return $this->accueil;
    }

    /**
     * Set actif.
     *
     * @param string $actifPublic
     *
     * @return Etablissement
     */
    public function setActif($actifPublic)
    {
        $this->actif = $actifPublic;

        return $this;
    }

    /**
     * get actif.
     *
     * @return string
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * Set siret.
     *
     * @param string $siret
     *
     * @return Etablissement
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * get siret.
     *
     * @return string
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * Set ca.
     *
     * @param string $ca
     *
     * @return Etablissement
     */
    public function setCa($ca)
    {
        $this->ca = $ca;

        return $this;
    }

    /**
     * get ca.
     *
     * @return string
     */
    public function getCa()
    {
        return $this->ca;
    }

    /**
     * Set ape.
     *
     * @param string $ape
     *
     * @return Etablissement
     */
    public function setApe($ape)
    {
        $this->ape = $ape;

        return $this;
    }

    /**
     * get ape.
     *
     * @return string
     */
    public function getApe()
    {
        return $this->ape;
    }

    /**
     * Set tvaintra.
     *
     * @param string $tvaintra
     *
     * @return Etablissement
     */
    public function setTvaintra($tvaintra)
    {
        $this->tvaintra = $tvaintra;

        return $this;
    }

    /**
     * get tvaintra.
     *
     * @return string
     */
    public function getTvaintra()
    {
        return $this->tvaintra;
    }

    /**
     * Set descriptionactivite.
     *
     * @param string $descriptionactivite
     *
     * @return Etablissement
     */
    public function setDescriptionactivite($descriptionactivite)
    {
        $this->descriptionactivite = $descriptionactivite;

        return $this;
    }

    /**
     * get descriptionactivite.
     *
     * @return string
     */
    public function getDescriptionactivite()
    {
        return $this->descriptionactivite;
    }

    /**
     * Set video.
     *
     * @param string $video
     *
     * @return Etablissement
     */
    public function setVideo($video)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * get video.
     *
     * @return string
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set galerie.
     *
     * @param array $galerie
     *
     * @return Etablissement
     */
    public function setGalerie($galerie)
    {
        $this->galerie = $galerie;

        return $this;
    }

    /**
     * get galerie.
     *
     * @return string
     */
    public function getGalerie()
    {
        return $this->galerie;
    }

    /**
     * Set meta_titre.
     *
     * @param string $metaTitre
     *
     * @return Etablissement
     */
    public function setMetaTitre($metaTitre)
    {
        $this->metaTitre = $metaTitre;

        return $this;
    }

    /**
     * get meta_titre.
     *
     * @return string
     */
    public function getMetaTitre()
    {
        return $this->metaTitre;
    }

    /**
     * Set meta_description.
     *
     * @param string $metaDescription
     *
     * @return Etablissement
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * get meta_description.
     *
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set meta_keywords.
     *
     * @param string $metaKeywords
     *
     * @return Etablissement
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    /**
     * get meta_keywords.
     *
     * @return string
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set nbsalarie.
     *
     * @param int $nbsalarie
     *
     * @return Etablissement
     */
    public function setNbsalarie($nbsalarie)
    {
        $this->nbsalarie = $nbsalarie;

        return $this;
    }

    /**
     * get nbsalarie.
     *
     * @return int
     */
    public function getNbsalarie()
    {
        return $this->nbsalarie;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return Etablissement
     */
    public function setNom($nom)
    {
        $this->nom = mb_strtoupper($nom, 'UTF-8');

        return $this;
    }

    /**
     * get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return mb_strtoupper($this->nom, 'UTF-8');
    }

    /**
     * Set position.
     *
     * @param int $position
     *
     * @return Etablissement
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * get position.
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set raisonsociale.
     *
     * @param string $raisonsociale
     *
     * @return Etablissement
     */
    public function setRaisonsociale($raisonsociale)
    {
        $this->raisonsociale = $raisonsociale;

        return $this;
    }

    /**
     * get raisonsociale.
     *
     * @return string
     */
    public function getRaisonsociale()
    {
        return $this->raisonsociale;
    }

    /**
     * Set formejuridique.
     *
     * @param string $formejuridique
     *
     * @return Etablissement
     */
    public function setFormejuridique($formejuridique)
    {
        $this->formejuridique = $formejuridique;

        return $this;
    }

    /**
     * get formejuridique.
     *
     * @return string
     */
    public function getFormejuridique()
    {
        return $this->formejuridique;
    }

    /**
     * Set vignette.
     *
     * @param string $vignette
     *
     * @return Etablissement
     */
    public function setVignette($vignette)
    {
        $this->vignette = $vignette;

        return $this;
    }

    /**
     * get vignette.
     *
     * @return string
     */
    public function getVignette()
    {
        $file = $this->vignette;

        return $file;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return Etablissement
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add users.
     *
     * @param User  $users
     * @param mixed $row
     *
     * @return Etablissement
     */
    public function addUser($row)
    {
        $this->users->add($row);

        return $this;
    }

    /**
     * Remove users.
     *
     * @param User  $users
     * @param mixed $user
     */
    public function removeUser($user)
    {
        $this->users->removeElement($user);
    }

    /**
     * get users.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add child.
     *
     * @param Etablissement $child
     * @param mixed         $row
     */
    public function addChild($row)
    {
        $row->setParent($this);
        $this->child->add($row);
    }

    /**
     * Add child.
     *
     * @param $childs
     */
    public function setChild($childs)
    {
        $this->child = $childs;
    }

    /**
     * get child.
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getChild()
    {
        return $this->child;
    }

    /**
     * Set parent.
     *
     * @param Etablissement $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * get parent.
     *
     * @return Etablissement
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add emails.
     *
     * @param Email $emails
     * @param mixed $row
     *
     * @return Etablissement
     */
    public function addEmail($row)
    {
        $row->setRefEtablissement($this);
        $this->emails->add($row);

        return $this;
    }

    /**
     * Remove emails.
     *
     * @param Email $emails
     */
    public function removeEmail($emails)
    {
        $this->emails->removeElement($emails);
    }

    /**
     * get emails.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * Add liens.
     *
     * @param \Lien $liens
     * @param mixed $row
     *
     * @return Etablissement
     */
    public function addLien($row)
    {
        $row->setRefEtablissement($this);
        $this->liens->add($row);

        return $this;
    }

    /**
     * Remove liens.
     *
     * @param Lien $liens
     */
    public function removeLien($liens)
    {
        $this->liens->removeElement($liens);
    }

    /**
     * get liens.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLiens()
    {
        return $this->liens;
    }

    /**
     * Add horaires.
     *
     * @param Horaire $horaires
     * @param mixed   $row
     *
     * @return Etablissement
     */
    public function addHoraire($row)
    {
        $row->setRefEtablissement($this);
        $this->horaires->add($row);

        return $this;
    }

    /**
     * Remove horaires.
     *
     * @param Horaire $horaires
     */
    public function removeHoraire($horaires)
    {
        $this->horaires->removeElement($horaires);
    }

    /**
     * get horaires.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHoraires()
    {
        return $this->horaires;
    }

    /**
     * Add adresses.
     *
     * @param Adresse $adresses
     * @param mixed   $row
     *
     * @return Etablissement
     */
    public function addAdress($row)
    {
        $row->setRefEtablissement($this);
        $this->adresses->add($row);

        return $this;
    }

    /**
     * Remove adresses.
     *
     * @param Adresse $adresses
     */
    public function removeAdress($adresses)
    {
        $this->adresses->removeElement($adresses);
    }

    /**
     * get adresses.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdresses()
    {
        return $this->adresses;
    }

    /**
     * Add telephones.
     *
     * @param Telephone $telephones
     * @param mixed     $telephone
     *
     * @return Etablissement
     */
    public function addTelephone($telephone)
    {
        $telephone->setRefEtablissement($this);
        $this->telephones->add($telephone);

        return $this;
    }

    /**
     * Remove telephones.
     *
     * @param Telephone $telephones
     */
    public function removeTelephone($telephones)
    {
        $this->telephones->removeElement($telephones);
    }

    /**
     * get telephones.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTelephones()
    {
        return $this->telephones;
    }

    public function getCoordonnees()
    {
        $data       = [];
        $telephones = $this->getTelephones();
        if (is_array($telephones)) {
            foreach ($telephones as $key => $row) {
                $data[$key] = $row;
            }
        }

        if (is_array($telephones)) {
            $emails = $this->getEmails();
            foreach ($emails as $key => $row) {
                $data[$key] = $row;
            }
        }

        if (is_array($telephones)) {
            $sites = $this->getLiens();
            foreach ($sites as $key => $row) {
                $data[$key] = $row;
            }
        }

        return $data;
    }

    /**
     * Set the value of Users.
     *
     * @param mixed users
     * @param mixed $users
     *
     * @return self
     */
    public function setUsers($users)
    {
        $this->users = $users;

        return $this;
    }

    /**
     * Set the value of Emails.
     *
     * @param mixed emails
     * @param mixed $emails
     *
     * @return self
     */
    public function setEmails($emails)
    {
        $this->emails = $emails;

        return $this;
    }

    /**
     * Set the value of Liens.
     *
     * @param mixed liens
     * @param mixed $liens
     *
     * @return self
     */
    public function setLiens($liens)
    {
        $this->liens = $liens;

        return $this;
    }

    /**
     * Set the value of Horaires.
     *
     * @param mixed horaires
     * @param mixed $horaires
     *
     * @return self
     */
    public function setHoraires($horaires)
    {
        $this->horaires = $horaires;

        return $this;
    }

    /**
     * Set the value of Telephones.
     *
     * @param mixed telephones
     * @param mixed $telephones
     *
     * @return self
     */
    public function setTelephones($telephones)
    {
        $this->telephones = $telephones;

        return $this;
    }

    /**
     * Set the value of Adresses.
     *
     * @param mixed adresses
     * @param mixed $adresses
     *
     * @return self
     */
    public function setAdresses($adresses)
    {
        $this->adresses = $adresses;

        return $this;
    }

    /**
     * Set the value of Configs.
     *
     * @param mixed configs
     * @param mixed $configs
     *
     * @return self
     */
    public function setConfigs($configs)
    {
        $this->configs = $configs;

        return $this;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * get the value of Vuesinterne.
     *
     * @return string
     */
    public function getVuesinterne()
    {
        return $this->vuesinterne;
    }

    /**
     * Set the value of Vuesinterne.
     *
     * @param string vuesinterne
     * @param mixed $vuesinterne
     *
     * @return self
     */
    public function setVuesinterne($vuesinterne)
    {
        $this->vuesinterne = $vuesinterne;

        return $this;
    }

    /**
     * get the value of Vuesexterne.
     *
     * @return string
     */
    public function getVuesexterne()
    {
        return $this->vuesexterne;
    }

    /**
     * Set the value of Vuesexterne.
     *
     * @param string vuesexterne
     * @param mixed $vuesexterne
     *
     * @return self
     */
    public function setVuesexterne($vuesexterne)
    {
        $this->vuesexterne = $vuesexterne;

        return $this;
    }

    /**
     * get the value of Locale.
     *
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set the value of Locale.
     *
     * @param mixed locale
     * @param mixed $locale
     *
     * @return self
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * get the value of Vuesequipe.
     *
     * @return string
     */
    public function getVuesequipe()
    {
        return $this->vuesequipe;
    }

    /**
     * Set the value of Vuesequipe.
     *
     * @param string vuesequipe
     * @param mixed $vuesequipe
     *
     * @return self
     */
    public function setVuesequipe($vuesequipe)
    {
        $this->vuesequipe = $vuesequipe;

        return $this;
    }

    /**
     * Add emplacements.
     *
     * @param Emplacement $emplacements
     * @param mixed       $row
     *
     * @return Etablissement
     */
    public function addEmplacement($row)
    {
        $row->setRefEtablissement($this);
        $this->emplacements->add($row);

        return $this;
    }

    /**
     * Remove emplacements.
     *
     * @param Emplacement $emplacements
     */
    public function removeEmplacement($emplacements)
    {
        $this->emplacements->removeElement($emplacements);
    }

    /**
     * get emplacements.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmplacements()
    {
        return $this->emplacements;
    }

    /**
     * get the value of Directeur.
     *
     * @return text
     */
    public function getDirecteur()
    {
        $tab       = get_html_translation_table(HTML_ENTITIES, ENT_QUOTES | ENT_HTML5);
        $directeur = strip_tags($this->directeur);
        foreach ($tab as $code => $val) {
            $directeur = str_replace($val, $code, $directeur);
        }

        return $directeur;
    }

    /**
     * Set the value of Directeur.
     *
     * @param text directeur
     * @param mixed $directeur
     *
     * @return self
     */
    public function setDirecteur($directeur)
    {
        $this->directeur = $directeur;

        return $this;
    }

    /**
     * Set the value of Emplacements.
     *
     * @param mixed emplacements
     * @param mixed $emplacements
     *
     * @return self
     */
    public function setEmplacements($emplacements)
    {
        $this->emplacements = $emplacements;

        return $this;
    }

    /**
     * get the value of Copyright.
     *
     * @return mixed
     */
    public function getCopyright()
    {
        return $this->copyright;
    }

    /**
     * Set the value of Copyright.
     *
     * @param mixed copyright
     * @param mixed $copyright
     *
     * @return self
     */
    public function setCopyright($copyright)
    {
        $this->copyright = $copyright;

        return $this;
    }

    /**
     * get the value of Prefix.
     *
     * @return text
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * Set the value of Prefix.
     *
     * @param text prefix
     * @param mixed $prefix
     *
     * @return self
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;

        return $this;
    }

    public function setLocaleSession($locale)
    {
        foreach ($this->adresses as $i => $adresse) {
            $this->adresses[$i]->setLocaleSession($locale);
        }
    }

    public function getSearchData()
    {
        $tab = [
            'id'  => $this->getId(),
            'nom' => $this->__toString(),
        ];

        return $tab;
    }

    /**
     * Add etablissement.
     *
     * @param Etablissement $etablissement
     * @param mixed         $row
     *
     * @return Etablissement
     */
    public function addEtablissement($row)
    {
        $row->setRefEtablissement($this);
        $this->etablissements->add($row);

        return $this;
    }

    /**
     * Remove etablissement.
     *
     * @param Etablissement $etablissement
     */
    public function removeEtablissement($etablissement)
    {
        $this->etablissements->removeElement($etablissement);
    }

    /**
     * get etablissements.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEtablissements()
    {
        return $this->etablissements;
    }

    /**
     * Set refetablissement.
     *
     * @param Etablissement $refetablissement
     *
     * @return Etablissement
     */
    public function setRefEtablissement($refetablissement = null)
    {
        $this->refetablissement = $refetablissement;

        return $this;
    }

    /**
     * get refetablissement.
     *
     * @return Etablissement
     */
    public function getRefEtablissement()
    {
        return $this->refetablissement;
    }

    /**
     * Get the value of Refnafsousclasse.
     *
     * @return mixed
     */
    public function getRefNafSousClasse()
    {
        return $this->refnafsousclasse;
    }

    public function getNafSousClasse()
    {
        return ! is_object($this->getRefNafSousClasse()) ? '' : $this->getRefNafSousClasse()->getId();
    }

    public function setNafSousClasse($v)
    {
        return $this;
    }

    /**
     * Set the value of Etablissements.
     *
     * @param mixed etablissements
     * @param mixed $etablissements
     *
     * @return self
     */
    public function setEtablissements($etablissements)
    {
        $this->etablissements = $etablissements;

        return $this;
    }

    /**
     * Set the value of Refnafsousclasse.
     *
     * @param mixed refnafsousclasse
     * @param mixed $refnafsousclasse
     *
     * @return self
     */
    public function setRefNafsousclasse($refnafsousclasse)
    {
        $this->refnafsousclasse = $refnafsousclasse;

        return $this;
    }

    /**
     * Get the value of Activite.
     *
     * @return string
     */
    public function getActivite()
    {
        return $this->activite;
    }

    /**
     * Set the value of Activite.
     *
     * @param string activite
     * @param mixed $activite
     *
     * @return self
     */
    public function setActivite($activite)
    {
        $this->activite = $activite;

        return $this;
    }

    /**
     * Get the value of Updated At.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set the value of Updated At.
     *
     * @param \DateTime updatedAt
     *
     * @return self
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get the value of Pdf.
     *
     * @return array
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Set the value of Pdf.
     *
     * @param array pdf
     * @param mixed $pdf
     *
     * @return self
     */
    public function setPdf($pdf)
    {
        $this->pdf = $pdf;

        return $this;
    }

    /**
     * Get the value of File Pdf.
     *
     * @return mixed
     */
    public function getFilePdf()
    {
        return $this->file_pdf;
    }

    /**
     * Set the value of File Pdf.
     *
     * @param mixed file_pdf
     * @param mixed $file_pdf
     *
     * @return self
     */
    public function setFilePdf($file_pdf)
    {
        $this->file_pdf = $file_pdf;

        return $this;
    }

    /**
     * Get the value of File Vignette.
     *
     * @return mixed
     */
    public function getFileVignette()
    {
        return $this->file_vignette;
    }

    /**
     * Set the value of File Vignette.
     *
     * @param mixed file_vignette
     * @param mixed $file_vignette
     *
     * @return self
     */
    public function setFileVignette($file_vignette)
    {
        $this->file_vignette = $file_vignette;

        return $this;
    }

    /**
     * Get the value of File Galerie.
     *
     * @return mixed
     */
    public function getFileGalerie()
    {
        return $this->file_galerie;
    }

    /**
     * Set the value of File Galerie.
     *
     * @param mixed file_galerie
     * @param mixed $file_galerie
     *
     * @return self
     */
    public function setFileGalerie($file_galerie)
    {
        $this->file_galerie = $file_galerie;

        return $this;
    }

    /**
     * Get the value of File Vuesinterne.
     *
     * @return mixed
     */
    public function getFileVuesinterne()
    {
        return $this->file_vuesinterne;
    }

    /**
     * Set the value of File Vuesinterne.
     *
     * @param mixed file_vuesinterne
     * @param mixed $file_vuesinterne
     *
     * @return self
     */
    public function setFileVuesinterne($file_vuesinterne)
    {
        $this->file_vuesinterne = $file_vuesinterne;

        return $this;
    }

    /**
     * Get the value of File Vuesexterne.
     *
     * @return mixed
     */
    public function getFileVuesexterne()
    {
        return $this->file_vuesexterne;
    }

    /**
     * Set the value of File Vuesexterne.
     *
     * @param mixed file_vuesexterne
     * @param mixed $file_vuesexterne
     *
     * @return self
     */
    public function setFileVuesexterne($file_vuesexterne)
    {
        $this->file_vuesexterne = $file_vuesexterne;

        return $this;
    }

    /**
     * Get the value of File Vuesequipe.
     *
     * @return mixed
     */
    public function getFileVuesequipe()
    {
        return $this->file_vuesequipe;
    }

    /**
     * Set the value of File Vuesequipe.
     *
     * @param mixed file_vuesequipe
     * @param mixed $file_vuesequipe
     *
     * @return self
     */
    public function setFileVuesequipe($file_vuesequipe)
    {
        $this->file_vuesequipe = $file_vuesequipe;

        return $this;
    }

    /**
     * Get the value of Videoiframe.
     *
     * @return string
     */
    public function getVideoiframe360()
    {
        return $this->videoiframe360;
    }

    /**
     * Set the value of Videoiframe.
     *
     * @param string videoiframe360
     * @param mixed $videoiframe360
     *
     * @return self
     */
    public function setVideoiframe360($videoiframe360)
    {
        $this->videoiframe360 = $videoiframe360;

        return $this;
    }
}
