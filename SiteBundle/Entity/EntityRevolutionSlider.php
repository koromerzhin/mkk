<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

class EntityRevolutionSlider
{
    /**
     * @var int
     *
     * @ORM\Column(name="revolutionslider_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="revolutionslider_titre", type="string", length=255, nullable=true)
     */
    protected $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="revolutionslider_code", type="string", length=255, nullable=true)
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="revolutionslider_langue", type="text", nullable=true)
     */
    protected $langue;

    /**
     * @var array
     *
     * @ORM\Column(name="revolutionslider_param", type="array", nullable=true)
     */
    protected $param;

    /**
     * @ORM\OneToMany(targetEntity="RevolutionSliderSlide", mappedBy="refrevolutionslider", cascade={"remove", "persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $revolutionsliderslides;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->slides = new ArrayCollection();
    }

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre.
     *
     * @param string $titre
     *
     * @return Slider
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * get titre.
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return Slider
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set langue.
     *
     * @param string $langue
     *
     * @return Slider
     */
    public function setLangue($langue)
    {
        $this->langue = $langue;

        return $this;
    }

    /**
     * get langue.
     *
     * @return string
     */
    public function getLangue()
    {
        return $this->langue;
    }

    /**
     * Set param.
     *
     * @param string $param
     *
     * @return Slider
     */
    public function setParam($param)
    {
        $this->param = $param;

        return $this;
    }

    /**
     * get param.
     *
     * @param mixed $type
     *
     * @return string
     */
    public function getParam()
    {
        return $this->param;
    }

    /**
     * Add revolutionsliderslides.
     *
     * @param $revolutionsliderslides
     *
     * @return RevolutionSliderSlide
     */
    public function addRevolutionSliderSlide($revolutionsliderslides)
    {
        $revolutionsliderslides->setRefRevolutionSlider($this);
        $this->revolutionsliderslides->add($revolutionsliderslides);

        return $this;
    }

    /**
     * Remove revolutionsliderslides.
     *
     * @param $revolutionsliderslides
     */
    public function removeRevolutionSliderSlide($revolutionsliderslides)
    {
        $this->revolutionsliderslides->removeElement($revolutionsliderslides);
    }

    /**
     * get revolutionsliderslides.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRevolutionSliderSlides()
    {
        return $this->revolutionsliderslides;
    }
}
