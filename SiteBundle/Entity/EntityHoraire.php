<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class EntityHoraire
{
    /**
     * @var int
     *
     * @ORM\Column(name="horaire_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="horaire_jour", type="integer")
     */
    protected $jour;

    /**
     * @var float
     *
     * @ORM\Column(name="horaire_dm", type="text", nullable=true)
     */
    protected $dm;

    /**
     * @var float
     *
     * @ORM\Column(name="horaire_fm", type="text", nullable=true)
     */
    protected $fm;

    /**
     * @var float
     *
     * @ORM\Column(name="horaire_da", type="text", nullable=true)
     */
    protected $da;

    /**
     * @var float
     *
     * @ORM\Column(name="horaire_fa", type="text", nullable=true)
     */
    protected $fa;

    /**
     * @ORM\ManyToOne(targetEntity="Etablissement", inversedBy="horaires")
     * @ORM\JoinColumn(name="horaire_refetablissement", nullable=true, referencedColumnName="etablissement_id", onDelete="CASCADE")
     */
    protected $refetablissement;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->dm = '00:00';
        $this->fm = '00:00';
        $this->da = '00:00';
        $this->fa = '00:00';
    }

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set jour.
     *
     * @param int $jour
     *
     * @return Horaire
     */
    public function setJour($jour)
    {
        $this->jour = $jour;

        return $this;
    }

    /**
     * get jour.
     *
     * @return int
     */
    public function getJour()
    {
        return $this->jour;
    }

    /**
     * Set dm.
     *
     * @param float $dm
     *
     * @return Horaire
     */
    public function setDm($dm)
    {
        $this->dm = $dm;

        return $this;
    }

    /**
     * get dm.
     *
     * @return float
     */
    public function getDm()
    {
        return $this->dm;
    }

    /**
     * Set fm.
     *
     * @param float $fm
     *
     * @return Horaire
     */
    public function setFm($fm)
    {
        $this->fm = $fm;

        return $this;
    }

    /**
     * get fm.
     *
     * @return float
     */
    public function getFm()
    {
        return $this->fm;
    }

    /**
     * Set da.
     *
     * @param float $da
     *
     * @return Horaire
     */
    public function setDa($da)
    {
        $this->da = $da;

        return $this;
    }

    /**
     * get da.
     *
     * @return float
     */
    public function getDa()
    {
        return $this->da;
    }

    /**
     * Set fa.
     *
     * @param float $fa
     *
     * @return Horaire
     */
    public function setFa($fa)
    {
        $this->fa = $fa;

        return $this;
    }

    /**
     * get fa.
     *
     * @return float
     */
    public function getFa()
    {
        return $this->fa;
    }

    /**
     * Set refetablissement.
     *
     * @param Etablissement $refetablissement
     *
     * @return Horaire
     */
    public function setRefEtablissement($refetablissement = null)
    {
        $this->refetablissement = $refetablissement;

        return $this;
    }

    /**
     * get refetablissement.
     *
     * @return Etablissement
     */
    public function getRefEtablissement()
    {
        return $this->refetablissement;
    }
}
