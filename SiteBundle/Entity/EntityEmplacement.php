<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

class EntityEmplacement
{
    /**
     * @ORM\OneToMany(targetEntity="Adresse", mappedBy="refemplacement", cascade={"remove", "persist"})
     */
    protected $adresses;

    /**
     * @var bool
     *
     * @ORM\Column(name="emplacement_placeillimite", type="boolean", nullable=true)
     */
    protected $placeillimite;

    /**
     * @ORM\ManyToOne(targetEntity="Etablissement", inversedBy="emplacements")
     * @ORM\JoinColumn(name="emplacement_refetablissement", referencedColumnName="etablissement_id")
     */
    protected $refetablissement;

    /**
     * @ORM\ManyToOne(targetEntity="Evenement", inversedBy="emplacements")
     * @ORM\JoinColumn(name="emplacement_refevenement", referencedColumnName="evenement_id")
     */
    protected $refevenement;

    /**
     * @ORM\OneToMany(targetEntity="Date", mappedBy="refemplacement", cascade={"remove", "persist"})
     * @ORM\OrderBy({"debut" = "ASC"})
     */
    protected $dates;

    /**
     * @var string
     *
     * @ORM\Column(name="emplacement_totalnbdate", type="integer", options={"default" = 0})
     */
    protected $totalnbdate;

    /**
     * @var string
     *
     * @ORM\Column(name="emplacement_totalnbplace", type="integer", options={"default" = 0})
     */
    protected $totalnbplace;

    /**
     * @var int
     *
     * @ORM\Column(name="emplacement_mindate", type="integer", options={"default" = 0})
     */
    protected $mindate;

    /**
     * @var int
     *
     * @ORM\Column(name="emplacement_maxdate", type="integer", options={"default" = 0})
     */
    protected $maxdate;
    /**
     * @var int
     *
     * @ORM\Column(name="emplacement_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->totalnbdate  = 0;
        $this->totalnbplace = 0;
        $this->mindate      = 0;
        $this->maxdate      = 0;
        $this->adresses     = new ArrayCollection();
    }

    public function __toString()
    {
        $adresses = $this->getAdresses();
        if (count($adresses) != 0) {
            foreach ($adresses as $adresse) {
                $texte = $adresse->getInfo() . ' - ' . $adresse->getCp() . ' ' . $adresse->getVille();
                break;
            }
        } else {
            $texte = $this->getRefEtablissement()->getNom();
        }

        return $texte;
    }

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add adresses.
     *
     * @param Adresse $adresses
     *
     * @return Emplacement
     */
    public function addAdress($adresses)
    {
        $adresses->setRefEmplacement($this);
        $this->adresses->add($adresses);

        return $this;
    }

    /**
     * Remove adresses.
     *
     * @param Adresse $adresses
     */
    public function removeAdress($adresses)
    {
        $this->adresses->removeElement($adresses);
    }

    /**
     * get adresses.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdresses()
    {
        return $this->adresses;
    }

    /**
     * Set refletablissement.
     *
     * @param Etablissement $refetablissement
     *
     * @return Emplacement
     */
    public function setRefEtablissement($refetablissement = null)
    {
        $this->refetablissement = $refetablissement;

        return $this;
    }

    /**
     * get refetablissement.
     *
     * @return Etablissement
     */
    public function getRefEtablissement()
    {
        return $this->refetablissement;
    }

    /**
     * Set refevenement.
     *
     * @param Evenement $refevenement
     *
     * @return Emplacement
     */
    public function setRefEvenement($refevenement = null)
    {
        $this->refevenement = $refevenement;

        return $this;
    }

    /**
     * get refevenement.
     *
     * @return Evenement
     */
    public function getRefEvenement()
    {
        return $this->refevenement;
    }

    /**
     * Add dates.
     *
     * @param Date $dates
     *
     * @return Emplacement
     */
    public function addDate($dates)
    {
        $dates->setRefEmplacement($this);
        $this->dates->add($dates);

        return $this;
    }

    /**
     * Remove dates.
     *
     * @param Date $dates
     */
    public function removeDate($dates)
    {
        $this->dates->removeElement($dates);
    }

    /**
     * get dates.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDates()
    {
        return $this->dates;
    }

    public function getDatesAvecPlace()
    {
        $places = [];
        $dates  = $this->getDates();
        foreach ($dates as $date) {
            if ($date->getReservationsRestant() > 0) {
                $places[] = $date;
            }
        }

        return $places;
    }

    /**
     * Set the value of Adresses.
     *
     * @param mixed adresses
     * @param mixed          $adresses
     *
     * @return self
     */
    public function setAdresses($adresses)
    {
        $this->adresses = $adresses;

        return $this;
    }

    /**
     * Set the value of Dates.
     *
     * @param mixed dates
     * @param mixed       $dates
     *
     * @return self
     */
    public function setDates($dates)
    {
        $this->dates = $dates;

        return $this;
    }

    /**
     * get the value of Totalnbdate.
     *
     * @return string
     */
    public function getTotalnbdate()
    {
        return $this->totalnbdate;
    }

    /**
     * Set the value of Totalnbdate.
     *
     * @param string totalnbdate
     * @param mixed              $totalnbdate
     *
     * @return self
     */
    public function setTotalnbdate($totalnbdate)
    {
        $this->totalnbdate = $totalnbdate;

        return $this;
    }

    /**
     * get the value of Mindate.
     *
     * @return int
     */
    public function getMindate()
    {
        return $this->mindate;
    }

    /**
     * Set the value of Mindate.
     *
     * @param int mindate
     * @param mixed       $mindate
     *
     * @return self
     */
    public function setMindate($mindate)
    {
        $this->mindate = $mindate;

        return $this;
    }

    /**
     * get the value of Maxdate.
     *
     * @return int
     */
    public function getMaxdate()
    {
        return $this->maxdate;
    }

    /**
     * Set the value of Maxdate.
     *
     * @param int maxdate
     * @param mixed       $maxdate
     *
     * @return self
     */
    public function setMaxdate($maxdate)
    {
        $this->maxdate = $maxdate;

        return $this;
    }

    /**
     * get the value of Totalnbplace.
     *
     * @return string
     */
    public function getTotalnbplace()
    {
        return $this->totalnbplace;
    }

    /**
     * Set the value of Totalnbplace.
     *
     * @param string totalnbplace
     * @param mixed               $totalnbplace
     *
     * @return self
     */
    public function setTotalnbplace($totalnbplace)
    {
        $this->totalnbplace = $totalnbplace;

        return $this;
    }

    /**
     * get the value of Placeillimite.
     *
     * @return bool
     */
    public function getPlaceillimite()
    {
        return $this->placeillimite;
    }

    /**
     * Set the value of Placeillimite.
     *
     * @param bool placeillimite
     * @param mixed              $placeillimite
     *
     * @return self
     */
    public function setPlaceillimite($placeillimite)
    {
        $this->placeillimite = $placeillimite;

        return $this;
    }
}
