<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class EntityTelephone
{
    /**
     * @var int
     *
     * @ORM\Column(name="telephone_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone_chiffre", type="text")
     */
    protected $chiffre;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone_type", type="string", length=255, nullable=true)
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone_utilisation", type="string", length=255, nullable=true)
     */
    protected $utilisation;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone_pays", type="string", length=255, nullable=true)
     */
    protected $pays;

    /**
     * @ORM\ManyToOne(targetEntity="Evenement", inversedBy="telephones")
     * @ORM\JoinColumn(name="telephone_refevenement", referencedColumnName="evenement_id", nullable=true, onDelete="CASCADE")
     */
    protected $refevenement;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="telephones")
     * @ORM\JoinColumn(name="telephone_refuser", referencedColumnName="user_id", nullable=true, onDelete="CASCADE")
     */
    protected $refuser;

    /**
     * @ORM\ManyToOne(targetEntity="Etablissement", inversedBy="telephones")
     * @ORM\JoinColumn(name="telephone_refetablissement", referencedColumnName="etablissement_id", nullable=true, onDelete="CASCADE")
     */
    protected $refetablissement;

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return Telephone
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set chiffre.
     *
     * @param string $chiffre
     *
     * @return Telephone
     */
    public function setChiffre($chiffre)
    {
        $this->chiffre = $chiffre;

        return $this;
    }

    /**
     * get chiffre.
     *
     * @return string
     */
    public function getChiffre()
    {
        $chiffre = str_replace(' ', '', $this->chiffre);

        return $chiffre;
    }

    /**
     * Set pays.
     *
     * @param string $pays
     *
     * @return Telephone
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * get pays.
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set utilisation.
     *
     * @param string $utilisation
     *
     * @return Telephone
     */
    public function setUtilisation($utilisation)
    {
        $this->utilisation = $utilisation;

        return $this;
    }

    /**
     * get utilisation.
     *
     * @return string
     */
    public function getUtilisation()
    {
        return $this->utilisation;
    }

    /**
     * Set refuser.
     *
     * @param User $refuser
     *
     * @return Telephone
     */
    public function setRefUser($refuser = null)
    {
        $this->refuser = $refuser;

        return $this;
    }

    /**
     * get refuser.
     *
     * @return User
     */
    public function getRefUser()
    {
        return $this->refuser;
    }

    /**
     * Set refetablissement.
     *
     * @param Etablissement $refetablissement
     *
     * @return Telephone
     */
    public function setRefEtablissement($refetablissement = null)
    {
        $this->refetablissement = $refetablissement;

        return $this;
    }

    /**
     * get refetablissement.
     *
     * @return Etablissement
     */
    public function getRefEtablissement()
    {
        return $this->refetablissement;
    }

    /**
     * Set refevenement.
     *
     * @param Etablissement $refevenement
     * @param null|mixed    $refvenement
     *
     * @return Telephone
     */
    public function setRefEvenement($refevenement = null)
    {
        $this->refevenement = $refevenement;

        return $this;
    }

    /**
     * get refevenement.
     *
     * @return Evenement
     */
    public function getRefEvenement()
    {
        return $this->refevenement;
    }
}
