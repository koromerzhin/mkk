<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class EntityConfig
{
    /**
     * @var int
     *
     * @ORM\Column(name="config_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="config_code", type="text")
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="config_valeur", type="text", nullable=true)
     */
    protected $valeur;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="configs")
     * @ORM\JoinColumn(name="config_refuser", referencedColumnName="user_id", nullable=true, onDelete="CASCADE")
     */
    protected $refuser;

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return Param
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set valeur.
     *
     * @param string $valeur
     *
     * @return Param
     */
    public function setValeur($valeur)
    {
        $this->valeur = $valeur;

        return $this;
    }

    /**
     * get valeur.
     *
     * @return string
     */
    public function getValeur()
    {
        return $this->valeur;
    }

    /**
     * Set refuser.
     *
     * @param User $refuser
     *
     * @return Config
     */
    public function setRefUser($refuser = null)
    {
        $this->refuser = $refuser;

        return $this;
    }

    /**
     * get refuser.
     *
     * @return User
     */
    public function getRefUser()
    {
        return $this->refuser;
    }
}
