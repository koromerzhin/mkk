<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

class EntityNafSection implements Translatable
{
    /**
     * @var int
     *
     * @ORM\Column(name="nafsection_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nafsection_code", type="string", length=255)
     */
    protected $code;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="nafsection_libelle", type="string", length=255)
     */
    protected $libelle;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     */
    protected $locale;

    public function __toString()
    {
        return $this->getCode() . ' ' . $this->getLibelle();
    }

    /**
     * Get the value of Id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * Get the value of Code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set the value of Code.
     *
     * @param string code
     * @param mixed       $code
     *
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get the value of Libelle.
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set the value of Libelle.
     *
     * @param string libelle
     * @param mixed          $libelle
     *
     * @return self
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getSearchData()
    {
        $tab = [
            'id'  => $this->getId(),
            'nom' => $this->getCode() . ' ' . $this->getLibelle(),
        ];

        return $tab;
    }
}
