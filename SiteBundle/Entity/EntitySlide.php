<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class EntitySlide
{
    /**
     * @var int
     *
     * @ORM\Column(name="slide_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="slide_titre", type="text", nullable=true)
     */
    protected $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="slide_soustitre", type="text", nullable=true)
     */
    protected $soustitre;

    /**
     * @var string
     *
     * @ORM\Column(name="slide_url", type="text", nullable=true)
     */
    protected $url;

    /**
     * @var string
     *
     * @ORM\Column(name="slide_contenu", type="text", nullable=true)
     */
    protected $contenu;

    /**
     * @var string
     *
     * @ORM\Column(name="slide_vignette", type="text", nullable=true)
     */
    protected $vignette;

    /**
     * @var string
     *
     * @ORM\Column(name="slide_type", type="text", nullable=true)
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="slide_image", type="text", nullable=true)
     */
    protected $image;

    /**
     * @var string
     *
     * @ORM\Column(name="slide_imagemobile", type="text", nullable=true)
     */
    protected $imagemobile;

    /**
     * @var string
     *
     * @ORM\Column(name="slide_opacity", type="text", nullable=true)
     */
    protected $opacity;

    /**
     * @var string
     *
     * @ORM\Column(name="slide_videobackground", type="text", nullable=true)
     */
    protected $videobackground;

    /**
     * @var string
     *
     * @ORM\Column(name="slide_videoiframe", type="text", nullable=true)
     */
    protected $videoiframe;

    /**
     * @var string
     *
     * @ORM\Column(name="slide_video", type="text", nullable=true)
     */
    protected $video;

    /**
     * @var int
     *
     * @ORM\Column(name="slide_position", type="integer", nullable=true)
     */
    protected $position;

    /**
     * @var text
     *
     * @ORM\Column(name="slide_actif_public", type="text", nullable=true)
     */
    protected $actifPublic;

    /**
     * @var string
     *
     * @ORM\Column(name="slide_description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @var string
     *
     * @ORM\Column(name="slide_filtre", type="boolean", nullable=true)
     */
    protected $filtre;

    /**
     * get the value of Id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * get the value of Titre.
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set the value of Titre.
     *
     * @param string titre
     * @param mixed        $titre
     *
     * @return self
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * get the value of Soustitre.
     *
     * @return string
     */
    public function getSoustitre()
    {
        return $this->soustitre;
    }

    /**
     * Set the value of Soustitre.
     *
     * @param string soustitre
     * @param mixed            $soustitre
     *
     * @return self
     */
    public function setSoustitre($soustitre)
    {
        $this->soustitre = $soustitre;

        return $this;
    }

    /**
     * get the value of Url.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set the value of Url.
     *
     * @param string url
     * @param mixed      $url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * get the value of Contenu.
     *
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set the value of Contenu.
     *
     * @param string contenu
     * @param mixed          $contenu
     *
     * @return self
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * get the value of Vignette.
     *
     * @return string
     */
    public function getVignette()
    {
        $file = $this->vignette;

        return $file;
    }

    /**
     * Set the value of Vignette.
     *
     * @param string vignette
     * @param mixed           $vignette
     *
     * @return self
     */
    public function setVignette($vignette)
    {
        $this->vignette = $vignette;

        return $this;
    }

    /**
     * get the value of Type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the value of Type.
     *
     * @param string type
     * @param mixed       $type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * get the value of Image.
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set the value of Image.
     *
     * @param string image
     * @param mixed        $image
     *
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * get the value of Imagemobile.
     *
     * @return string
     */
    public function getImagemobile()
    {
        return $this->imagemobile;
    }

    /**
     * Set the value of Imagemobile.
     *
     * @param string imagemobile
     * @param mixed              $imagemobile
     *
     * @return self
     */
    public function setImagemobile($imagemobile)
    {
        $this->imagemobile = $imagemobile;

        return $this;
    }

    /**
     * get the value of Opacity.
     *
     * @return string
     */
    public function getOpacity()
    {
        return $this->opacity;
    }

    /**
     * Set the value of Opacity.
     *
     * @param string opacity
     * @param mixed          $opacity
     *
     * @return self
     */
    public function setOpacity($opacity)
    {
        $this->opacity = $opacity;

        return $this;
    }

    /**
     * get the value of Videobackground.
     *
     * @return string
     */
    public function getVideobackground()
    {
        return $this->videobackground;
    }

    /**
     * Set the value of Videobackground.
     *
     * @param string videobackground
     * @param mixed                  $videobackground
     *
     * @return self
     */
    public function setVideobackground($videobackground)
    {
        $this->videobackground = $videobackground;

        return $this;
    }

    /**
     * get the value of Videoiframe.
     *
     * @return string
     */
    public function getVideoiframe()
    {
        return $this->videoiframe;
    }

    /**
     * Set the value of Videoiframe.
     *
     * @param string videoiframe
     * @param mixed              $videoiframe
     *
     * @return self
     */
    public function setVideoiframe($videoiframe)
    {
        $this->videoiframe = $videoiframe;

        return $this;
    }

    /**
     * get the value of Video.
     *
     * @return string
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set the value of Video.
     *
     * @param string video
     * @param mixed        $video
     *
     * @return self
     */
    public function setVideo($video)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * get the value of Position.
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set the value of Position.
     *
     * @param int position
     * @param mixed        $position
     *
     * @return self
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * get the value of Actif Public.
     *
     * @return text
     */
    public function getActifPublic()
    {
        return $this->actifPublic;
    }

    /**
     * Set the value of Actif Public.
     *
     * @param mixed $actifPublic
     *
     * @return self
     */
    public function setActifPublic($actifPublic)
    {
        $this->actifPublic = $actifPublic;

        return $this;
    }

    /**
     * get the value of Description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of Description.
     *
     * @param string description
     * @param mixed              $description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * get the value of Filtre.
     *
     * @return string
     */
    public function getFiltre()
    {
        return $this->filtre;
    }

    /**
     * Set the value of Filtre.
     *
     * @param string filtre
     * @param mixed         $filtre
     *
     * @return self
     */
    public function setFiltre($filtre)
    {
        $this->filtre = $filtre;

        return $this;
    }
}
