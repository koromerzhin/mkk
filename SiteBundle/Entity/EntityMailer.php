<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

class EntityMailer
{
    /**
     * @var int
     *
     * @ORM\Column(name="mailer_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="mailer_subject", type="string", length=255)
     */
    protected $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="mailer_from", type="string", length=255)
     */
    protected $from;

    /**
     * @var string
     *
     * @ORM\Column(name="mailer_reply", type="string", length=255, nullable=true)
     */
    protected $reply;

    /**
     * @var string
     *
     * @ORM\Column(name="mailer_to", type="string", length=255)
     */
    protected $to;

    /**
     * @var array
     *
     * @ORM\Column(name="mailer_cc", type="array", length=255, nullable=true)
     */
    protected $cc;

    /**
     * @var string
     *
     * @ORM\Column(name="mailer_body", type="string", length=255)
     */
    protected $body;

    /**
     * @var array
     *
     * @ORM\Column(name="mailer_fichiers", type="array", length=255, nullable=true)
     */
    protected $fichiers;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="mailer_date_enregistrement", type="datetime", length=255, nullable=true)
     */
    protected $dateEnregistrement;

    /**
     * Get the value of Id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Subject.
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set the value of Subject.
     *
     * @param mixed $subject
     *
     * @return self
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get the value of From.
     *
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set the value of From.
     *
     * @param mixed $from
     *
     * @return self
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Get the value of To.
     *
     * @return string
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Set the value of To.
     *
     * @param mixed $to
     *
     * @return self
     */
    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * Get the value of Body.
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set the value of Body.
     *
     * @param mixed $body
     *
     * @return self
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get the value of Fichiers.
     *
     * @return string
     */
    public function getFichiers()
    {
        return $this->fichiers;
    }

    /**
     * Set the value of Fichiers.
     *
     * @param mixed $fichiers
     *
     * @return self
     */
    public function setFichiers($fichiers)
    {
        $this->fichiers = $fichiers;

        return $this;
    }

    /**
     * Get the value of Date Enregistrement.
     *
     * @return \DateTime
     */
    public function getDateEnregistrement()
    {
        return $this->dateEnregistrement;
    }

    /**
     * Set the value of Date Enregistrement.
     *
     * @param \DateTime dateEnregistrement
     *
     * @return self
     */
    public function setDateEnregistrement(\DateTime $dateEnregistrement)
    {
        $this->dateEnregistrement = $dateEnregistrement;

        return $this;
    }

    /**
     * Get the value of Reply.
     *
     * @return string
     */
    public function getReply()
    {
        return $this->reply;
    }

    /**
     * Set the value of Reply.
     *
     * @param mixed $reply
     *
     * @return self
     */
    public function setReply($reply)
    {
        $this->reply = $reply;

        return $this;
    }

    /**
     * Get the value of Cc.
     *
     * @return string
     */
    public function getCc()
    {
        return $this->cc;
    }

    /**
     * Set the value of Cc.
     *
     * @param mixed $cc
     *
     * @return self
     */
    public function setCc($cc)
    {
        $this->cc = $cc;

        return $this;
    }
}
