<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class EntityParam
{
    /**
     * @var int
     *
     * @ORM\Column(name="param_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="param_code", unique=true, type="string", length=255)
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="param_valeur", type="text", nullable=true)
     */
    protected $valeur;

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return Param
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set valeur.
     *
     * @param string $valeur
     *
     * @return Param
     */
    public function setValeur($valeur)
    {
        $this->valeur = $valeur;

        return $this;
    }

    /**
     * get valeur.
     *
     * @return string
     */
    public function getValeur()
    {
        return $this->valeur;
    }
}
