<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Intl\Intl;

class EntityAdresse implements Translatable
{
    /**
     * @var int
     *
     * @ORM\Column(name="adresse_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="blog_pmr", type="boolean", nullable=true)
     */
    protected $pmr;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse_info", type="string", length=255, nullable=true)
     */
    protected $info;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse_pays", type="string", length=255, nullable=true)
     */
    protected $pays;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse_cp", type="string", length=255, nullable=true)
     */
    protected $cp;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse_ville", type="string", length=255, nullable=true)
     */
    protected $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse_gps", type="string", length=255, nullable=true)
     */
    protected $gps;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse_gps_lat", type="string", length=255, nullable=true)
     */
    protected $gpsLat;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse_gps_lon", type="string", length=255, nullable=true)
     */
    protected $gpsLon;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse_type", type="string", length=255, nullable=true)
     */
    protected $type;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="adresses")
     * @ORM\JoinColumn(name="adresse_refuser", referencedColumnName="user_id", nullable=true, onDelete="CASCADE")
     */
    protected $refuser;

    /**
     * @ORM\ManyToOne(targetEntity="Etablissement", inversedBy="adresses")
     * @ORM\JoinColumn(name="adresse_refetablissement", referencedColumnName="etablissement_id", nullable=true, onDelete="CASCADE")
     */
    protected $refetablissement;

    /**
     * @ORM\ManyToOne(targetEntity="Emplacement", inversedBy="adresses")
     * @ORM\JoinColumn(name="adresse_refemplacement", referencedColumnName="emplacement_id", nullable=true, onDelete="CASCADE")
     */
    protected $refemplacement;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     */
    protected $locale;

    public function __toString()
    {
        return $this->getVille();
    }

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set info.
     *
     * @param string $info
     *
     * @return Adresse
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * get info.
     *
     * @return string
     */
    public function getInfo()
    {
        $tab  = get_html_translation_table(HTML_ENTITIES, ENT_QUOTES | ENT_HTML5);
        $info = strip_tags($this->info);
        foreach ($tab as $code => $val) {
            $info = str_replace($val, $code, $info);
        }

        return $info;
    }

    /**
     * Set pays.
     *
     * @param string $pays
     *
     * @return Adresse
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * get pays.
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * get pays.
     *
     * @return string
     * @JMS\VirtualProperty()
     */
    public function getCountry()
    {
        $country = Intl::getRegionBundle()->getCountryNames($this->getLocaleSession());

        return isset($country[$this->pays]) ? isset($country[$this->pays]) : '';
    }

    /**
     * Set cp.
     *
     * @param string $cp
     *
     * @return Adresse
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * get cp.
     *
     * @return string
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set ville.
     *
     * @param string $ville
     *
     * @return Adresse
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * get ville.
     *
     * @return string
     */
    public function getVille()
    {
        return mb_strtoupper($this->ville, 'UTF-8');
    }

    /**
     * Set gps_lat.
     *
     * @param string $gpsLat
     *
     * @return Adresse
     */
    public function setGpsLat($gpsLat)
    {
        $this->gpsLat = $gpsLat;

        return $this;
    }

    /**
     * get gps_lat.
     *
     * @return string
     */
    public function getGpsLat()
    {
        return $this->gpsLat;
    }

    /**
     * Set gps_lon.
     *
     * @param string $gpsLon
     *
     * @return Adresse
     */
    public function setGpsLon($gpsLon)
    {
        $this->gpsLon = $gpsLon;

        return $this;
    }

    /**
     * get gps_lon.
     *
     * @return string
     */
    public function getGpsLon()
    {
        return $this->gpsLon;
    }

    /**
     * Set gps.
     *
     * @param string $gps
     *
     * @return Adresse
     */
    public function setGps($gps)
    {
        if ($gps != '') {
            list($lat, $lon) = explode(',', $gps);
        } else {
            $lat = '';
            $lon = '';
        }
        $this->setGpsLat($lat);
        $this->setGpsLon($lon);
        $this->gps = $gps;

        return $this;
    }

    /**
     * get gps.
     *
     * @return string
     */
    public function getGps()
    {
        return $this->gps;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return Adresse
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set refuser.
     *
     * @param User $refuser
     *
     * @return Adresse
     */
    public function setRefUser($refuser = null)
    {
        $this->refuser = $refuser;

        return $this;
    }

    /**
     * get refuser.
     *
     * @return User
     */
    public function getRefUser()
    {
        return $this->refuser;
    }

    /**
     * Set refetablissement.
     *
     * @param Etablissement $refetablissement
     *
     * @return Adresse
     */
    public function setRefEtablissement($refetablissement = null)
    {
        $this->refetablissement = $refetablissement;

        return $this;
    }

    /**
     * get refetablissement.
     *
     * @return Etablissement
     */
    public function getRefEtablissement()
    {
        return $this->refetablissement;
    }

    /**
     * Set refemplacement.
     *
     * @param Emplacement $refemplacement
     *
     * @return Adresse
     */
    public function setRefEmplacement($refemplacement = null)
    {
        $this->refemplacement = $refemplacement;

        return $this;
    }

    /**
     * get refemplacement.
     *
     * @return Emplacement
     */
    public function getRefEmplacement()
    {
        return $this->refemplacement;
    }

    /**
     * Set pmr.
     *
     * @param string $pmr
     *
     * @return Adresse
     */
    public function setPmr($pmr)
    {
        $this->pmr = $pmr;

        return $this;
    }

    /**
     * get pmr.
     *
     * @return string
     */
    public function getPmr()
    {
        return $this->pmr;
    }

    public function setLocaleSession($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return string
     * @JMS\VirtualProperty()
     */
    public function getLocaleSession()
    {
        if (isset($this->locale)) {
            return $this->locale;
        }
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return string
     * @JMS\VirtualProperty()
     */
    public function getGoogleUrl()
    {
        $url = '';
        if (isset($this->locale)) {
            $url  = 'https://maps.google.com/?q=';
            $info = $this->getInfo();
            if (substr_count($info, "\n") != 0) {
                $info = explode("\n", $info);
                $info = $info[0];
            }
            $url = $url . urlencode($info);
            $url = $url . '+' . $this->getCp();
            $url = $url . '+' . urlencode($this->getVille());
            $url = $url . '+' . urlencode($this->getCountry());
            $url = $url . '&ll=' . $this->getGps();
        }

        return $url;
    }
}
