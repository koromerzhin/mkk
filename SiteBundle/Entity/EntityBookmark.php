<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use Mkk\SiteBundle\Annotation\UploadableField;

class EntityBookmark implements Translatable
{
    /**
     * @var int
     *
     * @ORM\Column(name="bookmark_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Gedmo\Translatable
     * @Gedmo\Slug(updatable=false,fields={"titre"})
     * @ORM\Column(name="bookmark_alias", type="string")
     */
    protected $alias;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="bookmark_titre", type="string")
     */
    protected $titre;

    /**
     * @var text
     * @Gedmo\Translatable
     * @ORM\Column(name="bookmark_description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @var text
     *
     * @ORM\Column(name="bookmark_url", type="text")
     */
    protected $url;

    /**
     * @var string
     *
     * @ORM\Column(name="bookmark_image", type="string", length=255, nullable=true)
     */
    protected $image;

    /**
     * @UploadableField(filename="image", path="bookmark/image", unique=true, alias="titre")
     */
    protected $file_image;

    /**
     * @var int
     *
     * @ORM\Column(name="bookmark_date", type="integer", nullable=true)
     */
    protected $date;

    /**
     * @ORM\ManyToMany(targetEntity="Tag",  mappedBy="bookmarks")
     */
    protected $tags;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="bookmark_meta_titre", type="string", length=255, nullable=true)
     */
    protected $metaTitre;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="bookmark_meta_description", type="string", length=255, nullable=true)
     */
    protected $metaDescription;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="bookmark_meta_keywords", type="string", length=255, nullable=true)
     */
    protected $metaKeywords;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     * and it is not necessary because globally locale can be set in listener
     */
    protected $locale;

    /**
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getTitre();
    }

    /**
     * Add tags.
     *
     * @param Tag $tags
     *
     * @return Bookmark
     */
    public function addTag($tags)
    {
        $this->tags->add($tags);

        return $this;
    }

    /**
     * Remove tags.
     *
     * @param Tag $tags
     */
    public function removeTag($tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * get tags.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set the value of Tags.
     *
     * @param mixed tags
     * @param mixed $tags
     *
     * @return self
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * get the value of Id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * get the value of Alias.
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set the value of Alias.
     *
     * @param string alias
     * @param mixed $alias
     *
     * @return self
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * get the value of Titre.
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set the value of Titre.
     *
     * @param bool titre
     * @param mixed $titre
     *
     * @return self
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * get the value of Description.
     *
     * @return text
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of Description.
     *
     * @param text description
     * @param mixed $description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * get the value of Url.
     *
     * @return text
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set the value of Url.
     *
     * @param text url
     * @param mixed $url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * get the value of Image.
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set the value of Image.
     *
     * @param string image
     * @param mixed $image
     *
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * get the value of Date.
     *
     * @return int
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set the value of Date.
     *
     * @param int date
     * @param mixed $date
     *
     * @return self
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * get the value of Meta Titre.
     *
     * @return string
     */
    public function getMetaTitre()
    {
        return $this->metaTitre;
    }

    /**
     * Set the value of Meta Titre.
     *
     * @param string meta_titre
     * @param mixed $metaTitre
     *
     * @return self
     */
    public function setMetaTitre($metaTitre)
    {
        $this->metaTitre = $metaTitre;

        return $this;
    }

    /**
     * get the value of Meta Description.
     *
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set the value of Meta Description.
     *
     * @param string meta_description
     * @param mixed $metaDescription
     *
     * @return self
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * get the value of Meta Keywords.
     *
     * @return string
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set the value of Meta Keywords.
     *
     * @param string meta_keywords
     * @param mixed $metaKeywords
     *
     * @return self
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * Set the value of Updated At.
     *
     * @param \DateTime updatedAt
     *
     * @return self
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get the value of File Image.
     *
     * @return mixed
     */
    public function getFileImage()
    {
        return $this->file_image;
    }

    /**
     * Set the value of File Image.
     *
     * @param mixed file_image
     * @param mixed $file_image
     *
     * @return self
     */
    public function setFileImage($file_image)
    {
        $this->file_image = $file_image;

        return $this;
    }
}
