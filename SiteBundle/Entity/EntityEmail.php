<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class EntityEmail
{
    /**
     * @var int
     *
     * @ORM\Column(name="email_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email_adresse", type="text")
     */
    protected $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="email_type", type="string", length=255, nullable=true)
     */
    protected $type;

    /**
     * @ORM\ManyToOne(targetEntity="Evenement", inversedBy="emails")
     * @ORM\JoinColumn(name="email_refevenement", referencedColumnName="evenement_id", nullable=true, onDelete="CASCADE")
     */
    protected $refevenement;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="emails")
     * @ORM\JoinColumn(name="email_refuser", referencedColumnName="user_id", nullable=true, onDelete="CASCADE")
     */
    protected $refuser;

    /**
     * @ORM\ManyToOne(targetEntity="Etablissement", inversedBy="emails")
     * @ORM\JoinColumn(name="email_refetablissement", referencedColumnName="etablissement_id", nullable=true, onDelete="CASCADE")
     */
    protected $refetablissement;

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set adresse.
     *
     * @param string $adresse
     *
     * @return Email
     */
    public function setAdresse($adresse)
    {
        $adresse       = strtolower($adresse);
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * get adresse.
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return Email
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set refuser.
     *
     * @param User $refuser
     *
     * @return Email
     */
    public function setRefUser($refuser = null)
    {
        $this->refuser = $refuser;

        return $this;
    }

    /**
     * get refuser.
     *
     * @return User
     */
    public function getRefUser()
    {
        return $this->refuser;
    }

    /**
     * Set refetablissement.
     *
     * @param Etablissement $refetablissement
     *
     * @return Email
     */
    public function setRefEtablissement($refetablissement = null)
    {
        $this->refetablissement = $refetablissement;

        return $this;
    }

    /**
     * get refetablissement.
     *
     * @return Etablissement
     */
    public function getRefEtablissement()
    {
        return $this->refetablissement;
    }

    /**
     * Set refevenement.
     *
     * @param Evenement $refevenement
     *
     * @return Email
     */
    public function setRefEvenement($refevenement = null)
    {
        $this->refevenement = $refevenement;

        return $this;
    }

    /**
     * get refevenement.
     *
     * @return Evenement
     */
    public function getRefEvenement()
    {
        return $this->refevenement;
    }
}
