<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mkk\SiteBundle\Annotation\UploadableField;

class EntityDiaporama
{
    /**
     * @var int
     *
     * @ORM\Column(name="diaporama_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="diaporama_nom", type="string", length=255)
     */
    protected $nom;

    /**
     * @var array
     *
     * @ORM\Column(name="diaporama_images", type="array", nullable=true)
     */
    protected $images;

    /**
     * @UploadableField(filename="images", path="diaporama/images", unique=false, alias="nom")
     */
    protected $file_images;

    /**
     * @var string
     *
     * @ORM\Column(name="diaporama_totalnbimage", type="integer", options={"default" = 0})
     */
    protected $totalnbimage;

    /**
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    public function __construct()
    {
        $this->totalnbimage = 0;
    }

    public function __toString()
    {
        return $this->nom;
    }

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set images.
     *
     * @param string $images
     *
     * @return Diaporama
     */
    public function setImages($images)
    {
        $this->images = $images;

        return $this;
    }

    /**
     * get images.
     *
     * @return string
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * get the value of Totalnbimage.
     *
     * @return string
     */
    public function getTotalnbimage()
    {
        return $this->totalnbimage;
    }

    /**
     * Set the value of Totalnbimage.
     *
     * @param string totalnbimage
     * @param mixed               $totalnbimage
     *
     * @return self
     */
    public function setTotalnbimage($totalnbimage)
    {
        $this->totalnbimage = $totalnbimage;

        return $this;
    }

    /**
     * Get the value of Updated At.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set the value of Updated At.
     *
     * @param \DateTime updatedAt
     *
     * @return self
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get the value of File Images.
     *
     * @return mixed
     */
    public function getFileImages()
    {
        return $this->file_images;
    }

    /**
     * Set the value of File Images.
     *
     * @param mixed file_images
     * @param mixed             $file_images
     *
     * @return self
     */
    public function setFileImages($file_images)
    {
        $this->file_images = $file_images;

        return $this;
    }
}
