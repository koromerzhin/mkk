<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Mkk\SiteBundle\Annotation\UploadableField;

class EntityEvenement
{
    /**
     * @JMS\Exclude()
     * @ORM\ManyToOne(targetEntity="Categorie", inversedBy="evenements")
     * @ORM\JoinColumn(name="evenement_refcategorie", referencedColumnName="categorie_id")
     */
    protected $refcategorie;

    /**
     * @var string
     *
     * @ORM\Column(name="evenement_totalnbdate", type="integer", options={"default" = 0})
     */
    protected $totalnbdate;

    /**
     * @var string
     *
     * @ORM\Column(name="evenement_totalnbplace", type="integer", options={"default" = 0})
     */
    protected $totalnbplace;

    /**
     * @var bool
     *
     * @ORM\Column(name="evenement_placeillimite", type="boolean", nullable=true)
     */
    protected $placeillimite;

    /**
     * @JMS\Exclude()
     * @ORM\OneToMany(targetEntity="Prix", mappedBy="refevenement", cascade={"remove", "persist"})
     */
    protected $prixs;

    /**
     * @JMS\Exclude()
     * @ORM\OneToMany(targetEntity="Email", mappedBy="refevenement", cascade={"remove", "persist"})
     */
    protected $emails;

    /**
     * @JMS\Exclude()
     * @ORM\OneToMany(targetEntity="Telephone", mappedBy="refevenement", cascade={"remove", "persist"})
     */
    protected $telephones;

    /**
     * @JMS\Exclude()
     * @ORM\OneToMany(targetEntity="Lien", mappedBy="refevenement", cascade={"remove", "persist"})
     */
    protected $liens;

    /**
     * @JMS\Exclude()
     * @ORM\OneToMany(targetEntity="Emplacement",  mappedBy="refevenement", cascade={"remove", "persist"})
     */
    protected $emplacements;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="evenements")
     * @ORM\JoinColumn(name="evenement_refuser", referencedColumnName="user_id", nullable=true)
     */
    protected $refuser;
    /**
     * @var int
     *
     * @ORM\Column(name="evenement_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="evenement_titre", type="text", nullable=true)
     */
    protected $titre;

    /**
     * @var bool
     *
     * @ORM\Column(name="evenement_type", type="boolean", options={"default" = 0}, nullable=true)
     */
    protected $type;

    /**
     * @var text
     * @ORM\Column(name="evenement_copyright", type="text", nullable=true)
     */
    protected $copyright;

    /**
     * @var bool
     *
     * @ORM\Column(name="evenement_publier", type="boolean",nullable=true)
     */
    protected $publier;

    /**
     * @var string
     * @Gedmo\Translatable
     * @Gedmo\Slug(updatable=false,fields={"titre"})
     * @ORM\Column(name="evenement_alias", type="text", nullable=true)
     */
    protected $alias;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="evenement_description", type="text",nullable=true)
     */
    protected $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="evenement_externe", type="boolean",nullable=true)
     */
    protected $externe;

    /**
     * @var bool
     *
     * @ORM\Column(name="evenement_validation", type="boolean",nullable=true)
     */
    protected $validation;

    /**
     * @var bool
     *
     * @ORM\Column(name="evenement_correction", type="boolean",nullable=true)
     */
    protected $correction;

    /**
     * @var string
     *
     * @ORM\Column(name="evenement_vignette", type="string", length=255, nullable=true)
     */
    protected $vignette;

    /**
     * @UploadableField(filename="vignette", path="bookmark/vignette", unique=true, alias="titre")
     */
    protected $file_vignette;

    /**
     * @var array
     *
     * @ORM\Column(name="evenement_galerie", type="array", nullable=true)
     */
    protected $galerie;

    /**
     * @UploadableField(filename="galerie", path="bookmark/galerie", unique=false, alias="titre")
     */
    protected $file_galerie;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     */
    protected $locale;

    /**
     * @var string
     *
     * @ORM\Column(name="evenement_totalnbemplacement", type="integer", options={"default" = 0})
     */
    protected $totalnbemplacement;

    /**
     * @var int
     *
     * @ORM\Column(name="evenement_mindate", type="integer", options={"default" = 0})
     */
    protected $mindate;

    /**
     * @var int
     *
     * @ORM\Column(name="evenement_maxdate", type="integer", options={"default" = 0})
     */
    protected $maxdate;

    /**
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->totalnbdate        = 0;
        $this->totalnbplace       = 0;
        $this->totalnbemplacement = 0;
        $this->mindate            = 0;
        $this->maxdate            = 0;
        $this->prixs              = new ArrayCollection();
        $this->emails             = new ArrayCollection();
        $this->emplacements       = new ArrayCollection();
        $this->telephones         = new ArrayCollection();
        $this->liens              = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getTitre();
    }

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre.
     *
     * @param string $titre
     *
     * @return Evenement
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * get titre.
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Evenement
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set externe.
     *
     * @param bool $externe
     *
     * @return Evenement
     */
    public function setExterne($externe)
    {
        $this->externe = $externe;

        return $this;
    }

    /**
     * get externe.
     *
     * @return bool
     */
    public function getExterne()
    {
        return $this->externe;
    }

    /**
     * Set validation.
     *
     * @param bool $validation
     *
     * @return Evenement
     */
    public function setValidation($validation)
    {
        $this->validation = $validation;

        return $this;
    }

    /**
     * get validation.
     *
     * @return bool
     */
    public function getValidation()
    {
        return $this->validation;
    }

    /**
     * Set correction.
     *
     * @param bool $correction
     *
     * @return Evenement
     */
    public function setCorrection($correction)
    {
        $this->correction = $correction;

        return $this;
    }

    /**
     * get correction.
     *
     * @return bool
     */
    public function getCorrection()
    {
        return $this->correction;
    }

    /**
     * Set refcategorie.
     *
     * @param Categorie $refcategorie
     *
     * @return Evenement
     */
    public function setRefCategorie($refcategorie = null)
    {
        $this->refcategorie = $refcategorie;

        return $this;
    }

    /**
     * get refcategorie.
     *
     * @return Categorie
     */
    public function getRefCategorie()
    {
        return $this->refcategorie;
    }

    /**
     * Set alias.
     *
     * @param string $alias
     *
     * @return Evenement
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * get alias.
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Add emails.
     *
     * @param Email $emails
     *
     * @return Evenement
     */
    public function addEmail($emails)
    {
        $emails->setRefEvenement($this);
        $this->emails->add($emails);

        return $this;
    }

    /**
     * Remove emails.
     *
     * @param Email $emails
     */
    public function removeEmail($emails)
    {
        $this->emails->removeElement($emails);
    }

    /**
     * get emails.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * Add liens.
     *
     * @param Lien $liens
     *
     * @return Evenement
     */
    public function addLien($liens)
    {
        $liens->setRefEvenement($this);
        $this->liens->add($liens);

        return $this;
    }

    /**
     * Remove liens.
     *
     * @param Lien $liens
     */
    public function removeLien($liens)
    {
        $this->liens->removeElement($liens);
    }

    /**
     * get liens.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLiens()
    {
        return $this->liens;
    }

    /**
     * Set galerie.
     *
     * @param string $galerie
     *
     * @return Evenement
     */
    public function setGalerie($galerie)
    {
        $this->galerie = $galerie;

        return $this;
    }

    /**
     * get galerie.
     *
     * @return string
     */
    public function getGalerie()
    {
        return $this->galerie;
    }

    /**
     * Add telephones.
     *
     * @param Telephone $telephones
     *
     * @return Evenement
     */
    public function addTelephone($telephones)
    {
        $telephones->setRefEvenement($this);
        $this->telephones->add($telephones);

        return $this;
    }

    /**
     * Remove telephones.
     *
     * @param Telephone $telephones
     */
    public function removeTelephone($telephones)
    {
        $this->telephones->removeElement($telephones);
    }

    /**
     * get telephones.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTelephones()
    {
        return $this->telephones;
    }

    /**
     * Set refuser.
     *
     * @param User $refuser
     *
     * @return Evenement
     */
    public function setRefUser($refuser = null)
    {
        $this->refuser = $refuser;

        return $this;
    }

    /**
     * get refuser.
     *
     * @return User
     */
    public function getRefUser()
    {
        return $this->refuser;
    }

    public function getCoordonnees()
    {
        $data       = [];
        $telephones = $this->getTelephones();
        foreach ($telephones as $row) {
            array_push($data, $row);
        }
        $emails = $this->getEmails();
        foreach ($emails as $row) {
            array_push($data, $row);
        }
        $sites = $this->getLiens();
        foreach ($sites as $row) {
            array_push($data, $row);
        }

        return $data;
    }

    /**
     * get prixs.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrixs()
    {
        return $this->prixs;
    }

    /**
     * Set vignette.
     *
     * @param string $vignette
     *
     * @return Evenement
     */
    public function setVignette($vignette)
    {
        $this->vignette = $vignette;

        return $this;
    }

    /**
     * get vignette.
     *
     * @return string
     */
    public function getVignette()
    {
        $file = $this->vignette;

        return $file;
    }

    /**
     * Add prixs.
     *
     * @param Prix $prixs
     *
     * @return Evenement
     */
    public function addPrix($prixs)
    {
        $prixs->setRefEvenement($this);
        $this->prixs->add($prixs);

        return $this;
    }

    /**
     * Remove prixs.
     *
     * @param Prix $prixs
     */
    public function removePrix($prixs)
    {
        $this->prixs->removeElement($prixs);
    }

    /**
     * Add emplacements.
     *
     * @param Emplacement $emplacements
     *
     * @return Evenement
     */
    public function addemplacement($emplacements)
    {
        $emplacements->setRefEvenement($this);
        $this->emplacements->add($emplacements);

        return $this;
    }

    /**
     * Remove emplacements.
     *
     * @param Emplacement $emplacements
     */
    public function removeemplacement($emplacements)
    {
        $this->emplacements->removeElement($emplacements);
    }

    /**
     * get emplacements.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmplacements()
    {
        return $this->emplacements;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    public function getEmplacementsAvecPlace()
    {
        $places       = [];
        $emplacements = $this->getEmplacements();
        foreach ($emplacements as $emplacement) {
            $totalPlaces = $emplacement->getDatesAvecPlace();
            if (count($totalPlaces) > 0) {
                $places[] = $emplacement;
            }
        }

        return $places;
    }

    /**
     * get the value of Totalnbdate.
     *
     * @return string
     */
    public function getTotalnbdate()
    {
        return $this->totalnbdate;
    }

    /**
     * Set the value of Totalnbdate.
     *
     * @param string totalnbdate
     * @param mixed $totalnbdate
     *
     * @return self
     */
    public function setTotalnbdate($totalnbdate)
    {
        $this->totalnbdate = $totalnbdate;

        return $this;
    }

    /**
     * Set the value of Prixs.
     *
     * @param mixed prixs
     * @param mixed $prixs
     *
     * @return self
     */
    public function setPrixs($prixs)
    {
        $this->prixs = $prixs;

        return $this;
    }

    /**
     * Set the value of Emails.
     *
     * @param mixed emails
     * @param mixed $emails
     *
     * @return self
     */
    public function setEmails($emails)
    {
        $this->emails = $emails;

        return $this;
    }

    /**
     * Set the value of Telephones.
     *
     * @param mixed telephones
     * @param mixed $telephones
     *
     * @return self
     */
    public function setTelephones($telephones)
    {
        $this->telephones = $telephones;

        return $this;
    }

    /**
     * Set the value of Liens.
     *
     * @param mixed liens
     * @param mixed $liens
     *
     * @return self
     */
    public function setLiens($liens)
    {
        $this->liens = $liens;

        return $this;
    }

    /**
     * Set the value of Emplacements.
     *
     * @param mixed emplacements
     * @param mixed $emplacements
     *
     * @return self
     */
    public function setEmplacements($emplacements)
    {
        $this->emplacements = $emplacements;

        return $this;
    }

    /**
     * get the value of Locale.
     *
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set the value of Locale.
     *
     * @param mixed locale
     * @param mixed $locale
     *
     * @return self
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * get the value of Totalnbemplacement.
     *
     * @return string
     */
    public function getTotalnbemplacement()
    {
        return $this->totalnbemplacement;
    }

    /**
     * Set the value of Totalnbemplacement.
     *
     * @param string totalnbemplacement
     * @param mixed $totalnbemplacement
     *
     * @return self
     */
    public function setTotalnbemplacement($totalnbemplacement)
    {
        $this->totalnbemplacement = $totalnbemplacement;

        return $this;
    }

    /**
     * get the value of Mindate.
     *
     * @return int
     */
    public function getMindate()
    {
        return $this->mindate;
    }

    /**
     * Set the value of Mindate.
     *
     * @param int mindate
     * @param mixed $mindate
     *
     * @return self
     */
    public function setMindate($mindate)
    {
        $this->mindate = $mindate;

        return $this;
    }

    /**
     * get the value of Maxdate.
     *
     * @return int
     */
    public function getMaxdate()
    {
        return $this->maxdate;
    }

    /**
     * Set the value of Maxdate.
     *
     * @param int maxdate
     * @param mixed $maxdate
     *
     * @return self
     */
    public function setMaxdate($maxdate)
    {
        $this->maxdate = $maxdate;

        return $this;
    }

    /**
     * get the value of Totalnbplace.
     *
     * @return string
     */
    public function getTotalnbplace()
    {
        return $this->totalnbplace;
    }

    /**
     * Set the value of Totalnbplace.
     *
     * @param string totalnbplace
     * @param mixed $totalnbplace
     *
     * @return self
     */
    public function setTotalnbplace($totalnbplace)
    {
        $this->totalnbplace = $totalnbplace;

        return $this;
    }

    /**
     * get the value of Publier.
     *
     * @return bool
     */
    public function getPublier()
    {
        return $this->publier;
    }

    /**
     * Set the value of Publier.
     *
     * @param bool publier
     * @param mixed $publier
     *
     * @return self
     */
    public function setPublier($publier)
    {
        $this->publier = $publier;

        return $this;
    }

    /**
     * get the value of Placeillimite.
     *
     * @return bool
     */
    public function getPlaceillimite()
    {
        return $this->placeillimite;
    }

    /**
     * Set the value of Placeillimite.
     *
     * @param bool placeillimite
     * @param mixed $placeillimite
     *
     * @return self
     */
    public function setPlaceillimite($placeillimite)
    {
        $this->placeillimite = $placeillimite;

        return $this;
    }

    /**
     * get the value of Copyright.
     *
     * @return text
     */
    public function getCopyright()
    {
        return $this->copyright;
    }

    /**
     * Set the value of Copyright.
     *
     * @param text copyright
     * @param mixed $copyright
     *
     * @return self
     */
    public function setCopyright($copyright)
    {
        $this->copyright = $copyright;

        return $this;
    }

    /**
     * get the value of Type.
     *
     * @return bool
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the value of Type.
     *
     * @param bool type
     * @param mixed $type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function getCategorie()
    {
        return ! is_object($this->getRefCategorie()) ? '' : $this->getRefCategorie()->getId();
    }

    public function setCategorie($v)
    {
        return $this;
    }

    public function getEmplacementadresses()
    {
        $adresses = [];
        foreach ($this->getEmplacements() as $emplacement) {
            foreach ($emplacement->getAdresses() as $adresse) {
                $adresses[] = $adresse;
            }
        }

        return $adresses;
    }

    public function setEmplacementadresses($v)
    {
        return $this;
    }

    public function getEtablissements()
    {
        $etablissements = [];
        foreach ($this->getEmplacements() as $emplacement) {
            $etablissement = $emplacement->getRefEtablissement();
            if ($etablissement != null) {
                $etablissements[] = $etablissement;
            }
        }

        return $etablissements;
    }

    public function setEtablissements($v)
    {
        return $this;
    }

    /**
     * Get the value of File Vignette.
     *
     * @return mixed
     */
    public function getFileVignette()
    {
        return $this->file_vignette;
    }

    /**
     * Set the value of File Vignette.
     *
     * @param mixed file_vignette
     * @param mixed $file_vignette
     *
     * @return self
     */
    public function setFileVignette($file_vignette)
    {
        $this->file_vignette = $file_vignette;

        return $this;
    }

    /**
     * Get the value of File Galerie.
     *
     * @return mixed
     */
    public function getFileGalerie()
    {
        return $this->file_galerie;
    }

    /**
     * Set the value of File Galerie.
     *
     * @param mixed file_galerie
     * @param mixed $file_galerie
     *
     * @return self
     */
    public function setFileGalerie($file_galerie)
    {
        $this->file_galerie = $file_galerie;

        return $this;
    }

    /**
     * Get the value of Updated At.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set the value of Updated At.
     *
     * @param \DateTime updatedAt
     *
     * @return self
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
