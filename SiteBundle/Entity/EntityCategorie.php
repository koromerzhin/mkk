<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use JMS\Serializer\Annotation as JMS;

class EntityCategorie implements Translatable
{
    /**
     * @var int
     *
     * @ORM\Column(name="categorie_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Categorie", mappedBy="refcategorie", cascade={"remove", "persist"})
     * @ORM\OrderBy({"position" = "ASC", "nom" = "ASC"})
     */
    protected $categories;

    /**
     * @ORM\ManyToOne(targetEntity="Categorie", inversedBy="categories")
     * @ORM\JoinColumn(name="categorie_refcategorie", referencedColumnName="categorie_id", nullable=true)
     */
    protected $refcategorie;

    /**
     * @var string
     * @Gedmo\Translatable
     * @Gedmo\Slug(updatable=false,fields={"nom"})
     * @ORM\Column(name="categorie_alias", type="string")
     */
    protected $alias;

    /**
     * @var bool
     *
     * @ORM\Column(name="categorie_actif", type="boolean", nullable=true)
     */
    protected $actif;

    /**
     * @var text
     * @Gedmo\Translatable
     * @ORM\Column(name="categorie_nom", type="text", nullable=true)
     */
    protected $nom;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="categorie_meta_titre", type="string", length=255, nullable=true)
     */
    protected $metaTitre;

    /**
     * @var int
     *
     * @ORM\Column(name="categorie_position", type="integer", nullable=true)
     */
    protected $position;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="categorie_meta_description", type="string", length=255, nullable=true)
     */
    protected $metaDescription;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="categorie_meta_keywords", type="string", length=255, nullable=true)
     */
    protected $metaKeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie_totalnbblog", type="integer", nullable=true, options={"default" = 0})
     */
    protected $totalnbblog;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie_totalnbpartenaire", type="integer", nullable=true, options={"default" = 0})
     */
    protected $totalnbpartenaire;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie_totalnbevenement", type="integer", nullable=true, options={"default" = 0})
     */
    protected $totalnbevenement;

    /**
     * @JMS\Exclude()
     * @ORM\OneToMany(targetEntity="Blog", mappedBy="refcategorie", cascade={"remove", "persist"})
     */
    protected $blogs;

    /**
     * @JMS\Exclude()
     * @ORM\OneToMany(targetEntity="Partenaire", mappedBy="refcategorie", cascade={"remove", "persist"})
     */
    protected $partenaires;

    /**
     * @JMS\Exclude()
     * @ORM\OneToMany(targetEntity="Evenement", mappedBy="refcategorie", cascade={"remove", "persist"})
     */
    protected $evenements;

    /**
     * @var text
     *
     * @ORM\Column(name="categorie_type", type="text", nullable=true)
     */
    protected $type;

    /**
     * @JMS\Exclude()
     * @ORM\ManyToOne(targetEntity="Categorie", inversedBy="child")
     * @ORM\JoinColumn(name="categorie_parent", referencedColumnName="categorie_id")
     * @ORM\OrderBy({"nom" = "ASC"})
     */
    protected $parent;

    /**
     * @JMS\Exclude()
     * @ORM\OneToMany(targetEntity="Categorie", mappedBy="parent", cascade={"persist"})
     * @ORM\OrderBy({"nom" = "ASC"})
     */
    protected $child;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     */
    protected $locale;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->totalnbblog       = 0;
        $this->totalnbpartenaire = 0;
        $this->totalnbevenement  = 0;
        $this->blogs             = new ArrayCollection();
        $this->partenaires       = new ArrayCollection();
    }

    public function __toString()
    {
        $nom    = [];
        $parent = $this->getParent();
        $nom[]  = $this->getNom();
        while ($parent != null) {
            $nom[]  = $parent->getNom();
            $parent = $parent->getParent();
        }

        $nom = array_reverse($nom);

        return implode(' > ', $nom);
    }

    /**
     * Add child.
     *
     * @param Categorie $child
     *
     * @return Categorie
     */
    public function addChild($child)
    {
        $child->setParent($this);
        $this->child->add($child);

        return $this;
    }

    /**
     * Remove child.
     *
     * @param Categorie $child
     */
    public function removeChild($child)
    {
        $this->child->removeElement($child);
    }

    /**
     * get child.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChild()
    {
        return $this->child;
    }

    /**
     * Set parent.
     *
     * @param Categorie $parent
     *
     * @return Categorie
     */
    public function setParent($parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * get parent.
     *
     * @return Categorie
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alias.
     *
     * @param string $alias
     *
     * @return Categorie
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * get alias.
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return Categorie
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Add blogs.
     *
     * @param Blog $blogs
     *
     * @return Categorie
     */
    public function addBlog($blogs)
    {
        $blogs->setRefCategorie($this);
        $this->blogs->add($blogs);

        return $this;
    }

    /**
     * Remove blogs.
     *
     * @param Blog $blogs
     */
    public function removeBlog($blogs)
    {
        $this->blogs->removeElement($blogs);
    }

    /**
     * get blogs.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBlogs()
    {
        return $this->blogs;
    }

    /**
     * Set meta_titre.
     *
     * @param string $metaTitre
     *
     * @return Categorie
     */
    public function setMetaTitre($metaTitre)
    {
        $this->metaTitre = $metaTitre;

        return $this;
    }

    /**
     * get meta_titre.
     *
     * @return string
     */
    public function getMetaTitre()
    {
        return $this->metaTitre;
    }

    /**
     * Set meta_description.
     *
     * @param string $metaDescription
     *
     * @return Categorie
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * get meta_description.
     *
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set meta_keywords.
     *
     * @param string $metaKeywords
     *
     * @return Categorie
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    /**
     * get meta_keywords.
     *
     * @return string
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return Categorie
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add partenaires.
     *
     * @param partenaire $partenaires
     *
     * @return Categorie
     */
    public function addPartenaire($partenaires)
    {
        $partenaires->setRefCategorie($this);
        $this->partenaires->add($partenaires);

        return $this;
    }

    /**
     * Remove blogs.
     *
     * @param partenaire $partenaires
     */
    public function removePartenaire($partenaires)
    {
        $this->partenaires->removeElement($partenaires);
    }

    /**
     * get categories.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPartenaires()
    {
        return $this->partenaires;
    }

    /**
     * Set actif.
     *
     * @param bool $actif
     *
     * @return Categorie
     */
    public function setActif($actif)
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * get actif.
     *
     * @return bool
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * Set position.
     *
     * @param int $position
     *
     * @return Etablissement
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * get position.
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * get the value of Totalnbblog.
     *
     * @return string
     */
    public function getTotalnbblog()
    {
        return $this->totalnbblog;
    }

    /**
     * Set the value of Totalnbblog.
     *
     * @param string totalnbblog
     * @param mixed              $totalnbblog
     *
     * @return self
     */
    public function setTotalnbblog($totalnbblog)
    {
        $this->totalnbblog = $totalnbblog;

        return $this;
    }

    /**
     * get the value of Totalnbpartenaire.
     *
     * @return string
     */
    public function getTotalnbpartenaire()
    {
        return $this->totalnbpartenaire;
    }

    /**
     * Set the value of Totalnbpartenaire.
     *
     * @param string totalnbpartenaire
     * @param mixed                    $totalnbpartenaire
     *
     * @return self
     */
    public function setTotalnbpartenaire($totalnbpartenaire)
    {
        $this->totalnbpartenaire = $totalnbpartenaire;

        return $this;
    }

    /**
     * Set the value of Blogs.
     *
     * @param mixed blogs
     * @param mixed       $blogs
     *
     * @return self
     */
    public function setBlogs($blogs)
    {
        $this->blogs = $blogs;

        return $this;
    }

    /**
     * Set the value of Partenaires.
     *
     * @param mixed partenaires
     * @param mixed             $partenaires
     *
     * @return self
     */
    public function setPartenaires($partenaires)
    {
        $this->partenaires = $partenaires;

        return $this;
    }

    /**
     * Set the value of Child.
     *
     * @param mixed child
     * @param mixed       $child
     *
     * @return self
     */
    public function setChild($child)
    {
        $this->child = $child;

        return $this;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * get the value of Evenements.
     *
     * @return mixed
     */
    public function getEvenements()
    {
        return $this->evenements;
    }

    /**
     * Set the value of Evenements.
     *
     * @param mixed evenements
     * @param mixed            $evenements
     *
     * @return self
     */
    public function setEvenements($evenements)
    {
        $this->evenements = $evenements;

        return $this;
    }

    /**
     * Get the value of Totalnbevenement.
     *
     * @return string
     */
    public function getTotalnbevenement()
    {
        return $this->totalnbevenement;
    }

    /**
     * Set the value of Totalnbevenement.
     *
     * @param string totalnbevenement
     * @param mixed                   $totalnbevenement
     *
     * @return self
     */
    public function setTotalnbevenement($totalnbevenement)
    {
        $this->totalnbevenement = $totalnbevenement;

        return $this;
    }

    public function getSearchData()
    {
        $tab = [
            'id'  => $this->getId(),
            'nom' => $this->getNom(),
        ];

        return $tab;
    }

    /**
     * Add categorie.
     *
     * @param Categorie $categorie
     *
     * @return Categorie
     */
    public function addCategorie($categorie)
    {
        $categorie->setRefCategorie($this);
        $this->categories->add($categorie);

        return $this;
    }

    /**
     * Remove categorie.
     *
     * @param Categorie $categorie
     */
    public function removeCategorie($categorie)
    {
        $this->categories->removeElement($categorie);
    }

    /**
     * get categories.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set refcategorie.
     *
     * @param Categorie $refcategorie
     *
     * @return Categorie
     */
    public function setRefCategorie($refcategorie = null)
    {
        $this->refcategorie = $refcategorie;

        return $this;
    }

    /**
     * get refcategorie.
     *
     * @return Categorie
     */
    public function getRefCategorie()
    {
        return $this->refcategorie;
    }
}
