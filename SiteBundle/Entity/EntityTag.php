<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;

class EntityTag
{
    /**
     * @var int
     *
     * @ORM\Column(name="tag_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Gedmo\Slug(updatable=false,fields={"nom"})
     * @ORM\Column(name="tag_alias", type="string")
     */
    protected $alias;

    /**
     * @var text
     *
     * @ORM\Column(name="tag_nom", type="text", nullable=true)
     */
    protected $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="tag_totalnbblog", type="integer")
     */
    protected $totalnbblog;

    /**
     * @var string
     *
     * @ORM\Column(name="tag_totalnbbookmark", type="integer")
     */
    protected $totalnbbookmark;

    /**
     * @JMS\Exclude()
     * @ORM\ManyToMany(targetEntity="Blog", inversedBy="tags", cascade={"remove", "persist"})
     * @ORM\JoinTable(
     *     joinColumns={@ORM\JoinColumn(name="reftag", referencedColumnName="tag_id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="refblog", referencedColumnName="blog_id")}
     *     )
     */
    protected $blogs;

    /**
     * @JMS\Exclude()
     * @ORM\ManyToMany(targetEntity="Bookmark", inversedBy="tags", cascade={"remove", "persist"})
     * @ORM\JoinTable(
     *     joinColumns={@ORM\JoinColumn(name="reftag", referencedColumnName="tag_id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="refbookmark", referencedColumnName="bookmark_id")}
     *     )
     */
    protected $bookmarks;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->totalnbblog     = 0;
        $this->totalnbbookmark = 0;
        $this->blogs           = new ArrayCollection();
        $this->bookmarks       = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getNom();
    }

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alias.
     *
     * @param string $alias
     *
     * @return Tag
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * get alias.
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return Tag
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Add blogs.
     *
     * @param Blog $blogs
     *
     * @return Tag
     */
    public function addBlog($blogs)
    {
        $this->blogs->add($blogs);

        return $this;
    }

    /**
     * Remove blogs.
     *
     * @param Blog $blogs
     */
    public function removeBlog($blogs)
    {
        $this->blogs->removeElement($blogs);
    }

    /**
     * get blogs.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBlogs()
    {
        return $this->blogs;
    }

    /**
     * Add bookmarks.
     *
     * @param Blog $bookmarks
     *
     * @return Tag
     */
    public function addBookmark($bookmarks)
    {
        $this->bookmarks->add($bookmarks);

        return $this;
    }

    /**
     * Remove bookmarks.
     *
     * @param Blog $bookmarks
     */
    public function removeBookmark($bookmarks)
    {
        $this->bookmarks->removeElement($bookmarks);
    }

    /**
     * get bookmarks.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBookmarks()
    {
        return $this->bookmarks;
    }

    /**
     * get the value of Totalnbblog.
     *
     * @return string
     */
    public function getTotalnbblog()
    {
        return $this->totalnbblog;
    }

    /**
     * Set the value of Totalnbblog.
     *
     * @param string totalnbblog
     * @param mixed $totalnbblog
     *
     * @return self
     */
    public function setTotalnbblog($totalnbblog)
    {
        $this->totalnbblog = $totalnbblog;

        return $this;
    }

    /**
     * get the value of Totalnbbookmark.
     *
     * @return string
     */
    public function getTotalnbbookmark()
    {
        return $this->totalnbbookmark;
    }

    /**
     * Set the value of Totalnbbookmark.
     *
     * @param string totalnbbookmark
     * @param mixed $totalnbbookmark
     *
     * @return self
     */
    public function setTotalnbbookmark($totalnbbookmark)
    {
        $this->totalnbbookmark = $totalnbbookmark;

        return $this;
    }

    /**
     * Set the value of Blogs.
     *
     * @param mixed blogs
     * @param mixed $blogs
     *
     * @return self
     */
    public function setBlogs($blogs)
    {
        $this->blogs = $blogs;

        return $this;
    }

    /**
     * Set the value of Bookmarks.
     *
     * @param mixed bookmarks
     * @param mixed $bookmarks
     *
     * @return self
     */
    public function setBookmarks($bookmarks)
    {
        $this->bookmarks = $bookmarks;

        return $this;
    }
}
