<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Translatable\Translatable;
use JMS\Serializer\Annotation as JMS;

/**
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class EntityGroup implements Translatable
{
    use SoftDeleteableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="group_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="group_nom", type="string", length=255)
     */
    protected $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="group_code", unique=true, type="string", length=255)
     */
    protected $code;

    /**
     * @JMS\Exclude()
     * @ORM\OneToMany(targetEntity="User", mappedBy="refgroup", cascade={"persist"})
     */
    protected $users;

    /**
     * @JMS\Exclude()
     * @ORM\OneToMany(targetEntity="Action", mappedBy="refgroup", cascade={"remove"})
     */
    protected $actions;

    /**
     * @var string
     *
     * @ORM\Column(name="group_totalnbuser", type="integer", options={"default" = 0})
     */
    protected $totalnbuser;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     * and it is not necessary because globally locale can be set in listener
     */
    protected $locale;

    public function __construct()
    {
        $this->totalnbuser = 0;
        $this->users       = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getNom();
    }

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set code.
     *
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Add users.
     *
     * @param User $users
     */
    public function addUser($users)
    {
        $users->setRefGroup($this);
        $this->users->add($users);
    }

    /**
     * get users.
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Remove users.
     *
     * @param User $users
     */
    public function removeUser($users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Add actions.
     *
     * @param Action $actions
     *
     * @return Group
     */
    public function addAction($actions)
    {
        $actions->setRefGroup($this);
        $this->actions->add($actions);

        return $this;
    }

    /**
     * Remove actions.
     *
     * @param Action $actions
     */
    public function removeAction($actions)
    {
        $this->actions->removeElement($actions);
    }

    /**
     * get actions.
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * get the value of Totalnbuser.
     *
     * @return string
     */
    public function getTotalnbuser()
    {
        return $this->totalnbuser;
    }

    /**
     * Set the value of Totalnbuser.
     *
     * @param string totalnbuser
     * @param mixed              $totalnbuser
     *
     * @return self
     */
    public function setTotalnbuser($totalnbuser)
    {
        $this->totalnbuser = $totalnbuser;

        return $this;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    public function getSearchData()
    {
        $tab = [
            'id'  => $this->getId(),
            'nom' => $this->getNom(),
        ];

        return $tab;
    }
}
