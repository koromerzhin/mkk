<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mkk\SiteBundle\Annotation\UploadableField;

class EntityPage
{
    /**
     * @var int
     *
     * @ORM\Column(name="page_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="page_titre", type="text", nullable=true)
     */
    protected $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="page_url", unique=true, type="string", length=255)
     */
    protected $url;

    /**
     * @var string
     *
     * @ORM\Column(name="page_contenu", type="text", nullable=true)
     */
    protected $contenu;

    /**
     * @var string
     *
     * @ORM\Column(name="page_css", type="text", nullable=true)
     */
    protected $css;

    /**
     * @var array
     *
     * @ORM\Column(name="page_data", type="array", nullable=true)
     */
    protected $data;

    /**
     * @var string
     *
     * @ORM\Column(name="page_image", type="text", nullable=true)
     */
    protected $image;

    /**
     * @UploadableField(filename="image", path="page/image", unique=true, alias="titre")
     */
    protected $file_image;

    /**
     * @var string
     *
     * @ORM\Column(name="page_fondimage", type="text", nullable=true)
     */
    protected $fondimage;

    /**
     * @UploadableField(filename="fondimage", path="page/fondimage", unique=true, alias="titre")
     */
    protected $file_fondimage;

    /**
     * @var string
     *
     * @ORM\Column(name="page_filigramme", type="text", nullable=true)
     */
    protected $filigramme;

    /**
     * @UploadableField(filename="filigramme", path="page/filigramme", unique=true, alias="titre")
     */
    protected $file_filigramme;

    /**
     * @var string
     *
     * @ORM\Column(name="page_video", type="text", nullable=true)
     */
    protected $video;

    /**
     * @UploadableField(filename="video", path="page/video", unique=true, alias="titre")
     */
    protected $file_video;

    /**
     * @var array
     *
     * @ORM\Column(name="page_galerie", type="array", nullable=true)
     */
    protected $galerie;

    /**
     * @UploadableField(filename="galerie", path="page/galerie", unique=false, alias="titre")
     */
    protected $file_galerie;

    /**
     * @var string
     *
     * @ORM\Column(name="page_meta_titre", type="string", length=255, nullable=true)
     */
    protected $metaTitre;

    /**
     * @var string
     *
     * @ORM\Column(name="page_meta_description", type="string", length=255, nullable=true)
     */
    protected $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="page_meta_keywords", type="string", length=255, nullable=true)
     */
    protected $metaKeywords;

    /**
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    public function __toString()
    {
        return $this->getTitre();
    }

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre.
     *
     * @param string $titre
     *
     * @return Page
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * get titre.
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set url.
     *
     * @param string $url
     *
     * @return Page
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * get url.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set contenu.
     *
     * @param string $contenu
     *
     * @return Page
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * get contenu.
     *
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set param.
     *
     * @param text  $param
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * get param.
     *
     * @return text
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set image.
     *
     * @param string $image
     *
     * @return Page
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * get image.
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set galerie.
     *
     * @param string $galerie
     *
     * @return Piece
     */
    public function setGalerie($galerie)
    {
        $this->galerie = $galerie;

        return $this;
    }

    /**
     * get galerie.
     *
     * @return string
     */
    public function getGalerie()
    {
        return $this->galerie;
    }

    /**
     * get the value of Meta Titre.
     *
     * @return string
     */
    public function getMetaTitre()
    {
        return $this->metaTitre;
    }

    /**
     * Set the value of Meta Titre.
     *
     * @param string meta_titre
     * @param mixed $metaTitre
     *
     * @return self
     */
    public function setMetaTitre($metaTitre)
    {
        $this->metaTitre = $metaTitre;

        return $this;
    }

    /**
     * get the value of Meta Description.
     *
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set the value of Meta Description.
     *
     * @param string meta_description
     * @param mixed $metaDescription
     *
     * @return self
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * get the value of Meta Keywords.
     *
     * @return string
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set the value of Meta Keywords.
     *
     * @param string meta_keywords
     * @param mixed $metaKeywords
     *
     * @return self
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    /**
     * Get the value of Css.
     *
     * @return string
     */
    public function getCss()
    {
        return $this->css;
    }

    /**
     * Set the value of Css.
     *
     * @param string css
     * @param mixed $css
     *
     * @return self
     */
    public function setCss($css)
    {
        $this->css = $css;

        return $this;
    }

    /**
     * Get the value of Fondimage.
     *
     * @return string
     */
    public function getFondimage()
    {
        return $this->fondimage;
    }

    /**
     * Set the value of Fondimage.
     *
     * @param string fondimage
     * @param mixed $fondimage
     *
     * @return self
     */
    public function setFondimage($fondimage)
    {
        $this->fondimage = $fondimage;

        return $this;
    }

    /**
     * Get the value of Filigramme.
     *
     * @return string
     */
    public function getFiligramme()
    {
        return $this->filigramme;
    }

    /**
     * Set the value of Filigramme.
     *
     * @param string filigramme
     * @param mixed $filigramme
     *
     * @return self
     */
    public function setFiligramme($filigramme)
    {
        $this->filigramme = $filigramme;

        return $this;
    }

    /**
     * Get the value of Video.
     *
     * @return string
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set the value of Video.
     *
     * @param string video
     * @param mixed $video
     *
     * @return self
     */
    public function setVideo($video)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get the value of File Image.
     *
     * @return mixed
     */
    public function getFileImage()
    {
        return $this->file_image;
    }

    /**
     * Set the value of File Image.
     *
     * @param mixed file_image
     * @param mixed $file_image
     *
     * @return self
     */
    public function setFileImage($file_image)
    {
        $this->file_image = $file_image;

        return $this;
    }

    /**
     * Get the value of File Fondimage.
     *
     * @return mixed
     */
    public function getFileFondimage()
    {
        return $this->file_fondimage;
    }

    /**
     * Set the value of File Fondimage.
     *
     * @param mixed file_fondimage
     * @param mixed $file_fondimage
     *
     * @return self
     */
    public function setFileFondimage($file_fondimage)
    {
        $this->file_fondimage = $file_fondimage;

        return $this;
    }

    /**
     * Get the value of File Filigramme.
     *
     * @return mixed
     */
    public function getFileFiligramme()
    {
        return $this->file_filigramme;
    }

    /**
     * Set the value of File Filigramme.
     *
     * @param mixed file_filigramme
     * @param mixed $file_filigramme
     *
     * @return self
     */
    public function setFileFiligramme($file_filigramme)
    {
        $this->file_filigramme = $file_filigramme;

        return $this;
    }

    /**
     * Get the value of File Video.
     *
     * @return mixed
     */
    public function getFileVideo()
    {
        return $this->file_video;
    }

    /**
     * Set the value of File Video.
     *
     * @param mixed file_video
     * @param mixed $file_video
     *
     * @return self
     */
    public function setFileVideo($file_video)
    {
        $this->file_video = $file_video;

        return $this;
    }

    /**
     * Get the value of File Galerie.
     *
     * @return mixed
     */
    public function getFileGalerie()
    {
        return $this->file_galerie;
    }

    /**
     * Set the value of File Galerie.
     *
     * @param mixed file_galerie
     * @param mixed $file_galerie
     *
     * @return self
     */
    public function setFileGalerie($file_galerie)
    {
        $this->file_galerie = $file_galerie;

        return $this;
    }

    /**
     * Get the value of Updated At.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set the value of Updated At.
     *
     * @param \DateTime updatedAt
     *
     * @return self
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
