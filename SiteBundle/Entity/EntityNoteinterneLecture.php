<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class EntityNoteinterneLecture
{
    /**
     * @var int
     *
     * @ORM\Column(name="noteinternelecture_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="noteinternelectures")
     * @ORM\JoinColumn(name="noteinternelecture_refuser", referencedColumnName="user_id")
     */
    protected $refuser;

    /**
     * @ORM\ManyToOne(targetEntity="Noteinterne", inversedBy="noteinternelectures")
     * @ORM\JoinColumn(name="noteinternelecture_refnoteinterne", referencedColumnName="noteinterne_id")
     */
    protected $refnoteinterne;

    /**
     * @var bool
     *
     * @ORM\Column(name="noteinternelecture_lecture", type="boolean",nullable=true)
     */
    protected $lecture;

    /**
     * @var int
     *
     * @ORM\Column(name="noteinternelecture_date", type="integer", nullable=true)
     */
    protected $date;

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set refuser.
     *
     * @param int $refuser
     *
     * @return Lecture
     */
    public function setRefUser($refuser)
    {
        $this->refuser = $refuser;

        return $this;
    }

    /**
     * get refuser.
     *
     * @return int
     */
    public function getRefUser()
    {
        return $this->refuser;
    }

    /**
     * Set refnoteinterne.
     *
     * @param int $refnoteinterne
     *
     * @return Lecture
     */
    public function setRefNoteinterne($refnoteinterne)
    {
        $this->refnoteinterne = $refnoteinterne;

        return $this;
    }

    /**
     * get refnoteinterne.
     *
     * @return int
     */
    public function getRefNoteinterne()
    {
        return $this->refnoteinterne;
    }

    /**
     * Set lecture.
     *
     * @param bool $lecture
     *
     * @return Lecture
     */
    public function setLecture($lecture)
    {
        $this->lecture = $lecture;

        return $this;
    }

    /**
     * get lecture.
     *
     * @return bool
     */
    public function getLecture()
    {
        return $this->lecture;
    }

    /**
     * Set date.
     *
     * @param int $date
     *
     * @return Noteinterne
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * get datedebut.
     *
     * @return int
     */
    public function getDate()
    {
        return $this->date;
    }
}
