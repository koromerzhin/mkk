<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

class EntityTemplates implements Translatable
{
    /**
     * @var int
     * @ORM\Column(name="templates_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="templates_type", type="string", length=255)
     */
    protected $type;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="templates_nom", type="string", length=255, nullable=true)
     */
    protected $nom;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="templates_content", type="text", nullable=true, nullable=true)
     */
    protected $content;

    /**
     * @var string
     *
     * @ORM\Column(name="templates_code", type="string", length=255)
     */
    protected $code;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     * and it is not necessary because globally locale can be set in listener
     */
    protected $locale;

    public function __toString()
    {
        return $this->getNom();
    }

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type.
     *
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set content.
     *
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * get content.
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set code.
     *
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }
}
