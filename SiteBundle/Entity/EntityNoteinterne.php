<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

class EntityNoteinterne implements Translatable
{
    /**
     * @var int
     *
     * @ORM\Column(name="noteinterne_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var text
     * @Gedmo\Translatable
     * @ORM\Column(name="noteinterne_titre", type="text", nullable=true)
     */
    protected $titre;

    /**
     * @var int
     *
     * @ORM\Column(name="noteinterne_datemodification", type="integer", nullable=true)
     */
    protected $datemodification;

    /**
     * @var int
     *
     * @ORM\Column(name="noteinterne_datedebut", type="integer", nullable=true)
     */
    protected $datedebut;

    /**
     * @var int
     *
     * @ORM\Column(name="noteinterne_datefin", type="integer", nullable=true)
     */
    protected $datefin;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="noteinterne_contenu", type="text", nullable=true)
     */
    protected $contenu;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="noteinterne_lien", type="text", nullable=true)
     */
    protected $lien;

    /**
     * @var bool
     *
     * @ORM\Column(name="noteinterne_publier", type="boolean",nullable=true)
     */
    protected $publier;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="noteinternes")
     * @ORM\JoinColumn(name="noteinterne_refuser", referencedColumnName="user_id")
     */
    protected $refuser;

    /**
     * @ORM\OneToMany(targetEntity="NoteinterneLecture", mappedBy="refnoteinterne", cascade={"remove", "persist"})
     */
    protected $noteinternelectures;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     * and it is not necessary because globally locale can be set in listener
     */
    protected $locale;

    public function __toString()
    {
        return $this->getTitre();
    }

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre.
     *
     * @param text $titre
     *
     * @return Noteinterne
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * get titre.
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set datedebut.
     *
     * @param int $datedebut
     *
     * @return Noteinterne
     */
    public function setDatedebut($datedebut)
    {
        $this->datedebut = $datedebut;
        if (substr_count($datedebut, ' ') != 0) {
            list($date, $horaire)      = explode(' ', $datedebut);
            list($jour, $mois, $annee) = explode('/', $date);
            list($heure, $minute)      = explode(':', $horaire);
            $this->datedebut           = mktime($heure, $minute, 0, $mois, $jour, $annee);
        }

        return $this;
    }

    /**
     * get datedebut.
     *
     * @return int
     */
    public function getDatedebut()
    {
        return $this->datedebut;
    }

    /**
     * Set datemodification.
     *
     * @param int $datemodification
     *
     * @return Noteinterne
     */
    public function setDatemodification($datemodification)
    {
        $this->datemodification = $datemodification;
        if (substr_count($datemodification, ' ') != 0) {
            list($date, $horaire)      = explode(' ', $datemodification);
            list($jour, $mois, $annee) = explode('/', $date);
            list($heure, $minute)      = explode(':', $horaire);
            $this->datemodification    = mktime($heure, $minute, 0, $mois, $jour, $annee);
        }

        return $this;
    }

    /**
     * get datemodification.
     *
     * @return int
     */
    public function getDatemodification()
    {
        return $this->datemodification;
    }

    /**
     * Set datefin.
     *
     * @param int $datefin
     *
     * @return Noteinterne
     */
    public function setDatefin($datefin)
    {
        $this->datefin = $datefin;
        if (substr_count($datefin, ' ') != 0) {
            list($date, $horaire)      = explode(' ', $datefin);
            list($jour, $mois, $annee) = explode('/', $date);
            list($heure, $minute)      = explode(':', $horaire);
            $this->datefin             = mktime($heure, $minute, 0, $mois, $jour, $annee);
        }

        return $this;
    }

    /**
     * get datefin.
     *
     * @return int
     */
    public function getDatefin()
    {
        return $this->datefin;
    }

    /**
     * Set contenu.
     *
     * @param string $contenu
     *
     * @return Noteinterne
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * get contenu.
     *
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set lien.
     *
     * @param string $lien
     *
     * @return Noteinterne
     */
    public function setLien($lien)
    {
        $this->lien = $lien;

        return $this;
    }

    /**
     * get lien.
     *
     * @return string
     */
    public function getLien()
    {
        return $this->lien;
    }

    /**
     * Set publier.
     *
     * @param bool $publier
     *
     * @return Noteinterne
     */
    public function setPublier($publier)
    {
        $this->publier = $publier;

        return $this;
    }

    /**
     * get publier.
     *
     * @return bool
     */
    public function getPublier()
    {
        return $this->publier;
    }

    /**
     * Set refuser.
     *
     * @param int $refuser
     *
     * @return Noteinterne
     */
    public function setRefUser($refuser)
    {
        $this->refuser = $refuser;

        return $this;
    }

    /**
     * get refuser.
     *
     * @return int
     */
    public function getRefUser()
    {
        return $this->refuser;
    }

    /**
     * Add noteinternelectures.
     *
     * @param Noteinternelecture $noteinternelectures
     *
     * @return Noteinterne
     */
    public function addNoteinterneLecture($noteinternelectures)
    {
        $noteinternelectures->setRefNoteInterne($this);
        $this->noteinternelectures->add($noteinternelectures);

        return $this;
    }

    /**
     * Remove lectures.
     *
     * @param NoteinterneLecture $noteinternelectures
     */
    public function removeNoteinterneLecture($noteinternelectures)
    {
        $this->noteinternelectures->removeElement($noteinternelectures);
    }

    /**
     * get noteinternelectures.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNoteinterneLectures()
    {
        return $this->noteinternelectures;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }
}
