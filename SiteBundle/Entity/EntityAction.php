<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class EntityAction
{
    /**
     * @var int
     *
     * @ORM\Column(name="action_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="action_module", type="string", length=255)
     */
    protected $module;

    /**
     * @var string
     *
     * @ORM\Column(name="action_codes", type="text", nullable=true)
     */
    protected $codes;

    /**
     * @ORM\ManyToOne(targetEntity="Group", inversedBy="actions")
     * @ORM\JoinColumn(name="action_refgroup", referencedColumnName="group_id")
     */
    protected $refgroup;

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set module.
     *
     * @param string $module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * get module.
     *
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set codes.
     *
     * @param string $codes
     */
    public function setCodes($codes)
    {
        $this->codes = $codes;
    }

    /**
     * get codes.
     *
     * @return string
     */
    public function getCodes()
    {
        return $this->codes;
    }

    /**
     * Set refgroup.
     *
     * @param Group $refgroup
     */
    public function setRefGroup($refgroup)
    {
        $this->refgroup = $refgroup;
    }

    /**
     * get refgroup.
     *
     * @return Group
     */
    public function getRefGroup()
    {
        return $this->refgroup;
    }
}
