<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

class EntityDate
{
    /**
     * @var int
     *
     * @ORM\Column(name="date_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="date_debut", type="integer")
     */
    protected $debut;

    /**
     * @var int
     *
     * @ORM\Column(name="date_place", type="integer", nullable=true)
     */
    protected $place;

    /**
     * @var bool
     *
     * @ORM\Column(name="date_placeillimite", type="boolean", nullable=true)
     */
    protected $placeillimite;

    /**
     * @var int
     *
     * @ORM\Column(name="date_fin", type="integer")
     */
    protected $fin;

    /**
     * @JMS\Exclude()
     * @ORM\ManyToOne(targetEntity="Emplacement", inversedBy="dates")
     * @ORM\JoinColumn(name="date_refemplacement", referencedColumnName="emplacement_id", nullable=false)
     */
    protected $refemplacement;

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set debut.
     *
     * @param int $debut
     *
     * @return Date
     */
    public function setDebut($debut)
    {
        $this->debut = $debut;
        if (substr_count($debut, ' ') != 0) {
            list($date, $horaire)      = explode(' ', $debut);
            list($jour, $mois, $annee) = explode('/', $date);
            list($heure, $minute)      = explode(':', $horaire);
            $this->debut               = mktime($heure, $minute, 0, $mois, $jour, $annee);
        }

        return $this;
    }

    /**
     * get debut.
     *
     * @return int
     */
    public function getDebut()
    {
        return $this->debut;
    }

    /**
     * Set fin.
     *
     * @param \integer $fin
     *
     * @return Date
     */
    public function setFin($fin)
    {
        $this->fin = $fin;
        if (substr_count($fin, ' ') != 0) {
            list($date, $horaire)      = explode(' ', $fin);
            list($jour, $mois, $annee) = explode('/', $date);
            list($heure, $minute)      = explode(':', $horaire);
            $this->fin                 = mktime($heure, $minute, 0, $mois, $jour, $annee);
        }

        return $this;
    }

    /**
     * get fin.
     *
     * @return \datetime
     */
    public function getFin()
    {
        return $this->fin;
    }

    /**
     * Set refemplacement.
     *
     * @param Emplacement $refemplacement
     *
     * @return Date
     */
    public function setRefEmplacement($refemplacement)
    {
        $this->refemplacement = $refemplacement;

        return $this;
    }

    /**
     * get refemplacement.
     *
     * @return Emplacement
     */
    public function getRefEmplacement()
    {
        return $this->refemplacement;
    }

    /**
     * Set place.
     *
     * @param \integer $place
     *
     * @return Date
     */
    public function setPlace($place)
    {
        $this->place = intval($place);

        return $this;
    }

    /**
     * get fin.
     *
     * @return \datetime
     */
    public function getPlace()
    {
        return intval($this->place);
    }

    /**
     * get the value of Placeillimite.
     *
     * @return bool
     */
    public function getPlaceillimite()
    {
        return $this->placeillimite;
    }

    /**
     * Set the value of Placeillimite.
     *
     * @param bool placeillimite
     * @param mixed              $placeillimite
     *
     * @return self
     */
    public function setPlaceillimite($placeillimite)
    {
        $this->placeillimite = $placeillimite;

        return $this;
    }
}
