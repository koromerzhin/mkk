<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class EntityPrix
{
    /**
     * @var int
     *
     * @ORM\Column(name="prix_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="prix_chiffre", type="text", nullable=true)
     */
    protected $chiffre;

    /**
     * @var string
     *
     * @ORM\Column(name="prix_type", type="text", nullable=true)
     */
    protected $type;

    /**
     * @ORM\ManyToOne(targetEntity="Evenement", inversedBy="prixs")
     * @ORM\JoinColumn(name="prix_refevenement", referencedColumnName="evenement_id", nullable=true)
     */
    protected $refevenement;

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set chiffre.
     *
     * @param string $chiffre
     *
     * @return Prix
     */
    public function setChiffre($chiffre)
    {
        $this->chiffre = $chiffre;

        return $this;
    }

    /**
     * get chiffre.
     *
     * @return string
     */
    public function getChiffre()
    {
        return $this->chiffre;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return Prix
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set refevenement.
     *
     * @param Evenement $refevenement
     *
     * @return Prix
     */
    public function setRefEvenement($refevenement = null)
    {
        $this->refevenement = $refevenement;

        return $this;
    }

    /**
     * get refevenement.
     *
     * @return Evenement
     */
    public function getRefEvenement()
    {
        return $this->refevenement;
    }
}
