<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

class EntityMetariane implements Translatable
{
    /**
     * @var int
     *
     * @ORM\Column(name="metariane_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="metariane_route", unique=true, type="string", length=255)
     */
    protected $route;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="metariane_titre", type="text", nullable=true)
     */
    protected $titre;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="metariane_description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="metariane_keywords", type="text", nullable=true)
     */
    protected $keywords;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="metariane_ariane", type="text", nullable=true)
     */
    protected $ariane;

    /**
     * @var string
     * @ORM\Column(name="metariane_robots", type="text", nullable=true)
     */
    protected $robots;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     * and it is not necessary because globally locale can be set in listener
     */
    protected $locale;

    public function __toString()
    {
        return $this->getTitre();
    }

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set route.
     *
     * @param string $route
     *
     * @return Metariane
     */
    public function setRoute($route)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * get route.
     *
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Set titre.
     *
     * @param string $titre
     *
     * @return Metariane
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * get titre.
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Metariane
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set keywords.
     *
     * @param string $keywords
     *
     * @return Metariane
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * get keywords.
     *
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set ariane.
     *
     * @param string $ariane
     *
     * @return Metariane
     */
    public function setAriane($ariane)
    {
        $this->ariane = $ariane;

        return $this;
    }

    /**
     * get ariane.
     *
     * @return string
     */
    public function getAriane()
    {
        return $this->ariane;
    }

    /**
     * Set robots.
     *
     * @param string $robots
     *
     * @return Metariane
     */
    public function setRobots($robots)
    {
        $this->robots = $robots;

        return $this;
    }

    /**
     * get robots.
     *
     * @return string
     */
    public function getRobots()
    {
        return $this->robots;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }
}
