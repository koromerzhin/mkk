<?php

namespace Mkk\SiteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Mkk\SiteBundle\Annotation\UploadableField;

class EntityBlog
{
    /**
     * @var int
     *
     * @ORM\Column(name="blog_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="blog_accueil", type="boolean", nullable=true)
     */
    protected $accueil;

    /**
     * @var bool
     *
     * @ORM\Column(name="blog_commentaire", type="boolean", nullable=true)
     */
    protected $commentaire;

    /**
     * @var string
     *
     * @ORM\Column(name="blog_langue", type="text", nullable=true)
     */
    protected $langue;

    /**
     * @var string
     * @Gedmo\Slug(updatable=false,fields={"titre"})
     * @ORM\Column(name="blog_alias", type="string")
     */
    protected $alias;

    /**
     * @var text
     *
     * @ORM\Column(name="blog_titre", type="text", nullable=true)
     */
    protected $titre;

    /**
     * @var bool
     *
     * @ORM\Column(name="blog_actif_public", type="boolean", nullable=true)
     */
    protected $actifPublic;

    /**
     * @var string
     *
     * @ORM\Column(name="blog_vignette", type="string", length=255, nullable=true)
     */
    protected $vignette;

    /**
     * @UploadableField(filename="vignette", path="blog/vignette", unique=true, alias="titre")
     */
    protected $file_vignette;

    /**
     * @var string
     *
     * @ORM\Column(name="blog_image", type="string", length=255, nullable=true)
     */
    protected $image;

    /**
     * @UploadableField(filename="image", path="blog/image", unique=true, alias="titre")
     */
    protected $file_image;

    /**
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * @var array
     *
     * @ORM\Column(name="blog_galerie", type="array", nullable=true)
     */
    protected $galerie;

    /**
     * @UploadableField(filename="galerie", path="blog/galerie", unique=false, alias="titre")
     */
    protected $file_galerie;

    /**
     * @var string
     *
     * @ORM\Column(name="blog_intro", type="text", nullable=true)
     */
    protected $intro;

    /**
     * @var string
     *
     * @ORM\Column(name="blog_contenu", type="text", nullable=true)
     */
    protected $contenu;

    /**
     * @var int
     *
     * @ORM\Column(name="blog_datecreation", type="integer", nullable=true)
     */
    protected $datecreation;

    /**
     * @var int
     *
     * @ORM\Column(name="blog_datepublication", type="integer", nullable=true)
     */
    protected $datepublication;

    /**
     * @var int
     *
     * @ORM\Column(name="blog_datemodif", type="integer", nullable=true)
     */
    protected $datemodif;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="blogs")
     * @ORM\JoinColumn(name="blog_refuser", referencedColumnName="user_id")
     */
    protected $refuser;

    /**
     * @ORM\ManyToOne(targetEntity="Categorie", inversedBy="blogs")
     * @ORM\JoinColumn(name="blog_refcategorie", referencedColumnName="categorie_id")
     */
    protected $refcategorie;

    /**
     * @ORM\ManyToMany(targetEntity="Tag",  mappedBy="blogs")
     */
    protected $tags;

    /**
     * @var string
     *
     * @ORM\Column(name="blog_meta_titre", type="string", length=255, nullable=true)
     */
    protected $metaTitre;

    /**
     * @var string
     *
     * @ORM\Column(name="blog_meta_description", type="string", length=255, nullable=true)
     */
    protected $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="blog_meta_keywords", type="string", length=255, nullable=true)
     */
    protected $metaKeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="blog_video", type="string", length=255, nullable=true)
     */
    protected $video;

    /**
     * @var bool
     *
     * @ORM\Column(name="blog_avant", type="boolean", nullable=true)
     */
    protected $avant;

    /**
     * @var bool
     *
     * @ORM\Column(name="blog_redacteur", type="boolean", nullable=true)
     */
    protected $redacteur;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getTitre();
    }

    /**
     * get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alias.
     *
     * @param string $alias
     *
     * @return Blog
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * get alias.
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set langue.
     *
     * @param string $langue
     *
     * @return Blog
     */
    public function setLangue($langue)
    {
        $this->langue = $langue;

        return $this;
    }

    /**
     * get langue.
     *
     * @return string
     */
    public function getLangue()
    {
        return $this->langue;
    }

    /**
     * Set titre.
     *
     * @param string $titre
     *
     * @return Blog
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * get titre.
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set actif_public.
     *
     * @param string $actifPublic
     *
     * @return Blog
     */
    public function setActifPublic($actifPublic)
    {
        $this->actifPublic = $actifPublic;

        return $this;
    }

    /**
     * get actif_public.
     *
     * @return string
     */
    public function getActifPublic()
    {
        return $this->actifPublic;
    }

    /**
     * Set vignette.
     *
     * @param string $vignette
     *
     * @return Blog
     */
    public function setVignette($vignette)
    {
        $this->vignette = $vignette;

        return $this;
    }

    /**
     * get vignette.
     *
     * @return string
     */
    public function getVignette()
    {
        $file = $this->vignette;

        return $file;
    }

    /**
     * Set galerie.
     *
     * @param string $galerie
     *
     * @return Blog
     */
    public function setGalerie($galerie)
    {
        $this->galerie = $galerie;

        return $this;
    }

    /**
     * get galerie.
     *
     * @return string
     */
    public function getGalerie()
    {
        return $this->galerie;
    }

    /**
     * Set contenu.
     *
     * @param string $contenu
     *
     * @return Blog
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * get contenu.
     *
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set intro.
     *
     * @param string $intro
     *
     * @return Blog
     */
    public function setIntro($intro)
    {
        $this->intro = $intro;

        return $this;
    }

    /**
     * get intro.
     *
     * @return string
     */
    public function getIntro()
    {
        return $this->intro;
    }

    /**
     * Set datecreation.
     *
     * @param int $datecreation
     *
     * @return Blog
     */
    public function setDatecreation($datecreation)
    {
        $this->datecreation = $datecreation;

        return $this;
    }

    /**
     * get datecreation.
     *
     * @return int
     */
    public function getDatecreation()
    {
        return $this->datecreation;
    }

    /**
     * Set datemodif.
     *
     * @param int $datemodif
     *
     * @return Blog
     */
    public function setDatemodif($datemodif)
    {
        $this->datemodif = $datemodif;

        return $this;
    }

    /**
     * get datemodif.
     *
     * @return int
     */
    public function getDatemodif()
    {
        return $this->datemodif;
    }

    /**
     * Set meta_titre.
     *
     * @param string $metaTitre
     *
     * @return Blog
     */
    public function setMetaTitre($metaTitre)
    {
        $this->metaTitre = $metaTitre;

        return $this;
    }

    /**
     * get meta_titre.
     *
     * @return string
     */
    public function getMetaTitre()
    {
        return $this->metaTitre;
    }

    /**
     * Set meta_description.
     *
     * @param string $metaDescription
     *
     * @return Blog
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * get meta_description.
     *
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set meta_keywords.
     *
     * @param string $metaKeywords
     *
     * @return Blog
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    /**
     * get meta_keywords.
     *
     * @return string
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set video.
     *
     * @param string $video
     *
     * @return Blog
     */
    public function setVideo($video)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * get video.
     *
     * @return string
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set refuser.
     *
     * @param User $refuser
     *
     * @return Blog
     */
    public function setRefUser($refuser = null)
    {
        $this->refuser = $refuser;

        return $this;
    }

    /**
     * get refuser.
     *
     * @return User
     */
    public function getRefUser()
    {
        return $this->refuser;
    }

    /**
     * Set refcategorie.
     *
     * @param Categorie $refcategorie
     *
     * @return Blog
     */
    public function setRefCategorie($refcategorie = null)
    {
        $this->refcategorie = $refcategorie;

        return $this;
    }

    /**
     * get refcategorie.
     *
     * @return Categorie
     */
    public function getRefCategorie()
    {
        return $this->refcategorie;
    }

    /**
     * Add tags.
     *
     * @param Tag $tags
     *
     * @return Blog
     */
    public function addTag($tags)
    {
        $this->tags->add($tags);

        return $this;
    }

    /**
     * Remove tags.
     *
     * @param Tag $tags
     */
    public function removeTag($tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * get tags.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set image.
     *
     * @param string $image
     *
     * @return Blog
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * get image.
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set datepublication.
     *
     * @param int $datepublication
     *
     * @return Blog
     */
    public function setDatepublication($datepublication)
    {
        if (substr_count($datepublication, ' ') != 0) {
            list($date, $horaire)      = explode(' ', $datepublication);
            list($jour, $mois, $annee) = explode('/', $date);
            list($heure, $minute)      = explode(':', $horaire);
            $this->datepublication     = mktime($heure, $minute, 0, $mois, $jour, $annee);
        } else {
            $this->datepublication = $datepublication;
        }

        return $this;
    }

    /**
     * get datepublication.
     *
     * @return int
     */
    public function getDatepublication()
    {
        if ($this->datepublication == '') {
            return $this->datecreation;
        }

        return $this->datepublication;
    }

    /**
     * Set accueil.
     *
     * @param string $accueil
     *
     * @return Blog
     */
    public function setAccueil($accueil)
    {
        $this->accueil = $accueil;

        return $this;
    }

    /**
     * get accueil.
     *
     * @return string
     */
    public function getAccueil()
    {
        return $this->accueil;
    }

    /**
     * Set avant.
     *
     * @param bool $avant
     *
     * @return Blog
     */
    public function setAvant($avant)
    {
        $this->avant = $avant;

        return $this;
    }

    /**
     * get avant.
     *
     * @return bool
     */
    public function getAvant()
    {
        return $this->avant;
    }

    /**
     * Set redacteur.
     *
     * @param bool $redacteur
     *
     * @return Blog
     */
    public function setRedacteur($redacteur)
    {
        $this->redacteur = $redacteur;

        return $this;
    }

    /**
     * get redacteur.
     *
     * @return bool
     */
    public function getRedacteur()
    {
        return $this->redacteur;
    }

    /**
     * get the value of Commentaire.
     *
     * @return bool
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * Set the value of Commentaire.
     *
     * @param bool commentaire
     * @param mixed $commentaire
     *
     * @return self
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Set the value of Tags.
     *
     * @param mixed tags
     * @param mixed $tags
     *
     * @return self
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

    public function getCategorie()
    {
        return ! is_object($this->getRefCategorie()) ? '' : $this->getRefCategorie()->getId();
    }

    public function setCategorie($v)
    {
        return $this;
    }

    public function getUser()
    {
        return ! is_object($this->getRefUser()) ? '' : $this->getRefUser()->getId();
    }

    public function setUser($v)
    {
        return $this;
    }

    /**
     * Get the value of File Vignette.
     *
     * @return mixed
     */
    public function getFileVignette()
    {
        return $this->file_vignette;
    }

    /**
     * Set the value of File Vignette.
     *
     * @param mixed file_vignette
     * @param mixed $file_vignette
     *
     * @return self
     */
    public function setFileVignette($file_vignette)
    {
        $this->file_vignette = $file_vignette;

        return $this;
    }

    /**
     * Get the value of File Image.
     *
     * @return mixed
     */
    public function getFileImage()
    {
        return $this->file_image;
    }

    /**
     * Set the value of File Image.
     *
     * @param mixed file_image
     * @param mixed $file_image
     *
     * @return self
     */
    public function setFileImage($file_image)
    {
        $this->file_image = $file_image;

        return $this;
    }

    /**
     * Get the value of Updated At.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set the value of Updated At.
     *
     * @param \DateTime updatedAt
     *
     * @return self
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get the value of File Galerie.
     *
     * @return mixed
     */
    public function getFileGalerie()
    {
        return $this->file_galerie;
    }

    /**
     * Set the value of File Galerie.
     *
     * @param mixed file_galerie
     * @param mixed $file_galerie
     *
     * @return self
     */
    public function setFileGalerie($file_galerie)
    {
        $this->file_galerie = $file_galerie;

        return $this;
    }
}
