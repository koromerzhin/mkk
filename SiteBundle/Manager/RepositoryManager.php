<?php

namespace Mkk\SiteBundle\Manager;

use Doctrine\ORM\EntityManager;
use Mkk\SiteBundle\Lib\TranslatableRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RepositoryManager
{
    private $em;
    private $repository;

    public function __construct(ContainerInterface $container, EntityManager $em, $kernel, $table)
    {
        $this->container = $container;
        $this->kernel    = $kernel;
        $this->bundles   = $kernel->registerBundles();
        $this->em        = $em;
        $this->setTable($table);
        $this->deftable = $table;
        if (! defined('BUNDLESITE')) {
            $bundle = $this->getNamespace();
            define('BUNDLESITE', $bundle . 'SiteBundle');
        }

        if (! defined('BUNDLEU')) {
            $bundle = $this->getNameSpace();
            define('BUNDLEU', $bundle);
        }

        $this->translations = $em->getRepository('Gedmo\Translatable\Entity\Translation');
    }

    public function getTranslations()
    {
        return $this->translations;
    }

    public function setTable($table)
    {
        $emplacement = __DIR__ . '/../../..';
        foreach ($this->bundles as $row) {
            $name = $row->getName();
            if (substr_count($name, 'SiteBundle') != 0 && substr_count($name, 'MkkSiteBundle') == 0) {
                $namespace       = str_replace('SiteBundle', '', $name);
                $this->namespace = $namespace;
            }
        }
        $repository = $namespace . "SiteBundle:$table";
        $this->setRepository($repository);
        $this->table = "$namespace\SiteBundle\Entity\\$table";
        //echo $this->table;
    }

    public function getNamespace()
    {
        return $this->namespace;
    }

    public function getTable()
    {
        if (! isset($this->table)) {
            throw new \Exception('Bug sur la liaison table ' . $this->deftable);
        }

        return $this->table;
    }

    public function setRepository($repo)
    {
        $this->repository = $repo;
    }

    public function getEm()
    {
        return $this->em;
    }

    public function getRepository()
    {
        if (! $this->repository) {
            return false;
        }

        $return = $this->em->getRepository($this->repository);
        if ($return instanceof TranslatableRepository) {
            if ($this->container->has('request') == 1) {
                try {
                    $request = $this->container->get('request');
                    $locale  = $request->getLocale();
                    if ($locale != '') {
                        $return->setDefaultLocale($locale);
                    }
                } catch (\Exception $e) {
                }
            }
        }

        return $return;
    }

    public function flush()
    {
        $this->em->flush();
    }

    public function remove($entity)
    {
        $this->em->remove($entity);
    }

    public function detach($entity)
    {
        $this->em->detach($entity);
    }

    public function refresh($entity)
    {
        $this->em->refresh($entity);
    }

    public function persist($entity)
    {
        $this->em->persist($entity);
    }

    public function persistAndFlush($entity)
    {
        $this->persist($entity);
        $this->flush();
    }

    public function removeAndFlush($entity)
    {
        $this->remove($entity);
        $this->flush();
    }
}
