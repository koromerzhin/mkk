<?php

namespace Mkk\SiteBundle\Manager;

class AppManager
{
    public function getRoutes($container)
    {
        if (! isset($this->listroutes)) {
            $this->listroutes = [];
            $router           = $container->get('router');
            foreach ($router->getRouteCollection()->all() as $name => $route) {
                $pattern = $route->getPath();
                foreach ($route->getDefaults() as $code => $val) {
                    if ($code != '_controller') {
                        $pattern = str_replace('{' . $code . '}', $val, $pattern);
                    }
                }

                if (substr($name, 0, 1) != '_' && substr_count($name, '_') != 0) {
                    $this->listroutes[$name] = $pattern;
                }
            }
        }

        return $this->listroutes;
    }
}
