<?php

namespace Mkk\SiteBundle\Manager;

class TagManager
{
    private $kernel;
    private $bundles;
    private $container;

    public function __construct($kernel)
    {
        $this->kernel    = $kernel;
        $this->bundles   = $kernel->registerBundles();
        $this->container = $kernel->getContainer();
    }

    public function setTotalBookmarks()
    {
        $container     = $this->container;
        $tagManager    = $container->get('bdd.tag_manager');
        $tagRepository = $tagManager->getRepository();
        $data          = $tagRepository->commandTotalBookmarks();
        $batchSize     = 5;
        foreach ($data as $i => $row) {
            $idtag = $row['id'];
            $total = $row['total'];
            $tag   = $tagRepository->find($idtag);
            if ($tag) {
                $tag->setTotalnbbookmark($total);
                $tagManager->persist($tag);
                if (($i % $batchSize) == 0) {
                    $tagManager->flush();
                }
            }
        }

        $tagManager->flush();
    }

    public function setTotalBlogs()
    {
        $container     = $this->container;
        $tagManager    = $container->get('bdd.tag_manager');
        $tagRepository = $tagManager->getRepository();
        $data          = $tagRepository->commandTotalBlogs();
        $batchSize     = 5;
        foreach ($data as $i => $row) {
            $idtag = $row['id'];
            $total = $row['total'];
            $tag   = $tagRepository->find($idtag);
            if ($tag) {
                $tag->setTotalnbblog($total);
                $tagManager->persist($tag);
                if (($i % $batchSize) == 0) {
                    $tagManager->flush();
                }
            }
        }

        $tagManager->flush();
    }
}
