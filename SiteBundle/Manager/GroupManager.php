<?php

namespace Mkk\SiteBundle\Manager;

class GroupManager
{
    private $kernel;
    private $bundles;
    private $container;

    public function __construct($kernel)
    {
        $this->kernel    = $kernel;
        $this->bundles   = $kernel->registerBundles();
        $this->container = $kernel->getContainer();
    }

    public function setTotalUser()
    {
        $container       = $this->container;
        $groupManager    = $container->get('bdd.group_manager');
        $groupRepository = $groupManager->getRepository();
        $data            = $groupRepository->commandTotalUser();
        $batchSize       = 5;
        foreach ($data as $i => $row) {
            $idgroup = $row['id'];
            $total   = $row['total'];
            $group   = $groupRepository->find($idgroup);
            if ($group) {
                $group->setTotalnbuser($total);
                $groupManager->persist($group);
                if (($i % $batchSize) == 0) {
                    $groupManager->flush();
                }
            }
        }

        $groupManager->flush();
    }
}
