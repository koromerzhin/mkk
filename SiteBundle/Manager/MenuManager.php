<?php

namespace Mkk\SiteBundle\Manager;

class MenuManager
{
    public function listingMenu($menu, $parameters, $code)
    {
        $tab = [];
        if (isset($parameters['idgroup'])) {
            $idgroup = $parameters['idgroup'];
            foreach ($menu as $row) {
                if ($row['clef'] == $code) {
                    foreach ($row['child'] as $child) {
                        $tab = $this->lienActif($child, $idgroup, $tab);
                    }

                    break;
                }
            }
        }

        return $tab;
    }

    public function setactifMenuLeft($menu, $parameters)
    {
        $etat = 0;
        if (isset($parameters['idgroup'])) {
            $idgroup = $parameters['idgroup'];
            foreach ($menu as $row) {
                if ($row['clef'] == 'menuleft') {
                    foreach ($row['child'] as $child) {
                        $refgroup = $child['refgroup'];

                        if (in_array($idgroup, $refgroup)) {
                            $etat = 1;
                        }

                        break;
                    }

                    break;
                }
            }
        }

        return $etat;
    }

    private function nomFile()
    {
        $fichier = 'fichiers/menu.json';
        if (is_dir('web')) {
            $testdir = 'web/fichiers';
            $fichier = 'web/' . $fichier;
        } else {
            $testdir = 'fichiers';
        }

        if (! is_dir($testdir)) {
            mkdir($testdir);
        }

        return $fichier;
    }

    private function lienActif($menu, $idgroup, $tab)
    {
        $actif    = 0;
        $refgroup = $menu['refgroup'];
        if (in_array($idgroup, $refgroup)) {
            $actif = 1;
        }

        if ($actif == 1) {
            if (count($menu['child']) != 0) {
                $new = $menu;
                unset($new['child']);
                $new['child'] = [];
                foreach ($menu['child'] as $child) {
                    $new['child'] = $this->lienActif($child, $idgroup, $new['child']);
                }

                $tab[] = $new;
            } else {
                $tab[] = $menu;
            }
        }

        return $tab;
    }

    private function setTab($row)
    {
        $tab     = [];
        $methods = get_class_methods($row);
        foreach ($methods as $method) {
            if (substr($method, 0, 3) == 'get') {
                $code = strtolower(substr($method, 3));
                if ($code == 'refgroup') {
                    $tab[$code] = explode(',', $row->$method());
                } elseif ($code != 'child') {
                    $tab[$code] = $row->$method();
                } else {
                    $tab['child'] = [];
                    foreach ($row->getChild() as $child) {
                        $tab['child'][] = $this->setTab($child);
                    }
                }
            }
        }

        return $tab;
    }
}
