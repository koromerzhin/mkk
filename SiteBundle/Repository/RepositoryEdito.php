<?php

namespace Mkk\SiteBundle\Repository;

use Mkk\SiteBundle\Lib\TranslatableRepository;

class RepositoryEdito extends TranslatableRepository
{
    /**
     * Backend
     * Liste des articles du blog.
     *
     * @param array $search Paramêtres de recherche
     * @param Page  $page   Page des articles du blog
     *
     * @return array
     */
    public function searchEdito($search)
    {
        $param     = [];
        $recherche = [];
        if (isset($search['recherche'])) {
            $recherche[]    = 'e.titre = :titre';
            $param['titre'] = $search['recherche'];
        }

        $em    = $this->getEntityManager();
        $dql   = 'SELECT e FROM ' . BUNDLESITE . ':Edito e';
        $dql   = $this->searchImplode($recherche, $dql);
        $dql   = $dql . ' ORDER BY e.datedebut DESC';
        $query = $em->createQuery($dql)->setParameters($param);

        return $query;
    }
}
