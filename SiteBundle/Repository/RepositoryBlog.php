<?php

namespace Mkk\SiteBundle\Repository;

use Mkk\SiteBundle\Lib\Repositorylib;

/**
 * BlogRepository.
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class RepositoryBlog extends Repositorylib
{
    /**
     * Backend
     * Liste des articles du blog.
     *
     * @param array $search Paramêtres de recherche
     * @param Page  $page   Page des articles du blog
     *
     * @return array
     */
    public function searchBlog($search)
    {
        $param     = [];
        $recherche = [];
        if (isset($search['recherche'])) {
            $recherche[]    = 'p.titre = :titre';
            $param['titre'] = $search['recherche'];
        }

        if (isset($search['refcategorie'])) {
            $recherche[]        = 'b.refcategorie =:categorie';
            $param['categorie'] = $search['refcategorie'];
        }

        $em    = $this->getEntityManager();
        $sql   = 'SELECT b FROM ' . BUNDLESITE . ':Blog b';
        $sql   = $this->searchImplode($recherche, $sql);
        $sql   = $sql . ' ORDER BY b.datepublication DESC';
        $query = $em->createQuery($sql)->setParameters($param);

        return $query;
    }

    /**
     * Public
     * Liste des articles du blog mis en avant.
     *
     * @param Page $page Page des articles du blog
     *
     * @return array
     */
    public function getArticles($page)
    {
        $em  = $this->getEntityManager();
        $sql = 'SELECT AA
                FROM ' . BUNDLESITE . ":Blog b
                WHERE b.avant = 1
                AND b.actifPublic = 1
                AND b.datepublication != ''
                AND b.datepublication <= :date
                ORDER BY b.datepublication DESC";

        $param['date']     = time();
        $query             = str_replace('AA', 'COUNT(b)', $sql);
        $total             = $em->createQuery($query)->setParameters($param);
        $tab['pagination'] = $this->pagination($total, $page);
        $query             = str_replace('AA', 'b', $sql);
        $tab['blog']       = $em->createQuery($query)
                                ->setMaxResults($tab['pagination']['limit'])
                                ->setParameters($param)
                                ->setFirstResult($tab['pagination']['offset'])
                                ->getResult();

        return $tab;
    }

    /**
     * Public
     * Liste des articles du blog d'une catégorie.
     *
     * @param Page   $page  Page des articles du blog
     * @param string $alias Alias de la catégorie
     *
     * @return array
     */
    public function getArticlesByCategorie($alias)
    {
        $em  = $this->getEntityManager();
        $sql = 'SELECT b FROM ' . BUNDLESITE . ":Blog b
                LEFT JOIN b.refcategorie c
                WHERE c.alias = :alias
                AND b.actifPublic = 1
                AND b.datepublication != ''
                AND b.datepublication <= :date
                ORDER BY b.datepublication DESC";

        $param = [
            'date'  => time(),
            'alias' => $alias,
        ];
        $query = $em->createQuery($sql)->setParameters($param);

        return $query;
    }

    /**
     * Public
     * Liste des articles du blog d'un tag.
     *
     * @param Page   $page  Page des articles du blog
     * @param string $alias Alias du tag
     *
     * @return array
     */
    public function getArticlesByTag($alias)
    {
        $em  = $this->getEntityManager();
        $sql = 'SELECT b FROM ' . BUNDLESITE . ":Blog b
                LEFT JOIN b.tags t
                WHERE t.alias = :alias
                AND b.actifPublic = 1
                AND b.datepublication != ''
                AND b.datepublication <= :date
                ORDER BY b.datepublication DESC";

        $param = [
            'date'  => time(),
            'alias' => $alias,
        ];
        $query = $em->createQuery($sql)->setParameters($param);

        return $query;
    }

    /**
     * Public
     * Liste des articles du blog correspondant à la recherche.
     *
     * @param Page   $page   Page des articles du blog
     * @param string $search Recherche de l'utilisateur
     * @param mixed  $locale
     *
     * @return array
     */
    public function getArticlesBySearch($search, $locale = '')
    {
        $em  = $this->getEntityManager();
        $sql = 'SELECT b FROM ' . BUNDLESITE . ":Blog b
                WHERE (b.titre LIKE :titre OR b.contenu LIKE :contenu)
                AND b.actifPublic = 1
                AND b.datepublication != ''
                AND b.datepublication <= :date
                AND b.langue = :langue
                ORDER BY b.datepublication DESC";

        $param = [
            'titre'   => '%' . $search . '%',
            'contenu' => '%' . $search . '%',
            'date'    => time(),
            'langue'  => $locale,
        ];
        $query = $em->createQuery($sql)->setParameters($param);

        return $query;
    }

    /**
     * Public
     * Articles similaires.
     *
     * @param Article $article Article actuel
     * @param string  $alias   Alias de la catégorie
     *
     * @return array
     */
    public function getSimilarArticles($article, $alias)
    {
        $em  = $this->getEntityManager();
        $sql = 'SELECT b
                FROM ' . BUNDLESITE . ":Blog b
                LEFT JOIN b.tags t
                LEFT JOIN b.refcategorie c
                WHERE b.datepublication != ''
                AND b.datepublication <= :date
                AND b.alias != :aliasarticle
                AND b.actifPublic = 1
                AND (c.alias = :aliascategorie OR t.alias IN (:tags))
                ORDER BY b.datepublication DESC";

        $tags = [];
        foreach ($article->getTags() as $tag) {
            $tags[] = $tag->getAlias();
        }

        $param = [
            'date'           => time(),
            'aliascategorie' => $alias,
            'aliasarticle'   => $article->getAlias(),
            'tags'           => implode(',', $tags),
        ];

        $blogs = $em->createQuery($sql)
                    ->setParameters($param)
                    ->setMaxResults(3)
                    ->getResult();

        return $blogs;
    }

    /**
     * Public
     * Articles similaires en fonction de la catégorie.
     *
     * @param Article $article Article actuel
     * @param string  $alias   Alias de la catégorie
     *
     * @return array
     */
    public function getSimilarArticlesByCategorie($article, $alias)
    {
        $em  = $this->getEntityManager();
        $sql = 'SELECT b
                FROM ' . BUNDLESITE . ":Blog b
                LEFT JOIN b.refcategorie c
                WHERE c.alias = :aliascategorie
                AND b.alias != :aliasarticle
                AND b.actifPublic = 1
                AND b.datepublication != ''
                AND b.datepublication <= :date
                ORDER BY b.datepublication DESC";

        $param = [
            'date'           => time(),
            'aliascategorie' => $alias,
            'aliasarticle'   => $article->getAlias(),
        ];

        $blogs = $em->createQuery($sql)
                    ->setParameters($param)
                    ->setMaxResults(3)
                    ->getResult();

        return $blogs;
    }

    /**
     * Public
     * Articles similaires en fonction d'un tag'.
     *
     * @param Article $article Article actuel
     * @param string  $alias   Alias du tag
     *
     * @return array
     */
    public function getSimilarArticlesByTag($article, $alias)
    {
        $em  = $this->getEntityManager();
        $sql = 'SELECT b
                FROM ' . BUNDLESITE . ":Blog b
                LEFT JOIN b.tags t
                WHERE t.alias = :alias
                AND b.alias != :aliasarticle
                AND b.actifPublic = 1
                AND b.datepublication != ''
                AND b.datepublication <= :date
                ORDER BY b.datepublication DESC";

        $param = [
            'date'         => time(),
            'alias'        => $alias,
            'aliasarticle' => $article->getAlias(),
        ];

        $blogs = $em->createQuery($sql)
                    ->setParameters($param)
                    ->setMaxResults(3)
                    ->getResult();

        return $blogs;
    }

    public function getArticleSimilaire($type, $alias, $article)
    {
        $date         = time();
        $em           = $this->getEntityManager();
        $aliasarticle = $article->getAlias();
        if ($type == 'tag') {
            $sql = 'SELECT b FROM ' . BUNDLESITE . ":Blog b LEFT JOIN b.tags t WHERE t.alias = '$alias' AND b.alias != '$aliasarticle' AND b.actifPublic = 1 AND b.datepublication != '' AND b.datepublication <= $date ORDER BY b.datepublication DESC";
        } else {
            $aliascategoriearticle = $article->getRefCategorie()->getAlias();
            $tagsarticle           = $article->getTags();
            $sql                   = 'SELECT b FROM ' . BUNDLESITE . ":Blog b LEFT JOIN b.tags t LEFT JOIN b.refcategorie c WHERE b.datepublication != '' AND b.datepublication <= $date AND (c.alias = '$aliascategoriearticle'";
            if (count($tagsarticle) > 0) {
                $sql = $sql . ' OR (';
                foreach ($tagsarticle as $key => $tag) {
                    if ($key > 0) {
                        $sql = $sql . ' OR ';
                    }

                    $sql = $sql . "t.alias = '" . $tag->getAlias() . "'";
                }

                $sql = $sql . ')';
            }

            $sql = $sql . ") AND b.alias != '$aliasarticle' AND b.actifPublic = 1 ORDER BY b.datepublication DESC";
        }

        $result = $em->createQuery($sql)->setMaxResults(3)->getResult();

        return $result;
    }

    public function getArticlesSearch($search)
    {
        $em    = $this->getEntityManager();
        $sql   = 'SELECT b FROM ' . BUNDLESITE . ':Blog b WHERE b.actifPublic = 1 AND (b.titre LIKE :titre OR b.contenu LIKE :contenu) ORDER BY b.datepublication DESC';
        $param = [
            'titre'   => '%' . $search . '%',
            'contenu' => '%' . $search . '%',
        ];
        $query = $em->createQuery($sql)->setParameters($param);

        return $query;
    }

    public function publicgetArchiveListingDate($search, $date)
    {
        $em                    = $this->getEntityManager();
        $blogEntity            = BUNDLESITE . ':Blog';
        $search['actifPublic'] = 1;
        $param                 = [];
        $recherche             = [];
        foreach ($search as $code => $val) {
            $recherche[]  = "entity.$code=:$code";
            $param[$code] = $val;
        }

        if ($date != '') {
            $recherche[]         = 'CONCAT(CONCAT(YEAR(FROM_UNIXTIME(entity.datepublication)),\'-\'),MONTH(FROM_UNIXTIME(entity.datepublication)))=:traitement';
            $param['traitement'] = $date;
        }

        $recherche[]   = "entity.datepublication!=''";
        $recherche[]   = 'entity.datepublication IS NOT NULL';
        $recherche[]   = 'entity.datepublication<=:date';
        $param['date'] = time();

        $em     = $this->getEntityManager();
        $sql    = 'SELECT entity,';
        $sql    = $sql . 'entity.datepublication,';
        $sql    = $sql . 'YEAR(FROM_UNIXTIME(entity.datepublication)) as annee,';
        $sql    = $sql . 'MONTH(FROM_UNIXTIME(entity.datepublication)) as mois,';
        $sql    = $sql . 'CONCAT(CONCAT(YEAR(FROM_UNIXTIME(entity.datepublication)),\'-\'),MONTH(FROM_UNIXTIME(entity.datepublication))) as traitement';
        $sql    = $sql . ',COUNT(entity.id) as total FROM ' . $blogEntity . ' entity';
        $sql    = $this->searchImplode($recherche, $sql);
        $sql    = $sql . ' GROUP BY traitement';
        $sql    = $sql . ' ORDER BY entity.datepublication DESC';
        $query  = $em->createQuery($sql);
        $param  = $query->setParameters($param);
        $result = $param->getResult();

        return $result;
    }

    public function publicgetArchiveListing($search)
    {
        $em                    = $this->getEntityManager();
        $blogEntity            = BUNDLESITE . ':Blog';
        $search['actifPublic'] = 1;
        $param                 = [];
        $recherche             = [];
        foreach ($search as $code => $val) {
            $recherche[]  = "entity.$code=:$code";
            $param[$code] = $val;
        }

        $recherche[]   = "entity.datepublication!=''";
        $recherche[]   = 'entity.datepublication IS NOT NULL';
        $recherche[]   = 'entity.datepublication<=:date';
        $param['date'] = time();

        $em     = $this->getEntityManager();
        $sql    = 'SELECT entity.datepublication,';
        $sql    = $sql . 'YEAR(FROM_UNIXTIME(entity.datepublication)) as annee,';
        $sql    = $sql . 'MONTH(FROM_UNIXTIME(entity.datepublication)) as mois,';
        $sql    = $sql . 'CONCAT(CONCAT(year(FROM_UNIXTIME(entity.datepublication)),\'-\'),MONTH(FROM_UNIXTIME(entity.datepublication))) as traitement';
        $sql    = $sql . ',COUNT(entity.id) as total FROM ' . $blogEntity . ' entity';
        $sql    = $this->searchImplode($recherche, $sql);
        $sql    = $sql . ' GROUP BY traitement';
        $sql    = $sql . ' ORDER BY traitement DESC';
        $query  = $em->createQuery($sql);
        $param  = $query->setParameters($param);
        $result = $param->getResult();

        return $result;
    }

    public function publicgetArticle($search)
    {
        $search['actifPublic'] = 1;
        $param                 = [];
        $recherche             = [];
        foreach ($search as $code => $val) {
            $recherche[]  = "entity.$code=:$code";
            $param[$code] = $val;
        }

        $recherche[]   = "entity.datepublication!=''";
        $recherche[]   = 'entity.datepublication IS NOT NULL';
        $recherche[]   = 'entity.datepublication<=:date';
        $param['date'] = time();
        $tab           = $this->getRowTable($recherche, $param, BUNDLESITE . ':Blog');

        return $tab;
    }

    public function publicgetTagsListing($search, $order, $tag)
    {
        $entity                = BUNDLESITE . ':Blog';
        $search['actifPublic'] = 1;
        $param                 = [];
        $recherche             = [];
        foreach ($search as $code => $val) {
            $recherche[]  = "entity.$code=:$code";
            $param[$code] = $val;
        }

        $recherche[]   = 'tag.alias=:tag';
        $param['tag']  = $tag;
        $recherche[]   = "entity.datepublication!=''";
        $recherche[]   = 'entity.datepublication IS NOT NULL';
        $recherche[]   = 'entity.datepublication<=:date';
        $param['date'] = time();
        $sql           = "SELECT entity FROM $entity entity LEFT JOIN entity.tags tag";
        $tab           = $this->getResultPagination($recherche, $param, $order, $sql);

        return $tab;
    }

    public function publicgetCategoriesListing($search, $order, $categorie)
    {
        $entity                = BUNDLESITE . ':Blog';
        $search['actifPublic'] = 1;
        $param                 = [];
        $recherche             = [];
        foreach ($search as $code => $val) {
            $recherche[]  = "entity.$code=:$code";
            $param[$code] = $val;
        }

        $recherche[]        = 'categorie.alias=:categorie';
        $param['categorie'] = $categorie;
        $recherche[]        = "entity.datepublication!=''";
        $recherche[]        = 'entity.datepublication IS NOT NULL';
        $recherche[]        = 'entity.datepublication<=:date';
        $param['date']      = time();
        $sql                = "SELECT entity FROM $entity entity LEFT JOIN entity.refcategorie categorie";
        $tab                = $this->getResultPagination($recherche, $param, $order, $sql);

        return $tab;
    }

    public function publicgetListing($search, $order)
    {
        $search['actifPublic'] = 1;
        $param                 = [];
        $recherche             = [];
        foreach ($search as $code => $val) {
            $recherche[]  = "entity.$code=:$code";
            $param[$code] = $val;
        }

        $recherche[]   = "entity.datepublication!=''";
        $recherche[]   = 'entity.datepublication IS NOT NULL';
        $recherche[]   = 'entity.datepublication<=:date';
        $param['date'] = time();
        $tab           = $this->getListingTable($recherche, $param, $order, BUNDLESITE . ':Blog');

        return $tab;
    }
}
