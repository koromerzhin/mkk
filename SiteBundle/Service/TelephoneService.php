<?php

namespace Mkk\SiteBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class TelephoneService
{
    protected $request;
    protected $controller;

    public function __construct(ContainerInterface $container, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->request   = $requestStack->getCurrentRequest();
        $this->router    = $container->get('router');
    }

    public function parse($numero)
    {
        $numero = str_replace([' ', '-', '.'], '', $numero);
        $json   = $this->verif($numero);
        if (! isset($json['num'])) {
            $json['num'] = $numero;
        }

        return $json['num'];
    }

    public function getIndexSite()
    {
        $listroutes = [];
        $router     = $this->router;
        foreach ($router->getRouteCollection()->all() as $name => $route) {
            $pattern = $route->getPath();
            foreach ($route->getDefaults() as $code => $val) {
                if ($code != '_controller') {
                    $pattern = str_replace('{' . $code . '}', $val, $pattern);
                }
            }

            if (substr($name, 0, 1) != '_' && substr_count($name, '_') != 0) {
                $listroutes[$name] = $pattern;
            }
        }
        $generateurl = 'bord_index';
        $trouver     = 0;
        foreach (array_keys($listroutes) as $url) {
            if ($url == 'public_index') {
                $trouver     = 1;
                $generateurl = $url;
                break;
            }
        }

        return $generateurl;
    }

    public function verif($numero)
    {
        $generateurl = 'public_index';
        $numero      = str_replace([' ', '-', '.'], '', $numero);
        try {
            $phoneUtil   = \libphonenumber\PhoneNumberUtil::getInstance();
            $trouver     = 0;
            $numberProto = $phoneUtil->parseAndKeepRawInput($numero, 'FR');
            if ($phoneUtil->isViablePhoneNumber($numero)) {
                $trouver = 1;
            } else {
                $listepays = $phoneUtil->getSupportedRegions();
                foreach ($listepays as $pays) {
                    $numberProto = $phoneUtil->parseAndKeepRawInput($numero, $pays);
                    if ($phoneUtil->isViablePhoneNumber($numberProto)) {
                        $trouver = 1;
                        break;
                    }
                }
            }

            if ($trouver) {
                $json['country']       = $phoneUtil->getRegionCodeForNumber($numberProto);
                $json['international'] = $phoneUtil->format($numberProto, 0);
                $json['num']           = $phoneUtil->format($numberProto, 2);
                $json['type']          = $phoneUtil->getNumberType($numberProto);
            }
        } catch (\NumberParseException $e) {
            $json['error'] = $e->__toString();
        }

        if ($json['type'] == 1) {
            $json['icontypetel'] = 'glyphicon-phone';
            $json['type']        = 'mobile';
        } elseif ($json['type'] == 0 || $json['type'] == 6) {
            $json['icontypetel'] = 'glyphicon-phone-alt';
            $json['type']        = 'fixe';
        }

        if (isset($json['country']) && $json['country'] != '') {
            $json['iconpays'] = $this->router->generate($generateurl, [], true);
            $json['iconpays'] = $json['iconpays'] . 'bundles/mkksite/img/country/' . $json['country'] . '.png';
        }

        return $json;
    }
}
