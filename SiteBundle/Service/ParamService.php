<?php

namespace Mkk\SiteBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class ParamService
{
    protected $request;
    protected $controller;

    public function __construct(ContainerInterface $container, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->request   = $requestStack->getCurrentRequest();
    }

    public function listing()
    {
        $paramManager    = $this->container->get('bdd.param_manager');
        $paramRepository = $paramManager->getRepository();
        if (isset($this->paramlisting)) {
            return $this->paramlisting;
        }

        $params = $paramRepository->findall();
        foreach ($params as $param) {
            $code = $param->getCode();
            if ($code == '') {
                $paramManager->remove($param);
            } else {
                $data[$code] = $param->getValeur();
            }
        }

        $paramManager->flush();

        foreach ($data as $key => $value) {
            if ($this->isJson($value)) {
                $data[$key] = json_decode($value, true);
            } elseif (substr_count($key, '-min_') == 0 && substr_count($key, '-max_') == 0) {
                $data[$key] = $value;
            }
        }

        $languesite   = [];
        $languesite[] = $data['langueprincipal'];
        foreach ($data['languesite'] as $langue) {
            if ($langue != $data['langueprincipal']) {
                $languesite[] = $langue;
            }
        }

        $data['languesite'] = $languesite;
        $this->paramlisting = $data;

        return $this->paramlisting;
    }

    /**
     * Creer le code de configuration pour hybridauth.
     *
     * @param array  $param Parametre du site
     * @param string $url   Url du site Internet
     * @param array  $data  Configuration des providers
     *
     * @return array
     */
    public function social($param, $url, $data)
    {
        $param       = $this->listing();
        $paramsocial = [];
        $tab         = explode(',', 'yahoo,google,facebook,twitter,live,linkedin,foursquare');
        foreach ($tab as $code) {
            if (isset($param[$code . '_key'])) {
                $paramsocial[$code . '_key'] = $param[$code . '_key'];
            } else {
                $paramsocial[$code . '_key'] = '';
            }

            if (isset($param[$code . '_secret'])) {
                $paramsocial[$code . '_secret'] = $param[$code . '_secret'];
            } else {
                $paramsocial[$code . '_secret'] = '';
            }
        }

        $config = [
            'base_url'  => $url,
            'providers' => [
                // openid providers
                'OpenID' => [
                    'enabled' => true,
                ],
                'Yahoo' => [
                    'enabled' => true,
                    'keys'    => [
                        'key'    => $paramsocial['yahoo_key'],
                        'secret' => $paramsocial['yahoo_secret'],
                    ],
                ],
                'AOL' => [
                    'enabled' => true,
                ],
                'Google' => [
                    'enabled' => true,
                    'keys'    => [
                        'id'     => $paramsocial['google_key'],
                        'secret' => $paramsocial['google_secret'],
                    ],
                ],
                'Facebook' => [
                    'enabled' => true,
                    'keys'    => [
                        'id'     => $paramsocial['facebook_key'],
                        'secret' => $paramsocial['facebook_secret'],
                    ],
                    'trustForwarded' => false,
                ],
                'Twitter' => [
                    'enabled' => true,
                    'keys'    => [
                        'key'    => $paramsocial['twitter_key'],
                        'secret' => $paramsocial['twitter_secret'],
                    ],
                    'trustForwarded' => false,
                ],
                'Live' => [
                    'enabled' => true,
                    'keys'    => [
                        'id'     => $paramsocial['live_key'],
                        'secret' => $paramsocial['live_secret'],
                    ],
                ],
                'LinkedIn' => [
                    'enabled' => true,
                    'keys'    => [
                        'key'    => $paramsocial['linkedin_key'],
                        'secret' => $paramsocial['linkedin_secret'],
                    ],
                ],
                'Foursquare' => [
                    'enabled' => true,
                    'keys'    => [
                        'id'     => $paramsocial['foursquare_key'],
                        'secret' => $paramsocial['foursquare_secret'],
                    ],
                ],
            ],
            // If you want to enable logging, set 'debug_mode' to true.
            // You can also set it to
            // - "error" To log only error messages. Useful in production
            // - "info" To log info and error messages (ignore debug messages)
            'debug_mode' => false,
            // Path to file writable by the web server. Required if 'debug_mode' is not false
            'debug_file' => '',
        ];
        foreach ($data as $code => $tab) {
            $config['providers'][$code] = array_merge($config['providers'][$code], $data[$code]);
        }

        return $config;
    }

    /**
     * Sauvegarde un nouveau paramétre.
     *
     * @param string $key    code
     * @param string $valeur chaine de caractères
     *
     * @return nothing
     */
    public function delete($key)
    {
        $container       = $this->container;
        $paramManager    = $container->get('bdd.param_manager');
        $paramRepository = $paramManager->getRepository();
        $paramEntity     = $paramManager->getTable();
        $entity          = $paramRepository->findOneByCode($key);
        if ($entity) {
            $paramManager->remove($entity);
            $paramManager->flush();
        }
    }

    /**
     * Sauvegarde un nouveau paramétre.
     *
     * @param string $key    code
     * @param string $valeur chaine de caractères
     *
     * @return nothing
     */
    public function save($key, $valeur)
    {
        $container       = $this->container;
        $paramManager    = $container->get('bdd.param_manager');
        $paramRepository = $paramManager->getRepository();
        $paramEntity     = $paramManager->getTable();
        $entity          = $paramRepository->findOneByCode($key);
        if (! $entity) {
            $entity = new $paramEntity();
            $entity->setCode($key);
        }

        if (is_array($valeur)) {
            $valeur = json_encode($valeur, JSON_FORCE_OBJECT);
        }

        $entity->setValeur($valeur);
        $paramManager->persistandFlush($entity);
    }

    private function isJson($string)
    {
        return ((is_string($string) &&
                (is_object(json_decode($string)) ||
                is_array(json_decode($string))))) ? true : false;
    }
}
