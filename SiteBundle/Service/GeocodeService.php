<?php

namespace Mkk\SiteBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class GeocodeService
{
    protected $request;
    protected $controller;

    public function __construct(ContainerInterface $container, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->request   = $requestStack->getCurrentRequest();
    }

    /**
     * Récupére les info sur la région.
     *
     * @param int    $id   id de la région
     * @param string $user code pour geonames
     *
     * @return array
     */
    public function getRegion($id)
    {
        $param = $this->container->get('mkk.param_service')->listing();
        $url   = 'http://api.geonames.org/childrenJSON?geonameId=' . $id;
        $md5   = md5($url);
        if (! is_file('tmp/geonames/' . $md5)) {
            if (isset($param['geonames']) && $param['geonames'] != '') {
                $url = $url . '&username=' . $param['geonames'];
            }

            $contents = file_get_contents($url);
            file_put_contents('tmp/geonames/' . $md5, $contents);
        }

        $contents = file_get_contents('tmp/geonames/' . $md5);
        $tab      = json_decode($contents, true);
        if (count($tab) == 0) {
            unlink('tmp/geonames/' . $md5);
        }

        return $tab['geonames'];
    }

    /**
     * Récupére les info sur la ville.
     *
     * @param int    $id   id geonames
     * @param string $user code pour geonames
     *
     * @return array
     */
    public function getInfoGeoname($id)
    {
        $param = $this->container->get('mkk.param_service')->listing();
        $url   = 'http://api.geonames.org/getJSON?geonameId=' . $id;
        $md5   = md5($url);
        if (! is_file('tmp/geonames/' . $md5)) {
            if (isset($param['geonames']) && $param['geonames'] != '') {
                $url = $url . '&username=' . $param['geonames'];
            }

            $contents = file_get_contents($url);
            file_put_contents('tmp/geonames/' . $md5, $contents);
        }

        $contents = file_get_contents('tmp/geonames/' . $md5);
        $tab      = json_decode($contents, true);
        if (count($tab) == 0) {
            unlink('tmp/geonames/' . $md5);
        }

        return $tab;
    }

    /**
     * Récupére les info sur la ville.
     *
     * @param array  $request tableau pour les requetes
     * @param string $user    code pour geonames
     *
     * @return array
     */
    public function getcpville($request)
    {
        $param    = $this->container->get('mkk.param_service')->listing();
        $response = [];
        $pays     = $request['pays'];
        $cp       = $request['cp'];
        $ville    = $request['ville'];
        $gps      = $request['gps'];
        $url      = '';
        if ($cp != '') {
            $url = 'postalCodeSearchJSON?country=' . $pays . '&postalcode=' . urlencode($cp);
        } elseif ($ville != '') {
            $url = 'searchJSON?&name_startsWith=' . urlencode($ville) . '&country=';
            $url = $url . $pays . '&style=FULL&featureClass=A&featureCode=ADM4';
        }

        if ($url != '') {
            $url = 'http://api.geonames.org/' . $url;
            $md5 = md5($url);
            if (! is_file('tmp/geonames/' . $md5)) {
                if (isset($param['geonames']) && $param['geonames'] != '') {
                    $url = $url . '&username=' . $param['geonames'];
                }

                $contents = file_get_contents($url);
                file_put_contents('tmp/geonames/' . $md5, $contents);
            }

            $contents = file_get_contents('tmp/geonames/' . $md5);
            $tab      = json_decode($contents, true);
            if (count($tab) == 0) {
                unlink('tmp/geonames/' . $md5);
                $response   = [];
                $response[] = [
                    'postalCode' => $cp,
                    'placeName'  => '',
                ];
            }

            if (isset($tab['postalCodes'])) {
                $response = $tab['postalCodes'];
                if ($ville != '') {
                    $tab      = $response;
                    $response = [];
                    foreach ($tab as $key => $data) {
                        if (mb_strtoupper($ville, 'UTF-8') == mb_strtoupper($data['placeName'], 'UTF-8')) {
                            if ($gps != '') {
                                list($data['lat'], $data['lng']) = explode(',', $gps);
                            }

                            $response[] = $data;
                        }
                    }
                }

                if (count($response) == 0) {
                    $response[] = [
                        'postalCode' => $cp,
                        'placeName'  => '',
                    ];
                }
            } elseif (isset($tab['geonames'])) {
                $response = $tab['geonames'];
            } else {
                $response = $tab;
                if (isset($tab['status']['message'])) {
                    if (is_file('tmp/geonames/' . $md5)) {
                        unlink('tmp/geonames/' . $md5);
                    }
                }
            }

            foreach ($response as $code => $val) {
                if (isset($val['postalCode'])) {
                    $response[$code]['id'] = $val['postalCode'];
                }
            }
        }

        return $response;
    }
}
