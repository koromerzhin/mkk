<?php

namespace Mkk\SiteBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class PostService
{
    protected $request;
    protected $controller;
    protected $entity;
    protected $etat;
    protected $forms;
    protected $id;
    protected $new;

    public function __construct(ContainerInterface $container, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->request   = $requestStack->getCurrentRequest();
        $this->router    = $container->get('router');
        $this->forms     = [];
        $this->response  = [];
        $this->id        = 0;
        $this->new       = false;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function isNew(): bool
    {
        return $this->new;
    }

    public function init($entity, $form, $etat)
    {
        $this->entity = $entity;
        if (is_object($form)) {
            $this->forms[] = $form;
        } else {
            $this->forms = $form;
        }

        $this->etat            = $etat;
        $this->response[$etat] = 0;
        $this->InitReference();
        if ($this->request->isMethod('POST')) {
            $this->ifContinuer($etat);
        }
    }

    public function getResponse()
    {
        return $this->response;
    }

    private function getRoutes()
    {
        if (! isset($this->listroutes)) {
            $this->listroutes = [];
            $router           = $this->router;
            foreach ($router->getRouteCollection()->all() as $name => $route) {
                $pattern = $route->getPath();
                foreach ($route->getDefaults() as $code => $val) {
                    if ('_controller' != $code) {
                        $pattern = str_replace('{' . $code . '}', $val, $pattern);
                    }
                }

                if ('_' != substr($name, 0, 1) && 0 != substr_count($name, '_')) {
                    $this->listroutes[$name] = $pattern;
                }
            }
        }

        return $this->listroutes;
    }

    private function initReference()
    {
        $entity    = $this->entity;
        $originals = [];
        $methods   = get_class_methods($entity);
        if (is_array($methods)) {
            foreach ($methods as $method) {
                if (0 != substr_count($method, 'get') && is_object($entity->$method())) {
                    $data      = $entity->$method();
                    $code      = str_replace('get', '', $method);
                    $fonctions = get_class_methods($data);
                    if (in_array('contains', $fonctions)) {
                        $originals[$code] = new ArrayCollection();
                        foreach ($entity->$method() as $data) {
                            $originals[$code]->add($data);
                        }
                    }
                }
            }
        }
        $this->reference = $originals;
    }

    private function delReference()
    {
        $entityManager = $this->container->get('bdd.menu_manager');
        $entity        = $this->entity;
        foreach ($this->reference as $id => $originals) {
            $fonction = 'get' . ucfirst($id);
            foreach ($originals as $row) {
                if (! $entity->$fonction()->contains($row)) {
                    $entityManager->remove($row);
                }
            }
        }
        $entityManager->flush();
    }

    private function setRef($name)
    {
        $entity       = $this->entity;
        $methods      = get_class_methods($entity);
        $class        = get_class($entity);
        $classname    = substr($class, strrpos($class, '\\') + 1);
        $setReference = 'setRef' . $classname;
        foreach ($methods as $method) {
            if ('getTab' == substr($method, 0, 6)) {
                $this->setTabEntity($name, $entity, $method);
            } elseif ('getRef' == substr($method, 0, 6)) {
                $this->setEntity($name, $entity, $method);
            }
        }
    }

    private function setTabEntity($name, $entity, $method)
    {
        $methodGet       = str_replace('getTab', 'get', $method);
        $referenceAdd    = get_class($entity);
        $referenceAdd    = strtolower(substr($referenceAdd, strrpos($referenceAdd, '\\') + 1));
        $referenceAdd    = 'add' . ucfirst($referenceAdd);
        $referenceRemove = get_class($entity);
        $referenceRemove = strtolower(substr($referenceRemove, strrpos($referenceRemove, '\\') + 1));
        $referenceRemove = 'remove' . ucfirst($referenceRemove);
        $methodRemove    = substr(str_replace('getTab', 'remove', $method), 0, -1);
        $methodAdd       = substr(str_replace('getTab', 'add', $method), 0, -1);
        $champs          = strtolower(str_replace('get', '', $method));
        $request         = $this->request;
        $post            = $request->request->get($name);
        $newEntity       = strtolower(str_replace('getTab', '', $method));
        if ('s' == substr($newEntity, -1)) {
            $newEntity = substr($newEntity, 0, -1);
        }

        if (isset($post[$champs]) && $this->container->has('bdd.' . $newEntity . '_manager')) {
            $manager    = $this->container->get('bdd.' . $newEntity . '_manager');
            $repository = $manager->getRepository();
            $val        = $post[$champs];
            $list       = explode(',', $val);
            if (0 == count($val)) {
                $data = $entity->$methodGet();
                foreach ($data as $row) {
                    $entity->$methodRemove($row);
                }
            } else {
                foreach ($list as $id) {
                    $data = $repository->find($id);
                    if ($data && ! $entity->$methodGet()->contains($data)) {
                        $entity->$methodAdd($data);
                        $data->$referenceAdd($entity);
                        $manager->persist($data);
                        $manager->persist($entity);
                    }
                }
            }
            $data = $entity->$methodGet();
            foreach ($data as $row) {
                $id = $row->getId();
                if (! in_array($id, $list)) {
                    $entity->$methodRemove($row);
                    $row->$referenceRemove($entity);
                    $manager->persist($row);
                    $manager->persist($entity);
                }
            }

            $manager->flush();
        }
    }

    private function setEntity($name, $entity, $method)
    {
        $methodSet = str_replace('getRef', 'setRef', $method);
        $champs    = strtolower(str_replace('getRef', '', $method));
        $request   = $this->request;
        $post      = $request->request->get($name);
        if (isset($post[$champs]) && $this->container->has('bdd.' . $champs . '_manager')) {
            $id         = $post[$champs];
            $manager    = $this->container->get('bdd.' . $champs . '_manager');
            $repository = $manager->getRepository();
            $data       = $repository->find($id);
            if ($data) {
                $entity->$methodSet($data);
            } else {
                $entity->$methodSet(null);
            }
            $manager->persistAndFlush($entity);
        }
    }

    private function setPersist($form)
    {
        $params        = $this->container->get('mkk.param_service')->listing();
        $entityManager = $this->container->get('bdd.menu_manager');
        $entity        = $this->entity;
        $name          = $form->getName();
        if (0 != substr_count($name, 'langue')) {
            $locale = str_replace('langue', '', $name);
            if ($locale != $params['langueprincipal']) {
                $entity->setTranslatableLocale($locale);
                //$entityManager->refresh($entity);
            }
        }
        $entityManager->persistAndFlush($entity);
    }

    private function ifContinuer($etat)
    {
        $methods = get_class_methods($this->entity);
        foreach ($this->forms as $form) {
            $form->handleRequest($this->request);
            if ($form->isSubmitted() && $form->isValid()) {
                if (in_array('setUpdatedAt', $methods)) {
                    $this->entity->setUpdatedAt(new \DateTime());
                }

                $this->setPersist($form);
                $this->delReference();
                $this->setRef($form->getName());
                if ('ajouter' == $etat) {
                    $this->new = true;
                }
                $this->id = $this->entity->getId();
            }
        }
    }

    private function getErrorMessages($form)
    {
        $html = $this->container->get('templating')->render(
            'MkkSiteBundle::form.html.twig',
            [
                'name' => $form->getName(),
                'form' => $form->createView(),
            ]
        );

        return $html;
    }

    private function verifRecaptcha($request)
    {
        $param     = $this->container->get('mkk.param_service')->listing();
        $recaptcha = 0;
        $post      = $request->request->all();
        if (isset($post['g-recaptcha-response'])) {
            $recaptcha = 1;
        }

        if (isset($param['recaptcha_clef']) && '' != $param['recaptcha_clef']) {
            $recaptcha = $recaptcha + 1;
        }

        if (isset($param['recaptcha_secret']) && '' != $param['recaptcha_secret']) {
            $recaptcha = $recaptcha + 1;
        }

        if (3 == $recaptcha) {
            $url     = 'https://www.google.com/recaptcha/api/siteverify?secret=';
            $url     = $url . $post['g-recaptcha-response'];
            $url     = $url . '&response=' . $request->request->get('g-recaptcha-response');
            $url     = $url . '&remoteip=' . $request->server->get('REMOTE_ADDR');
            $content = file_get_contents($url);
            $json    = json_decode($content, true);
            if (false == $json['success']) {
                return false;
            }
        }

        return true;
    }
}
