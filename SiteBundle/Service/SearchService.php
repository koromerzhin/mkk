<?php

namespace Mkk\SiteBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class SearchService
{
    protected $request;
    protected $controller;

    public function __construct(ContainerInterface $container, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->request   = $requestStack->getCurrentRequest();
    }

    public function getResponse($managerCode, $fonction)
    {
        $request               = $this->request;
        $container             = $this->container;
        $manager               = $container->get($managerCode);
        $data                  = [];
        $repository            = $manager->getRepository();
        $id                    = $request->request->get('id');
        $required              = $request->query->get('required');
        $placeholder           = $request->query->get('placeholder');
        $data['lettre']        = $request->query->get('lettre');
        $token                 = $container->get('security.token_storage')->getToken();
        $data['user']          = $token->getUser();
        $data['params_config'] = $container->get('mkk.param_service')->listing();
        $responsejson          = [];
        if ($id != '') {
            if (substr_count($id, ',') == 0) {
                $entity = $repository->find($id);
                if ($entity) {
                    $methods = get_class_methods($entity);
                    $manager->refresh($entity);
                    if (! in_array('getSearchData', $methods)) {
                        $responsejson['error'] = 'Fonction getSearchData nom présent dans ' . get_class($entity);
                    } else {
                        $responsejson = $entity->getSearchData();
                    }
                }
            } else {
                $tab = explode(',', $id);
                foreach ($tab as $id) {
                    $entity = $repository->find($id);
                    if ($entity) {
                        $methods = get_class_methods($entity);
                        $manager->refresh($entity);
                        if (! in_array('getSearchData', $methods)) {
                            $responsejson['error'] = 'Fonction getSearchData nom présent dans ' . get_class($entity);
                        } else {
                            $responsejson[] = $entity->getSearchData();
                        }
                    }
                }
            }
        } else {
            $methods                 = get_class_methods($repository);
            $responsejson['results'] = [];
            $responsejson['total']   = 0;
            if (in_array($fonction, $methods)) {
                $listingService = $container->get('mkk.listing_service');
                $listingService->setRequest($managerCode, $fonction, $data);
                $paginator = $container->get('paginator');
                if ($paginator->getPage() == 1 && $required == 0) {
                    $responsejson['results'][] = [
                        'id'  => '',
                        'nom' => $placeholder,
                    ];
                }
                foreach ($paginator as $entity) {
                    $methods = get_class_methods($entity);
                    if (! in_array('getSearchData', $methods)) {
                        $responsejson['error'] = 'Fonction getSearchData nom présent dans ' . get_class($entity);
                        break;
                    }

                    $responsejson['results'][] = $entity->getSearchData();
                }

                $responsejson['total'] = $paginator->getTotalItemCount();
            } else {
                $responsejson['error'] = 'Fonction ' . $fonction . ' nom présent dans ' . get_class($repository);
            }
        }

        return $responsejson;
    }
}
