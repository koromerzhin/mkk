<?php

namespace Mkk\SiteBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class ListingService
{
    protected $request;
    protected $controller;

    public function __construct(ContainerInterface $container, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->request   = $requestStack->getCurrentRequest();
        $this->router    = $container->get('router');
    }

    public function setRequest($managerCode, $fonction, $data = [])
    {
        $request               = $this->request;
        $container             = $this->container;
        $manager               = $container->get($managerCode);
        $data                  = array_merge($data, $request->query->all());
        $token                 = $container->get('security.token_storage')->getToken();
        $data['user']          = $token->getUser();
        $data['params_config'] = $container->get('mkk.param_service')->listing();
        $repository            = $manager->getRepository();
        $query                 = $repository->$fonction($data);
        $translations          = $manager->getTranslations();
        $this->container->set('translations', $translations);
        $this->setPaginator($query);

        return $query;
    }

    public function setSearch($service, $options = [])
    {
        $request     = $this->request;
        $param       = $request->query->all();
        $container   = $this->container;
        $formFactory = $container->get('form.factory');
        $servicesID  = $container->getServiceIds();
        if (! is_string($service)) {
            return false;
        }

        $data = [];
        foreach ($servicesID as $code) {
            if (substr_count($code, $service) != 0) {
                $data[] = $code;
            }
        }

        if (count($data) == 0) {
            $flashbag = $request->getSession()->getFlashBag();
            $flashbag->add('warning', 'Aucun formulaire de recherche existe avec le service ' . $service);

            return false;
        }

        if (count($data) == 1) {
            $serviceId = $data[0];
        } else {
            $flashbag = $request->getSession()->getFlashBag();
            $flashbag->add('warning', 'Fonction de recherche de formulaire de recherche non terminé');

            return false;
        }

        $formService = $container->get($serviceId);
        $form        = $formFactory->create(
            $formService,
            $param,
            $options
        );
        $urlfichier  = 'MkkAdminBundle:Default:search.html.twig';
        $searchForm  = [
                'form' => (is_object($form)) ? $form->createView() : '',
                'url'  => $urlfichier,
                'init' => 1,
            ];
        $this->container->set('searchform', $searchForm);
    }

    private function setPaginator($query)
    {
        $request   = $this->request;
        $container = $this->container;
        $param     = $container->get('mkk.param_service')->listing();
        $route     = $request->get('_route');
        $limit     = 50;
        if (substr($route, 0, 2) == 'be' && isset($param['longueurliste']) && $param['longueurliste'] != 0) {
            $limit = $param['longueurliste'];
        }

        if (substr($route, 0, 2) != 'be' && isset($param['publicliste']) && $param['publicliste'] != 0) {
            $limit = $param['publicliste'];
        }

        list($module) = explode('_', $route);
        if (isset($param['module_listing'])) {
            foreach ($param['module_listing'] as $tab) {
                if ($tab['module'] == $module) {
                    $limit = $tab['val'];
                }
            }
        }

        if ($request->query->get('limit') != '') {
            $limit = $request->query->get('limit');
        }

        $page = $request->get('page');
        if ($page <= 0) {
            $page = 1;
        }

        $paginator  = $container->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $page/*page number*/,
            $limit/*limit per page*/
        );

        $route = $pagination->getRoute();
        $route = str_replace('index', 'page', $route);
        $pagination->setUsedRoute($route);
        // total des pages
        // $pagination->getPageCount();
        // page actuel
        // $pagination->getPage();
        // route actuel
        // $pagination->getRoute();
        // param route
        // $pagination->getParams();
        $this->container->set('paginatordata', $pagination);
        $this->container->set('paginator', $pagination);
    }
}
