<?php

namespace Mkk\SiteBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class StatsService
{
    protected $request;
    protected $controller;

    public function __construct(ContainerInterface $container, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->request   = $requestStack->getCurrentRequest();
    }

    /**
     * Set les stats.
     *
     * @param string $type    code
     * @param string $valeur  code
     * @param mixed  $request
     */
    public function set($type, $valeur)
    {
        $request     = $this->request;
        $filename    = $request->server->get('SCRIPT_FILENAME');
        $url         = substr($filename, strpos($filename, 'web/') + strlen('web/'));
        $site        = substr($url, 0, strpos($url, '/'));
        $fichierbase = 'tmp/sqlite/bdd.sqlite';
        $fichiermois = 'tmp/sqlite/' . $site . '-' . date('Y-m') . '.sqlite';
        if (is_file($fichierbase)) {
            if (! is_file($fichiermois)) {
                copy($fichierbase, $fichiermois);
            }

            $cnx     = new \PDO('sqlite:' . $fichiermois);
            $time    = time();
            $browser = $request->server->get('HTTP_USER_AGENT');
            $ip      = $request->server->get('REMOTE_ADDR');
            $sql     = sprintf(
                "INSERT INTO stats (type,valeur,date,browser,ip) VALUES ('%s','%s',%d,'%s','%s');",
                $type,
                $valeur,
                $time,
                $browser,
                $ip
            );
            $cnx->query($sql);
        }
    }

    /**
     * Récupére les stats sur un fichier SQLITE.
     *
     * @param string $selection code a récupérer
     * @param array  $data      contenu récupérer
     * @param mixed  $request
     *
     * @return array
     */
    public function get($selection, $data = [])
    {
        $request        = $this->request;
        $userManager    = $this->get('bdd.user_manager');
        $userRepository = $userManager->getRepository();
        $tab            = [
            'suivant' => '',
            'data'    => [],
        ];
        $liste          = '';
        if (count($data) != 0) {
            foreach (array_keys($data) as $val) {
                if ($liste != '') {
                    $liste = $liste . ',';
                }

                $liste = $liste . sprintf("'%s'", $val);
            }
        }

        $bdd      = $request->request->get('bdd');
        $postdata = $request->request->get('data');
        $type     = $request->request->get('type');
        if ($postdata != '') {
            if ($type == 'connexion') {
                $datauser = explode(',', $postdata);
                foreach ($datauser as $id) {
                    $user = $userRepository->find($id);
                    if ($user) {
                        $nom = trim($user->getNom() . ' ' . $user->getPrenom());
                        if ($nom == '') {
                            $nom = $user->getUsername();
                        }

                        if ($nom == '') {
                            $nom = $user->getAppel();
                        }

                        $tab['data'][$id] = $nom;
                    }
                }
            }
        } else {
            $tab['moisactuel']  = date('n');
            $tab['anneeactuel'] = date('Y');
            $filename           = $request->server->get('SCRIPT_FILENAME');
            $url                = str_replace(['app.php', 'app_dev.php', '/web/'], '', $filename);
            $site               = substr($url, strrpos($url, '/') + 1);
            $fichiers           = glob('tmp/sqlite/' . $site . '*.sqlite');
            foreach ($fichiers as $i => $databasefile) {
                if ($i == $bdd || $bdd == '') {
                    $cnx = new \PDO('sqlite:' . $databasefile);
                    if (isset($fichiers[$i + 1])) {
                        $tab['suivant'] = $i + 1;
                    }

                    if ($selection != 'connexion') {
                        $sql = sprintf("SELECT valeur,date FROM stats WHERE type='%s'", $selection);
                        if ($liste != '') {
                            $sql = $sql . ' AND valeur IN (' . $liste . ')';
                        }

                        foreach ($cnx->query($sql) as $row) {
                            $jour   = date('d', $row['date']);
                            $mois   = date('n', $row['date']);
                            $annee  = date('Y', $row['date']);
                            $valeur = $row['valeur'];
                            if (count($data) != 0) {
                                $valeur = $data[$valeur];
                            }

                            if (! isset($tab['data'][$valeur][$annee][$mois][$jour])) {
                                $tab['data'][$valeur][$annee][$mois][$jour] = 0;
                            }

                            ++$tab['data'][$valeur][$annee][$mois][$jour];
                        }
                    } else {
                        $sql = 'SELECT user,time FROM login';
                        foreach ($cnx->query($sql) as $row) {
                            $jour  = date('d', $row['time']);
                            $mois  = date('n', $row['time']);
                            $annee = date('Y', $row['time']);
                            $user  = $row['user'];
                            if (! isset($tab['data'][$user][$annee][$mois][$jour])) {
                                $tab['data'][$user][$annee][$mois][$jour] = 0;
                            }

                            ++$tab['data'][$user][$annee][$mois][$jour];
                        }
                    }

                    break;
                }
            }
        }

        return $tab;
    }
}
