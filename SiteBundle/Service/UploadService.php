<?php

namespace Mkk\SiteBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class UploadService
{
    protected $request;
    protected $controller;
    protected $md5;

    public function __construct(ContainerInterface $container, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->md5       = [];
        $this->request   = $requestStack->getCurrentRequest();
        $this->router    = $container->get('router');
    }

    /**
     * Lancement du système d'upload de fichier
     * Utilise la librairie Externe UploadHandler.
     *
     * @param array $options options pour les fichiers
     * @param mixed $request
     *
     * @return response
     */
    public function ajax($options)
    {
        $request     = $this->request;
        $routeparams = $request->attributes->get('_route_params');
        if (isset($routeparams['md5'])) {
            $md5 = $routeparams['md5'];
        } else {
            $get = $request->query->all();
            if (isset($get['md5'])) {
                $md5 = $get['md5'];
            }
        }

        $options['md5'] = $md5;
        $route          = $request->attributes->get('_route');
        $params         = $this->container->get('mkk.param_service')->listing();
        $tab            = [
            'min_height' => 0,
            'max_height' => 0,
            'min_width'  => 0,
            'max_width'  => 0,
        ];

        $total = 0;
        if (isset($params['upload'])) {
            foreach ($params['upload'] as $tabupload) {
                if ($tabupload['url'] == $route) {
                    foreach ($tabupload as $id => $val) {
                        if (isset($tab[$id])) {
                            $tab[$id] = $val;
                        }
                    }
                    break;
                }
            }
        }

        if (0 != count($tab)) {
            $options = array_merge($options, $tab);
        }
        $tab = [
            'image_versions' => [
                'thumbnail' => [
                    'max_width'  => 80,
                    'max_height' => 80,
                ],
                '' => [
                    'max_height' => 0,
                    'max_width'  => 0,
                ],
            ],
        ];
        if (isset($tabupload)) {
            foreach ($tabupload as $id => $val) {
                if (isset($tab['image_versions'][''][$id])) {
                    $tab['image_versions'][''][$id] = $val;
                }
            }
        }

        if (0 != count($tab['image_versions'][''])) {
            $options = array_merge($options, $tab);
        }

        $urlsite = $request->getSchemeAndHttpHost() . $this->router->getContext()->getBaseUrl();
        if (isset($options['md5'])) {
            $route                    = $request->get('_route');
            $routeParams              = $request->attributes->get('_route_params');
            $options['script_url']    = $this->router->generate($route, $routeParams);
            $md5                      = $options['md5'];
            $filename                 = $request->server->get('SCRIPT_FILENAME');
            $emplacement              = dirname($filename);
            $options['upload_dir']    = $emplacement . '/tmp/' . $md5 . '/';
            $options['thumbnail_dir'] = $emplacement . '/tmp/' . $md5 . '/thumbnail/';
            $options['upload_url']    = $urlsite . '/tmp/' . $md5 . '/';
        }

        if (isset($options['upload_video'])) {
            unset($options['upload_video']);
            $options['accept_file_types'] = '/\.(MP4|mp4|MOV|mov|AVI|avi)$/i';
        }

        if (! isset($options['accept_file_types'])) {
            $options['accept_file_types'] = '/\.(JPG|jpg|JPEG|jpeg|PNG|png|PDF|pdf|ico|ICO)$/i';
        }

        $folder = '../';
        if (is_dir('web')) {
            $folder = '';
        }

        $classfile = $folder . 'vendor/blueimp/jquery-file-upload/server/php/UploadHandler.php';
        require_once $classfile;
        if (! is_dir($options['upload_dir'])) {
            mkdir($options['upload_dir'], 0775, true);
        }

        if (isset($options['thumbnail_dir'])) {
            if (! is_dir($options['thumbnail_dir'])) {
                mkdir($options['thumbnail_dir'], 0775, true);
            }
        }

        error_reporting(E_ALL | E_STRICT);
        ob_start();
        $errorMessages = [
            1                     => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
            2                     => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
            3                     => 'The uploaded file was only partially uploaded',
            4                     => 'No file was uploaded',
            6                     => 'Missing a temporary folder',
            7                     => 'Failed to write file to disk',
            8                     => 'A PHP extension stopped the file upload',
            'post_max_size'       => 'The uploaded file exceeds the post_max_size directive in php.ini',
            'max_file_size'       => 'File is too big',
            'min_file_size'       => 'File is too small',
            'accept_file_types'   => 'Filetype not allowed',
            'max_number_of_files' => 'Maximum number of files exceeded',
            'max_width'           => 'Image exceeds maximum width',
            'min_width'           => 'Image requires a minimum width',
            'max_height'          => 'Image exceeds maximum height',
            'min_height'          => 'Image requires a minimum height',
        ];
        if ('' != $folder) {
            $uploadHandler = new \UploadHandler($options, true, $errorMessages);
        }

        $contenu = ob_get_contents();
        ob_end_clean();
        unset($uploadHandler);

        return $contenu;
    }

    public function getMd5()
    {
        return $this->md5;
    }

    public function init($md5, $filename)
    {
        if (is_array($filename)) {
            $data = $filename;
        } else {
            $data = json_decode($filename, true);
            if (! is_array($data)) {
                $data = [$filename];
            }
        }
        $this->initFolderUpload($md5, $data);
        $this->md5 = 1;
    }

    public function move($recuperer, $entity, $accessor, $annotation)
    {
        $request     = $this->request;
        $filename    = $request->server->get('filename');
        $emplacement = substr(dirname($filename) . '/fichiers/', 1);
        $finfo       = finfo_open(FILEINFO_MIME_TYPE);
        if ($annotation->isUnique()) {
            $data = '';
        } else {
            $data = [];
        }

        foreach ($recuperer as $i => $file) {
            $info                   = finfo_file($finfo, $file);
            list($type, $extension) = explode('/', $info);
            $extension              = strtolower($extension);
            $alias                  = $accessor->getValue($entity, $annotation->getAlias());
            $newemplacement         = $emplacement;
            $entityName             = get_class($entity);
            $entityName             = substr($entityName, strrpos($entityName, '\\') + 1);
            $entityName             = strtolower($entityName);
            $newemplacement         = $newemplacement . $entityName;
            $dirname                = $newemplacement . '/' . $annotation->getFilename();
            if (! is_dir($dirname)) {
                mkdir($dirname, 0775, true);
            }

            if ($annotation->isUnique()) {
                $fichier = $dirname . '/' . $alias . '.' . $extension;
                $data    = $fichier;
                copy($file, $fichier);
            } else {
                $fichier = $dirname . '/' . $alias . $i . '.' . $extension;
                $data[]  = $fichier;
                copy($file, $fichier);
            }
        }

        return $data;
    }

    /**
     * Récupre la liste des fichiers présent dans le dossier $md5.
     */
    public function get(string $md5)
    {
        $filename    = $this->request->server->get('SCRIPT_FILENAME');
        $emplacement = dirname($filename);
        $data        = [];
        if (is_dir($emplacement . '/tmp/' . $md5)) {
            $fichiers = glob($emplacement . '/tmp/' . $md5 . '/*');
            foreach ($fichiers as $file) {
                if (is_file($file)) {
                    $data[] = $file;
                }
            }
        }

        return $data;
    }

    /**
     * (WIP).
     *
     * @param
     * @param mixed $file
     *
     * @author
     * @copyright
     */
    public function remove($file)
    {
    }

    /**
     * Initialisation de l'upload quand c'est pas lié a un formulaire.
     *
     * @param string $md5      code unique
     * @param array  $fichiers tableau de fichiers
     * @param mixed  $request
     */
    private function initFolderUpload($md5, $fichiers)
    {
        $filename    = $this->request->server->get('SCRIPT_FILENAME');
        $emplacement = dirname($filename);
        $finfo       = finfo_open(FILEINFO_MIME_TYPE);
        if (! is_dir($emplacement . '/tmp/' . $md5)) {
            mkdir($emplacement . '/tmp/' . $md5, 0775, true);
        } else {
            if (is_dir($emplacement . '/tmp/' . $md5 . '/thumbnail')) {
                $oldfichiers = glob($emplacement . '/tmp/' . $md5 . '/thumbnail/*.*');
                foreach ($oldfichiers as $fichier) {
                    unlink($fichier);
                }
            }

            $oldfichiers = glob($emplacement . '/tmp/' . $md5 . '/*.*');
            foreach ($oldfichiers as $fichier) {
                unlink($fichier);
            }
        }

        foreach ($fichiers as $fichier) {
            if (is_file($fichier)) {
                $tab         = explode('/', $fichier);
                $depart      = $fichier;
                $destination = $emplacement . '/tmp/' . $md5 . '/' . $tab[count($tab) - 1];
                copy($depart, $destination);
                $depart = $fichier;
                if (0 != substr_count(finfo_file($finfo, $depart), 'image')) {
                    $destination = $emplacement . '/tmp/' . $md5 . '/thumbnail/' . $tab[count($tab) - 1];
                    if (! is_dir($emplacement . '/tmp/' . $md5 . '/thumbnail')) {
                        mkdir($emplacement . '/tmp/' . $md5 . '/thumbnail', 0775, true);
                    }

                    $this->image_resize($depart, $destination, 80, 80);
                }
            }
        }
    }

    private function image_resize($src, $dst, $width, $height, $crop = 0)
    {
        if (! list($w, $h) = getimagesize($src)) {
            return 'Unsupported picture type!';
        }
        $type = strtolower(substr(strrchr($src, '.'), 1));
        if ('jpeg' == $type) {
            $type = 'jpg';
        }
        switch ($type) {
        case 'bmp':
            $img = imagecreatefromwbmp($src);
            break;
        case 'gif':
            $img = imagecreatefromgif($src);
            break;
        case 'jpg':
            $img = imagecreatefromjpeg($src);
            break;
        case 'png':
            $img = imagecreatefrompng($src);
            break;
        default:
            return 'Unsupported picture type!';
        }

        // resize
        if ($crop) {
            if ($w < $width or $h < $height) {
                return 'Picture is too small!';
            }
            $ratio = max($width / $w, $height / $h);
            $h     = $height / $ratio;
            $x     = ($w - $width / $ratio) / 2;
            $w     = $width / $ratio;
        } else {
            if ($w < $width and $h < $height) {
                return 'Picture is too small!';
            }
            $ratio  = min($width / $w, $height / $h);
            $width  = $w * $ratio;
            $height = $h * $ratio;
            $x      = 0;
        }

        $new = imagecreatetruecolor($width, $height);

        // preserve transparency
        if ('gif' == $type or 'png' == $type) {
            imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
            imagealphablending($new, false);
            imagesavealpha($new, true);
        }

        imagecopyresampled($new, $img, 0, 0, $x, 0, $width, $height, $w, $h);

        switch ($type) {
        case 'bmp':
            imagewbmp($new, $dst);
 break;
        case 'gif':
            imagegif($new, $dst);
 break;
        case 'jpg':
            imagejpeg($new, $dst);
 break;
        case 'png':
            imagepng($new, $dst);
 break;
        }

        return true;
    }
}
