<?php

namespace Mkk\SiteBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class ApiService
{
    protected $request;
    protected $controller;

    public function __construct(ContainerInterface $container, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->request   = $requestStack->getCurrentRequest();
    }

    /**
     * Récupérer les images des vidéo vimeo / youtube.
     *
     * @param string $contenu message
     *
     * @return array
     */
    public function getAllImageContenu($contenu)
    {
        preg_match_all('/(src=["\'](.*?)["\'])/', $contenu, $matches);
        $data   = $matches[count($matches) - 1];
        $images = [];
        foreach ($data as $key => $url) {
            if (substr_count($url, 'vimeo') == 1) {
                $code   = substr($url, strpos($url, 'vimeo.com/video/') + strlen('vimeo.com/video/'));
                $lien   = 'http://vimeo.com/' . $code;
                $oembed = $this->oembed($lien);
                if (isset($oembed['thumbnail_url'])) {
                    $images[$key] = $oembed['thumbnail_url'];
                }
            } elseif (substr_count($url, 'youtube') == 1) {
                $oembed = $this->oembed($url);
                if (isset($oembed['thumbnail_url'])) {
                    $images[$key] = $oembed['thumbnail_url'];
                }
            } else {
                $images[$key] = $url;
            }
        }

        return $images;
    }

    /**
     * Transforme un lien en code html oembed.
     *
     * @param string $lien URL
     *
     * @return array
     */
    public function oembed($lien)
    {
        $tab = [];
        if (substr_count($lien, 'vimeo') == 1) {
            $url = 'http://vimeo.com/api/oembed.json?url=';
        } elseif (substr_count($lien, 'youtube') == 1) {
            $url = 'http://www.youtube.com/oembed?format=json&url=';
        } elseif (substr_count($lien, 'ifttt') == 1) {
            $url = 'https://ifttt.com/oembed.json?url=';
        } elseif (substr_count($lien, 'flickr') == 1) {
            $url = 'https://www.flickr.com/services/oembed.json?url=';
        }

        if (isset($url)) {
            $url = $url . urlencode($lien);
            $tab = json_decode(file_get_contents($url), true);
        }

        return $tab;
    }
}
