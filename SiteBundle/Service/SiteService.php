<?php

namespace Mkk\SiteBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class SiteService
{
    protected $request;
    protected $controller;

    public function __construct(ContainerInterface $container, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->request   = $requestStack->getCurrentRequest();
        $this->router    = $container->get('router');
        $this->redirect  = false;
    }

    public function isRedirect()
    {
        return $this->redirect;
    }

    public function getFormSystemParam($type, $param, $options = [])
    {
        $request = $this->request;
        $methods = get_class_methods($request);
        if (is_array($methods) && in_array('getSession', $methods)) {
            $flashbag = $request->getSession()->getFlashBag();
        }
        $container   = $this->container;
        $formFactory = $container->get('form.factory');
        $servicesID  = $container->getServiceIds();
        $data        = [];
        $service     = '';
        if ($type == 'system' || $type == 'param') {
            $service = 'form' . $type;
        }

        if ($service == '') {
            if (isset($flashbag)) {
                $flashbag->add('warning', 'Aucun formulaire existe avec le service ' . $type);
            }
            $this->redirect = true;

            return false;
        }

        foreach ($servicesID as $code) {
            if (substr_count($code, $service) != 0) {
                $data[] = $code;
            }
        }

        if (count($data) == 0) {
            if (isset($flashbag)) {
                $flashbag->add('warning', 'Aucun formulaire existe avec le service ' . $type);
            }
            $this->redirect = true;

            return false;
        }

        $forms = [];

        foreach ($data as $code) {
            $id          = str_replace('admin.' . $service . '.', '', $code);
            $formService = $container->get($code);
            $forms[$id]  = $formFactory->create(
                $formService,
                $param,
                $options
            );
        }

        ksort($forms);

        return $forms;
    }

    public function getHtmlForm($type, $forms)
    {
        if ($type == 'system' || $type == 'param') {
            $service = ucfirst($type);
        }

        $tmpl = [];
        if (! isset($service)) {
            return $tmpl;
        }

        $container = $this->container;
        $bundles   = $container->getParameter('kernel.bundles');
        foreach (array_keys($forms) as $key) {
            list($namespace, $module) = explode('_', $key);
            $namespace                = ucfirst($namespace);
            foreach ($bundles as $bundle) {
                if (substr_count($bundle, $namespace . '\\AdminBundle') != 0) {
                    $file = '../src/' . $namespace;
                    $file = $file . '/AdminBundle/Resources/views/' . $service . '/module/';
                    $file = $file . $module . '.html.twig';
                    if (is_file($file)) {
                        $tmpl[$key] = $namespace . 'AdminBundle:' . $service . ':module/' . $module . '.html.twig';
                    }
                }
            }
        }

        return $tmpl;
    }

    public function getForm($service, $manager = null, $entity = null, $etat = null, $options = [])
    {
        $request     = $this->request;
        $container   = $this->container;
        $formFactory = $container->get('form.factory');
        $params      = $container->get('mkk.param_service')->listing();
        $servicesID  = $container->getServiceIds();
        $data        = [];
        foreach ($servicesID as $code) {
            if (substr_count($code, $service) != 0) {
                $data[] = $code;
            }
        }

        $flashbag = $request->getSession()->getFlashBag();
        if ($entity != null) {
            $methods          = get_class_methods($entity);
            $uploadableFields = $container->get('entity.annotation_reader')->getUploadableFields($entity);
            if (! in_array('setUpdatedAt', $methods) && count($uploadableFields) != 0) {
                $flashbag->add('warning', 'Il manque un champs updatedAt dans ' . substr(get_class($entity), strpos(get_class($entity), 'Entity')));
                $this->redirect = true;
            }
        }

        if (count($data) == 0) {
            $flashbag->add('warning', 'Aucun formulaire existe avec le service ' . $service);
            $this->redirect = true;

            return false;
        }

        if (count($data) == 1) {
            $formService = $container->get($data[0]);
            $form        = $formFactory->create(
                $formService,
                $entity,
                $options
            );

            return $form;
        }

        $forms = [];
        if (! isset($params['languesite']) || ! isset($params['langueprincipal'])) {
            $flashbag->add('warning', 'Aucune langue est configuré dans les paramètres');
            $this->redirect = true;

            return false;
        }

        foreach ($data as $code) {
            $type = str_replace($service . '.', '', $code);
            if ($type == 'standard') {
                $formService  = $container->get($code);
                $forms[$type] = $formFactory->create(
                    $formService,
                    $entity,
                    $options
                );
            } else {
                $formService = $container->get($code);
                foreach ($params['languesite'] as $locale) {
                    if ($etat == 'modifier') {
                        $entity->setTranslatableLocale($locale);
                        $manager->refresh($entity);
                    }
                    $form = $formFactory->createNamedBuilder(
                        'langue' . $locale,
                        $formService,
                        $entity
                    );

                    $forms['langue' . $locale] = $form->getForm();
                }
            }
        }

        $newforms             = [];
        $newforms['standard'] = $forms['standard'];
        foreach ($forms as $code => $form) {
            if ($code != 'standard') {
                $newforms[$code] = $form;
            }
        }

        return $newforms;
    }

    public function createView($forms)
    {
        if (is_object($forms)) {
            return $forms->createView();
        }
        if (is_array($forms)) {
            foreach ($forms as $id => $form) {
                $forms[$id] = $form->createView();
            }
        }

        return $forms;
    }

    public function setBreadcrumb($breadcrumb)
    {
        $this->container->set('breadcrumb', $breadcrumb);
    }
}
