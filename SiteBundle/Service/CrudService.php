<?php

namespace Mkk\SiteBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

class CrudService
{
    protected $request;
    protected $controller;
    protected $manager;
    protected $id;
    protected $formulaireID;
    protected $url;
    protected $formService;
    protected $twigHTML;
    protected $entity;

    public function __construct(ContainerInterface $container, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->request   = $requestStack->getCurrentRequest();
        $this->router    = $container->get('router');
        $this->url       = [];
    }

    public function init()
    {
        $managerID    = $this->manager;
        $id           = $this->id;
        $container    = $this->container;
        $request      = $this->request;
        $manager      = $container->get($managerID);
        $repository   = $manager->getRepository();
        $entityNew    = $manager->getTable();
        $this->etat   = 'modifier';
        if (null == $this->entity) {
            $this->entity = $repository->find($id);
        }

        if (! $this->entity) {
            $this->etat         = 'ajouter';
            $this->entity       = new $entityNew();
            $this->methods      = get_class_methods($this->entity);
        } else {
            if (0 != (int) $this->entity->getId()) {
                $this->methods = get_class_methods($this->entity);
                if (in_array('setTranslatableLocale', $this->methods)) {
                    $this->entity->setTranslatableLocale($request->getLocale());
                    $manager->refresh($this->entity);
                }
            } else {
                $this->methods = get_class_methods($this->entity);
                $this->etat    = 'ajouter';
            }
        }
    }

    public function launch()
    {
        $container    = $this->container;
        $formulaireID = $this->formulaireID;
        $formService  = $this->formService;
        $url          = $this->url;
        $urlListing   = $url['listing'];
        $urlForm      = $url['form'];
        $managerID    = $this->manager;
        $manager      = $container->get($managerID);
        $request      = $this->request;
        $controller   = $this->controller;
        $twigHTML     = $this->twigHTML;
        $siteService  = $container->get('mkk.site_service');
        $options      = [
            'attr'   => [
                'id' => $formulaireID,
            ],
            'action' => $this->router->generate($urlForm, ['id' => $this->entity->getId()]),
        ];
        $form        = $siteService->getForm($formService, $manager, $this->entity, $this->etat, $options);
        if ($siteService->isRedirect()) {
            $url      = $this->router->generate($urlListing);
            $redirect = new RedirectResponse($url);

            return $redirect;
        }
        $service = $container->get('mkk.post_service');
        $service->init($this->entity, $form, $this->etat);
        if ($service->isNew()) {
            $url      = $this->router->generate(
                $urlForm,
                [
                    'id' => $service->getId(),
                ]
            );
            $redirect = new RedirectResponse($url);

            return $redirect;
        }

        $actions = [];
        if ($urlListing != $urlForm) {
            $actions[] = ['type' => 'return', 'url' => $urlListing];
        }

        $actions[] = [
            'type' => 'save',
            'attr' => ['data-submit' => $formulaireID],
        ];
        $container->get('mkk.action_service')->set($actions);
        if (in_array('setTranslatableLocale', $this->methods) && 'modifier' == $this->etat) {
            $this->entity->setTranslatableLocale($request->getLocale());
            $manager->refresh($this->entity);
        }
        $htmlTwig = $controller->renderMkk(
            $request,
            $twigHTML,
            [
                'etat'   => $this->etat,
                'id'     => $this->id,
                'stitre' => $this->entity->__toString(),
                'entity' => $this->entity,
                'form'   => $siteService->createView($form),
            ]
        );

        return $htmlTwig;
    }

    /**
     * Set the value of Controller.
     *
     * @param mixed controller
     * @param mixed $controller
     *
     * @return self
     */
    public function setController($controller)
    {
        $this->controller = $controller;

        return $this;
    }

    /**
     * Set the value of Manager.
     *
     * @param mixed manager
     * @param mixed $manager
     *
     * @return self
     */
    public function setManager($manager)
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * Set the value of Id.
     *
     * @param mixed id
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set the value of Formulaire.
     *
     * @param mixed formulaireID
     * @param mixed $formulaireID
     *
     * @return self
     */
    public function setFormulaireID($formulaireID)
    {
        $this->formulaireID = $formulaireID;

        return $this;
    }

    /**
     * Set the value of Url.
     *
     * @param mixed url
     * @param mixed $url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Set the value of Form Service.
     *
     * @param mixed formService
     * @param mixed $formService
     *
     * @return self
     */
    public function setFormService($formService)
    {
        $this->formService = $formService;

        return $this;
    }

    /**
     * Set the value of Twig.
     *
     * @param mixed twigHTML
     * @param mixed $twigHTML
     *
     * @return self
     */
    public function setTwigHTML($twigHTML)
    {
        $this->twigHTML = $twigHTML;

        return $this;
    }

    /**
     * Get the value of Request.
     *
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Get the value of Controller.
     *
     * @return mixed
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * Get the value of Manager.
     *
     * @return mixed
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * Get the value of Id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Formulaire.
     *
     * @return mixed
     */
    public function getFormulaireID()
    {
        return $this->formulaireID;
    }

    /**
     * Get the value of Url.
     *
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get the value of Form Service.
     *
     * @return mixed
     */
    public function getFormService()
    {
        return $this->formService;
    }

    /**
     * Get the value of Twig.
     *
     * @return mixed
     */
    public function getTwigHTML()
    {
        return $this->twigHTML;
    }

    /**
     * Get the value of Entity.
     *
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    public function setEntity($entity)
    {
        $this->entity = $entity;
    }
}
