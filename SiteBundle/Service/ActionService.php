<?php

namespace Mkk\SiteBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class ActionService
{
    protected $request;
    protected $controller;
    protected $tab;

    public function __construct(ContainerInterface $container, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->request   = $requestStack->getCurrentRequest();
        $this->router    = $container->get('router');
        $this->tab       = [];
        $this->bouton    = [
            'supprimer' => 0,
        ];
    }

    public function getBouton()
    {
        return $this->bouton;
    }

    public function set($actions)
    {
        $this->tab = $actions;
    }

    public function traiter()
    {
        $request   = $this->request;
        $container = $this->container;
        if ($container->has('container') && 0 == $container->get('container')->paginator->getTotalItemCount()) {
            $supprimer = 0;
            $vider     = 0;
            $onoff     = 0;
        } else {
            $supprimer = 1;
            $vider     = 1;
            $onoff     = 1;
        }

        $routes    = $this->getRoutes();
        $route     = $request->attributes->get('_route');
        $ajoutbord = 0;
        if ('bord_index' != $route && 'be' == substr($route, 0, 2) && ! isset($this->tab)) {
            $ajoutbord = 1;
            $this->tab = [
                ['type' => 'bord'],
            ];
        }

        if (0 != count($this->tab)) {
            $actions = $this->tab;
            $route   = $request->attributes->get('_route');
            foreach ($actions as $i => $tab) {
                list($actions, $tab) = $this->traitementLienActions($actions, $tab);
                $continuer           = 1;
                $token               = $container->get('security.token_storage')->getToken();
                $utilisateur         = $token->getUser();
                if (0 == count($tab)) {
                    $continuer = 0;
                }

                if (isset($tab['url'])) {
                    $url    = $tab['url'];
                    $routes = $this->getRoutes();
                    if (isset($routes[$url])) {
                        $retour = $this->getDroitRoute($tab['url']);
                        $test1  = (
                            'superadmin' != $utilisateur->getRefGroup()->getCode() &&
                            0 != substr_count($tab['url'], 'vider')
                        );

                        $test2 = (isset($tab['type']) && 'vider' == $tab['type'] && 0 == $vider);
                        $test3 = (isset($tab['type']) && 'delete' == $tab['type'] && 0 == $supprimer);
                        $test4 = (isset($tab['type']) && 'actif' == $tab['type'] && 0 == $onoff);
                        $test5 = (isset($tab['type']) && 'desactif' == $tab['type'] && 0 == $onoff);
                        $test6 = ($test4 || $test5);
                        if (0 != $retour || $test1 || $test2 || $test3 || $test6) {
                            $continuer = 0;
                        }
                    }
                }

                if ($continuer) {
                    if (isset($tab['liens'])) {
                        foreach ($tab['liens'] as $key => $lien) {
                            list(
                                $actions,
                                $tab['liens'][$key]
                            ) = $this->traitementLienActions($actions, $tab['liens'][$key]);
                            if (! isset($lien['url']) || '' == $lien['url']) {
                                $tab['liens'][$key]['url'] = '#';
                            } elseif ('' != $lien['url']) {
                                if (isset($routes[$lien['url']])) {
                                    if (isset($lien['data'])) {
                                        $tab['liens'][$key]['url'] = $this->router->generate(
                                            $lien['url'],
                                            $lien['data']
                                        );
                                    } else {
                                        $tab['liens'][$key]['url'] = $this->router->generate($lien['url']);
                                    }
                                }
                            }
                        }
                    } elseif (! isset($tab['url']) || '' == $tab['url']) {
                        $tab['url'] = '#';
                    } elseif ('' != $tab['url']) {
                        if (isset($routes[$tab['url']])) {
                            if (isset($tab['data'])) {
                                $tab['url'] = $this->router->generate($tab['url'], $tab['data']);
                            } else {
                                $tab['url'] = $this->router->generate($tab['url']);
                            }
                        }
                    }

                    $actions[$i] = $tab;
                } else {
                    unset($actions[$i]);
                }
            }

            $this->actions = $actions;
        }
    }

    public function get()
    {
        if (isset($this->actions)) {
            return $this->actions;
        }

        return [];
    }

    private function getDataGroupUser($token)
    {
        $container             = $this->container;
        $groupManager          = $container->get('bdd.group_manager');
        $groupRepository       = $groupManager->getRepository();
        $param                 = [];
        $param['visiteur']     = 'visiteur';
        $param['superadmin']   = 'superadmin';
        $dataGroup             = $groupRepository->getGroupSuperVisiteur($param);
        $this->groupvisiteur   = $dataGroup['visiteur'];
        $this->groupsuperadmin = $dataGroup['superadmin'];

        if (null == $token) {
            $idgroup = $this->groupvisiteur->getId();
        } else {
            $user = $token->getUser();
            if (is_string($user)) {
                $idgroup = $this->groupvisiteur->getId();
            } else {
                $idgroup = $user->getRefGroup()->getId();
            }
        }

        return $idgroup;
    }

    private function getDroitRoute($route)
    {
        $container             = $this->container;
        $token                 = $container->get('security.token_storage')->getToken();
        $actionManager         = $container->get('bdd.action_manager');
        $actionRepository      = $actionManager->getRepository();
        $userManager           = $container->get('bdd.user_manager');
        $userRepository        = $userManager->getRepository();
        $retour                = 0;
        list($module, $action) = explode('_', $route);
        $action                = str_replace('page', 'index', $action);
        $user                  = $token->getUser();
        $group                 = $this->getDataGroupUser($token);
        $annuler               = 0;
        if (isset($this->groupvisiteur)) {
            $visiteur = $this->groupvisiteur;
        }

        if (isset($this->groupsuperadmin)) {
            $superadmin = $this->groupsuperadmin;
        }

        $urlsearch1 = substr_count($route, 'besearch_');
        $urlsearch2 = substr_count($route, 'search');
        $urlupload  = substr_count($route, 'upload');
        $urljson    = substr_count($route, 'json');
        $urlvider   = substr_count($route, 'vider');
        if (1 == $urljson) {
            return $retour;
        }

        if ((1 == $urlsearch1 || 1 == $urlsearch2 || 1 == $urlupload) && $group != $visiteur->getId()) {
            return $retour;
        }

        if (1 == $urlsearch2) {
            return $retour;
        }

        // Empeche l'accès au bouton vider quand on n'est pas superadmin
        if (1 == $urlvider && $group != $superadmin->getId()) {
            return 403;
        }

        $urlanepasverifier = 'script_image,user_login,user_check,script_cache,beimport_set';
        $urlanepasverifier = $urlanepasverifier . ',script_auth,script_disable,script_showtel,script_showlien';
        $urlanepasverifier = ! in_array($route, explode(',', $urlanepasverifier));
        $urlanepasverifier = ($urlanepasverifier || 0 != substr_count($route, 'fos_user'));
        if ($urlanepasverifier) {
            $actiongroup = $actionRepository->getActionGroup($group);
            if ('be' == substr($route, 0, 2) || 'bord_index' == $route || 'script_tinymce' == $route) {
                if ($group == $visiteur->getid()) {
                    $annuler = 1;
                } elseif ($group != $superadmin->getId() && 0 != substr_count($route, 'vider')) {
                    $annuler = 1;
                } elseif ($group != $superadmin->getId() && 'beapropos_index' != $route) {
                    if (! isset($actiongroup[$module])) {
                        $annuler = 1;
                    } else {
                        $codes   = explode(',', $actiongroup[$module]);
                        $annuler = 1;
                        foreach ($codes as $code) {
                            if ($code == $action) {
                                $annuler = 0;
                                break;
                            }
                        }
                    }
                }
            } else {
                if (! isset($actiongroup[$module])) {
                    $annuler = 1;
                } else {
                    $codes   = explode(',', $actiongroup[$module]);
                    $annuler = 1;
                    foreach ($codes as $code) {
                        if ($code == $action) {
                            $annuler = 0;
                            break;
                        }
                    }
                }
            }

            if ($annuler) {
                if (is_object($user)) {
                    $data = $userRepository->trouverUser($user);
                    if (0 == count($data)) {
                        $retour = 401;
                    } else {
                        $retour = 403;
                    }
                } else {
                    $retour = 401;
                }
            }
        }

        return $retour;
    }

    private function getRoutes()
    {
        if (! isset($this->listroutes)) {
            $this->listroutes = [];
            $router           = $this->router;
            foreach ($router->getRouteCollection()->all() as $name => $route) {
                $pattern = $route->getPath();
                foreach ($route->getDefaults() as $code => $val) {
                    if ('_controller' != $code) {
                        $pattern = str_replace('{' . $code . '}', $val, $pattern);
                    }
                }

                if ('_' != substr($name, 0, 1) && 0 != substr_count($name, '_')) {
                    $this->listroutes[$name] = $pattern;
                }
            }
        }

        return $this->listroutes;
    }

    private function traitementLienActions($actions, $tab)
    {
        if (isset($tab['type'])) {
            if ('save' == $tab['type']) {
                $tab['id']  = 'BoutonSave';
                $tab['img'] = 'glyphicon-floppy-saved';
                if (! isset($tab['text'])) {
                    $tab['text'] = 'Enregistrer';
                }

                $trouver = 0;
                foreach ($actions as $check) {
                    if ((isset($check['id'])
                        && 'BoutonReturn' == $check['id'])
                        || (isset($tab['type'])
                        && 'return' == $tab['type'])
                    ) {
                        $trouver = 1;
                        break;
                    }
                }

                // if ($trouver) {
                //     $actions[] = [
                //         'id'   => 'BoutonSaveAndClose',
                //         'img'  => 'glyphicon glyphicon-save',
                //         'url'  => '#',
                //         'text' => 'Enregistrer et fermer',
                //     ];
                // }
            } elseif ('add' == $tab['type']) {
                $tab['id']  = 'BoutonAdd';
                $tab['img'] = 'glyphicon-plus';
                if (! isset($tab['text'])) {
                    $tab['text'] = 'Ajouter';
                }
            } elseif ('param' == $tab['type']) {
                $tab['id']  = 'BoutonParam';
                $tab['img'] = 'glyphicon-wrench';
                if (! isset($tab['text'])) {
                    $tab['text'] = 'Paramètres';
                }
            } elseif ('return' == $tab['type']) {
                $tab['id']  = 'BoutonReturn';
                $tab['img'] = 'glyphicon-list';
                if (! isset($tab['text'])) {
                    $tab['text'] = 'Retour liste';
                }
            } elseif ('delete' == $tab['type']) {
                $tab['id']  = 'BoutonSupprimer';
                $tab['img'] = 'glyphicon-trash';
                if (! isset($tab['text'])) {
                    $tab['text'] = 'Supprimer';
                }

                $tab['disabled']           = '1';
                $this->bouton['supprimer'] = 1;
            } elseif ('dupliquer' == $tab['type']) {
                $tab['id']  = 'BoutonDupliquer';
                $tab['img'] = 'fa fa-files-o';
                if (! isset($tab['text'])) {
                    $tab['text'] = 'Dupliquer';
                }

                $tab['disabled'] = '1';
            } elseif ('actif' == $tab['type']) {
                $tab['id']  = 'BoutonActiver';
                $tab['img'] = 'glyphicon-thumbs-up';
                if (! isset($tab['text'])) {
                    $tab['text'] = 'Activer';
                }

                $tab['disabled'] = '1';
            } elseif ('data' == $tab['type']) {
                $actions[] = [
                    'id'       => 'BoutonDupliquer',
                    'img'      => 'fa fa-files-o',
                    'url'      => $this->router->generate('bedata_duplicate', ['entity' => $tab['entity']]),
                    'text'     => 'Dupliquer',
                    'disabled' => 1,
                ];
                $actions[] = [
                    'id'       => 'BoutonSupprimer',
                    'img'      => 'glyphicon glyphicon-trash',
                    'url'      => $this->router->generate('bedata_delete', ['entity' => $tab['entity']]),
                    'text'     => 'Supprimer',
                    'disabled' => 1,
                ];
                $actions[] = [
                    'id'   => 'BoutonVider',
                    'img'  => 'fa fa-bomb',
                    'url'  => $this->router->generate('bedata_empty', ['entity' => $tab['entity']]),
                    'text' => 'Vider',
                ];
                $tab       = [];
            } elseif ('desactif' == $tab['type']) {
                $tab['id']  = 'BoutonDesactiver';
                $tab['img'] = 'glyphicon-thumbs-down';
                if (! isset($tab['text'])) {
                    $tab['text'] = 'D&eacute;sactiver';
                }

                $tab['disabled'] = '1';
            } elseif ('stats' == $tab['type']) {
                $tab['id']  = 'BoutonStats';
                $tab['img'] = 'glyphicon-signal';
                if (! isset($tab['text'])) {
                    $tab['text'] = 'Statistiques';
                }
            }
        }
        if (0 != count($tab)) {
            if (! isset($tab['img'])) {
                $tab['img'] = '';
            } elseif (0 != substr_count($tab['img'], 'glyphicon')) {
                $tab['img'] = 'glyphicon ' . $tab['img'];
            }
        }

        return [$actions, $tab];
    }
}
