<?php

namespace Mkk\SiteBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class DataService
{
    protected $request;
    protected $controller;

    public function __construct(ContainerInterface $container, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->request   = $requestStack->getCurrentRequest();
    }

    public function all()
    {
        $response   = [];
        $container  = $this->container;
        $serviceIds = $container->getServiceIds();
        foreach ($serviceIds as $code) {
            if (substr_count($code, 'bdd.') != 0) {
                $response[] = str_replace(['bdd.', '_manager'], '', $code);
            }
        }

        return $response;
    }

    public function duplicate($entity)
    {
        $response = [];

        return $response;
    }

    public function empty($entity)
    {
        $response   = [];
        $container  = $this->container;
        $serializer = $container->get('serializer');
        if ($container->has('bdd.' . $entity . '_manager')) {
            $tableManager    = $container->get('bdd.' . $entity . '_manager');
            $tableRepository = $tableManager->getRepository();
            $batchSize       = 25;
            $response        = ['vider' => 1];
            set_time_limit(0);
            $entities = $tableRepository->findall();
            foreach ($entities as $i => $entity) {
                $tableManager->remove($entity);
                if (($i % $batchSize) == 0) {
                    $tableManager->flush();
                }
            }

            $tableManager->flush();
        }

        return $response;
    }

    public function list($entity)
    {
        $response   = [];
        $container  = $this->container;
        $serializer = $container->get('serializer');
        if ($container->has('bdd.' . $entity . '_manager')) {
            $listingService = $container->get('mkk.listing_service');
            $listingService->setRequest('bdd.' . $entity . '_manager', 'findall');
            $manager      = $container->get('bdd.' . $entity . '_manager');
            $translations = $manager->getTranslations();
            $paginators   = $container->get('paginator');
            foreach ($paginators as $paginator) {
                $dataSerializer     = $serializer->serialize($paginator, 'json');
                $data               = json_decode($dataSerializer, true);
                $translates         = $translations->findTranslations($paginator);
                $data['translates'] = $translates;

                $response[] = $data;
            }
        }

        return $response;
    }

    public function delete($entity)
    {
        $response   = [];
        $container  = $this->container;
        $request    = $this->request;
        $serializer = $container->get('serializer');
        if ($container->has('bdd.' . $entity . '_manager')) {
            set_time_limit(0);
            $post['id']        = $request->request->get('id');
            $post['selection'] = $request->request->get('selection');
            $response          = ['supprimer' => 0];
            try {
                $response = $this->tryDelete($entity, $post, $response);
            } catch (Exception $e) {
                if ($this->debugAdmin()) {
                    $response['message'] = $e->getMessage();
                }

                $response['supprimer'] = 2;
            }
        }

        return $response;
    }

    public function show($entity, $id)
    {
        $response   = [];
        $container  = $this->container;
        $serializer = $container->get('serializer');
        if ($container->has('bdd.' . $entity . '_manager')) {
            $manager      = $container->get('bdd.' . $entity . '_manager');
            $translations = $manager->getTranslations();
            $repository   = $manager->getRepository();
            $entity       = $repository->find($id);
            if ($entity) {
                $manager->refresh($entity);
                $dataSerializer     = $serializer->serialize($entity, 'json');
                $data               = json_decode($dataSerializer, true);
                $translates         = $translations->findTranslations($entity);
                $data['translates'] = $translates;
                $response           = $data;
            }
        }

        return $response;
    }

    /**
     * Try delete.
     *
     * @param string $table        table qui subit la suppression
     * @param string $search       champs de recherche
     * @param string $post         post
     * @param string $responsejson array de retour
     *
     * @return array
     */
    private function tryDelete($table, $post, $responsejson)
    {
        $container       = $this->container;
        $code            = 'bdd.' . $table . '_manager';
        $tableManager    = $container->get($code);
        $tableRepository = $tableManager->getRepository();
        set_time_limit(0);
        $batchSize = 5;
        if ($post['id'] != '') {
            $selection = [$post['id']];
        } elseif ($post['selection'] != '') {
            $selection = explode(',', $post['selection']);
        }

        $i = 0;
        foreach ($selection as $id) {
            $data = $tableRepository->find($id);
            if ($data) {
                ++$i;
                $responsejson['supprimer'] = 1;
                $tableManager->remove($data);
                if (($i % $batchSize) == 0) {
                    $tableManager->flush();
                }
            }
        }

        $tableManager->flush();

        return $responsejson;
    }
}
