<?php

namespace Mkk\SiteBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class DateService
{
    protected $request;
    protected $controller;

    public function __construct(ContainerInterface $container, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->request   = $requestStack->getCurrentRequest();
    }

    public function verification($date, $country, $inferieur, $ferier, $jourenlever, $horaire)
    {
        $continuer = $this->check($date, $country, $inferieur, $ferier, $jourenlever, $horaire);
        if ($continuer != 1) {
            $info   = explode('/', date('d/m/Y/H/i/N', $date));
            $jour   = $info[0];
            $mois   = $info[1];
            $annee  = $info[2];
            $heure  = $info[3];
            $minute = $info[4];
            if ($continuer == 4) {
                list($heure, $minute) = explode(':', $horaire['destination']);
            }

            for ($i = 0;; ++$i) {
                $date      = mktime($heure, $minute, 0, $mois, $jour + $i, $annee);
                $continuer = $this->check($date, $country, $inferieur, $ferier, $jourenlever, $horaire);
                if ($continuer == 1) {
                    break;
                }
            }
        }

        return $date;
    }

    /**
     * Indique tout les jours fériés d'une année pour la france.
     *
     * @param int $year année
     *
     * @return array
     */
    public function getHolidays($year = null)
    {
        if ($year == null) {
            $year = intval(date('Y'));
        }
        $easterDate  = easter_date($year);
        $easterDay   = date('j', $easterDate);
        $easterMonth = date('n', $easterDate);
        $easterYear  = date('Y', $easterDate);
        $holidays    = [
                'fr' => [
                    $easterDate,
                    // Dates fixes
                    mktime(0, 0, 0, 1, 1, $year), // 1er janvier
                    mktime(0, 0, 0, 5, 1, $year), // Fête du travail
                    mktime(0, 0, 0, 5, 8, $year), // Victoire des alliés
                    mktime(0, 0, 0, 7, 14, $year), // Fête nationale
                    mktime(0, 0, 0, 8, 15, $year), // Assomption
                    mktime(0, 0, 0, 11, 1, $year), // Toussaint
                    mktime(0, 0, 0, 11, 11, $year), // Armistice
                    mktime(0, 0, 0, 12, 25, $year), // Noel
                    // Dates variables
                    mktime(0, 0, 0, $easterMonth, $easterDay + 1, $easterYear), // Lundi de paques
                    mktime(0, 0, 0, $easterMonth, $easterDay + 39, $easterYear), // L'ascension
                    mktime(0, 0, 0, $easterMonth, $easterDay + 49, $easterYear), // Pentecote
                    mktime(0, 0, 0, $easterMonth, $easterDay + 50, $easterYear), // Lundi de la pentecote
                ],
            ];
        sort($holidays['fr']);

        return $holidays;
    }

    public function check($date, $country, $inferieur, $ferier, $jourenlever, $horaire)
    {
        $today         = time();
        $continuer     = 1;
        $info          = explode('/', date('d/m/Y/H/i/N', $date));
        $jour          = $info[0];
        $mois          = $info[1];
        $annee         = $info[2];
        $joursemaine   = $info[5];
        $joursansheure = mktime(0, 0, 0, $mois, $jour, $annee);
        $dateprendre   = mktime(0, 0, 0, date('m', $today), date('d', $today), date('Y', $today));
        if ($ferier) {
            $jourferiers = $this->getHolidays($annee);
            foreach ($jourferiers[$country] as $jourferier) {
                if ($jourferier == $joursansheure) {
                    $continuer = 2;
                }
            }
        }

        if ($continuer == 1 && is_array($jourenlever) && count($jourenlever) != 0) {
            if (in_array($joursemaine, $jourenlever)) {
                $continuer = 3;
            }
        }

        if ($continuer == 1 && $horaire != '' && $dateprendre == $joursansheure) {
            list($h, $m) = explode(':', $horaire['fin']);
            $periode     = mktime($h, $m, 0, $mois, $jour, $annee);
            if ($today >= $periode) {
                $continuer = 4;
            }
        }

        if ($inferieur && $dateprendre > $joursansheure) {
            $continuer = 5;
        }

        return $continuer;
    }
}
